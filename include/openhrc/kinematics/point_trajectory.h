/*
 *
 * Copyright (C) 2015
 * Julio Jarquin <jjarquin.ct@gmail.com>, Gustavo Arechavaleta <garechav@cinvestav.edu.mx>
 * CINVESTAV - Saltillo Campus
 *
 * This file is part of OpenHRC
 * OpenHRC is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * OpenHRC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/**
 *	\file include/openhrc/core/point_trajectory.h
 *	\author Julio Jarquin, Gustavo Arechavaleta
 *	\version 1.0
 *	\date 2015
 *
 *	Declaration of a PointTrajectory class.
 */


#ifndef HR_KINEMATICS_POINT_TRAJECTORY_H
#define HR_KINEMATICS_POINT_TRAJECTORY_H

#include "types.h"
#include "trajectory_abstract.h"

namespace hr{
namespace core{

class PointTrajectory : public TrajectoryAbstract
{
public:

    PointTrajectory(const hr::real_t & initialTime, const hr::real_t & finalTime, const SpatialVector & initialPose, const SpatialVector &  finalPose, TrajectoryType trajType = kTrapezoidal);
    PointTrajectory(const hr::real_t & initialTime, const hr::real_t &  finalTime, const SpatialVector &  initialPose, const SpatialVector &  finalPose, const SpatialVector &  initialTwist, const SpatialVector &  finalTwist, TrajectoryType trajType = kCubicPolynomial);
    PointTrajectory(const hr::real_t & initialTime, const hr::real_t &  finalTime, const SpatialVector &  initialPose, const SpatialVector &  finalPose, const SpatialVector &  initialTwist, const SpatialVector &  finalTwist, const SpatialVector &  initialAccel, const SpatialVector &  finalAccel, TrajectoryType trajType = kQuinticPolynomial);
    PointTrajectory(const hr::real_t & initialTime, const hr::real_t &  finalTime, const SpatialVector &  initialPose, const SpatialVector &  finalPose, const SpatialVector &  initialTwist, const SpatialVector &  finalTwist, const SpatialVector &  initialAccel, const SpatialVector &  finalAccel, const SpatialVector &  initialJerk, const SpatialVector &  finalJerk, TrajectoryType trajType = kSepticPolynomial);


    TaskSpace getTaskSpace() {return taskSpace;}
    hr::real_t getInitialTime() {return t0;}
    hr::real_t getFinalTime() {return tf;}
    SpatialVector getPose(const hr::real_t time);
    SpatialVector getVelocity(const hr::real_t time);
    SpatialVector getAcceleration(const hr::real_t time);


protected:
    //! Initial time of the trajectory
    hr::real_t t0;

    //! Final time of the trajectory
    hr::real_t tf;

    //! Time duration of the trajectory
    hr::real_t Tt;



    //! Initial pose
    hr::SpatialVector X0;

    //! Final pose
    hr::SpatialVector Xf;

    //! Initial twist
    hr::SpatialVector dX0;

    //! Final twist
    hr::SpatialVector dXf;


    //! Initial acceleration
    hr::SpatialVector ddX0;

    //! Final acceleration
    hr::SpatialVector ddXf;

    //! Initial Jerk
    hr::SpatialVector dddX0;

    //! Final Jerk
    hr::SpatialVector dddXf;

    //! The Active space of the trajectory
    TaskSpace taskSpace;

    //! The type of the trajectory \sa TrajectoryType
    TrajectoryType trajectoryType;


    //! Polynomial Interpolation parameters.
    hr::VectorXr a0;
    hr::VectorXr a1;
    hr::VectorXr a2;
    hr::VectorXr a3;
    hr::VectorXr a4;
    hr::VectorXr a5;
    hr::VectorXr a6;
    hr::VectorXr a7;

    //! Trapezoidal Interpolation parameters.
    hr::VectorXr dXv;
    hr::real_t ta;
    //! Time duration of the acceleration Time.
    hr::real_t Ta;
    hr::real_t Td;
};


} // end of namespace core
} // end of namespace hr

#endif // HR_KINEMATICS_POINT_TRAJECTORY_H
