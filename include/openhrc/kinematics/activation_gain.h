/*
 * Copyright (C) 2015
 * Julio Jarquin <jjarquin.ct@gmail.com>, Gustavo Arechavaleta <garechav@cinvestav.edu.mx>
 * CINVESTAV - Saltillo Campus
 *
 * This file is part of OpenHRC
 * OpenHRC is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * OpenHRC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */


/**
 *	\file include/openhrc/kinematics/activation_gain.h
 *	\author Julio Jarquin, Gustavo Arechavaleta
 *	\version 1.0
 *	\date 2015
 *
 *	Declaration of a ActivationGain class.
 */

#ifndef HR_KINEMATICS_ACTIVATION_GAIN_H
#define HR_KINEMATICS_ACTIVATION_GAIN_H

#include "types.h"
#include "openhrc/kinematics/gain_abstract.h"

namespace hr{
namespace core{

class ActivationGain : public GainAbstract
{
public:
    ActivationGain(const hr::real_t initialTime, const hr::real_t finalTime, const hr::real_t gain);
    virtual ~ActivationGain();

    hr::real_t operator() (const hr::real_t time);

private:
    //! Initial time of the gain. Begin Activation.
    hr::real_t t0;

    //! Final time of the gain. Full Activation.
    hr::real_t tf;

    //! Time duration of the ActivationGain.
    hr::real_t Tt;

    //! Scalar gain
    hr::real_t gain;
};

} //end of namespace core
} //end of namespace hr

#endif // ACTIVATION_GAIN_H
