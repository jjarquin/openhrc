/*
 *
 * Copyright (C) 2015
 * Julio Jarquin <jjarquin.ct@gmail.com>, Gustavo Arechavaleta <garechav@cinvestav.edu.mx>
 * CINVESTAV - Saltillo Campus
 *
 * This file is part of OpenHRC
 * OpenHRC is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * OpenHRC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/**
 *	\file include/openhrc/core/trapezoidal_function.h
 *	\author Julio Jarquin, Gustavo Arechavaleta
 *	\version 1.0
 *	\date 2015
 *
 *	Declaration of the TrapezoidalFunction class.
 */


#ifndef HR_KINEMATICS_TRAPEZOIDAL_FUNCTION_H
#define HR_KINEMATICS_TRAPEZOIDAL_FUNCTION_H
#include "types.h"


namespace hr{
namespace core{

class TrapezoidalFunction
{
public:
    TrapezoidalFunction(const real_t &fn_value, const real_t &initialTime, const real_t &finalTime);
    TrapezoidalFunction(const real_t &fn_value, const real_t &initialTime, const real_t &attackTime, const real_t &decayTime , const real_t &finalTime);

    hr::real_t getValue(const hr::real_t & time);

protected:
    hr::real_t height;

    hr::real_t Ta;
    hr::real_t Td;

    hr::real_t ta;
    hr::real_t td;

    hr::real_t ti;
    hr::real_t tf;

    hr::real_t Tf;

};

} // end of namespace hr
} // end of namespace core

#endif // HR_KINEMATICS_TRAPEZOIDAL_FUNCTION_H
