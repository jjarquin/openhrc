/*
 *
 * Copyright (C) 2015
 * Julio Jarquin <jjarquin.ct@gmail.com>, Gustavo Arechavaleta <garechav@cinvestav.edu.mx>
 * CINVESTAV - Saltillo Campus
 *
 * This file is part of OpenHRC
 * OpenHRC is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * OpenHRC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/**
 *	\file include/openhrc/core/Joint_trajectory.h
 *	\author Julio Jarquin, Gustavo Arechavaleta
 *	\version 1.0
 *	\date 2015
 *
 *	Declaration of a JointTrajectory class.
 */


#ifndef HR_KINEMATICS_JOINT_TRAJECTORY_H
#define HR_KINEMATICS_JOINT_TRAJECTORY_H

#include "types.h"

namespace hr{
namespace core{

class JointTrajectory
{
public:
    JointTrajectory(hr::real_t initialTime, hr::real_t finalTime, hr::VectorXr initialConfiguration, hr::VectorXr finalConfiguration, TrajectoryType trajType = kQuinticPolynomial);
    //JointTrajectory(hr::real_t initialTime, hr::real_t finalTime, hr::VectorXr initialPose, hr::VectorXr finalPose, hr::VectorXr initialTwist, hr::VectorXr finalTwist, TrajectoryType trajType = kCubicPolynomial);


    hr::real_t getInitialTime() {return t0;}
    hr::real_t getFinalTime() {return tf;}
    hr::VectorXr getJointConfiguration(hr::real_t time);
    hr::VectorXr getJointSpeed(hr::real_t time);
    hr::VectorXr getJointAcceleration(hr::real_t time);


protected:
    //! Number of joints
    int n_joints;

    //! Initial time of the trajectory
    hr::real_t t0;

    //! Final time of the trajectory
    hr::real_t tf;

    //! Time duration of the trajectory
    hr::real_t Tt;

    //! Zero vector of size n_joints
    hr::VectorXr zero;

    //! Initial pose
    hr::VectorXr X0;

    //! Final pose
    hr::VectorXr Xf;

    //! Initial twist
    hr::VectorXr dX0;

    //! Final twist
    hr::VectorXr dXf;

    //! The Active space of the trajectory
    TaskSpace taskSpace;

    //! The type of the trajectory \sa TrajectoryType
    TrajectoryType trajectoryType;


    //! Polynomial Interpolation parameters.
    hr::VectorXr a0;
    hr::VectorXr a1;
    hr::VectorXr a2;
    hr::VectorXr a3;
    hr::VectorXr a4;
    hr::VectorXr a5;

    //! Initial acceleration
    hr::VectorXr ddX0;

    //! Final acceleration
    hr::VectorXr ddXf;

    //! Trapezoidal Interpolation parameters.
    hr::VectorXr dXv;
    hr::real_t ta;
    //! Time duration of the acceleration Time.
    hr::real_t Ta;
    hr::real_t Td;
};


} // end of namespace core
} // end of namespace hr

#endif // HR_KINEMATICS_Joint_TRAJECTORY_H
