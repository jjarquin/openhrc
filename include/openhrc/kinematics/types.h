/*
 *
 * Copyright (C) 2015
 * Julio Jarquin <jjarquin.ct@gmail.com>, Gustavo Arechavaleta <garechav@cinvestav.edu.mx>
 * CINVESTAV - Saltillo Campus
 *
 * This file is part of OpenHRC
 * OpenHRC is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * OpenHRC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/**
 *	\file include/openhrc/kinematics/types.h
 *	\author Julio Jarquin, Gustavo Arechavaleta
 *	\version 1.0
 *	\date 2015
 *
 *	Declaration of the HRLocomotion types.
 */

#ifndef HR_KINEMATICS_TYPES_H
#define HR_KINEMATICS_TYPES_H
#include "Eigen/Dense"
#include "openhrc/types.h"
namespace hr{

//! \ingroup kinematics_module
/*! */
typedef Eigen::Matrix<real_t, 3 , Eigen::Dynamic , Eigen::RowMajor> Matrix3Xr;

typedef Eigen::Matrix<real_t, 2 , Eigen::Dynamic , Eigen::RowMajor> Matrix2Xr;


namespace core{

//! \ingroup kinematics_module Enum for the KinematicTask type.
/*! This enum will be used to set up the corresponding rows of the Jacobian. */
enum TaskSpace{
    kX = 1,  /*!< The task is defined in X */
    kY = 2,  /*!< The task is defined in Y */
    kZ = 4,  /*!< The task is defined in Z */
    kWX = 8,  /*!< The task is defined in WX */
    kWY = 16,  /*!< The task is defined in WY */
    kWZ = 32,  /*!< The task is defined in WZ */

    kXY = 3,/*!< The task is defined in XY */
    kYZ = 6,/*!< The task is defined in YZ */
    kXZ = 5,/*!< The task is defined in XZ */
    kXYZ = 7,/*!< The task is defined in XYZ */

    kWXY = 24,/*!< The task is defined in WXY */
    kWYZ = 48,/*!< The task is defined in WYZ */
    kWXZ = 40,/*!< The task is defined in WXZ */
    kWXYZ = 56,/*!< The task is defined in WXYZ */

    kPosition = 7, /*!< The task is defined in XYZ */
    kOrientation = 56, /*!< The task is defined in WXYZ */
    kPositionOrientation = 63  /*!< The task is defined in XYZ and WXYZ */

};

enum ParameterType{
  kAuto, /*!< The parameter is auto adjusted */
  kFixed /*!< The parameter is fixed */

};

enum TaskType{
    kRegulationTask,  /*!< The task is regulating to a goal */
    kTrackingTask /*!< The task is tracking a trajectory */
};

enum GainType{
    kConstantGain, /*!< The gain is a constant matrix or scalar */
    kSmoothGain   /*!< The gain is a continous function */
};

enum TrajectoryType{
    kTrapezoidal,  /*!< The task velocity is defined as a trapezoidal function. */
    kCubicPolynomial,  /*!< The task trajectory is interpolated with a cubic polynomial. */
    kQuinticPolynomial,  /*!< The task trajectory is interpolated with a five order polynomial. */
    kSepticPolynomial,  /*!< The task trajectory is interpolated with a seven order polynomial. */
    kCycloidal,  /*!< The task trajectory is interpolated with the cycloidal function. */
    kSpline /*!< The task trajectory is interpolated with a B-spline. */
};

enum FootTrajectoryType{
    kFootBezier,  /*!< The foot position is interpolated with a third order bezier function. */
    kFootCycloidal  /*!< The foot position is interpolated with cycloid function. */
};



//! \ingroup kinematics_module Euler convention used to calculate the rotation matrix.
/*! The three angles are used to calculate the orientation of a rigid body. */
enum EulerConvention
{
    kEulerXYZ, /*!< The euler convetion is XYZ */
    kEulerXZY, /*!< The euler convetion is ZYX */
    kEulerXYX, /*!< The euler convetion is XYX */
    kEulerXZX, /*!< The euler convetion is XZX */
    kEulerYXZ, /*!< The euler convetion is YXZ */
    kEulerYZX, /*!< The euler convetion is YZX */
    kEulerYXY, /*!< The euler convetion is YXY */
    kEulerYZY, /*!< The euler convetion is YZY */
    kEulerZXY, /*!< The euler convetion is ZXY */
    kEulerZYX, /*!< The euler convetion is ZYX */
    kEulerZXZ, /*!< The euler convetion is ZXZ */
    kEulerZYZ /*!< The euler convetion is ZYZ */
};

#ifndef HR_CORE_FOOT_STATUS
#define HR_CORE_FOOT_STATUS
//! Status of the robot's feet.
/*! Depending on the current configuration of the robot, the FootStatus must change. */
enum FootStatus{
    kLeftFootOnGround,  /*!< The left foot is the robots support foot. */
    kRightFootOnGround, /*!< The right foot is the robots support foot. */
    kBothFeetOnGround  /*!< The robot is supported by both feet. */
};
#endif //end if HR_CORE_FOOT_STATUS

} //end of namespace core
} //end of namespace hr

#endif // HR_KINEMATICS_TYPES_H
