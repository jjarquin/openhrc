/*
 *
 * Copyright (C) 2015
 * Julio Jarquin <jjarquin.ct@gmail.com>, Gustavo Arechavaleta <garechav@cinvestav.edu.mx>
 * CINVESTAV - Saltillo Campus
 *
 * This file is part of OpenHRC
 * OpenHRC is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * OpenHRC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/**
 *	\file include/openhrc/kinematics/nao_constraints.h
 *	\author Julio Jarquin, Gustavo Arechavaleta
 *	\version 1.0
 *	\date 2015
 *
 *	Declaration of the Nao Constraints.
 */

#ifndef HR_KINEMATICS_NAO_CONSTRAINTS_H
#define HR_KINEMATICS_NAO_CONSTRAINTS_H
#include "types.h"
#include "openhrc/utils/constraints_abstract.h"
#include <vector>

namespace hr{
namespace core{
namespace nao{

class SupportBaseConstraint : public hr::utils::ConstraintsAbstract
{
    // --------------------------------------------
    // Constructors and Destructor
    // --------------------------------------------
public:
    SupportBaseConstraint(){
        LP0 << 0.06 , 0.03;  //changed from 0.07 as the robot may fall
        LP1 << 0.06 , -0.02;
        LP2 <<-0.03 ,-0.02;
        LP3 <<-0.03 , 0.03;

        RP0 << 0.06 , 0.02;
        RP1 << 0.06 ,-0.03;
        RP2 <<-0.03 ,-0.03;
        RP3 <<-0.03 , 0.02;

    }

    //! Get the Hessian Matrix.
    MatrixXr getMatrixC(){
        return CMatrix;
    }

    //! Get the associated vector.
    VectorXr getVectord(){
        return dVector;
    }

    //! Set the constraint matrix.
    /*! \param footPose The pose of the foot (x,y,theta)
        \param footStatus The supporting foot or feet.*/
    void setConstraint(const hr::Matrix2Xr & comJacobian, const Vector2r & comXY, const Vector3r & leftFootPose, const Vector3r & rightFootPose, const FootStatus & footStatus, const real_t & deltaT);


    // --------------------------------------------
    // Members
    // --------------------------------------------

protected:
    MatrixXr CMatrix;
    VectorXr dVector;

    hr::Vector2r LP0;
    hr::Vector2r LP1;
    hr::Vector2r LP2;
    hr::Vector2r LP3;

    hr::Vector2r RP0;
    hr::Vector2r RP1;
    hr::Vector2r RP2;
    hr::Vector2r RP3;


    std::vector<int> convex_hull( std::vector<hr::Vector2r> V);
    hr::real_t isLeft(hr::Vector2r P0, hr::Vector2r P1, hr::Vector2r P2){
        return (P1(0) - P0(0))*(P2(1)-P0(1))-(P2(0)-P0(0))*(P1(1)-P0(1));
    }

};


class AreaConstraint : public hr::utils::ConstraintsAbstract{
public:
    AreaConstraint(const hr::Vector2r & center, const hr::real_t & width, const hr::real_t & height);
    void setConstraint(const hr::Matrix2Xr & endEffectorJacobian, const hr::Vector2r & endEffectorPosition, const hr::real_t & deltaT);
    void setConstraint(const hr::Matrix2Xr & endEffectorJacobian, const hr::Vector2r &endEffectorPosition, const hr::Vector2r &  originFrame, const hr::real_t & deltaT);
    //! Get the Hessian Matrix.
    MatrixXr getMatrixC(){return CMatrix;}
    //! Get the associated vector.
    VectorXr getVectord(){ return dVector;}
protected:
    MatrixXr CMatrix;
    VectorXr dVector;

    hr::real_t x_max;
    hr::real_t y_max;
    hr::real_t x_min;
    hr::real_t y_min;



};
} //end of namespace nao
} //end of namespace core
} //end of namespace hr

#endif // HR_KINEMATICS_NAO_CONSTRAINTS_H
