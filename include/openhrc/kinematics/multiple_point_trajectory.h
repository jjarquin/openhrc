/*
 *
 * Copyright (C) 2015
 * Julio Jarquin <jjarquin.ct@gmail.com>, Gustavo Arechavaleta <garechav@cinvestav.edu.mx>
 * CINVESTAV - Saltillo Campus
 *
 * This file is part of OpenHRC
 * OpenHRC is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * OpenHRC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/**
 *	\file include/openhrc/core/multiple_point_trajectory.h
 *	\author Julio Jarquin, Gustavo Arechavaleta
 *	\version 1.0
 *	\date 2015
 *
 *	Declaration of a MultiPointTrajectory class which is based on a B-spline.
 */


#ifndef HR_KINEMATICS_MULTIPLE_POINT_TRAJECTORY_H
#define HR_KINEMATICS_MULTIPLE_POINT_TRAJECTORY_H

#include <vector>
#include <boost/shared_ptr.hpp>
#include "types.h"
#include "trajectory_abstract.h"
#include "openhrc/core/spatial_algebra.h"
#include "unsupported/Eigen/Splines"

namespace hr{
namespace core{

typedef Eigen::Spline<real_t,6> SpatialSpline;
typedef Eigen::SplineFitting<SpatialSpline > SpatialFitter;
typedef Eigen::Matrix<real_t,6,Eigen::Dynamic> PointsType;

class MultiplePointTrajectory : public TrajectoryAbstract
{
public:

    MultiplePointTrajectory(const std::vector<real_t> times, const std::vector<SpatialVector> & points, const std::vector<SpatialVector> &  initialConditions,const std::vector<SpatialVector> &  finalConditions, const int _degree = 3);

    hr::real_t getInitialTime() {return ti;}
    hr::real_t getFinalTime() {return tf;}
    SpatialVector getPose(const hr::real_t time);
    SpatialVector getVelocity(const hr::real_t time);
    SpatialVector getAcceleration(const hr::real_t time);
    TaskSpace getTaskSpace() {return taskSpace;}


protected:
    //! Initial time of the trajectory
    hr::real_t ti;

    //! Final time of the trajectory
    hr::real_t tf;

    //! Time duration of the trajectory
    hr::real_t Tt;

    //! Time for every point.
    std::vector<hr::real_t> mTimes;

    //! Initial pose, intermediate poses, Final pose
    std::vector<hr::SpatialVector> mPoints;

//    //! Initial velocity, acceleration, jerk, snap..
//    std::vector<hr::SpatialVector> mInitialConditions;

//    //! Final velocity, acceleration, jerk, snap..
//    std::vector<hr::SpatialVector> mFinalConditions;

    TrajectoryType trajectoryType;
    TaskSpace taskSpace;
    boost::shared_ptr<SpatialSpline> mSpline;

private:

    int mNumPoints;

};


} // end of namespace core
} // end of namespace hr

#endif // HR_KINEMATICS_MULTIPLE_POINT_TRAJECTORY_H
