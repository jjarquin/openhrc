/*
 *
 * Copyright (C) 2015
 * Julio Jarquin <jjarquin.ct@gmail.com>, Gustavo Arechavaleta <garechav@cinvestav.edu.mx>
 * CINVESTAV - Saltillo Campus
 *
 * This file is part of OpenHRC
 * OpenHRC is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * OpenHRC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/**
 *	\file include/openhrc/kinematics/center_of_mass_task.h
 *	\author Julio Jarquin, Gustavo Arechavaleta
 *	\version 1.0
 *	\date 2015
 *
 *	Declaration of the CoMTask class.
 */

#ifndef HR_KINEMATICS_COM_TASK_H
#define HR_KINEMATICS_COM_TASK_H

#include "types.h"
#include "openhrc/core.h"
#include "openhrc/utils/task_abstract.h"
#include "trajectory_abstract.h"
#include "point_trajectory.h"

#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>

namespace hr{
namespace core{
class CoMTask : public hr::utils::TaskAbstract
{
    // --------------------------------------------
    // Constructors and Destructor
    // --------------------------------------------
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW //128-bit alignment

    //! Custom constructor. Initializes the task with the given parameters.
    /*! \param id The id of the body.
        \param name The name of the body.*/
    CoMTask(      const short int &taskId,
                  boost::shared_ptr< MultiBody > robot,
                  const int taskSpace = kPositionOrientation, const hr::real_t & gain = 1.0){
        this->taskTrajectory = boost::make_shared<PointTrajectory>(0,1,hr::SpatialVector::Zero(),hr::SpatialVector::Zero());
        this->taskType = hr::core::kRegulationTask;
        this->robot = robot;
        this->taskId = taskId;
        this->bodyId = hr::core::nao::kTorso;
        this->handleId = 0;
        this->taskSpace = taskSpace;
        this->taskGain = gain;
        taskConstrained = false;
        eulerConvention << 0 , 1 , 2;
        int m = 0;
        if(taskSpace & kX){
            m++;
        }
        if(taskSpace & kY){
            m++;
        }
        if(taskSpace & kZ){
            m++;
        }
        taskError.resize(m);
        taskJacobian.resize(m,robot->getDoF() - 1 );  //Nao is DoF - 1
        taskDimension = m;
    }

    ~CoMTask(){}


    // --------------------------------------------
    // Members
    // --------------------------------------------


    //! Get the Task Identifier.
    short int getTaskId(){ return taskId;}

    //! Get the Handle Identifier.
    short int getHandleId() {return handleId;}

    //! Get the body Identifier.
    short int getBodyId() {return bodyId;}

    //! Return the task error dimension.
    short int getErrorDim(){ return taskError.rows(); }

    //! Return the square norm of the error.
    hr::real_t getErrorSquaredNorm() {return taskError.squaredNorm();}

    //! Return the active space of the task.
    //TaskSpace getTaskSpace() {return taskSpace; }

    //! Returns the task jacobian.
    const hr::MatrixXr getTaskJacobian() const {return taskJacobian; }

    //! Method inherited from TaskAbstract. Returns the task jacobian. \sa hr::utils::TaskAbstract.
    hr::MatrixXr getMatrixA() {return taskJacobian;}

    //! Returns the task error.
    const hr::VectorXr getTaskError() const {return taskError; }

    //! Method inherited from TaskAbstract. Returns the task error. \sa  hr::utils::TaskAbstract.
    hr::VectorXr getVectorb() {return taskError;}


    //! Method inherited from TaskAbstract. Returns the constraints jacobian. \sa hr::utils::TaskAbstract.
     MatrixXr getMatrixC(){return taskConstraintsMatrix;}

    //! Method inherited from TaskAbstract. Returns the constraints vector. \sa  hr::utils::TaskAbstract.
     VectorXr getVectord(){return taskConstraintsVector;}

    //! Method inherited from TaskAbstract. Returns true if the task is constrained. \sa  hr::utils::TaskAbstract.
     bool constrained(){ return taskConstrained;}

    //! Update the task error and jacobian.
    void update();

    //! Update the task error and jacobian, with time.
    void update(const hr::real_t & time);

    //! Set a new goal for the robot.
    void setGoal(const SpatialVector & taskGoal);

    //! Set a new trajectory for the robot.
    void setTrajectory(boost::shared_ptr< TrajectoryAbstract > trajectory);

    //! Set a new task space.
    void setTaskSpace(hr::core::TaskSpace taskSpace){this->taskSpace = taskSpace;}

    //! Set a new gain for the task.
    void setGain(hr::real_t gain){
        taskGain = gain;
    }

protected:
    //! The type of the task (Regulation, Tracking, ).
    hr::core::TaskType taskType;

    //! The gain of the task
    hr::real_t taskGain;

    //! The id of the task.
    boost::shared_ptr< MultiBody > robot;

    //! The id of the task.
    boost::shared_ptr< TrajectoryAbstract > taskTrajectory;

    //! The id of the task.
    short int taskId;

    //! The id of the body for the task.
    short int bodyId;

    //! The id of the handle asociated to the task.
    short int handleId;

    //! The dimension of the task
    short int taskDimension;

    //! The position of the handle relative to the WorldFrame (Torso).
    hr::VectorXr handlePosition;

    //! The orientation of the handle relative to the WorldFrame (Torso).
    hr::VectorXr handleOrientation;

    //! The active space of the task.
    int taskSpace;

    //! Task is constrained.
    bool taskConstrained;

    //! Vector used to store the eulerConvention axis.
    Eigen::Vector3i eulerConvention;

    //! The task goal, a 6D vector.
    hr::SpatialVector desiredValue;

    //! Task error between the desired and current values.
    hr::VectorXr taskError;

    //! Task Jacobian.
    hr::MatrixXr taskJacobian;


    //! The constraints matrix C. C \le d.
    hr::VectorXr taskConstraintsMatrix;

    //! The constraints vector d. C \le d.
    hr::MatrixXr taskConstraintsVector;


    //! Boolean used to decide wether or not calculate the linear velocity Jacobian.
    bool positionTask;

    //! Boolean used to decide wether or not calculate the angular velocity Jacobian.
    bool orientationTask;



};


}
}

#endif // HR_KINEMATICS_COM_TASK_H
