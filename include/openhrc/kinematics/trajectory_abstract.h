/*
 *
 * Copyright (C) 2015
 * Julio Jarquin <jjarquin.ct@gmail.com>, Gustavo Arechavaleta <garechav@cinvestav.edu.mx>
 * CINVESTAV - Saltillo Campus
 *
 * This file is part of OpenHRC
 * OpenHRC is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * OpenHRC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/**
 *	\file include/openhrc/core/trajectory_abstract.h
 *	\author Julio Jarquin, Gustavo Arechavaleta
 *	\version 1.0
 *	\date 2015
 *
 *	Declaration of a TrajectoryAbstract class.
 */


#ifndef HR_KINEMATICS_TRAJECTORY_ABSTRACT_H
#define HR_KINEMATICS_TRAJECTORY_ABSTRACT_H

#include "types.h"
#include "openhrc/core/spatial_algebra.h"

namespace hr{
namespace core{

class TrajectoryAbstract
{
public:
    //TaskAbstract();
    virtual TaskSpace getTaskSpace() = 0;
    virtual hr::real_t getInitialTime() = 0;
    virtual hr::real_t getFinalTime() = 0;

    virtual SpatialVector getPose(const hr::real_t time) = 0;
    virtual SpatialVector getVelocity(const hr::real_t time) = 0;
    virtual SpatialVector getAcceleration(const hr::real_t time) = 0;


protected:

};

} // end of namespace utils
} // end of namespace hr

#endif // HR_KINEMATICS_TRAJECTORY_ABSTRACT_H

