/*
 *
 * Copyright (C) 2015
 * Julio Jarquin <jjarquin.ct@gmail.com>, Gustavo Arechavaleta <garechav@cinvestav.edu.mx>
 * CINVESTAV - Saltillo Campus
 *
 * This file is part of OpenHRC
 * OpenHRC is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * OpenHRC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/**
 *	\file include/openhrc/core/foot_trajectory.h
 *	\author Julio Jarquin, Gustavo Arechavaleta
 *	\version 1.0
 *	\date 2015
 *
 *	Declaration of the FootTrajectory class.
 */


#ifndef HR_KINEMATICS_FOOT_TRAJECTORY_H
#define HR_KINEMATICS_FOOT_TRAJECTORY_H

#include "types.h"
#include "trajectory_abstract.h"
#include "openhrc/core/spatial_algebra.h"


namespace hr{
namespace core{

class FootTrajectory : public TrajectoryAbstract
{
public:
    FootTrajectory(const hr::real_t & initialTime, const hr::real_t & finalTime, const SpatialVector & initialPose, const SpatialVector & finalPose, const FootTrajectoryType & trajType, const int & heightCalcType = hr::core::kAuto , const hr::real_t & fixedStepZ = 0.0075, const hr::real_t & maxStepZ = 0.10);
    FootTrajectory(const hr::real_t & initialTime, const hr::real_t & finalTime, const Vector3r & initialPose, const Vector3r & finalPose, const FootTrajectoryType & trajType, const int & heightCalcType = hr::core::kAuto , const hr::real_t & fixedStepZ = 0.0075, const hr::real_t & maxStepZ = 0.10);

    TaskSpace getTaskSpace() {return taskSpace;}
    hr::real_t getInitialTime() {return t0;}
    hr::real_t getFinalTime() {return tf;}
    SpatialVector getPose(const hr::real_t time);
    SpatialVector getVelocity(const hr::real_t time);
    SpatialVector getAcceleration(const hr::real_t time);

protected:
    //! Initial time of the trajectory
    hr::real_t t0;

    //! Final time of the trajectory
    hr::real_t tf;

    //! Time duration of the trajectory
    hr::real_t Tt;

    //! Initial pose
    hr::SpatialVector X0;

    //! Final pose
    hr::SpatialVector Xf;

    //! The Active space of the trajectory
    TaskSpace taskSpace;

    //! The type of the trajectory \sa FootTrajectoryType
    FootTrajectoryType trajectoryType;


    // For Bezier trajectory.

    //! Internal point 1
    hr::SpatialVector X1;

    //! Internal point 2
    hr::SpatialVector X2;

    // For cycliod trajectory.

    //! Internal point 1
    hr::real_t rad;


};

} // end of namespace core
} // end of namespace hr

#endif // HR_KINEMATICS_FOOT_TRAJECTORY_H
