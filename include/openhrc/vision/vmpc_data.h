/*
 *
 * Copyright (C) 2015
 * Julio Jarquin <jjarquin.ct@gmail.com>, Gustavo Arechavaleta <garechav@cinvestav.edu.mx>
 * CINVESTAV - Saltillo Campus
 *
 * This file is part of OpenHRC
 * OpenHRC is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * OpenHRC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/**
 *	\file include/openhrc/vision/vmpc_data.h
 *	\author Julio Jarquin, Gustavo Arechavaleta
 *	\version 1.0
 *	\date 2015
 *
 *	Declaration of the VMPCData class.
 *  The VMPCData class holds and initialize the Hessian matrix and the vector matrix
 *  associated with the quadratic program.
 */

#ifndef HR_VISION_VMPC_DATA_H
#define HR_VISION_VMPC_DATA_H

#include "openhrc/vision/types.h"

namespace hr{
namespace vision{


class VMPCData
{

  friend class VMPCConstraints; // MPCConstraints can access to the private members of class MPCData

public:
    VMPCData(): N_horizon(3), n_features(8), n_dof(6), alpha(1),beta(1000),T_s(0.1){}
    VMPCData(const int & controlHorizonPeriods): N_horizon(controlHorizonPeriods), n_features(8), n_dof(6), alpha(1),beta(100),T_s(0.1){}
    //VMPCData(const int & controlHorizonPeriods, const int & numberOfFeatures): N_horizon(controlHorizonPeriods), n_features(numberOfFeatures), n_dof(6), alpha(100),beta(100),T_s(0.1){}
    //VMPCData(const int & controlHorizonPeriods, const int & numberOfFeatures, const real_t & sampleTime = 0.1): N_horizon(controlHorizonPeriods), n_features(numberOfFeatures), n_dof(6), alpha(100),beta(100),T_s(sampleTime){}
    VMPCData(const int & controlHorizonPeriods, const int & numberOfFeatures, const real_t & sampleTime = 0.1 , const real_t & controlGain = 100, const real_t & errorGain = 100): N_horizon(controlHorizonPeriods), n_features(numberOfFeatures), n_dof(6), alpha(controlGain),beta(errorGain),T_s(sampleTime){}

    //VMPCData& operator=(const VMPCData &);

    void init(const hr::MatrixX6r & interactionMatrix);
    void setInteractionMatrix(const hr::MatrixX6r interactionMatrix);
    MatrixXr getHessian(){return H;}
    VectorXr getFvector(const hr::VectorXr & currentFeatures, const hr::VectorXr & desiredFeatures);

protected:

private:
    ///MPC Constants

    const real_t alpha;         /** Control gain */
    const real_t beta;         /** Error gain */
    const real_t T_s;           /** Double support period */

    const unsigned int n_features;      /** Future m steps to be considered */
    const unsigned int n_dof;     /** Duration of single support stage in Periods T_s. Double support stage < T_s */
    const unsigned int N_horizon;      /** Previewable window in periods T_s */

    MatrixXr H;
    VectorXr f;

    MatrixXr A_sys;  //Recursive system matrix A       S_{k+1} = A*S_k + B*U
    MatrixXr B_sys;  //Recursive system matrix B       S_{k+1} = A*S_k + B*U
};

} //end of namespace locomotion
} //end of namespace hr

#endif // MPC_DATA_H
