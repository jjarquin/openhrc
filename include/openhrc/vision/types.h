/*
 *
 * Copyright (C) 2015
 * Julio Jarquin <jjarquin.ct@gmail.com>, Gustavo Arechavaleta <garechav@cinvestav.edu.mx>
 * CINVESTAV - Saltillo Campus
 *
 * This file is part of OpenHRC
 * OpenHRC is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * OpenHRC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/**
 *	\file include/openhrc/vision/types.h
 *	\author Julio Jarquin, Gustavo Arechavaleta
 *	\version 1.0
 *	\date 2015
 *
 *	Definition of Types
 */

#ifndef HR_VISION_TYPES_H
#define HR_VISION_TYPES_H

#include "openhrc/types.h"


namespace hr{

//! Namespace vision. Namespace of the vision Module
/*!
    More information about vision.
*/

//! Defines a new eigen-based matrix type that is based on the real_t type.
/*!
  Defines a real_t matrix of size Xx6.
 */
typedef  Eigen::Matrix<real_t, Eigen::Dynamic , 6 , Eigen::RowMajor> MatrixX6r;


//! Defines a new eigen-based matrix type that is based on the real_t type.
/*!
  Defines a real_t matrix of size 4x4 (Homogeneus Matrix).
 */
typedef Eigen::Matrix<real_t, 4 , 4 , Eigen::RowMajor> MatrixHr;


//! Defines a new eigen-based matrix type that is based on the real_t type.
/*!
  Defines a real_t matrix of size 4x4 (Homogeneus Matrix).
 */
typedef  MatrixHr Matrix4r;


//! Defines a new eigen-based vector type that is based on the real_t type.
/*!
  Defines a real_t vector of size 4x1 (Homogeneus Vector).
 */
typedef Eigen::Matrix<real_t , 4 , 1> VectorHr;


//! Defines a new eigen-based vector type that is based on the real_t type.
/*!
  Defines a real_t vector of size 4x1 (Homogeneus Vector).
  */
typedef  VectorHr Vector4r;

namespace vision{

//! Defines a new eigen-based matrix type that is based on the real_t type.
/*!
  Defines a real_t matrix of size 3x4 (Projection Matrix).
 */
typedef Eigen::Matrix<real_t, 3 , 4 , Eigen::RowMajor> Matrix3x4r;

//! Point definition.
/*! This enum is used to parse a vector of features, it helps to decide wether it is a 2D, 3D or 4D (homogeneous coordinates). */
enum PointType{
    k2DPoint = 2,  /*!< The points must be parsed as 2D Points. */
    k3DPoint = 3,  /*!< The points must be parsed as 3D Points.  */
    k4DPoint = 4  /*!< The points must be parsed as 4D Points.  */
};

//! Resolution definition.
/*! This enum is used to define the resolution of the camera. */
enum Resolution{
    kQVGA,  /*!< The resolution is 320x240. */
    kVGA,  /*!< The resolution is 640x480.  */
    kHD  /*!< The resolution is 1280x960.  */
};

//! CoordinateConversion definition.
/*! This enum is used to define space of the point. */
enum CoordinateConversion{
    kWorldCoordinates,  /*!< World coordinates (U V W 1). */
    kCameraCoordinates,  /*!< Camera frame coordinates (X Y Z 1).  */
    kImageCoordinates,  /*!< Image coordinates (x' y' z').  */
    kFilmCoordinates,  /*!< Pixel coordinates (u' v' w').  */
    kPixelCoordinates  /*!< Pixel coordinates (u v).  */
};

enum ConversionType{
    kWorld2CameraCoordinates,
    kWorld2ImageCoordinates,
    kWorld2FilmCoordinates,
    kWorld2PixelCoordinates,

    kCamera2WorldCoordinates,
    kCamera2ImageCoordinates,
    kCamera2FilmCoordinates,
    kCamera2PixelCoordinates,

    kImage2WorldCoordinates,
    kImage2CameraCoordinates,
    kImage2FilmCoordinates,
    kImage2PixelCoordinates,

    kFilm2WorldCoordinates,
    kFilm2CameraCoordinates,
    kFilm2ImageCoordinates,
    kFilm2PixelCoordinates,

    kPixel2WorldCoordinates,
    kPixel2CameraCoordinates,
    kPixel2ImageCoordinates,
    kPixel2FilmCoordinates

};

//! Type definition of

} // end of namespace vision
} // end of namespace hr


#endif // HR_VISION_TYPES_H
