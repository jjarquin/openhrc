/*
 *
 * Copyright (C) 2015
 * Julio Jarquin <jjarquin.ct@gmail.com>, Gustavo Arechavaleta <garechav@cinvestav.edu.mx>
 * CINVESTAV - Saltillo Campus
 *
 * This file is part of OpenHRC
 * OpenHRC is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * OpenHRC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/**
 *	\file include/openhrc/vision/vwmpc_data.h
 *	\author Julio Jarquin, Gustavo Arechavaleta
 *	\version 1.0
 *	\date 2015
 *
 *	Declaration of the MPCData class.
 *  The MPCData class holds and initialize the Hessian matrix and the vector matrix
 *  associated with the quadratic program.
 */

#ifndef HR_VISION_VWMPC_DATA_H
#define HR_VISION_VWMPC_DATA_H

#include "openhrc/vision/types.h"
#include "openhrc/locomotion/types.h"
#include "openhrc/locomotion/robot_state.h"
#include <vector>

namespace hr{
namespace vision{


class VWMPCData
{
    friend class VWMPCConstraints; // VWMPCConstraints can access to the private members of class VWMPCData
public:


    VWMPCData():
        g(9.80665), h_com(0.315), alpha(10e-6), beta(1), gamma(1), T_s(0.1), m_steps(2), s_stage(8), stage(0),N_horizon(s_stage*m_steps),n_features(8), n_dof(6){}

    VWMPCData(const real_t& com_height):
        g(9.80665), h_com(com_height), alpha(10e-6), beta(1), gamma(1), T_s(0.1), m_steps(2), s_stage(8), stage(0),N_horizon(s_stage*m_steps),n_features(8), n_dof(6){}

    VWMPCData(const real_t& com_height, const real_t& T_sampling):
        g(9.80665), h_com(com_height), alpha(10e-6), beta(1), gamma(1), T_s(T_sampling), m_steps(2), s_stage(8), stage(0),N_horizon(s_stage*m_steps),n_features(8), n_dof(6){}

    VWMPCData(const real_t& com_height, const real_t& T_sampling, const real_t& kalpha, const real_t& kbeta, const real_t& kgamma):
        g(9.80665), h_com(com_height), alpha(kalpha), beta(kbeta), gamma(kgamma), T_s(T_sampling), m_steps(2), s_stage(8), stage(0),N_horizon(s_stage*m_steps),n_features(8), n_dof(6){}

    VWMPCData(const real_t& com_height, const real_t& T_sampling, VectorXr gains):
            g(9.80665), h_com(com_height), alpha(gains(0)), beta(gains(1)), gamma(gains(2)), T_s(T_sampling),
                                                        m_steps(2), s_stage(8), stage(0),N_horizon(s_stage*m_steps),n_features(8), n_dof(6){}

    VWMPCData(const real_t& com_height, const real_t& T_sampling, const real_t& kalpha, const real_t& kbeta, const real_t& kgamma,
            const int& stepHorizon, const int& singleSupportPeriods):
        g(9.80665), h_com(com_height), alpha(kalpha), beta(kbeta), gamma(kgamma), T_s(T_sampling),
                                                        m_steps(stepHorizon), s_stage(singleSupportPeriods), stage(0),N_horizon(s_stage*m_steps),n_features(8), n_dof(6){}

    VWMPCData(const real_t& com_height, const real_t& T_sampling, VectorXr gains, const int& stepHorizon, const int& singleSupportPeriods):
        g(9.80665), h_com(com_height), alpha(gains(0)), beta(gains(1)), gamma(gains(2)), T_s(T_sampling),
                                                    m_steps(stepHorizon), s_stage(singleSupportPeriods), stage(0),N_horizon(s_stage*m_steps),n_features(8), n_dof(6){}

    VWMPCData(const real_t& com_height, const real_t& T_sampling, const int& stepHorizon, const int& singleSupportPeriods):
        g(9.80665), h_com(com_height), alpha(10e-6), beta(1), gamma(1), T_s(T_sampling), m_steps(stepHorizon),
                                                        s_stage(singleSupportPeriods), stage(0),N_horizon(s_stage*m_steps),n_features(8), n_dof(6){}

    VWMPCData(const real_t& com_height, const real_t& T_sampling, const int& stepHorizon, const int& singleSupportPeriods, VectorXr gains):
            g(9.80665), h_com(com_height), alpha(gains(0)), beta(gains(1)), gamma(gains(2)), T_s(T_sampling),
                                                        m_steps(stepHorizon), s_stage(singleSupportPeriods), stage(0),N_horizon(s_stage*m_steps),n_features(8), n_dof(6){}


    VWMPCData(const real_t& com_height, const real_t& T_sampling, const int& stepHorizon, const int& singleSupportPeriods,
            const real_t& kalpha, const real_t& kbeta, const real_t& kgamma):
        g(9.80665), h_com(com_height), alpha(kalpha), beta(kbeta), gamma(kgamma), T_s(T_sampling),
                                                        m_steps(stepHorizon), s_stage(singleSupportPeriods), stage(0),N_horizon(s_stage*m_steps),n_features(8), n_dof(6){}


    VWMPCData& operator=(const VWMPCData &);

    void init(const hr::MatrixX6r & interactionMatrix, const SpatialMatrix & cameraTransformation);
    void setInteractionMatrix(const hr::MatrixX6r & interactionMatrix, const hr::SpatialMatrix & cameraTransformation);

    int getStage(){return stage;}
    void next();
    MatrixXr getHessian();
    MatrixXr getHessian(const hr::MatrixX6r & interactionMatrix, const hr::SpatialMatrix & cameraTransformation);
    VectorXr getFvector(hr::locomotion::RobotState state,const VectorXr & footState,  const hr::VectorXr & currentFeatures, const hr::VectorXr & desiredHorizonFeatures);


#ifdef _COMPILE_DEBUGGING_METHODS
    void    printConstants();
#endif //_COMPILE_DEBUGGING_METHODS
protected:

private:
    ///MPC Constants

    const real_t g;      /** Gravity constant */
    const real_t h_com;      /** CoM fixed heigh */
    const real_t alpha;         /** Jerk gain */
    const real_t beta;         /** Velocity gain */
    const real_t gamma;        /** ZMP gain */
    const real_t T_s;           /** Double support period */

    const unsigned int m_steps;      /** Future m steps to be considered */
    const unsigned int s_stage;     /** Duration of single support stage in Periods T_s. Double support stage < T_s */
    const unsigned int N_horizon;      /** Previewable window in periods T_s */
    const unsigned int n_features;      /** Number of features */
    const unsigned int n_dof;     /** camera degrees of freedom */
    int stage; /** Current stage for the varying matrix Q*/

    MatrixXr Pvs;        /** Mtrices P of lenght N, are the recursivity of dynamics,  */
    MatrixXr Pvu;
    MatrixXr Pzs;
    MatrixXr Pzu;
    std::vector<VectorXr> Uc;    /** Container of selection vectors Uc, current ZMP reference */
    std::vector<MatrixXr> Ur;    /** Container of selection matrices Ur, next ZMP reference */
    std::vector<MatrixXr> Q;     /** Container of matrices Q, ------ */

    //MatrixXr A_vsys;  //Recursive system matrix A       S_{k+1} = A*S_k + B*U
    //MatrixXr B_vsys;  //Recursive system matrix B       S_{k+1} = A*S_k + B*U

    MatrixXr Miu; //Recursive system matrix.      S_{k+1} = Miu*S_k + Cx*dotX_k+Cy*dotY_k+Ctheta*dotTheta_k
    MatrixXr Cx;  //Recursive system matrix that relates the robot velocity in Y direction and the features velocity
    MatrixXr Cy;  //Recursive system matrix that relates the robot velocity in X direction and the features velocity
    //MatrixXr Ctheta;  //Recursive system matrix that relates the robot velocity in Theta direction and the features velocity

    //VectorXr pk;         /** vector of N size, ------ */



    void initMatricesP();    /** Needed by Q and Pk */
    void initMatricesU();    /** Needed by Q and Pk*/

    void initHessian(const hr::MatrixX6r & interactionMatrix, const SpatialMatrix & cameraTransformation);    //Q for QP Hessian Matrix


};

} //end of namespace vision
} //end of namespace hr

#endif // HR_VISION_VWMPC_DATA_H
