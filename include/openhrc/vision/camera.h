/*
 *
 * Copyright (C) 2015
 * Julio Jarquin <jjarquin.ct@gmail.com>, Gustavo Arechavaleta <garechav@cinvestav.edu.mx>
 * CINVESTAV - Saltillo Campus
 *
 * This file is part of OpenHRC
 * OpenHRC is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * OpenHRC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/**
 *	\file include/openhrc/vision/camera.h
 *	\author Julio Jarquin, Gustavo Arechavaleta
 *	\version 1.0
 *	\date 2015
 *
 *	Declaration of a Camera class.
 */
#ifndef HR_VISION_CAMERA_H
#define HR_VISION_CAMERA_H

#include "types.h"

namespace hr{
namespace vision{

class Camera
{
public:
    Camera(const Matrix3r & cameraMatrix, const int & resolution = kVGA);
    Camera(const real_t & focalLength, const real_t & pixelSize_x, const real_t & pixelSize_y, const real_t & opticalCenter_x, const real_t & opticalCenter_y, const int & resolution = kVGA);

    bool projectPoints(const VectorXr & srcPoints, VectorXr &  destPoints, const int &  pointType = hr::vision::k3DPoint);
    bool convertPoints(const VectorXr & srcPoints, VectorXr &  destPoints, const int & conversionType, const bool & maskPoints = false, const int &  pointType = hr::vision::k3DPoint);
    bool projectPoint(const Vector3r & srcPoint, Vector3r &  destPoint);
    bool projectPoint(const Vector4r & srcPoint, Vector4r &  destPoint);
    MatrixX6r getInteractionMatrix(const VectorXr & points, const int & pointsCoordinates = hr::vision::kImageCoordinates, const int &  pointType = hr::vision::k2DPoint, const real_t & z_parameter = 1.0);
    Matrix3r kMatrix;
    Matrix3r kInverseMatrix;
protected:

};
} // end of namespace hr
} // end of namespace vision

#endif // HR_VISION_CAMERA_H
