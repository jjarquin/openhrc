/*
 *
 * Copyright (C) 2015
 * Julio Jarquin <jjarquin.ct@gmail.com>, Gustavo Arechavaleta <garechav@cinvestav.edu.mx>
 * CINVESTAV - Saltillo Campus
 *
 * This file is part of OpenHRC
 * OpenHRC is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * OpenHRC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/**
 *	\file include/openhrc/vision/vmpc_constraints.h
 *	\author Julio Jarquin, Gustavo Arechavaleta
 *	\version 1.0
 *	\date 2015
 *
 *	Declaration of the VMPCConstraints class.
 *  The VMPCConstraints class holds and initialize the constraints for the VMPC
 */

#ifndef HR_VISION_VMPC_CONSTRAINTS_H
#define HR_VISION_VMPC_CONSTRAINTS_H

#include "types.h"
#include "openhrc/vision/vmpc_data.h"

namespace hr{
namespace vision{
class VMPCConstraints
{
public:
    VMPCConstraints():kMaxX(0.53),kMinX(-0.53),kMaxY(0.3556),kMinY(-0.39){}

  MatrixXr getMatrixC(){
      return CMatrix;
  }

  /**
  \brief This function returns lower bounds vector related to the matrix A
    */
  VectorXr getVectord(){
      return dVector;
  }

  /** Init the required parameters that do not depend on the current constraints, the implementation of the method depends on the robot itself */
  void init(const VMPCData & vmpcData);

  /** The implementation of this method depends on the robot itself */
  void setConstraints(const VMPCData&, const hr::VectorXr & currentFeatures);


protected:
  MatrixXr CMatrix;
  VectorXr dVector;

private:
  const real_t kMaxX;     //Maximum  X for NAO
  const real_t kMinX;     //Minimum  X for NAO
  const real_t kMaxY;     //Maximum  Y for NAO
  const real_t kMinY;     //Minimum  Y for NAO

  VectorXr uB;
  VectorXr lB;

};

}
}

#endif // VMPC_CONSTRAINTS_H
