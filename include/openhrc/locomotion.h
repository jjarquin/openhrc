/*
 *
 * Copyright (C) 2015
 * Julio Jarquin <jjarquin.ct@gmail.com>, Gustavo Arechavaleta <garechav@cinvestav.edu.mx>
 * CINVESTAV - Saltillo Campus
 *
 * This file is part of OpenHRC
 * OpenHRC is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * OpenHRC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/**
 *	\file include/openhrc/locomotion.h
 *	\author Julio Jarquin, Gustavo Arechavaleta
 *	\version 1.0
 *	\date 2015
 *
 * General include for the locomotion module.
 */

/** \defgroup locomotion_Module Locomotion module
  * This is a module of OpenHRC   *
  * \code
  * #include <openhrc/locomotion.h>
  * \endcode
  */


#ifndef HR_LOCOMOTION_H
#define HR_LOCOMOTION_H

#include "locomotion/types.h"
#include "locomotion/robot_state.h"
#include "locomotion/robot_system.h"
#include "locomotion/bezier_curve.h"
#include "locomotion/mpc_data.h"
#include "locomotion/mpc_constraints.h"

#endif // HR_LOCOMOTION_H
