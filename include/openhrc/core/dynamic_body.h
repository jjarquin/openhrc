/*
 *
 * Copyright (C) 2015
 * Julio Jarquin <jjarquin.ct@gmail.com>, Gustavo Arechavaleta <garechav@cinvestav.edu.mx> , Gerardo Jarquin
 * CINVESTAV - Saltillo Campus
 *
 * This file is part of OpenHRC
 * OpenHRC is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * OpenHRC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/**
 *	\file include/openhrc/core/dynamic_body.h
 *	\author Gerardo Jarquin, Gustavo Arechavaleta, Julio Jarquin.
 *	\version 1.0
 *	\date 2015
 *
 * DynamicBody class. It has functions to build a body from a xml file. It is a derived class from body.
 */

#ifndef HR_CORE_DYNAMIC_BODY_H
#define HR_CORE_DYNAMIC_BODY_H

#include "body.h"
#include "types.h"

namespace hr{
namespace core{

//! DynamicBody class, derived from Body class.
/*! \ingroup core_module
    \sa Body
  */
class DynamicBody : public Body
{

    // --------------------------------------------
    // Constructors and Destructor
    // --------------------------------------------
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW //128-bit alignment
    //! Custom constructor. Initializes the body with the given parameters.
    /*! \param id The id of the body.*/
    /*! \param name The name of the body.*/
    DynamicBody(const unsigned int id, const std::string &name);

    //! Custom constructor. Initializes the body with the given parameters.
    /*! \param id The id of the body.*/
    /*! \param name The name of the body.*/
    /*! \param pos The position of the body.*/
    /*! \param rot The rotation matrix (Rx, Ry, Rz).*/
    DynamicBody(const unsigned int id, const std::string &name,
                const Vector3r &pos,
                const Matrix3r &rot);

    //! Default destructor.
    ~DynamicBody();

    // --------------------------------------------
    // Members
    // --------------------------------------------
protected:

    //! Mass of the body.
    real_t mass;

    //! 3D position of the body CoM w.r.t the body reference frame.
    Vector3r centerOfMass;

    //! body inertia tensor w.r.t. the body reference frame.
    Matrix3r inertiaTensor;

    //! spatial inertia matrix w.r.t. the body reference frame.
    SpatialMatrix spatialInertia;

    //! OperationalHandle linear and angular velocity vector (vx,vy,vz,omegax,omegawy,omegaz) relative to the associated body.
    SpatialVector twist;

    //! OperationalHandle force and moments vector (fx,fy,fz,nx,ny,nz) relative to the associated body.
    SpatialVector wrench;

    // --------------------------------------------
    // Methods
    // --------------------------------------------
public:

    bool loadGeometry( const std::string &xmlFileName, Vector3r &relativeBodyPosition);

    //! Set the mass of the body.
    /*! \param a real_t precision number
        \return void
             */
    void setMass( real_t mass ){ this->mass = mass; }

    //! Set the mass of the body.
    /*! \param a 3-dimensional real_t precision array.
        \return void
             */
    void setCoM( const Vector3r &centerOfMass );

    //! Set the inertia Tensor of the body
    /*! \param a real_t precision matrix (3x3 matrix)
        \return void
             */
    void setInertiaTensor( const Matrix3r &inertiaTensor );

    //! Set the spatial inertia of the body
    /*! \param a real_t precision matrix (6x6 matrix)
        \return void
             */
    void setSpatialInertia( const SpatialMatrix &spatialInertia );

    //! Set the linear and angular velocity at opHandle
    /*! \param a 6D point of real_t precision numbers.
        \return void
             */
    void setTwist( const SpatialVector &twist );

    //! Set the force and moment at opHandle
    /*! \param a 6D point of real_t precision numbers.
        \return void
             */
    void setWrench( const SpatialVector &wrench );

    //! Get the mass of the body.
    /*! \param none
        \return a real_t precision number
             */
    real_t getMass(){ return mass; }

    //! Get the center of mass of the body.
    /*! \param none
        \return a 6-dimensional real_t precision array (3D position, orientation)
             */
    Vector3r getCoM(){ return centerOfMass; }

    //! Get the inertia tensor of the body.
    /*! \param none
        \return a real_t precision matrix (3x3 matrix)
             */
    Matrix3r getInertiaTensor(){ return inertiaTensor; }

    //! Get the inertia tensor of the body.
    /*! \param none
        \return a real_t precision matrix (6x6 matrix)
             */
    SpatialMatrix getSpatialInertia() { return spatialInertia; }

    //! Get the twist of the OperationalHandle.
    /*! \param none
         \return a 6-dimensional real_t precision array (linear and angular velocity)
             */
    SpatialVector getTwist() { return twist; }

    //! Get the wrench of the OperationalHandle.
    /*! \param none
        \return a 6-dimensional real_t precision array (force and moment)
             */
    SpatialVector getWrench() { return wrench; }

};
} // end of namespace core
} // end of namespace hr
#endif // HR_CORE_DYNAMIC_BODY_H
