/*
 *
 * Copyright (C) 2015
 * Julio Jarquin <jjarquin.ct@gmail.com>, Gustavo Arechavaleta <garechav@cinvestav.edu.mx>
 * CINVESTAV - Saltillo Campus
 *
 * This file is part of OpenHRC
 * OpenHRC is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * OpenHRC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/**
 *	\file include/openhrc/utils/simple_hierarchical_solver.h
 *	\author Julio Jarquin, Gustavo Arechavaleta
 *	\version 1.0
 *	\date 2015
 *
 *	Declaration of the SimpleHierarchicalSolver abstract class.
 */

#ifndef HR_UTILS_SIMPLE_HIERARCHICAL_SOLVER_H
#define HR_UTILS_SIMPLE_HIERARCHICAL_SOLVER_H

#include "types.h"
#include "openhrc/utils/task_abstract.h"
#include <vector>

namespace hr{
namespace utils{

class SimpleHierarchicalSolver
{
public:
    SimpleHierarchicalSolver();
    VectorXr Solve( std::vector<TaskAbstract*> taskList, MatrixXr  & C , VectorXr & ld ,VectorXr & ud ,  VectorXr & lb,  VectorXr & ub);
};

}
}

#endif // HR_UTILS_SIMPLE_HIERARCHICAL_SOLVER_H
