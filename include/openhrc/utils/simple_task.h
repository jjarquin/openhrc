/*
 *
 * Copyright (C) 2015
 * Julio Jarquin <jjarquin.ct@gmail.com>, Gustavo Arechavaleta <garechav@cinvestav.edu.mx>
 * CINVESTAV - Saltillo Campus
 *
 * This file is part of OpenHRC
 * OpenHRC is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * OpenHRC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/**
 *	\file include/openhrc/utils/simple_task.h
 *	\author Julio Jarquin, Gustavo Arechavaleta
 *	\version 1.0
 *	\date 2015
 *
 *	Declaration of a SimpleTask class.
 */
#ifndef HR_UTILS_SIMPLE_TASK_H
#define HR_UTILS_SIMPLE_TASK_H

#include "types.h"
#include "task_abstract.h"

namespace hr{
namespace utils{

class SimpleTask : public TaskAbstract
{
public:
    SimpleTask();
    inline MatrixXr getMatrixA(){return A;}
    inline VectorXr getVectorb(){return b;}

    inline MatrixXr getMatrixC(){return C;}
    inline VectorXr getVectord(){return d;}

    inline bool constrained(){ return !constraintsEmpty;}


    void addRows(const MatrixXr & rowsA, const VectorXr & scalarsb);
    void addConstraintRows(const MatrixXr & rowsC, const VectorXr & scalarsd);

protected:
    bool empty;
    bool constraintsEmpty;

    MatrixXr A;
    VectorXr b;
    MatrixXr C;
    VectorXr d;

};
} // end of namespace hr
} // end of namespace utils

#endif // HR_UTILS_SIMPLE_KINEMATIC_TASK_H
