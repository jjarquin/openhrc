/*
 *
 * Copyright (C) 2015
 * Julio Jarquin <jjarquin.ct@gmail.com>, Gustavo Arechavaleta <garechav@cinvestav.edu.mx>
 * CINVESTAV - Saltillo Campus
 *
 * This file is part of OpenHRC
 * OpenHRC is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * OpenHRC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/**
 *	\file include/openhrc/utils/quadratic_program.h
 *	\author Julio Jarquin, Gustavo Arechavaleta
 *	\version 1.0
 *	\date 2015
 *
 *	Declaration of the QuadraticProgram abstract class.
 */

#ifndef HR_UTILS_QUADRATICPROGRAM_H
#define HR_UTILS_QUADRATICPROGRAM_H

#include "types.h"

namespace hr{
namespace utils{

//!  QuadraticProgram class.
/*!
  The QuadraticProgram class is an interface for QP classes.
  \f$ \min x^T H x  + x^T f \f$
*/
class QuadraticProgram
{
    friend class QPBounds; // Bounds can access to the private members of QuadraticProgram
    friend class QPConstraints; // Constraints can access to the private members of QuadraticProgram
public:
    //! Default constructor.
    QuadraticProgram();

    //! Get the Hessian Matrix.
    virtual MatrixXr getHessian() = 0;
    //! Get the associated vector.
    virtual VectorXr getfVector() = 0;

private:


};

} //end of namespace utils
} //end of namespace hr

#endif // HR_UTILS_QUADRATICPROGRAM_H
