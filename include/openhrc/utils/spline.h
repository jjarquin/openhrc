/*
 *
 * Copyright (C) 2015
 * Julio Jarquin <jjarquin.ct@gmail.com>, Gustavo Arechavaleta <garechav@cinvestav.edu.mx>
 * CINVESTAV - Saltillo Campus
 *
 * This file is part of OpenHRC
 * OpenHRC is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * OpenHRC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/**
 *	\file include/openhrc/utils/spline.h
 *	\author Julio Jarquin, Gustavo Arechavaleta
 *	\version 1.0
 *	\date 2015
 *
 *	Definition of Splines
 */

#ifndef HR_UTILS_SPLINE_H
#define HR_UTILS_SPLINE_H

#include "openhrc/types.h"
#include <vector>
#include <iostream>

namespace hr{

namespace utils{

void BasisFn(const int & i , const real_t & u, const int & p, const std::vector<real_t> & U, std::vector<real_t> & B){
    B.clear();

    real_t temp, acc;
    std::vector<real_t> DL;
    std::vector<real_t> DR;
    B.push_back(1.0);
    for( int j = 1 ; j <= p ; j++){
        DL.push_back(u-U[i+1-j]);
        DR.push_back(U[i+j]-u);
        acc = 0.0;
        for(int r = 0 ; r <= j-1 ; r++){
            temp = B[r]/(DR[r+1]+DL[j-r]);
            B[r] = acc + DR[r+1]*temp;
            acc = DL[j-r]*temp;
            std::cout << "acc " << acc << std::endl;
        }
        B.push_back(acc);
        }
    }


} // end of namespace utils

} // end of namespace hr

#endif // HR_UTILS_SPLINE_H
