/*
 *
 * Copyright (C) 2015
 * Julio Jarquin <jjarquin.ct@gmail.com>, Gustavo Arechavaleta <garechav@cinvestav.edu.mx>
 * CINVESTAV - Saltillo Campus
 *
 * This file is part of OpenHRC
 * OpenHRC is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * OpenHRC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/**
 *	\file include/openhrc/utils/task_abstract.h
 *	\author Julio Jarquin, Gustavo Arechavaleta
 *	\version 1.0
 *	\date 2015
 *
 *	Declaration of a TaskAbstract class.
 */

#ifndef HR_UTILS_TASK_ABSTRACT_H
#define HR_UTILS_TASK_ABSTRACT_H

#include "types.h"

namespace hr{
namespace utils{

class TaskAbstract
{
public:
    //TaskAbstract();
    virtual MatrixXr getMatrixA() = 0;
    virtual VectorXr getVectorb() = 0;
    virtual MatrixXr getMatrixC() = 0;
    virtual VectorXr getVectord() = 0;
    virtual bool constrained() = 0;
};

} // end of namespace utils
} // end of namespace hr

#endif // HR_UTILS_TASK_ABSTRACT_H
