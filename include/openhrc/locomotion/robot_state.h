/*
 *
 * Copyright (C) 2015
 * Julio Jarquin <jjarquin.ct@gmail.com>, Gustavo Arechavaleta <garechav@cinvestav.edu.mx>
 * CINVESTAV - Saltillo Campus
 *
 * This file is part of OpenHRC
 * OpenHRC is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * OpenHRC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/**
 *	\file include/openhrc/locomotion/robot_state.h
 *	\author Julio Jarquin, Gustavo Arechavaleta
 *	\version 1.0
 *	\date 2015
 *
 *	Declaration of the RobotState class designed to manage the robot minimal state.
 */

#ifndef HR_LOCOMOTION_STATE_H
#define HR_LOCOMOTION_STATE_H
#include "Eigen/Dense"
#include "openhrc/locomotion/types.h"
#include <iostream>

namespace hr{
namespace locomotion{

class RobotState : public  Vector9r
{
public:
    RobotState(void):Vector9r() {}
    typedef Vector9r Base;

    // This constructor allows you to construct RobotState from Eigen expressions
    template<typename OtherDerived>
    RobotState(const Eigen::MatrixBase<OtherDerived>& other)
        : Vector9r(other)
    { }


    // This method allows you to assign Eigen expressions to RobotState
    template<typename OtherDerived>
    RobotState & operator= (const Eigen::MatrixBase <OtherDerived>& other)
    {
        this->Base::operator=(other);
        return *this;
    }


    void setX(const Vector3r &);
    void setY(const Vector3r &);
    void setTheta(const Vector3r &);
    void setState(const Vector3r&,const Vector3r&,const Vector3r&);

    VectorXr getX(){ return (this)->segment(0,3);}
    VectorXr getY(){ return (this)->segment(3,3); }
    VectorXr getTheta(){ return (this)->segment(6,3); }

    //MyVectorType(int numberOfRows):RobotVector(numberOfRows){}


};
} //end of namespace locomotion
} //end of namespace hr

#endif // HR_LOCOMOTION_STATE_H
