/*
 *
 * Copyright (C) 2015
 * Julio Jarquin <jjarquin.ct@gmail.com>, Gustavo Arechavaleta <garechav@cinvestav.edu.mx>
 * CINVESTAV - Saltillo Campus
 *
 * This file is part of OpenHRC
 * OpenHRC is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * OpenHRC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/**
 *	\file include/openhrc/locomotion/types.h
 *	\author Julio Jarquin, Gustavo Arechavaleta
 *	\version 1.0
 *	\date 2015
 *
 *	Declaration of the HRLocomotion types.
 */

#ifndef HR_LOCOMOTION_TYPES_H
#define HR_LOCOMOTION_TYPES_H
#include "Eigen/Dense"
#include "openhrc/types.h"

namespace hr{




//! Defines SUCCESS_INIT as a true bool type, used for return in many methods.
const bool SUCCESS_INIT = true;
//! Defines FAILED_INIT as a false bool type, used for return in many methods.
const bool FAILED_INIT = false;

//! Defines a new eigen-based vector type.
/*!
        Defines a real_t vector of size 9x1.
     */
typedef Eigen::Matrix<real_t , 9 , 1> Vector9r;

//! Namespace locomotion. Namespace of the Locomotion Module
/*!
    More information about locomotion.
*/

namespace core{

//! Status of the robot's feet.
/*! Depending on the current configuration of the robot, the FootStatus will change. */
#ifndef HR_CORE_FOOT_STATUS
#define HR_CORE_FOOT_STATUS
enum FootStatus{
    kLeftFootOnGround,  /*!< The left foot is the robots support foot. */
    kRightFootOnGround, /*!< The right foot is the robots support foot. */
    kBothFeetOnGround  /*!< The robot is supported by both feet. */
};
#endif
}

namespace locomotion{


//! The status of the robot, reveals the current action.
/*! It changes depending on the action being executed by the robot. */
enum RobotStatus{
    kInit,      /*!< The Robot is ready to walk. */
    kStopped,   /*!< The Robot was walking, but it was stopped. */
    kPaused,    /*!< The Robot is walking, but is currently paused. */
    kWalking,   /*!< The Robot is walking. */
    kError      /*!< The Robot might have fallen or the stiffness was removed. */
};



} //end of namespace locomotion
} //end of namespace hr

#endif // HR_LOCOMOTION_TYPES_H
