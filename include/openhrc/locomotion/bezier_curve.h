/*
 *
 * Copyright (C) 2015
 * Julio Jarquin <jjarquin.ct@gmail.com>, Gustavo Arechavaleta <garechav@cinvestav.edu.mx>
 * CINVESTAV - Saltillo Campus
 *
 * This file is part of OpenHRC
 * OpenHRC is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * OpenHRC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/**
 *	\file include/openhrc/locomotion/bezier_curve.h
 *	\author Julio Jarquin, Gustavo Arechavaleta
 *	\version 1.0
 *	\date 2015
 *
 *	Declaration of the BezierCurve class designed to generate a 4th order bezier curve.
 */

#ifndef HR_LOCOMOTION_BEZIERCURVE_H
#define HR_LOCOMOTION_BEZIERCURVE_H
#include "openhrc/locomotion/types.h"
#include <vector>

namespace hr{
namespace locomotion{
class BezierCurve
{
    /*
     *	PUBLIC MEMBER FUNCTIONS
     */
public:
    //! Default constructor.
    BezierCurve();
    //! Destructor.
    ~BezierCurve();

    //! Evaluates the bezier curve for the points and segments it was initialized.
    /*!	\return Bezier curve value at index.  */
    void init(const VectorXr&,const VectorXr&,const VectorXr&,const VectorXr&);

    /** Evaluates the bezier curve for the points and segments it was initialized.
     *	\return Bezier curve value at index.  */
    void init(const VectorXr&,const VectorXr&,const VectorXr&,const VectorXr&, const int&);

    /** Evaluates the bezier curve for the points it was initialized.
     *	\return Bezier curve value at time t.  */
    VectorXr eval(const real_t&);

    /** Evaluates the bezier curve for the points and segments it was initialized.
     *	\return Bezier curve value at index.  */
    VectorXr eval(const int&);


    /** Static implementation of the bezier curve.
     *	\return Bezier curve value at time t for the given points  */
static VectorXr Eval(const real_t &, const VectorXr&, const VectorXr&,
                  const VectorXr&, const VectorXr&);

/** Static implementation of the bezier curve. Internal points are not replaced.
 *	\return Bezier curve value at time t for the given points  */
    VectorXr eval(const real_t &, const VectorXr&, const VectorXr&,
                  const VectorXr&, const VectorXr&);

    /** Static implementation of the bezier curve. Internal points are not replaced.
     *	\return Bezier curve value at time t for the given points  */
    VectorXr eval(const int &, const int &, const VectorXr&, const VectorXr&,
                  const VectorXr&, const VectorXr&);

    /** Static implementation of the bezier curve. \n If rewritePoints is true, the internal points will be replaced by the new ones.
     *	\return Bezier curve value at time t for the given points  */
    VectorXr eval(const real_t &, const VectorXr&, const VectorXr&,
                  const VectorXr&, const VectorXr&, const bool &);

    /** Static implementation of the bezier curve.  \n If rewritePoints is true, the internal points will be replaced by the new ones.
     *	\return Bezier curve value at time t for the given points */
    VectorXr eval(const int &, const int &, const VectorXr&, const VectorXr&,
                  const VectorXr&, const VectorXr&, const bool &);

private:
    VectorXr p_0;
    VectorXr p_1;
    VectorXr p_2;
    VectorXr p_3;
    int n_segments;
    std::vector<VectorXr> p_segments;
    bool initialized;
};

} //end of namespace locomotion
} //end of namespace hr

#endif // HR_LOCOMOTION_BEZIERCURVE_H
