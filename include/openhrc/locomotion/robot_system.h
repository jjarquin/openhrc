/*
 *
 * Copyright (C) 2015
 * Julio Jarquin <jjarquin.ct@gmail.com>, Gustavo Arechavaleta <garechav@cinvestav.edu.mx>
 * CINVESTAV - Saltillo Campus
 *
 * This file is part of OpenHRC
 * OpenHRC is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * OpenHRC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/**
 *	\file include/openhrc/locomotion/robot_system.h
 *	\author Julio Jarquin, Gustavo Arechavaleta
 *	\version 1.0
 *	\date 2015
 *
 *	Declaration of the RobotSystem class designed to storage the robot system dynamic equations as proposed by S. Kajita et.al.
 */

#ifndef HR_LOCOMOTION_RSYSTEM_H
#define HR_LOCOMOTION_RSYSTEM_H
#include "openhrc/locomotion/types.h"
#include "openhrc/locomotion/robot_state.h"

namespace hr{
namespace locomotion{

class RobotSystem
{
public:
    RobotSystem();
    void init(const real_t & T,const real_t & h);
    void nextState(const Vector3r & jerkXYTheta, RobotState &);

    /*
    void init(const real_t & T,const real_t & h, VectorXr, VectorXr);
    void init(const real_t & T,const real_t & h, VectorXr, VectorXr, VectorXr);

    void nextState(VectorXr, VectorXr);
    void nextState(VectorXr, VectorXr, VectorXr);
    void setState(VectorXr, VectorXr);
    void setState(VectorXr, VectorXr, VectorXr);
    */
    VectorXr getZx_k(){ return zx_k;}
    VectorXr getZy_k(){ return zy_k;}

private:
    real_t T_s;
    real_t g;
    MatrixXr A_s;
    MatrixXr B_s;
    MatrixXr C_s;

    VectorXr x_k;
    VectorXr y_k;
    VectorXr theta_k;
    VectorXr zx_k;
    VectorXr zy_k;
};

} //end of namespace locomotion
} //end of namespace hr

#endif // HR_LOCOMOTION_RSYSTEM_H
