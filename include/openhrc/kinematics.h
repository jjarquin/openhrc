/*
 *
 * Copyright (C) 2015
 * Julio Jarquin <jjarquin.ct@gmail.com>, Gustavo Arechavaleta <garechav@cinvestav.edu.mx>
 * CINVESTAV - Saltillo Campus
 *
 * This file is part of OpenHRC
 * OpenHRC is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * OpenHRC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/**
 *	\file include/openhrc/kinematics.h
 *	\author Julio Jarquin, Gustavo Arechavaleta
 *	\version 1.0
 *	\date 2015
 *
 * General Include for the Kinematics module.
 */


/** \defgroup kinematics_module Kinematics module
  * This is the main module of OpenHRC   *
  * \code
  * #include <openhrc/kinematics.h>
  * \endcode
  */

#ifndef HR_KINEMATICS_H
#define HR_KINEMATICS_H

#include "core.h"
#include "kinematics/types.h"
#include "kinematics/kinematic_task.h"
#include "kinematics/center_of_mass_task.h"
#include "kinematics/trajectory_abstract.h"
#include "kinematics/point_trajectory.h"
#include "kinematics/multiple_point_trajectory.h"
#include "kinematics/gain_abstract.h"
#include "kinematics/activation_gain.h"
#include "kinematics/deactivation_gain.h"
#include "kinematics/bezier_trajectory.h"
#include "kinematics/foot_trajectory.h"
#include "kinematics/nao_constraints.h"
#include "kinematics/joint_trajectory.h"
#include "kinematics/trapezoidal_function.h"
#endif // HR_KINEMATICS_H

