#include "openhrc/kinematics/trapezoidal_function.h"
#define _USE_MATH_DEFINES
#include <math.h>

namespace hr{
namespace core{
TrapezoidalFunction::TrapezoidalFunction( const real_t &fn_value, const real_t &initialTime, const real_t &finalTime){
    ti = initialTime;
    tf = finalTime;
    Tf = tf - ti;
    Ta = 0.040;
    Td = 0.040;
    ta = ti + Ta;
    td = tf - Td;
    height = fn_value;
}

TrapezoidalFunction::TrapezoidalFunction(const real_t &fn_value, const real_t &initialTime, const real_t &attackTime, const real_t &decayTime , const real_t &finalTime){
    ti = initialTime;
    tf = finalTime;
    ta = attackTime;
    td = decayTime;

    Tf = tf - ti;

    Ta = ta - ti;
    Td = tf - td;

    height = fn_value;
}
hr::real_t TrapezoidalFunction::getValue(const real_t &time){
    if(time <= ti) return 0.0;
    if(time >= tf) return 0.0;

    if(time >= td){ //deccay phase.
        return -height*(time-tf)/Td;
    }
    else if( time >= ta){ //Constant  phase.
        return height;
    }
    else{ //attack phase.
        return height*(time-ti)/Ta;
    }
}

}
}
