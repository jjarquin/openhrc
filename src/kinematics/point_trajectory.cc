#include "openhrc/kinematics/point_trajectory.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include <assert.h>


namespace hr{
namespace core{

PointTrajectory::PointTrajectory(const hr::real_t & initialTime, const hr::real_t & finalTime, const SpatialVector &  initialPose, const SpatialVector &  finalPose, TrajectoryType trajType)
{
    trajectoryType = trajType;

    t0 = initialTime;
    tf = finalTime;
    Tt = tf-t0;

    X0 = initialPose;
    Xf = finalPose;

    dX0 = hr::SpatialVector::Zero();
    dXf = hr::SpatialVector::Zero();

    ddX0 = hr::SpatialVector::Zero();
    ddXf = hr::SpatialVector::Zero();

    dddX0 = hr::SpatialVector::Zero();
    dddXf = hr::SpatialVector::Zero();

    if(trajectoryType == kCubicPolynomial){
        a0 = X0;
        a1 = dX0;
        a2 =-3*(X0-Xf)/std::pow(tf-t0,2);
        a3 = 2*(X0-Xf)/std::pow(tf-t0,3);
    }
    else if(trajectoryType == kQuinticPolynomial){
        a0 = X0;
        a1 = dX0;
        a2 = 0.5*ddX0;
        a3=(1/(2*std::pow(Tt,3)))*(20*(Xf-X0)-(8*dXf+12*dX0)*Tt-(3*ddX0-ddXf)*Tt*Tt);
        a4=(1/(2*std::pow(Tt,4)))*(-30*(Xf-X0)+(14*dXf+16*dX0)*Tt+(3*ddX0-2*ddXf)*Tt*Tt);
        a5=(1/(2*std::pow(Tt,5)))*(12*(Xf-X0)-6*(dXf+dX0)*Tt+(ddXf-ddX0)*Tt*Tt);
    }
    else if(trajectoryType == kSepticPolynomial){
        a0 = X0;
        a1 = dX0;
        a2 = 0.5*ddX0;
        a3 = dddX0/6;
        a4 = (210*(Xf-X0)-Tt*((30*ddX0-15*ddXf)*Tt+(4*dddX0+dddXf)*Tt*Tt+120*dX0+90*dXf))/(6*std::pow(Tt,4));
        a5 = (-168*(Xf-X0)+Tt*((20*ddX0-14*ddXf)*Tt+(2*dddX0+dddXf)*Tt*Tt+90*dX0+78*dXf))/(2*std::pow(Tt,5));
        a6 = (420*(Xf-X0)-Tt*((45*ddX0-39*ddXf)*Tt+(4*dddX0+3*dddXf)*Tt*Tt+216*dX0+204*dXf))/(6*std::pow(Tt,6));
        a7 = (-120*(Xf-X0)+Tt*((12*ddX0-12*ddXf)*Tt+(dddX0+dddXf)*Tt*Tt+60*dX0+60*dXf))/(6*std::pow(Tt,7));
    }
    else if(trajectoryType == kTrapezoidal){
        ta = Tt*0.2 + t0; //0.2 is de acceleration phase duration
        Ta = ta-t0;
        Td = Ta;
        assert(ta>t0);
        assert(ta<tf);
        assert(ta<t0+Tt/2);
        dXv = (Xf - X0)/(tf-Ta-t0);
    }
    else if(trajectoryType == kCycloidal){

    }


}

PointTrajectory::PointTrajectory(const hr::real_t & initialTime, const hr::real_t & finalTime, const SpatialVector &  initialPose, const SpatialVector &  finalPose, const SpatialVector &  initialTwist, const SpatialVector &  finalTwist, TrajectoryType trajType)
{
    trajectoryType = trajType;

    t0 = initialTime;
    tf = finalTime;
    Tt = tf-t0;

    X0 = initialPose;
    Xf = finalPose;

    dX0 = initialTwist;
    dXf = finalTwist;

    ddX0 = hr::SpatialVector::Zero();
    ddXf = hr::SpatialVector::Zero();

    dddX0 = hr::SpatialVector::Zero();
    dddXf = hr::SpatialVector::Zero();

    if(trajectoryType == kCubicPolynomial){
        a0 = X0;
        a1 = dX0;
        a2 = (-3*(X0-Xf)-(2*dX0+dXf)*(tf-t0))/std::pow(tf-t0,2);
        a3 = (2*(X0-Xf)+(dX0+dXf)*(tf-t0))/std::pow(tf-t0,3);
    }
    else if(trajectoryType == kQuinticPolynomial){
        a0 = X0;
        a1 = dX0;
        a2 = 0.5*ddX0;
        a3=(1/(2*std::pow(Tt,3)))*(20*(Xf-X0)-(8*dXf+12*dX0)*Tt-(3*ddX0-ddXf)*Tt*Tt);
        a4=(1/(2*std::pow(Tt,4)))*(-30*(Xf-X0)+(14*dXf+16*dX0)*Tt+(3*ddX0-2*ddXf)*Tt*Tt);
        a5=(1/(2*std::pow(Tt,5)))*(12*(Xf-X0)-6*(dXf+dX0)*Tt+(ddXf-ddX0)*Tt*Tt);
    }
    else if(trajectoryType == kSepticPolynomial){
        a0 = X0;
        a1 = dX0;
        a2 = 0.5*ddX0;
        a3 = dddX0/6;
        a4 = (210*(Xf-X0)-Tt*((30*ddX0-15*ddXf)*Tt+(4*dddX0+dddXf)*Tt*Tt+120*dX0+90*dXf))/(6*std::pow(Tt,4));
        a5 = (-168*(Xf-X0)+Tt*((20*ddX0-14*ddXf)*Tt+(2*dddX0+dddXf)*Tt*Tt+90*dX0+78*dXf))/(2*std::pow(Tt,5));
        a6 = (420*(Xf-X0)-Tt*((45*ddX0-39*ddXf)*Tt+(4*dddX0+3*dddXf)*Tt*Tt+216*dX0+204*dXf))/(6*std::pow(Tt,6));
        a7 = (-120*(Xf-X0)+Tt*((12*ddX0-12*ddXf)*Tt+(dddX0+dddXf)*Tt*Tt+60*dX0+60*dXf))/(6*std::pow(Tt,7));
    }

    else if(trajectoryType == kTrapezoidal){

        a0 = X0;
        a1 = dX0;
        a2 = (-3*(X0-Xf)-(2*dX0+dXf)*(tf-t0))/std::pow(tf-t0,2);
        a3 = (2*(X0-Xf)+(dX0+dXf)*(tf-t0))/std::pow(tf-t0,3);

        trajectoryType = kCubicPolynomial;
    }

    else if(trajectoryType == kCycloidal){
        a0 = X0;
        a1 = dX0;
        a2 = (-3*(X0-Xf)-(2*dX0+dXf)*(tf-t0))/std::pow(tf-t0,2);
        a3 = (2*(X0-Xf)+(dX0+dXf)*(tf-t0))/std::pow(tf-t0,3);

        trajectoryType = kCubicPolynomial;
    }

}

PointTrajectory::PointTrajectory(const hr::real_t & initialTime, const hr::real_t & finalTime, const SpatialVector &  initialPose, const SpatialVector &  finalPose, const SpatialVector &  initialTwist, const SpatialVector &  finalTwist, const SpatialVector &  initialAccel, const SpatialVector &  finalAccel, TrajectoryType trajType)
{
    trajectoryType = trajType;

    t0 = initialTime;
    tf = finalTime;
    Tt = tf-t0;

    X0 = initialPose;
    Xf = finalPose;

    dX0 = initialTwist;
    dXf = finalTwist;

    ddX0 = initialAccel;
    ddXf = finalAccel;

    dddX0 = hr::SpatialVector::Zero();
    dddXf = hr::SpatialVector::Zero();

    if(trajectoryType == kCubicPolynomial){
        a0 = X0;
        a1 = dX0;
        a2 = (-3*(X0-Xf)-(2*dX0+dXf)*(tf-t0))/std::pow(tf-t0,2);
        a3 = (2*(X0-Xf)+(dX0+dXf)*(tf-t0))/std::pow(tf-t0,3);
    }
    else if(trajectoryType == kQuinticPolynomial){
        a0 = X0;
        a1 = dX0;
        a2 = 0.5*ddX0;
        a3=(1/(2*std::pow(Tt,3)))*(20*(Xf-X0)-(8*dXf+12*dX0)*Tt-(3*ddX0-ddXf)*Tt*Tt);
        a4=(1/(2*std::pow(Tt,4)))*(-30*(Xf-X0)+(14*dXf+16*dX0)*Tt+(3*ddX0-2*ddXf)*Tt*Tt);
        a5=(1/(2*std::pow(Tt,5)))*(12*(Xf-X0)-6*(dXf+dX0)*Tt+(ddXf-ddX0)*Tt*Tt);
    }
    else if(trajectoryType == kSepticPolynomial){
        a0 = X0;
        a1 = dX0;
        a2 = 0.5*ddX0;
        a3 = dddX0/6;
        a4 = (210*(Xf-X0)-Tt*((30*ddX0-15*ddXf)*Tt+(4*dddX0+dddXf)*Tt*Tt+120*dX0+90*dXf))/(6*std::pow(Tt,4));
        a5 = (-168*(Xf-X0)+Tt*((20*ddX0-14*ddXf)*Tt+(2*dddX0+dddXf)*Tt*Tt+90*dX0+78*dXf))/(2*std::pow(Tt,5));
        a6 = (420*(Xf-X0)-Tt*((45*ddX0-39*ddXf)*Tt+(4*dddX0+3*dddXf)*Tt*Tt+216*dX0+204*dXf))/(6*std::pow(Tt,6));
        a7 = (-120*(Xf-X0)+Tt*((12*ddX0-12*ddXf)*Tt+(dddX0+dddXf)*Tt*Tt+60*dX0+60*dXf))/(6*std::pow(Tt,7));
    }

    else if(trajectoryType == kTrapezoidal){

        a0 = X0;
        a1 = dX0;
        a2 = (-3*(X0-Xf)-(2*dX0+dXf)*(tf-t0))/std::pow(tf-t0,2);
        a3 = (2*(X0-Xf)+(dX0+dXf)*(tf-t0))/std::pow(tf-t0,3);

        trajectoryType = kCubicPolynomial;
    }

    else if(trajectoryType == kCycloidal){
        a0 = X0;
        a1 = dX0;
        a2 = (-3*(X0-Xf)-(2*dX0+dXf)*(tf-t0))/std::pow(tf-t0,2);
        a3 = (2*(X0-Xf)+(dX0+dXf)*(tf-t0))/std::pow(tf-t0,3);

        trajectoryType = kCubicPolynomial;
    }

}

PointTrajectory::PointTrajectory(const hr::real_t & initialTime, const hr::real_t &  finalTime,
                                 const SpatialVector &  initialPose, const SpatialVector &  finalPose,
                                 const SpatialVector &  initialTwist, const SpatialVector &  finalTwist,
                                 const SpatialVector &  initialAccel, const SpatialVector &  finalAccel,
                                 const SpatialVector &  initialJerk, const SpatialVector &  finalJerk,
                                 TrajectoryType trajType)
{

  trajectoryType = trajType;

  t0 = initialTime;
  tf = finalTime;
  Tt = tf-t0;

  X0 = initialPose;
  Xf = finalPose;

  dX0 = initialTwist;
  dXf = finalTwist;

  ddX0 = initialAccel;
  ddXf = finalAccel;

  dddX0 = initialJerk;
  dddXf = finalJerk;

  if(trajectoryType == kCubicPolynomial){
      a0 = X0;
      a1 = dX0;
      a2 = (-3*(X0-Xf)-(2*dX0+dXf)*(tf-t0))/std::pow(tf-t0,2);
      a3 = (2*(X0-Xf)+(dX0+dXf)*(tf-t0))/std::pow(tf-t0,3);
  }
  else if(trajectoryType == kQuinticPolynomial){
      a0 = X0;
      a1 = dX0;
      a2 = 0.5*ddX0;
      a3=(1/(2*std::pow(Tt,3)))*(20*(Xf-X0)-(8*dXf+12*dX0)*Tt-(3*ddX0-ddXf)*Tt*Tt);
      a4=(1/(2*std::pow(Tt,4)))*(-30*(Xf-X0)+(14*dXf+16*dX0)*Tt+(3*ddX0-2*ddXf)*Tt*Tt);
      a5=(1/(2*std::pow(Tt,5)))*(12*(Xf-X0)-6*(dXf+dX0)*Tt+(ddXf-ddX0)*Tt*Tt);
  }
  else if(trajectoryType == kSepticPolynomial){
      a0 = X0;
      a1 = dX0;
      a2 = 0.5*ddX0;
      a3 = dddX0/6;
      a4 = (210*(Xf-X0)-Tt*((30*ddX0-15*ddXf)*Tt+(4*dddX0+dddXf)*Tt*Tt+120*dX0+90*dXf))/(6*std::pow(Tt,4));
      a5 = (-168*(Xf-X0)+Tt*((20*ddX0-14*ddXf)*Tt+(2*dddX0+dddXf)*Tt*Tt+90*dX0+78*dXf))/(2*std::pow(Tt,5));
      a6 = (420*(Xf-X0)-Tt*((45*ddX0-39*ddXf)*Tt+(4*dddX0+3*dddXf)*Tt*Tt+216*dX0+204*dXf))/(6*std::pow(Tt,6));
      a7 = (-120*(Xf-X0)+Tt*((12*ddX0-12*ddXf)*Tt+(dddX0+dddXf)*Tt*Tt+60*dX0+60*dXf))/(6*std::pow(Tt,7));
  }

  else if(trajectoryType == kTrapezoidal){

      a0 = X0;
      a1 = dX0;
      a2 = (-3*(X0-Xf)-(2*dX0+dXf)*(tf-t0))/std::pow(tf-t0,2);
      a3 = (2*(X0-Xf)+(dX0+dXf)*(tf-t0))/std::pow(tf-t0,3);

      trajectoryType = kCubicPolynomial;
  }

  else if(trajectoryType == kCycloidal){
      a0 = X0;
      a1 = dX0;
      a2 = (-3*(X0-Xf)-(2*dX0+dXf)*(tf-t0))/std::pow(tf-t0,2);
      a3 = (2*(X0-Xf)+(dX0+dXf)*(tf-t0))/std::pow(tf-t0,3);

      trajectoryType = kCubicPolynomial;
  }


}

hr::SpatialVector PointTrajectory::getPose(const hr::real_t time){

    if(time > tf){
        return Xf;
    }
    if(time < t0){
        return X0;
    }

    if(trajectoryType == kCubicPolynomial){

        return a0+a1*(time-t0)+a2*std::pow(time-t0,2)+a3*std::pow(time-t0,3);

    }
    else if(trajectoryType == kQuinticPolynomial){
        return a0+a1*(time-t0)+a2*std::pow(time-t0,2)+a3*std::pow(time-t0,3)+a4*std::pow(time-t0,4)+a5*std::pow(time-t0,5);
    }
    else if(trajectoryType == kSepticPolynomial){
        return a0+a1*(time-t0)+a2*std::pow(time-t0,2)+a3*std::pow(time-t0,3)+a4*std::pow(time-t0,4)+a5*std::pow(time-t0,5)+a6*std::pow(time-t0,6)+a7*std::pow(time-t0,7);
    }

    else if(trajectoryType == kTrapezoidal){
        if(time >= tf-Td){ //Deceleraton phase.
            return Xf-dXf*(tf-time)-(dXv-dXf)/(2*Td)*std::pow(tf-time,2);
        }
        else if( time >= ta){ //Constant velocity phase.
            return X0+dX0*Ta/2+dXv*(time-t0-Ta/2);
        }
        else{ //Acceleration phase.
            return X0+dX0*(time-t0)+(dXv-dX0)/(2*Ta)*std::pow((time-t0),2);
        }
    }

    else if(trajectoryType == kCycloidal){
        return (Xf-X0)*((time-t0)/Tt-1/(2*M_PI)*std::sin(2*M_PI*(time-t0)/Tt))+X0;
    }

    return hr::SpatialVector::Zero();

}

hr::SpatialVector PointTrajectory::getVelocity(const hr::real_t time){

    if(time > tf){  //There is a problem while comparing two doubles e.g.  2 > 2.
        return hr::SpatialVector::Zero();
    }
    if(time < t0){
        return hr::SpatialVector::Zero();
    }

    if(trajectoryType == kCubicPolynomial){
        return a1+2*a2*(time-t0)+3*a3*std::pow(time-t0,2);
    }
    else if(trajectoryType == kQuinticPolynomial){
        return a1+2*a2*(time-t0)+3*a3*std::pow(time-t0,2)+4*a4*std::pow(time-t0,3)+5*a5*std::pow(time-t0,4);
    }
    else if(trajectoryType == kSepticPolynomial){
        return a1+2*a2*(time-t0)+3*a3*std::pow(time-t0,2)+4*a4*std::pow(time-t0,3)+5*a5*std::pow(time-t0,4)+6*a6*std::pow(time-t0,5)+7*a7*std::pow(time-t0,6);
    }
    else if(trajectoryType == kTrapezoidal){
        if(time >= tf-Td){ //Deceleration phase.
            return dXf + (dXv-dXf)/(Td)*(tf-time);
        }
        else if( time >= ta){ //Constant velocity phase.
            return dXv;
        }
        else{ //Acceleration phase.
            return dX0+(dXv-dX0)/(Ta)*(time-t0);
        }
    }
    else if(trajectoryType == kCycloidal){
        return (Xf-X0)/Tt*(1-std::cos(2*M_PI*(time-t0)/Tt));
    }

    return hr::SpatialVector::Zero();
}

hr::SpatialVector PointTrajectory::getAcceleration(const hr::real_t time){

    if(time > tf){  //There is a problem while comparing two doubles e.g.  2 > 2.
        return hr::SpatialVector::Zero();
    }
    if(time < t0){
        return hr::SpatialVector::Zero();
    }

    if(trajectoryType == kCubicPolynomial){
        return 2*a2+6*a3*(time-t0);
    }
    else if(trajectoryType == kQuinticPolynomial){
        return 2*a2+6*a3*(time-t0)+12*a4*std::pow(time-t0,2)+20*a5*std::pow(time-t0,3);
    }
    else if(trajectoryType == kSepticPolynomial){
        return 2*a2+6*a3*(time-t0)+12*a4*std::pow(time-t0,2)+20*a5*std::pow(time-t0,3)+30*a6*std::pow(time-t0,4)+42*a7*std::pow(time-t0,5);

    }

    else if(trajectoryType == kTrapezoidal){
        if(time >= tf-Td){ //Deceleration phase.
            return -(dXv-dXf)/(Td);
        }
        else if( time >= ta){ //Constant velocity phase.
            return hr::SpatialVector::Zero();
        }
        else{ //Acceleration phase.
            return (dXv-dX0)/(Ta);
        }
    }
    else if(trajectoryType == kCycloidal){
        return 2*M_PI*(Xf-X0)/(Tt*Tt)*(std::sin(2*M_PI*(time-t0)/Tt));
    }

    return hr::SpatialVector::Zero();
}



}
}


