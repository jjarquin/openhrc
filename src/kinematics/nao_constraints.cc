#include "openhrc/kinematics/nao_constraints.h"
#include <iostream>

namespace hr{
namespace core{
namespace nao{


void SupportBaseConstraint::setConstraint(const Matrix2Xr & comJacobian, const Vector2r & comXY, const Vector3r & leftFootPose, const Vector3r & rightFootPose, const FootStatus & footStatus, const real_t & deltaT){

    Vector2r leftFootXY;
    Vector2r rightFootXY;
    Matrix2r leftRot;
    Matrix2r rightRot;


    if(footStatus == hr::core::kLeftFootOnGround){

        MatrixXr Cfoot(4,2);
        VectorXr dFoot(4);

        leftFootXY << leftFootPose(0), leftFootPose(1);
        dFoot << 0.05 , 0.02 , 0.02, 0.02;

        Cfoot << 1 , 0 , -1 , 0 , 0 , 1 , 0 , -1 ;
        leftRot << std::cos(leftFootPose(2)), - std::sin(leftFootPose(2)), std::sin(leftFootPose(2)), std::cos(leftFootPose(2));
        Cfoot = Cfoot*leftRot.transpose();

        dVector = dFoot + Cfoot * (leftFootXY - comXY);
        CMatrix = deltaT*Cfoot*comJacobian;
    }
    else if(footStatus == hr::core::kRightFootOnGround){
        MatrixXr Cfoot(4,2);
        VectorXr dFoot(4);

        rightFootXY << rightFootPose(0), rightFootPose(1);
        dFoot << 0.05 , 0.02 , 0.02, 0.02;

        Cfoot << 1 , 0 , -1 , 0 , 0 , 1 , 0 , -1 ;
        rightRot << std::cos(rightFootPose(2)), - std::sin(rightFootPose(2)), std::sin(rightFootPose(2)), std::cos(rightFootPose(2));
        Cfoot = Cfoot*rightRot.transpose();

        dVector = dFoot + Cfoot * (rightFootXY - comXY);
        CMatrix = deltaT*Cfoot*comJacobian;
    }
    else if(footStatus == hr::core::kBothFeetOnGround){
        std::vector<hr::Vector2r> hull;
        std::vector<hr::Vector2r> c_hull;

        leftFootXY << leftFootPose(0), leftFootPose(1);
        rightFootXY << rightFootPose(0), rightFootPose(1);
        rightRot << std::cos(leftFootPose(2)), - std::sin(leftFootPose(2)), std::sin(leftFootPose(2)), std::cos(leftFootPose(2));
        leftRot << std::cos(rightFootPose(2)), - std::sin(rightFootPose(2)), std::sin(rightFootPose(2)), std::cos(rightFootPose(2));


        hull.push_back(leftRot*LP0+leftFootXY);
        hull.push_back(leftRot*LP1+leftFootXY);
        hull.push_back(rightRot*RP0+rightFootXY);
        hull.push_back(rightRot*RP1+rightFootXY);

        hull.push_back(rightRot*RP2+rightFootXY);
        hull.push_back(rightRot*RP3+rightFootXY);
        hull.push_back(leftRot*LP2+leftFootXY);
        hull.push_back(leftRot*LP3+leftFootXY);

        hull.push_back(leftRot*LP0+leftFootXY);

        std::vector<int> index = convex_hull(hull);
        for(int k = 0; k != index.size(); k++){
            c_hull.push_back(hull[index[k]]);
        }
        c_hull.push_back(hull[0]);

        hr::MatrixXr Cfeet(index.size(),2);
        hr::VectorXr dFeet(index.size());

        for(int k = 0; k != index.size(); k++){
            Cfeet(k,0) = -(c_hull[k+1](1)-c_hull[k](1));
            Cfeet(k,1) = (c_hull[k+1](0)-c_hull[k](0));
            dFeet(k) = (c_hull[k+1](0)-c_hull[k](0))*c_hull[k+1](1)-(c_hull[k+1](1)-c_hull[k](1))*c_hull[k+1](0);
        }

        CMatrix = deltaT*Cfeet*comJacobian;
        dVector = dFeet - Cfeet * comXY;
    }
}

std::vector<int> SupportBaseConstraint::convex_hull( std::vector<hr::Vector2r> V){
    std::vector<int> index;
    int n = V.size() - 1;
    index.push_back(0);
    bool exit = false;
    for(int i = 1 ; i != n ; i ++){
        for(int k = i + 1 ; k <= n ; k++){
            if(isLeft(V[index.back()],V[i],V[k]) > 0){
                exit = true;
                break;
            }
            else{

            }
        }
        if(exit){
            exit = false;
            continue;
        }
        else{
            index.push_back(i);
        }
    }
    return index;
}


AreaConstraint::AreaConstraint(const Vector2r &center, const real_t &width, const real_t &height){
    x_max = center(0) + width/2.0;
    x_min = center(0) - width/2.0;

    y_max = center(1) + height/2.0;
    y_min = center(1) - height/2.0;
}

void AreaConstraint::setConstraint(const Matrix2Xr &endEffectorJacobian, const Vector2r &endEffectorPosition, const real_t & deltaT){
    MatrixXr CArea(4,2);
    VectorXr dArea(4);

    dArea << x_max, -x_min, y_max,-y_min;
    CArea << 1 , 0 , -1 , 0 , 0 , 1 , 0 , -1 ;

    dVector = dArea - CArea * endEffectorPosition;
    CMatrix = deltaT*CArea*endEffectorJacobian;
}

void AreaConstraint::setConstraint(const Matrix2Xr &endEffectorJacobian, const Vector2r &endEffectorPosition, const Vector2r &  originFrame, const real_t & deltaT){
    MatrixXr CArea(4,2);
    VectorXr dArea(4);

    dArea << x_max, -x_min, y_max,-y_min;
    CArea << 1 , 0 , -1 , 0 , 0 , 1 , 0 , -1 ;

    dVector = dArea - CArea * (endEffectorPosition-originFrame);
    CMatrix = deltaT*CArea*endEffectorJacobian;
}

} // end of namespace nao
} // end of namespace core
} // end of namespace hr
