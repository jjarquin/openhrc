#include "openhrc/kinematics/multiple_point_trajectory.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include <assert.h>

#include <boost/make_shared.hpp>
#include <iostream>

namespace hr{
  namespace core{

    MultiplePointTrajectory::MultiplePointTrajectory(const std::vector<real_t> times, const std::vector<SpatialVector> & points, const std::vector<SpatialVector> &  initialConditions,const std::vector<SpatialVector> &  finalConditions, const int _degree):
      mTimes(times), mPoints(points){

      //assertions
        assert(times.size() == points.size());

      //end of assertions

      ti = times[0];
      tf = times.back();
      Tt = tf-ti;
      trajectoryType = kSpline;

      VectorXr _times = VectorXr::Map(mTimes.data(),mTimes.size());
      _times = (_times - ti*VectorXr::Ones(_times.rows()))*(1/Tt);

      PointsType _points;
      _points.resize(6,points.size());
      for(int k = 0 ; k != points.size(); k++){
          _points.col(k) = points[k];
        }

      SpatialFitter fitter;
      mSpline = boost::make_shared<SpatialSpline>(fitter.Interpolate<PointsType>(_points,_degree,_times));
    }


    hr::SpatialVector MultiplePointTrajectory::getPose(const hr::real_t time){

      if(time > tf){
          return mPoints.back();
        }
      if(time < ti){
          return mPoints[0];

        }


      return (*mSpline)((time-ti)/Tt);

    }

    hr::SpatialVector MultiplePointTrajectory::getVelocity(const hr::real_t time){

      if(time > tf){  //There is a problem while comparing two doubles e.g.  2 > 2.
          return hr::SpatialVector::Zero();
        }
      if(time < ti){
          return hr::SpatialVector::Zero();
        }

      return (1/Tt)*mSpline->derivatives<1>((time-ti)/Tt).col(1);

    }

    hr::SpatialVector MultiplePointTrajectory::getAcceleration(const hr::real_t time){

      if(time > tf){  //There is a problem while comparing two doubles e.g.  2 > 2.
          return hr::SpatialVector::Zero();
        }
      if(time < ti){
          return hr::SpatialVector::Zero();
        }

      return (1/(Tt*Tt))*mSpline->derivatives<2>((time-ti)/Tt).col(2);
    }
  } // End of namespace core
} // End of namespace hr


