#include "openhrc/kinematics/joint_trajectory.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include <assert.h>


namespace hr{
namespace core{

JointTrajectory::JointTrajectory(hr::real_t initialTime, hr::real_t finalTime, hr::VectorXr initialConfiguration, hr::VectorXr finalConfiguration, TrajectoryType trajType)
{
    assert( initialConfiguration.rows() == finalConfiguration.rows());

    n_joints = initialConfiguration.rows();
    zero.setZero(n_joints);    trajectoryType = trajType;

    t0 = initialTime;
    tf = finalTime;
    Tt = tf-t0;

    X0 = initialConfiguration;
    Xf = finalConfiguration;

    dX0.setZero(n_joints);
    dXf.setZero(n_joints);

    ddX0.setZero(n_joints);
    ddXf.setZero(n_joints);

    if(trajectoryType == kCubicPolynomial){
        a0 = X0;
        a1 = dX0;
        a2 =-3*(X0-Xf)/std::pow(tf-t0,2);
        a3 = 2*(X0-Xf)/std::pow(tf-t0,3);
    }
    else if(trajectoryType == kQuinticPolynomial){
        a0 = X0;
        a1 = dX0;
        a2 = 0.5*ddX0;
        a3=(1/(2*std::pow(Tt,3)))*(20*(Xf-X0)-(8*dXf+12*dX0)*Tt-(3*ddX0-ddXf)*Tt*Tt);
        a4=(1/(2*std::pow(Tt,4)))*(-30*(Xf-X0)+(14*dXf+16*dX0)*Tt+(3*ddX0-2*ddXf)*Tt*Tt);
        a5=(1/(2*std::pow(Tt,5)))*(12*(Xf-X0)-6*(dXf+dX0)*Tt+(ddXf-ddX0)*Tt*Tt);
    }
    else if(trajectoryType == kTrapezoidal){
        ta = Tt*0.2 + t0; //0.2 is de acceleration phase duration
        Ta = ta-t0;
        Td = Ta;
        assert(ta>t0);
        assert(ta<tf);
        assert(ta<t0+Tt/2);
        dXv = (Xf - X0)/(tf-Ta-t0);
    }
    else if(trajectoryType == kCycloidal){

    }


}

/*
JointTrajectory::JointTrajectory(hr::real_t initialTime, hr::real_t finalTime, hr::VectorXr initialConfiguration, hr::VectorXr finalConfiguration, hr::VectorXr initialTwist, hr::VectorXr finalTwist, TrajectoryType trajType)
{
    trajectoryType = trajType;

    t0 = initialTime;
    tf = finalTime;
    Tt = tf-t0;

    X0 = initialConfiguration;
    Xf = finalConfiguration;

    dX0 = initialTwist;
    dXf = finalTwist;

    ddX0 = hr::VectorXr::Zero();
    ddXf = hr::VectorXr::Zero();

    if(trajectoryType == kCubicPolynomial){
        a0 = X0;
        a1 = dX0;
        a2 = (-3*(X0-Xf)-(2*dX0+dXf)*(tf-t0))/std::pow(tf-t0,2);
        a3 = (2*(X0-Xf)+(dX0+dXf)*(tf-t0))/std::pow(tf-t0,3);
    }
    else if(trajectoryType == kQuinticPolynomial){
        a0 = X0;
        a1 = dX0;
        a2 = 0.5*ddX0;
        a3=(1/(2*std::pow(Tt,3)))*(20*(Xf-X0)-(8*dXf+12*dX0)*Tt-(3*ddX0-ddXf)*Tt*Tt);
        a4=(1/(2*std::pow(Tt,4)))*(-30*(Xf-X0)+(14*dXf+16*dX0)*Tt+(3*ddX0-2*ddXf)*Tt*Tt);
        a5=(1/(2*std::pow(Tt,5)))*(12*(Xf-X0)-6*(dXf+dX0)*Tt+(ddXf-ddX0)*Tt*Tt);
    }
    else if(trajectoryType == kDegreeSeven){
        a0 = X0;
        a1 = dX0;
        a2 = 0.5*ddX0;
        a3=(1/(2*std::pow(Tt,3)))*(20*(Xf-X0)-(8*dXf+12*dX0)*Tt-(3*ddX0-ddXf)*Tt*Tt);
        a4=(1/(2*std::pow(Tt,4)))*(-30*(Xf-X0)+(14*dXf+16*dX0)*Tt+(3*ddX0-2*ddXf)*Tt*Tt);
        a5=(1/(2*std::pow(Tt,5)))*(12*(Xf-X0)-6*(dXf+dX0)*Tt+(ddXf-ddX0)*Tt*Tt);
    }

    else if(trajectoryType == kTrapezoidal){

        a0 = X0;
        a1 = dX0;
        a2 = (-3*(X0-Xf)-(2*dX0+dXf)*(tf-t0))/std::pow(tf-t0,2);
        a3 = (2*(X0-Xf)+(dX0+dXf)*(tf-t0))/std::pow(tf-t0,3);

        trajectoryType = kCubicPolynomial;
    }

    else if(trajectoryType == kCycloidal){
        a0 = X0;
        a1 = dX0;
        a2 = (-3*(X0-Xf)-(2*dX0+dXf)*(tf-t0))/std::pow(tf-t0,2);
        a3 = (2*(X0-Xf)+(dX0+dXf)*(tf-t0))/std::pow(tf-t0,3);

        trajectoryType = kCubicPolynomial;
    }

}

*/


hr::VectorXr JointTrajectory::getJointConfiguration(hr::real_t time){

    if(time > tf){
        return Xf;
    }
    if(time < t0){
        return X0;
    }

    if(trajectoryType == kCubicPolynomial){

        return a0+a1*(time-t0)+a2*std::pow(time-t0,2)+a3*std::pow(time-t0,3);

    }
    else if(trajectoryType == kQuinticPolynomial){
        return a0+a1*(time-t0)+a2*std::pow(time-t0,2)+a3*std::pow(time-t0,3)+a4*std::pow(time-t0,4)+a5*std::pow(time-t0,5);
    }
    else if(trajectoryType == kTrapezoidal){
        if(time >= tf-Td){ //Deceleraton phase.
            return Xf-dXf*(tf-time)-(dXv-dXf)/(2*Td)*std::pow(tf-time,2);
        }
        else if( time >= ta){ //Constant velocity phase.
            return X0+dX0*Ta/2+dXv*(time-t0-Ta/2);
        }
        else{ //Acceleration phase.
            return X0+dX0*(time-t0)+(dXv-dX0)/(2*Ta)*std::pow((time-t0),2);
        }
    }

    else if(trajectoryType == kCycloidal){
        return (Xf-X0)*((time-t0)/Tt-1/(2*M_PI)*std::sin(2*M_PI*(time-t0)/Tt))+X0;
    }

}

hr::VectorXr JointTrajectory::getJointSpeed(hr::real_t time){

    if(time > tf){  //There is a problem while comparing two doubles e.g.  2 > 2.
        return zero;
    }
    if(time < t0){
        return zero;
    }

    if(trajectoryType == kCubicPolynomial){
        return a1+2*a2*(time-t0)+3*a3*std::pow(time-t0,2);
    }
    else if(trajectoryType == kQuinticPolynomial){
        return a1+2*a2*(time-t0)+3*a3*std::pow(time-t0,2)+4*a4*std::pow(time-t0,3)+5*a5*std::pow(time-t0,4);
    }
    else if(trajectoryType == kTrapezoidal){
        if(time >= tf-Td){ //Deceleration phase.
            return dXf + (dXv-dXf)/(Td)*(tf-time);
        }
        else if( time >= ta){ //Constant velocity phase.
            return dXv;
        }
        else{ //Acceleration phase.
            return dX0+(dXv-dX0)/(Ta)*(time-t0);
        }
    }
    else if(trajectoryType == kCycloidal){
        return (Xf-X0)/Tt*(1-std::cos(2*M_PI*(time-t0)/Tt));
    }
}

hr::VectorXr JointTrajectory::getJointAcceleration(hr::real_t time){

    if(time > tf){  //There is a problem while comparing two doubles e.g.  2 > 2.
        return zero;
    }
    if(time < t0){
        return zero;
    }

    if(trajectoryType == kCubicPolynomial){
        return 2*a2+6*a3*(time-t0);
    }
    else if(trajectoryType == kQuinticPolynomial){
        return 2*a2+6*a3*(time-t0)+12*a4*std::pow(time-t0,2)+20*a5*std::pow(time-t0,3);
    }
    else if(trajectoryType == kTrapezoidal){
        if(time >= tf-Td){ //Deceleration phase.
            return -(dXv-dXf)/(Td);
        }
        else if( time >= ta){ //Constant velocity phase.
            return zero;
        }
        else{ //Acceleration phase.
            return (dXv-dX0)/(Ta);
        }
    }
    else if(trajectoryType == kCycloidal){
        return 2*M_PI*(Xf-X0)/(Tt*Tt)*(std::sin(2*M_PI*(time-t0)/Tt));
    }
}



}  // end of namespace core
}  // end of namespace hr


