/*
 *
 * Copyright (C) 2015
 * Julio Jarquin <jjarquin.ct@gmail.com>, Gustavo Arechavaleta <garechav@cinvestav.edu.mx>
 * CINVESTAV - Saltillo Campus
 *
 * This file is part of OpenHRC
 * OpenHRC is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * OpenHRC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/**
 *	\file src/kinematics/center_of_mass_task.h
 *	\author Julio Jarquin, Gustavo Arechavaleta
 *	\version 1.0
 *	\date 2015
 *
 *	Implementation of the CoMTask.
 */

#include "openhrc/kinematics/center_of_mass_task.h"

namespace hr{
namespace core{

void CoMTask::update(const hr::real_t & time){
    SpatialVector error;
    error.setZero();

    Matrix3Xr jacobian;
    jacobian = robot->getCoMJacobian();
    handlePosition = robot->getCoM();

    if( taskType == kRegulationTask){
        error.head(3) = desiredValue.head(3) - handlePosition;
        error = taskGain * error;
    }
    else if (taskType == kTrackingTask){

        error.head(3) = taskTrajectory->getPose(time).head(3) - handlePosition;
        error = taskGain*error+taskTrajectory->getVelocity(time);

    }

    int i = 0;
    if(taskSpace & kX){
        taskJacobian.row(i) =jacobian.row(0);
        taskError(i) = error(0);
        i++;
    }
    if(taskSpace & kY){
        taskJacobian.row(i) =jacobian.row(1);
        taskError(i) = error(1);
        i++;
    }
    if(taskSpace & kZ){
        taskJacobian.row(i) =jacobian.row(2);
        taskError(i) = error(2);
        i++;
    }
 }

void CoMTask::setGoal(const SpatialVector & taskGoal){
    taskType = kRegulationTask;
    desiredValue = taskGoal;
}

void CoMTask::setTrajectory(boost::shared_ptr< TrajectoryAbstract > trajectory){
    taskType = kTrackingTask;
    taskTrajectory = trajectory;
}

}
}

