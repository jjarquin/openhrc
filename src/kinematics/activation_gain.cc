/*
 * Copyright (C) 2015
 * Julio Jarquin <jjarquin.ct@gmail.com>, Gustavo Arechavaleta <garechav@cinvestav.edu.mx>
 * CINVESTAV - Saltillo Campus
 *
 * This file is part of OpenHRC
 * OpenHRC is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * OpenHRC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */


/**
 *	\file src/kinematics/activation_gain.h
 *	\author Julio Jarquin, Gustavo Arechavaleta
 *	\version 1.0
 *	\date 2015
 *
 *	Implementation of a ActivationGain class.
 */

#include "openhrc/kinematics/activation_gain.h"
using std::pow;
namespace hr{
namespace core{

ActivationGain::ActivationGain(const real_t initialTime, const real_t finalTime, const real_t gain)
{
    t0 = initialTime;
    tf = finalTime;
    Tt = tf-t0;

    this->gain = gain;

}
hr::real_t ActivationGain::operator ()(const hr::real_t time) {
    if(time > tf){
        return gain;
    }

    else if(time < t0){
        return 0.0;
    }

    else{
        return  gain*(10*( (pow(time - t0,3))/(pow(Tt,3)) ) - 15*( (pow(time - t0,4))/(pow(Tt,4)) ) + 6*( (pow(time - t0, 5))/(pow(Tt, 5)) ));
    }
}

ActivationGain::~ActivationGain(){

}

} //end of namespace core
} //end of namespace hr

