#include "openhrc/kinematics/foot_trajectory.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include <iostream>

namespace hr{
namespace core{

FootTrajectory::FootTrajectory(const hr::real_t & initialTime, const hr::real_t & finalTime, const SpatialVector & initialPose, const SpatialVector & finalPose, const FootTrajectoryType & trajType, const int & heightCalcType, const hr::real_t & fixedStepZ , const hr::real_t & maxStepZ )
{
    t0 = initialTime;
    tf = finalTime;
    Tt = tf-t0;
    X0 = initialPose;
    Xf = finalPose;
    trajectoryType = trajType;

    hr::real_t dx = finalPose(0)-initialPose(0);
    hr::real_t dy = finalPose(1)-initialPose(1);

    if(heightCalcType == hr::core::kAuto)  rad = 0.5*sqrt(dx*dx+dy*dy);
    else rad = fixedStepZ;

    if(rad > maxStepZ){
        rad = maxStepZ;
    }


    if(trajectoryType == hr::core::kFootBezier){

        X1 = X0+(Xf-X0)/3;
        X2 = Xf;

        X1(2) += rad; // Foot height in Z
        X2(2) += rad; // Foot height in Z

    }
    else if (trajectoryType == hr::core::kFootCycloidal){
      rad *= 0.5;

    }

}



FootTrajectory::FootTrajectory(const hr::real_t & initialTime, const hr::real_t & finalTime, const Vector3r & initialPose, const Vector3r & finalPose, const FootTrajectoryType & trajType, const int & heightCalcType, const hr::real_t & fixedStepZ, const hr::real_t & maxStepZ)
{
    t0 = initialTime;
    tf = finalTime;
    Tt = tf-t0;
    X0 << initialPose(0), initialPose(1), 0 , 0 , 0 , initialPose(2);
    Xf << finalPose(0), finalPose(0), 0, 0, 0, finalPose(2);
    trajectoryType = trajType;

    hr::real_t dx = finalPose(0)-initialPose(0);
    hr::real_t dy = finalPose(1)-initialPose(1);

    if(heightCalcType == hr::core::kAuto)  rad = 0.5*sqrt(dx*dx+dy*dy);
    else rad = fixedStepZ;

    if(rad > maxStepZ){
        rad = maxStepZ;
    }

    if(trajectoryType == hr::core::kFootBezier){
        X1 = X0+(Xf-X0)/3;
        X2 = Xf;

        X1(2) += rad; // Foot height in Z
        X2(2) += rad; // Foot height in Z

    }
    else if (trajectoryType == hr::core::kFootCycloidal){
      rad *= 0.5;
    }


}

SpatialVector FootTrajectory::getPose(const hr::real_t time){
    if(time > tf){
        return Xf;
    }
    if(time < t0){
        return X0;
    }

    if(trajectoryType == hr::core::kFootBezier){
        hr::real_t t = ((time-t0)/Tt);

        return X0*pow(1-t,3) + 3*X1*t*pow(1-t,2) + 3*X2*t*t*(1-t) + Xf*t*t*t;
    }
    else if (trajectoryType == hr::core::kFootCycloidal){
        hr::SpatialVector cyctraj = (Xf-X0)*((time-t0)/Tt-1/(2*M_PI)*sin(2*M_PI*(time-t0)/Tt))+X0;
        cyctraj(2) = rad*(1-cos(2*M_PI*(time-t0)/Tt))+X0(2);
        return cyctraj;

    }


}

SpatialVector FootTrajectory::getVelocity(const hr::real_t time){
    if(time > tf){
        return SpatialVector::Zero();
    }
    if(time < t0){
        return SpatialVector::Zero();
    }

    if(trajectoryType == hr::core::kFootBezier){
        hr::real_t t = ((time-t0)/Tt);

        return (1/Tt)*(3*pow(1-t,2)*(X1-X0)+6*(1-t)*t*(X2-X1)+3*t*t*(Xf-X2));

    }
    else if (trajectoryType == hr::core::kFootCycloidal){
        hr::SpatialVector cyctraj = (Xf-X0)/Tt*(1-std::cos(2*M_PI*(time-t0)/Tt));
        cyctraj(2) = 2*M_PI*rad/Tt*sin(2*M_PI*(time-t0)/Tt);

        return cyctraj;
    }

}

SpatialVector FootTrajectory::getAcceleration(const hr::real_t time){
    if(time > tf){
        return SpatialVector::Zero();
    }
    if(time < t0){
        return SpatialVector::Zero();
    }

    if(trajectoryType == hr::core::kFootBezier){
        hr::real_t t = ((time-t0)/Tt);

        return 1/(Tt*Tt)*(6*(1-t)*(X2-2*X1+X0)+6*t*(Xf-2*X2+X1));

    }
    else if (trajectoryType == hr::core::kFootCycloidal){
        hr::SpatialVector cyctraj = 2*M_PI*(Xf-X0)/(Tt*Tt)*(std::sin(2*M_PI*(time-t0)/Tt));
        cyctraj(2) = 4*M_PI*M_PI*rad/(Tt*Tt)*cos(2*M_PI*(time-t0)/Tt);
        return cyctraj;
    }

}

} // end of namespace core
} // end of namespace hr

