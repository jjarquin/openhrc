/*
 * Copyright (C) 2015
 * Julio Jarquin <jjarquin.ct@gmail.com>, Gustavo Arechavaleta <garechav@cinvestav.edu.mx>
 * CINVESTAV - Saltillo Campus
 *
 * This file is part of OpenHRC
 * OpenHRC is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * OpenHRC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */


/**
 *	\file openhrc/kinematics/deactivation_gain.h
 *	\author Julio Jarquin, Gustavo Arechavaleta
 *	\version 1.0
 *	\date 2015
 *
 *  Declaration of the DeactivationGain class.
 */

#include "openhrc/kinematics/deactivation_gain.h"
namespace hr{
namespace core{

DeactivationGain::DeactivationGain(const hr::real_t initialTime, const hr::real_t finalTime, const hr::real_t gain)
{
    t0 = initialTime;
    tf = finalTime;
    Tt = tf-t0;

    this->gain = gain;
}

hr::real_t DeactivationGain::operator() (const hr::real_t time) {
    if(time > tf){
        return 0.0;
    }

    else if(time < t0){
        return gain;
    }

    else{

        return gain*(1.0 - (10*( (pow(time - t0,3))/(pow(Tt,3)) ) - 15*( (pow(time - t0,4))/(pow(Tt,4)) ) + 6*( (pow(time - t0, 5))/(pow(Tt, 5)) )));
    }


}

DeactivationGain::~DeactivationGain(){

}
} //end of namespace core
} //end of namespace hr

