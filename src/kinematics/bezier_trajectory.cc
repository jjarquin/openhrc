#include "openhrc/kinematics/bezier_trajectory.h"

namespace hr{
namespace core{

BezierTrajectory::BezierTrajectory(hr::real_t initialTime, hr::real_t finalTime, SpatialVector initialPose, SpatialVector internalP1, SpatialVector internalP2, SpatialVector finalPose)
{
    t0 = initialTime;
    tf = finalTime;
    Tt = tf-t0;
    X0 = initialPose;
    X1 = internalP1;
    X2 = internalP2;
    Xf = finalPose;

}

SpatialVector BezierTrajectory::getPose(hr::real_t time){

    if(time > tf){
        return Xf;
    }
    if(time < t0){
        return X0;
    }
    hr::real_t t = ((time-t0)/Tt);

    return X0*pow(1-t,3) + 3*X1*t*pow(1-t,2) + 3*X2*pow(t,2)*(1-t) + Xf*pow(t,3);
}

SpatialVector BezierTrajectory::getVelocity(hr::real_t time){


    if(time > tf){
        return SpatialVector::Zero();
    }
    if(time < t0){
        return SpatialVector::Zero();
    }
    hr::real_t t = ((time-t0)/Tt);

    return 1/Tt*(3*pow(1-t,2)*(X1-X0)+6*(1-t)*t*(X2-X1)+3*t*t*(Xf-X2));
}

SpatialVector BezierTrajectory::getAcceleration(hr::real_t time){

    if(time > tf){
        return SpatialVector::Zero();
    }
    if(time < t0){
        return SpatialVector::Zero();
    }
    hr::real_t t = ((time-t0)/Tt);

    return 1/(Tt*Tt)*(6*(1-t)*(X2-2*X1+X0)+6*t*(Xf-2*X2+X1));


}


} // end of namespace core
} // end of namespace hr
