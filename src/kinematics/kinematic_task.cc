#include "openhrc/kinematics/kinematic_task.h"

#include <iostream>
namespace hr{

namespace core{

void KinematicTask::computeTaskJacobian(const Vector3r & handleOrientation){

    MatrixXr jacobian;
    jacobian = robot->getJacobian(kGeometricJacobian, bodyId, handleId);

    MatrixXr J_g_w(3,jacobian.cols());
    J_g_w = jacobian.block(3,0,3, jacobian.cols());

    Matrix3Xr J_w = geometricToAnalytical(J_g_w,handleOrientation);

    //We have the geometric jacobian "jacobian" and the angular part of the analitical jacobian now we have to build the matrix.

    int i = 0;
    if(taskSpace & kX){
        taskJacobian.row(i) =jacobian.row(0);
        i++;
    }
    if(taskSpace & kY){
        taskJacobian.row(i) =jacobian.row(1);
        i++;
    }
    if(taskSpace & kZ){
        taskJacobian.row(i) =jacobian.row(2);
        i++;
    }
    if(taskSpace & kWX){
        taskJacobian.row(i) =J_w.row(0);
        i++;
    }
    if(taskSpace & kWY){
        taskJacobian.row(i) =J_w.row(1);
        i++;
    }
    if(taskSpace & kWZ){
        taskJacobian.row(i) =J_w.row(2);
    }


    //taskJacobian.block(3,0,6,taskJacobian.cols()) = J_w ;
}

void KinematicTask::computeTaskError(const Vector3r & handlePosition, const Vector3r & handleOrientation, const hr::real_t & time){
    SpatialVector error;
    if( taskType == kRegulationTask){
        error.head(3) = desiredValue.head(3) - handlePosition;
        error.tail(3) = desiredValue.tail(3) - handleOrientation;
        if(gainType == kConstantGain){
            error = taskGainMatrix * error;
        }
        else if(gainType == kSmoothGain){
            error = (*smoothGain)(time) * error;

        }
    }
    else if (taskType == kTrackingTask){

        error.head(3) = taskTrajectory->getPose(time).head(3) - handlePosition;
        error.tail(3) = taskTrajectory->getPose(time).tail(3) - handleOrientation;
        if(gainType == kConstantGain){
            error = taskGainMatrix*error+taskTrajectory->getVelocity(time);
        }
        else if(gainType == kSmoothGain){
            error = (*smoothGain)(time) * error + taskTrajectory->getVelocity(time);
        }
    }
    int i = 0;
    if(taskSpace & kX){
        taskError(i) = error(0);
        i++;
    }
    if(taskSpace & kY){
        taskError(i) = error(1);
        i++;
    }
    if(taskSpace & kZ){
        taskError(i) = error(2);
        i++;
    }
    if(taskSpace & kWX){
        taskError(i) = error(3);
        i++;
    }
    if(taskSpace & kWY){
        taskError(i) = error(4);
        i++;
    }
    if(taskSpace & kWZ){
        taskError(i) = error(5);
    }
    //std::cout << "task error " << taskError.transpose() << std::endl;

}

void KinematicTask::update(){
    update(0.0);
}

void KinematicTask::update(const hr::real_t & time){

    handlePosition = robot->getOperationalHandlePosition(bodyId,handleId);
    handleOrientation = robot->getOperationalHandleOrientation(bodyId,handleId).eulerAngles(
                eulerConvention(0),
                eulerConvention(1),
                eulerConvention(2));

    computeTaskJacobian(handleOrientation);
    computeTaskError(handlePosition, handleOrientation, time);
}

void KinematicTask::setGoal(const SpatialVector & taskGoal){
    taskType = kRegulationTask;
    desiredValue = taskGoal;
}

void KinematicTask::setTrajectory(boost::shared_ptr< TrajectoryAbstract > trajectory){
    taskType = kTrackingTask;
    taskTrajectory = trajectory;
}


void KinematicTask::setSmoothGain(boost::shared_ptr< GainAbstract > gain){
    gainType = kSmoothGain;
    smoothGain = gain;
}

void KinematicTask::setTaskSpace(const int &taskSpace){
    this->taskSpace = taskSpace;
    int m = 0;
    if(taskSpace & kX){
        m++;
    }
    if(taskSpace & kY){
        m++;
    }
    if(taskSpace & kZ){
        m++;
    }
    if(taskSpace & kWX){
        m++;
    }
    if(taskSpace & kWY){
        m++;
    }
    if(taskSpace & kWZ){
        m++;
    }
    taskError.resize(m);
    taskJacobian.resize(m,robot->getDoF() - 1 );  //Nao is DoF - 1
    taskDimension = m;
    update();
}


} // end of namespace core
} // end of namespace hr

