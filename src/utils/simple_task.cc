/*
 *
 * Copyright (C) 2015
 * Julio Jarquin <jjarquin.ct@gmail.com>, Gustavo Arechavaleta <garechav@cinvestav.edu.mx>
 * CINVESTAV - Saltillo Campus
 *
 * This file is part of OpenHRC
 * OpenHRC is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * OpenHRC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/**
 *	\file src/utils/simple_kinematic_task.cc
 *	\author Julio Jarquin, Gustavo Arechavaleta
 *	\version 1.0
 *	\date 2015
 *
 *	Implementation of a SimpleKinematicTask class.
 */

#include "openhrc/utils/simple_task.h"
#include "assert.h"

namespace hr{
namespace utils{

SimpleTask::SimpleTask()
{
    empty = true;
    constraintsEmpty = true;
}

void SimpleTask::addRows(const MatrixXr & rowsA, const VectorXr & scalarsb){
    assert(rowsA.rows() == scalarsb.rows());
    if(empty){
        A = rowsA;
        b = scalarsb;
        empty = false;
    }
    else{
    assert(rowsA.cols() == A.cols());
    A.conservativeResize(A.rows()+rowsA.rows(),A.cols());
    b.conservativeResize(b.rows()+scalarsb.rows());
    A.block(A.rows()-rowsA.rows(),0,rowsA.rows(),rowsA.cols()) = rowsA;
    b.segment(b.rows()-scalarsb.rows(),scalarsb.rows()) = scalarsb;
    }
}

void SimpleTask::addConstraintRows(const MatrixXr &rowsC, const VectorXr &scalarsd){
    assert(rowsC.rows() == scalarsd.rows());
    if(constraintsEmpty){
        C = rowsC;
        d = scalarsd;
        constraintsEmpty = false;
    }
    else{
    assert(rowsC.cols() == C.cols());
    C.conservativeResize(C.rows()+rowsC.rows(),C.cols());
    d.conservativeResize(d.rows()+scalarsd.rows());
    C.block(C.rows()-rowsC.rows(),0,rowsC.rows(),rowsC.cols()) = rowsC;
    d.segment(d.rows()-scalarsd.rows(),scalarsd.rows()) = scalarsd;
    }
}



} //end of namespace utils
} //end of namespace hr
