#include "openhrc/utils/convex_hierarchical_solver.h"
#include <vector>
#include "qpOASES.hpp"
#include <iostream>
namespace hr{
namespace utils{

ConstrainedHierarchicalSolver::ConstrainedHierarchicalSolver()
{
}



VectorXr ConstrainedHierarchicalSolver::Solve( std::vector<TaskAbstract*> taskList, const MatrixXr  & C, const VectorXr & ld, const VectorXr & ud, const VectorXr & lb, const VectorXr & ub, const hr::real_t & eps){
    int n = C.cols();
    qpOASES::Options myOptions;
    myOptions.setToMPC();
    myOptions.printLevel = qpOASES::PL_NONE;
    myOptions.enableRegularisation = qpOASES::BT_TRUE;
    myOptions.epsRegularisation =  eps;  //
    qpOASES::QProblem qp;

    VectorXr dq(n);
    dq.setZero();
    std::vector<TaskAbstract*>::iterator taskIt;

    qpOASES::real_t xOpt[29];  //size is nV          primal Solution

    VectorXr ld_ = ld;
    VectorXr ud_ = ud;
    MatrixXr C_ = C;

    VectorXr w; //slack vector
    int m = 0, mi = 0;  //constraints
    m = n; //initial constraints are equal to the velocity constraints  (n constraints)


    for(taskIt = taskList.begin(); taskIt != taskList.end() ; ++taskIt){

        MatrixXr J = (*taskIt)->getMatrixA();
        VectorXr e = (*taskIt)->getVectorb();

        MatrixXr H = J.transpose()*J;
        VectorXr F = -J.transpose()*e;

        if((*taskIt)->constrained()){
            MatrixXr constraintsMatrix = (*taskIt)->getMatrixC();
            VectorXr constraintsVector = (*taskIt)->getVectord();
            VectorXr lowerConstraintsVector;
            lowerConstraintsVector.resize(constraintsVector.rows());
            lowerConstraintsVector.setOnes();
            lowerConstraintsVector *= -10000;

            int m_constraints = constraintsMatrix.rows();
            m += m_constraints;

            C_.conservativeResize(m,n);
            C_.block(m-m_constraints,0,m_constraints,n) = constraintsMatrix;

            ld_.conservativeResize(m);
            ud_.conservativeResize(m);

            ld_.segment(m-m_constraints,m_constraints) = lowerConstraintsVector;


            ud_.segment(m-m_constraints,m_constraints) = constraintsVector;

        }

        mi = e.rows();


        int nV = n; //Number of variables
        int nC = m; //Number of constraints
        int nWSR = 32;

        qp = qpOASES::QProblem(nV,nC,qpOASES::HST_SEMIDEF);
        qp.setOptions( myOptions );

        qp.init(H.data(),F.data(),C_.data(),lb.data(),ub.data(), ld_.data(), ud_.data(),nWSR,0,dq.data());
        qp.getPrimalSolution( xOpt );


        VectorXr dqt = Eigen::Map<VectorXr>( xOpt, 29, 1 );  //Map the solution vector
        dq = VectorXr(dqt);

        w = J*dq-e;  //slack 1   wk-1

        m += mi; //Number of constraints increases as new tasks are added

        C_.conservativeResize(m,n);
        C_.block(m-mi,0,mi,n) = J;

        //Update constraints
        ld_.conservativeResize(m);
        ud_.conservativeResize(m);
        ld_.segment(m-mi,mi) = e + w;
        ud_.segment(m-mi,mi) = e + w;

    }

    return dq;
}

VectorXr ConstrainedHierarchicalSolver::Solve( std::vector<TaskAbstract*> taskList,  VectorXr & lb,  VectorXr & ub, const hr::real_t & eps){
    int n = ub.rows();
    qpOASES::Options myOptions;
    myOptions.setToMPC();
    myOptions.printLevel = qpOASES::PL_NONE;
    myOptions.enableRegularisation = qpOASES::BT_TRUE;
    myOptions.epsRegularisation =  eps;  //
    qpOASES::QProblem qp;

    VectorXr dq(n);
    dq.setZero();
    std::vector<TaskAbstract*>::iterator taskIt;

    qpOASES::real_t xOpt[29];  //size is nV          primal Solution

    VectorXr ld_;
    VectorXr ud_;
    MatrixXr C_;

    VectorXr w; //slack vector
    int m = 0, mi = 0;  //constraints
    m = 0; //initial constraints are zero


    for(taskIt = taskList.begin(); taskIt != taskList.end() ; ++taskIt){

        MatrixXr J = (*taskIt)->getMatrixA();
        VectorXr e = (*taskIt)->getVectorb();

        MatrixXr H = J.transpose()*J;
        VectorXr F = -J.transpose()*e;

        if((*taskIt)->constrained()){
            MatrixXr constraintsMatrix = (*taskIt)->getMatrixC();
            VectorXr constraintsVector = (*taskIt)->getVectord();
            VectorXr lowerConstraintsVector;
            lowerConstraintsVector.resize(constraintsVector.rows());
            lowerConstraintsVector.setOnes();
            lowerConstraintsVector *= -10000;

            int m_constraints = constraintsMatrix.rows();
            m += m_constraints;

            C_.conservativeResize(m,n);
            C_.block(m-m_constraints,0,m_constraints,n) = constraintsMatrix;

            ld_.conservativeResize(m);
            ud_.conservativeResize(m);

            ld_.segment(m-m_constraints,m_constraints) = lowerConstraintsVector;
            ud_.segment(m-m_constraints,m_constraints) = constraintsVector;
        }

        mi = e.rows();


        int nV = n; //Number of variables
        int nC = m; //Number of constraints
        int nWSR = 32;

        qp = qpOASES::QProblem(nV,nC,qpOASES::HST_SEMIDEF);
        qp.setOptions( myOptions );

        qp.init(H.data(),F.data(),C_.data(),lb.data(),ub.data(), ld_.data(), ud_.data(),nWSR,0,dq.data());
        qp.getPrimalSolution( xOpt );


        VectorXr dqt = Eigen::Map<VectorXr>( xOpt, 29, 1 );  //Map the solution vector
        dq = VectorXr(dqt);

        w = J*dq-e;  //slack 1   wk-1

        m += mi; //Number of constraints increases as new tasks are added

        C_.conservativeResize(m,n);
        C_.block(m-mi,0,mi,n) = J;

        //Update constraints
        ld_.conservativeResize(m);
        ud_.conservativeResize(m);
        ld_.segment(m-mi,mi) = e + w;
        ud_.segment(m-mi,mi) = e + w;

    }

    return dq;
}

} // end of namespace utils
} // end of namespace hr


