
#include "openhrc/utils/simple_hierarchical_solver.h"
#include <vector>
#include "qpOASES.hpp"

namespace hr{
namespace utils{

SimpleHierarchicalSolver::SimpleHierarchicalSolver()
{

}

VectorXr SimpleHierarchicalSolver::Solve( std::vector<TaskAbstract*> taskList, MatrixXr  & C , VectorXr & ld ,VectorXr & ud ,  VectorXr & lb,  VectorXr & ub){
    int current_task_number = -1;
    int n = C.cols();
    qpOASES::Options myOptions;
    myOptions.setToMPC();
    myOptions.printLevel = qpOASES::PL_NONE;
    myOptions.enableRegularisation = qpOASES::BT_TRUE;
    myOptions.epsRegularisation = 0.000001; // Perfect tracking, good performance with no kinematic singularities
   // myOptions.epsRegularisation = 0.000001; // Perfect tracking, good performance with no kinematic singularities
    //myOptions.epsRegularisation = 0.001; // Smooth trajectory, good performance upon kinematic singularities
    qpOASES::QProblem qp;

    VectorXr dq(n);
    dq.setZero();
    std::vector<TaskAbstract*>::iterator taskIt;

    qpOASES::real_t xOpt[29];  //size is nV          primal Solution


    VectorXr w; //slack vector
    int m = 0, mi = 0;  //constraints
    m = n; //initial constraints are equal to the velocity constraints  (n constraints)


    for(taskIt = taskList.begin(); taskIt != taskList.end() ; ++taskIt){

        MatrixXr J = (*taskIt)->getMatrixA();
        VectorXr e = (*taskIt)->getVectorb();

        MatrixXr H = J.transpose()*J;
        VectorXr F = -J.transpose()*e;

        mi = e.rows();


        int nV = n; //Number of variables
        int nC = m; //Number of constraints
        int nWSR = 100;

        qp = qpOASES::QProblem(nV,nC,qpOASES::HST_SEMIDEF);

//        //! eps regularization
//        current_task_number++;
//        if(current_task_number == task_for_max_eps){
//            myOptions.epsRegularisation = 0.001; // Smooth trajectory, good performance upon kinematic singularities
//        }
//        else{
//            myOptions.epsRegularisation = 0.000001; // Perfect tracking, good performance with no kinematic singularities
//        }

        qp.setOptions( myOptions );

        qp.init(H.data(),F.data(),C.data(),lb.data(),ub.data(),ld.data(),ud.data(),nWSR,0,dq.data());

        qp.getPrimalSolution( xOpt );


        VectorXr dqt = Eigen::Map<VectorXr>( xOpt, 29, 1 );  //Map the solution vector
        dq = VectorXr(dqt);

        w = J*dq-e;  //slack 1   wk-1

        m += mi; //Number of constraints increases as new tasks are added

        C.conservativeResize(m,n);
        C.block(m-mi,0,mi,n) = J;

        //Update constraints
        ld.conservativeResize(m);
        ud.conservativeResize(m);
        ld.segment(m-mi,mi) = e + w;
        ud.segment(m-mi,mi) = e + w;

    }

    return dq;
}

}
}
