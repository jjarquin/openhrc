/*
 *
 * Copyright (C) 2015
 * Julio Jarquin <jjarquin.ct@gmail.com>, Gustavo Arechavaleta <garechav@cinvestav.edu.mx>
 * CINVESTAV - Saltillo Campus
 *
 * This file is part of OpenHRC
 * OpenHRC is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * OpenHRC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/**
 *	\file src/vision/vmpc_constraints.cc
 *	\author Julio Jarquin, Gustavo Arechavaleta
 *	\version 1.0
 *	\date 2015
 *
 *	Implementation of the VMPCConstraints class.
 *  The VMPCConstraints class holds and initialize the constraints for the VMPC
 */

#include "openhrc/vision/vmpc_constraints.h"

namespace hr{
namespace vision{

void VMPCConstraints::init(const VMPCData &vmpcData){
    CMatrix.setZero(2*vmpcData.n_features*vmpcData.N_horizon,vmpcData.n_dof*vmpcData.N_horizon);
    dVector.setZero(2*vmpcData.n_features*vmpcData.N_horizon);
    uB.setZero(vmpcData.n_features);
    lB.setZero(vmpcData.n_features);
    for(int k = 0 ; k != vmpcData.n_features ; k = k + 2 ){
        uB(k) = kMaxX;
        uB(k+1) = kMaxY;
        lB(k) = kMinX;
        lB(k+1) = kMinY;
    }
}

void VMPCConstraints::setConstraints(const VMPCData & vmpcData, const VectorXr &currentFeatures){
    CMatrix << vmpcData.B_sys , - vmpcData.B_sys;
    dVector << vmpcData.A_sys*uB - vmpcData.A_sys*currentFeatures , - vmpcData.A_sys*uB + vmpcData.A_sys*currentFeatures;

}

}
}


