/*
 *
 * Copyright (C) 2015
 * Julio Jarquin <jjarquin.ct@gmail.com>, Gustavo Arechavaleta <garechav@cinvestav.edu.mx>
 * CINVESTAV - Saltillo Campus
 *
 * This file is part of OpenHRC
 * OpenHRC is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * OpenHRC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/**
 *	\file src/vision/vmpc_data.cc
 *	\author Julio Jarquin, Gustavo Arechavaleta
 *	\version 1.0
 *	\date 2015
 *
 *	Implementation of the VMPCData class.
 *  The VMPCData class holds and initialize the Hessian matrix and the vector matrix
 *  associated with the quadratic program.
 */


#include "openhrc/vision/vmpc_data.h"
#include <iostream>
namespace hr{
namespace vision{

//VMPCData& VMPCData::operator =(const VMPC Data & other){

//    return *this;
//}


void VMPCData::init(const hr::MatrixX6r & interactionMatrix){

    B_sys.setZero(n_features*N_horizon,n_dof*N_horizon);
    A_sys.setZero(N_horizon*n_features,n_features);
    for(int i = 0 ; i != N_horizon ; i++){ //vertical
        for(int k = 0 ; k!= i+1 ; k++){  //horizontal
            B_sys.block(i*n_features,k*n_dof,n_features,n_dof) = T_s*interactionMatrix;
            //B_sys.block<8,6>(i,k) = T_s*interactionMatrix; //hard coded but improved performance
        }
        A_sys.block(i*n_features,0,n_features,n_features) = hr::MatrixXr::Identity(n_features,n_features);
    }
    H = beta*(B_sys.transpose()*B_sys)+alpha*hr::MatrixXr::Identity(n_dof*N_horizon,n_dof*N_horizon);
}

void VMPCData::setInteractionMatrix(const hr::MatrixX6r interactionMatrix){
    B_sys.setZero(n_features*N_horizon,n_dof*N_horizon);

    for(int i = 0 ; i != N_horizon ; i++){ //vertical
        for(int k = 0 ; k!= i+1 ; k++){  //horizontal
            B_sys.block(i*n_features,k*n_dof,n_features,n_dof) = T_s*interactionMatrix;
        }
    }
    H = beta*(B_sys.transpose()*B_sys)+alpha*hr::MatrixXr::Identity(n_dof*N_horizon,n_dof*N_horizon);
}

VectorXr VMPCData::getFvector(const hr::VectorXr & currentFeatures,const  hr::VectorXr & desiredFeatures){
    assert(desiredFeatures.rows() == currentFeatures.rows()*N_horizon);
    f = beta*B_sys.transpose()*(A_sys*currentFeatures-desiredFeatures);
    return f;
}


} //end of namespace locomotion
} //end of namespace hr




