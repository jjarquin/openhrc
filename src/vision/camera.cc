#include "openhrc/vision/camera.h"
#include <Eigen/LU>

#include <iostream>

namespace hr{
  namespace vision{

    Camera::Camera(const Matrix3r & cameraMatrix, const int & resolution) : kMatrix(cameraMatrix){
      kInverseMatrix = kMatrix.inverse();
    }

    Camera::Camera(const real_t & focalLength, const real_t & pixelSize_x, const real_t & pixelSize_y, const real_t & opticalCenter_x, const real_t & opticalCenter_y, const int & resolution){
      kMatrix = hr::Matrix3r::Identity();
    }

    bool Camera::projectPoints(const VectorXr & srcPoints, VectorXr &  destPoints, const int &  pointType){
      assert(srcPoints.rows() == destPoints.rows());
      assert(pointType == k3DPoint | pointType == k4DPoint);
      switch(pointType){

        case k3DPoint:
          for(int k = 0; k!= srcPoints.rows(); k = k + 3){
              destPoints.segment<3>(k) = kMatrix * srcPoints.segment<3>(k);
            }
          return true;
        case k4DPoint:
          for(int k = 0; k!= srcPoints.rows(); k = k + 4){
              destPoints.segment<3>(k) = kMatrix * srcPoints.segment<3>(k);
              destPoints(k+3) = srcPoints(k+3);
            }
          return true;
        default:
          return false;

        }
    }

    bool Camera::convertPoints(const VectorXr & srcPoints, VectorXr &  destPoints, const int & conversionType, const bool & maskPoints, const int & pointType){
      //this code must be optimized,
      // select transform matrix | direct or inverse
      // select maskPoint matrix
      // apply matrix to every point

      switch(conversionType){
        case kPixel2ImageCoordinates:
          if(pointType == k3DPoint){
              assert(srcPoints.rows()*3 == destPoints.rows()*2);
              Vector3r temp;
              for(int k = 0; k!= srcPoints.rows()/2; k++){
                  temp << srcPoints.segment<2>(2*k) , 1;
                  //destPoints.segment<3>(3*k) = kMatrix.lu().solve(temp) ;
                  destPoints.segment<3>(3*k) = kInverseMatrix * temp ;
                }
              return true;
            }
          else if(pointType == k2DPoint){
              assert(srcPoints.rows() == destPoints.rows());
              Vector3r temp;
              for(int k = 0; k!= srcPoints.rows()/2; k++){
                  temp << srcPoints.segment<2>(2*k) , 1;
                  destPoints.segment<2>(2*k) = (kInverseMatrix*temp).head<2>() ;
                }

              return true;

            }

        case kImage2PixelCoordinates:
          return true;
        default:
          return false;

        }
    }

    bool Camera::projectPoint(const Vector3r & srcPoint, Vector3r &  destPoint){
      destPoint = kMatrix * srcPoint;
      return true;
    }


    bool Camera::projectPoint(const Vector4r & srcPoint, Vector4r &  destPoint){
      destPoint.head<3>() = kMatrix * srcPoint.head<3>();
      destPoint(3) = srcPoint(3);
      return true;
    }

    MatrixX6r Camera::getInteractionMatrix(const VectorXr & points, const int & pointsCoordinates, const int & pointType, const real_t & z_parameter){
      MatrixX6r Ls;
      Ls.resize(points.rows(),6);

      real_t x;
      real_t y;
      real_t z;
      for(int k = 0; k != points.rows() ; k = k + pointType){
          x = points(k);
          y = points(k+1);
          if(pointType == k3DPoint)
            z = points(k+2);
          else
            z = z_parameter;

          Ls.block<2,6>(k,0) << -1/z, 0, x/z, x*y, -(1+x*x), y, 0, -1/z, y/z, 1+y*y, -x*y, -x;
        }
      return Ls;

    }

  } // end of namespace hr
} // end of namespace vision
