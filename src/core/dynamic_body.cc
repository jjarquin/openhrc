/*
 *
 * Copyright (C) 2015
 * Julio Jarquin <jjarquin.ct@gmail.com>, Gustavo Arechavaleta <garechav@cinvestav.edu.mx> , Gerardo Jarquin
 * CINVESTAV - Saltillo Campus
 *
 * This file is part of OpenHRC
 * OpenHRC is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * OpenHRC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/**
 *	\file src/core/dynamic_body.cc
 *	\author Gerardo Jarquin, Gustavo Arechavaleta, Julio Jarquin.
 *	\version 1.0
 *	\date 2015
 *
 * DynamicBody class. It has functions to build a body from a xml file. It is a derived class from body.
 */
#include "openhrc/core/dynamic_body.h"
#include "PQP/PQP.h"
#include "openhrc/core/markup.h"

namespace hr{
namespace core{

DynamicBody::DynamicBody( const unsigned int id, const std::string &name ):
    Body( id, name )
{
    mass = 0.0;
    centerOfMass.setZero();
    inertiaTensor.setIdentity();
    spatialInertia.setIdentity();
    twist.setZero();
    wrench.setZero();
}

DynamicBody::DynamicBody( const unsigned int id, const std::string &name,
                          const Vector3r &pos, const Matrix3r &rot ):
    Body( id, name, pos, rot )
{
    mass = 0.0;
    centerOfMass.setZero();
    inertiaTensor.setIdentity();
    spatialInertia.setIdentity();
    twist.setZero();
    wrench.setZero();
}

bool DynamicBody::loadGeometry( const std::string &xmlFileName, Vector3r &relativeBodyPosition)
{
    //std::cout << "onDynamicBody" << std::endl;
    CMarkup xmlFile;

    if ( xmlFile.Load(xmlFileName) ) {
        //qDebug()<<"File"<<xmlFileName.data()<<"found";
        bool bodyShapeFound = false;
        bool transformFound;

        //qDebug()<<"The geometry of the body"<<this->name.data()<<"will be searched in the file"<<xmlFileName.data();
        transformFound = xmlFile.FindElemByPath("vrml/Transform");
        while(transformFound) {
            std::string geometryName( xmlFile.GetAttrib("DEFName") );
            //qDebug()<<"A body named"<<geometryName.data()<<"was found";
            if ( geometryName == this->frame.getName() ) {
                //qDebug()<<"The required geometry was found";
                CMarkup xmlShapeAttrib;
                CMarkup xmlAppearence, xmlCoordinates;
                bool rotFlag, geomRotFlag, transFlag, scaleFlag, centerFlag, geomCenterFlag, auxFlag;
                bool shapeAttribFound, autoshapeAttribFound = false;
                Eigen::Vector3f geomCenter;
                Eigen::Vector3f autoShapeTranslation;
                Eigen::Vector3f center;
                Eigen::Vector3f geomScale;
                Eigen::AngleAxisf geomRotation;
                Eigen::AngleAxisf rotation;
                std::string attributeValue;
                GeometryPiece* bodyShapePiece;
                int shapesCounter = 0;



                centerOfMass = stringToVector3r( transFlag, xmlFile.GetAttrib("com")," " );

                std::string sAttrib = xmlFile.GetAttrib("mass");
                mass =  (hr::real_t) atof( sAttrib.c_str() );
                //std::cout << "Mass " << mass << std::endl;
                //std::cout << "CoM " << centerOfMass.transpose() << std::endl;

                bodyShapeFound = true;
                transformFound = false;
                relativeBodyPosition = stringToVector3r( transFlag, xmlFile.GetAttrib("translation")," " );
                geomScale = stringToVector3f(scaleFlag, xmlFile.GetAttrib("scale")," " );
                geomCenter = stringToVector3f(geomCenterFlag, xmlFile.GetAttrib("center")," " );
                geomRotation = stringToAngleAxisf(geomRotFlag, xmlFile.GetAttrib("rotation"), " ");
                //copy the position of xmlFile in xmlShape
                xmlShapeAttrib.SetDoc(xmlFile.GetElemContent());
                shapeAttribFound = xmlShapeAttrib.FindElem("Shape");
                if (!shapeAttribFound) {
                    xmlShapeAttrib.ResetPos();
                    shapeAttribFound = xmlShapeAttrib.FindElemByPath("children/Shape");
                    if (!shapeAttribFound) {
                        //If the first child of the transform is not a Shape, then an AutoShape is expected.
                        //It is necessary to check correct performance of the following two lines.
                        xmlShapeAttrib.ResetPos();
                        autoshapeAttribFound = xmlShapeAttrib.FindElemByPath("children/AutoShape");
                    }
                }
                //qDebug()<<"A new shape was found? "<<gFound;
                while(shapeAttribFound || autoshapeAttribFound) {
                    shapesCounter++;
                    bodyShapePiece = new GeometryPiece();
                    //Capture the color of the piece
                    xmlAppearence.SetDoc(xmlShapeAttrib.GetElemContent());
                    xmlAppearence.FindElemByPath("Appearance/Material");
                    bodyShapePiece->colorRGB = stringToVector3f( auxFlag, xmlAppearence.GetAttrib("diffuseColor")," " );
                    //Capture the points of the piece
                    xmlCoordinates.SetDoc(xmlShapeAttrib.GetElemContent());
                    if ( xmlCoordinates.FindElemByPath("IndexedFaceSet/Coordinate/point") ) {
                        attributeValue = xmlCoordinates.GetAttrib("value");
                        if ( shapeAttribFound )
                            bodyShapePiece->setVertices(attributeValue, geomRotFlag, geomRotation, scaleFlag, geomScale, geomCenterFlag, geomCenter, false, autoShapeTranslation);
                        else if ( autoshapeAttribFound ) {
                            autoShapeTranslation = stringToVector3f( transFlag, xmlShapeAttrib.GetAttrib("translation")," " );
                            center = stringToVector3f( centerFlag, xmlShapeAttrib.GetAttrib("center")," " );
                            rotation = stringToAngleAxisf(rotFlag, xmlShapeAttrib.GetAttrib("rotation"), " ");
                            if ( geomCenterFlag )
                                autoShapeTranslation += geomCenter;
                            bodyShapePiece->setVertices(attributeValue, rotFlag, rotation, scaleFlag, geomScale, centerFlag, center, transFlag, autoShapeTranslation);
                        }
                    }
                    //qDebug()<<"The number of points of this piece is "<<bodyShapePiece->points.size();  qDebug();
                    xmlCoordinates.ResetPos();
                    if ( xmlCoordinates.FindElemByPath("IndexedFaceSet/coordIndex") ) {
                        attributeValue = xmlCoordinates.GetAttrib("value");
                        bodyShapePiece->setTriangles(attributeValue);
                    }
                    shape.geomPieces.push_back( bodyShapePiece );
                    shapeAttribFound = xmlShapeAttrib.FindElem("Shape");
                    if (!shapeAttribFound) {
                        autoshapeAttribFound = xmlShapeAttrib.FindElem("AutoShape");
                        //if (shFound)
                        //qDebug()<<"A new Autoshape was found? "<<shFound;
                    }
                    //qDebug()<<"A new shape was found? "<<gFound;
                }
                if (shapesCounter == 0) {
                    //qDebug()<<"No shape attribute was found in the xml file for filling the geometry of this body";
                    return false;
                }
                return true;
            }
            else
                transformFound = xmlFile.FindElem( "Transform" );
        }
        if ( !bodyShapeFound ) {
            //qDebug()<<"The body geometry for the body"<<this->name.data()<<"was not found in the xml file";
            return false;
        }
    }
    else {
        //qDebug()<<"File"<<xmlFileName.data()<<"not found";
        return false;
    }
    return true;
}

void DynamicBody::setCoM( const Vector3r &centerOfMass )
{
    this->centerOfMass = centerOfMass;
}

void DynamicBody::setInertiaTensor( const Matrix3r &inertiaTensor )
{
    this->inertiaTensor = inertiaTensor;
}

inline void DynamicBody::setSpatialInertia( const SpatialMatrix &spatialInertia )
{
    this->spatialInertia = spatialInertia;
}

inline void DynamicBody::setTwist( const SpatialVector &twist )
{
    this->twist = twist;
}

inline void DynamicBody::setWrench( const SpatialVector &wrench )
{
    this->wrench = wrench;
}

DynamicBody::~DynamicBody()
{
    delete &mass;
    delete &centerOfMass;
    delete &inertiaTensor;
    delete &spatialInertia;
    delete &twist;
    delete &wrench;
}
} // end of namespace core
} // end of namespace hr
