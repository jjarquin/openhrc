/*
 *
 * Copyright (C) 2015
 * Julio Jarquin <jjarquin.ct@gmail.com>, Gustavo Arechavaleta <garechav@cinvestav.edu.mx> , Gerardo Jarquin
 * CINVESTAV - Saltillo Campus
 *
 * This file is part of OpenHRC
 * OpenHRC is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * OpenHRC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/**
 *	\file src/core/geometry.cc
 *	\author Gerardo Jarquin, Gustavo Arechavaleta, Julio Jarquin.
 *	\version 1.0
 *	\date 2015
 *
 * Geometry classes implementation (Triangle, Geometry Piece, Geometry).
 */

#include "openhrc/core/geometry.h"
#include <math.h>

namespace hr
{
namespace core{

void GeometryPiece::setVertices(std::string verticesString,
                                bool rotFlag, const Eigen::AngleAxisf &rotation,
                                bool scaleFlag, const Eigen::Vector3f &scale,
                                bool centerFlag, const Eigen::Vector3f &center,
                                bool transFlag, const Eigen::Vector3f &translation)
{
    Eigen::Vector3f vertex;
    Eigen::Matrix3f rotationMatrix;
    std::vector<std::string> results;
    int verticesNumber = Split(verticesString,",", results, false);
    bool status;

    for (int j=0; j<verticesNumber; j++) {
        vertex = stringToVector3f( status, results.at(j)," " );
        //Scale the vertex
        if ( scaleFlag ) {
            for (short int i=0; i<3; i++)
                vertex(i) = vertex(i)*scale(i);
        }
        //Translate the vertex to center it at [0,0,0]
        if ( centerFlag )
            vertex -= center;
        //Rotate the vertex
        if ( rotFlag ){
            rotationMatrix = rotation.toRotationMatrix();
            vertex = rotationMatrix*vertex;
        }
        //Translate the vertex inside the Geometry
        if ( transFlag )
            vertex += translation;
        //Store the vertex
        vertices.push_back( vertex );
    }
}

void GeometryPiece::setTriangles(std::string vertexIndicesString)
{
    std::vector<std::string> surfacesVector;
    std::vector<std::string> surfaceVertexIndices;
    int valuesNum;
    Triangle triangle;
    Eigen::Vector3f vertexA, vertexB, vertexC;

    int coordIndicesNum = Split(vertexIndicesString,"-1,", surfacesVector, false);
    for (int i=0; i<coordIndicesNum; i++) {
        valuesNum = stringToVector(surfaceVertexIndices, surfacesVector.at(i), ",");
        //If the last element of "surfaceVertexIndices" is -1, it is eliminated
        if ( i == (coordIndicesNum-1) ) {
            if ( atoi( surfaceVertexIndices.back().c_str() ) == -1) {
                surfaceVertexIndices.pop_back();
                valuesNum = surfaceVertexIndices.size();
            }
        }
        if (valuesNum == 3) {
            vertexA = vertices.at( atoi(surfaceVertexIndices.at(0).c_str()) );
            vertexB = vertices.at( atoi(surfaceVertexIndices.at(1).c_str()) );
            vertexC = vertices.at( atoi(surfaceVertexIndices.at(2).c_str()) );
            triangle.setVertices( vertexA, vertexB, vertexC);
            triangles.push_back( triangle );
        }
        else if(valuesNum == 4) {
            vertexA = vertices.at( atoi(surfaceVertexIndices.at(0).c_str()) );
            vertexB = vertices.at( atoi(surfaceVertexIndices.at(1).c_str()) );
            vertexC = vertices.at( atoi(surfaceVertexIndices.at(2).c_str()) );
            triangle.setVertices( vertexA, vertexB, vertexC);
            triangles.push_back( triangle );
            vertexB = vertices.at( atoi(surfaceVertexIndices.at(3).c_str()) );
            triangle.setVertices( vertexA, vertexC, vertexB );
            triangles.push_back( triangle );
        }
        surfaceVertexIndices.clear();
    }
}


// -------------------------------------------------------- Tringle class implementation ---------------------------------------------------------


void Triangle::setVertices(const Eigen::Vector3f &vertexA, const Eigen::Vector3f &vertexB, const Eigen::Vector3f &vertexC)
{
    Eigen::Vector3f v1;
    Eigen::Vector3f v2;

    this->vertexA = vertexA;
    this->vertexB = vertexB;
    this->vertexC = vertexC;
    v1 = vertexB - vertexA;
    v2 = vertexC - vertexA;
    this->normal = v1.cross(v2);
    this->normal.normalize();
}
} // end of namespace core
} // end of namespace hr
