/*
 *
 * Copyright (C) 2015
 * Julio Jarquin <jjarquin.ct@gmail.com>, Gustavo Arechavaleta <garechav@cinvestav.edu.mx> , Gerardo Jarquin
 * CINVESTAV - Saltillo Campus
 *
 * This file is part of OpenHRC
 * OpenHRC is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * OpenHRC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/**
 *	\file src/core/body.cc
 *	\author Gerardo Jarquin, Gustavo Arechavaleta, Julio Jarquin.
 *	\version 1.0
 *	\date 2015
 *
 * Body class implementation. It has functions to build a body from a xml file.
 */

#include "openhrc/core/body.h"
#include "PQP/PQP.h"
#include "openhrc/core/markup.h"

namespace hr
{
namespace core{
/*! --------------------------------------------------------
     * Body class implementation
     * --------------------------------------------------------- */

  Body::Body(const unsigned int id, const std::string &name) : frame(id,name) {
    pqpBodyModel = 0;
}

Body::Body(const unsigned int id, const std::string &name,
           const Vector3r &pos, const Matrix3r &rot) : frame(id,name) {

    frame.setPosition(pos);
    frame.setOrientation(rot);
    pqpBodyModel = 0;
}

bool Body::loadGeometry( const std::string &xmlFileName, Vector3r &relativeBodyPosition)
{
    CMarkup xmlFile;

    if ( xmlFile.Load(xmlFileName) ) {
        //qDebug()<<"File"<<xmlFileName.data()<<"found";
        bool bodyShapeFound = false;
        bool transformFound;

        //qDebug()<<"The geometry of the body"<<this->name.data()<<"will be searched in the file"<<xmlFileName.data();
        transformFound = xmlFile.FindElemByPath("vrml/Transform");
        while(transformFound) {
            std::string geometryName( xmlFile.GetAttrib("DEFName") );
            //qDebug()<<"A body named"<<geometryName.data()<<"was found";
            if ( geometryName == this->frame.getName() ) {
                //qDebug()<<"The required geometry was found";
                CMarkup xmlShapeAttrib;
                CMarkup xmlAppearence, xmlCoordinates;
                bool rotFlag, geomRotFlag, transFlag, scaleFlag, centerFlag, geomCenterFlag, auxFlag;
                bool shapeAttribFound, autoshapeAttribFound = false;
                Eigen::Vector3f geomCenter;
                Eigen::Vector3f autoShapeTranslation;
                Eigen::Vector3f center;
                Eigen::Vector3f geomScale;
                Eigen::AngleAxisf geomRotation;
                Eigen::AngleAxisf rotation;
                std::string attributeValue;
                GeometryPiece* bodyShapePiece;
                int shapesCounter = 0;

//                hr::Vector3r centerOfMass;
//                hr::Matrix3r inertiaTensor;
//                inertiaTensor.setZero();
//                hr::real_t mass;

//                centerOfMass = stringToVector3r( transFlag, xmlFile.GetAttrib("com")," " );

//                std::string sAttrib = xmlFile.GetAttrib("mass");
//                mass =  (hr::real_t) atof( sAttrib.c_str() );
//                std::cout << "Mass " << mass << std::endl;
//                std::cout << "CoM " << centerOfMass.transpose() << std::endl;
//                std::cout << "Intertia "<< std::endl << inertiaTensor << std::endl;

                bodyShapeFound = true;
                transformFound = false;
                relativeBodyPosition = stringToVector3r( transFlag, xmlFile.GetAttrib("translation")," " );
                geomScale = stringToVector3f(scaleFlag, xmlFile.GetAttrib("scale")," " );
                geomCenter = stringToVector3f(geomCenterFlag, xmlFile.GetAttrib("center")," " );
                geomRotation = stringToAngleAxisf(geomRotFlag, xmlFile.GetAttrib("rotation"), " ");
                //copy the position of xmlFile in xmlShape
                xmlShapeAttrib.SetDoc(xmlFile.GetElemContent());
                shapeAttribFound = xmlShapeAttrib.FindElem("Shape");
                if (!shapeAttribFound) {
                    xmlShapeAttrib.ResetPos();
                    shapeAttribFound = xmlShapeAttrib.FindElemByPath("children/Shape");
                    if (!shapeAttribFound) {
                        //If the first child of the transform is not a Shape, then an AutoShape is expected.
                        //It is necessary to check correct performance of the following two lines.
                        xmlShapeAttrib.ResetPos();
                        autoshapeAttribFound = xmlShapeAttrib.FindElemByPath("children/AutoShape");
                    }
                }
                //qDebug()<<"A new shape was found? "<<gFound;
                while(shapeAttribFound || autoshapeAttribFound) {
                    shapesCounter++;
                    bodyShapePiece = new GeometryPiece();
                    //Capture the color of the piece
                    xmlAppearence.SetDoc(xmlShapeAttrib.GetElemContent());
                    xmlAppearence.FindElemByPath("Appearance/Material");
                    bodyShapePiece->colorRGB = stringToVector3f( auxFlag, xmlAppearence.GetAttrib("diffuseColor")," " );
                    //Capture the points of the piece
                    xmlCoordinates.SetDoc(xmlShapeAttrib.GetElemContent());
                    if ( xmlCoordinates.FindElemByPath("IndexedFaceSet/Coordinate/point") ) {
                        attributeValue = xmlCoordinates.GetAttrib("value");
                        if ( shapeAttribFound )
                            bodyShapePiece->setVertices(attributeValue, geomRotFlag, geomRotation, scaleFlag, geomScale, geomCenterFlag, geomCenter, false, autoShapeTranslation);
                        else if ( autoshapeAttribFound ) {
                            autoShapeTranslation = stringToVector3f( transFlag, xmlShapeAttrib.GetAttrib("translation")," " );
                            center = stringToVector3f( centerFlag, xmlShapeAttrib.GetAttrib("center")," " );
                            rotation = stringToAngleAxisf(rotFlag, xmlShapeAttrib.GetAttrib("rotation"), " ");
                            if ( geomCenterFlag )
                                autoShapeTranslation += geomCenter;
                            bodyShapePiece->setVertices(attributeValue, rotFlag, rotation, scaleFlag, geomScale, centerFlag, center, transFlag, autoShapeTranslation);
                        }
                    }
                    //qDebug()<<"The number of points of this piece is "<<bodyShapePiece->points.size();  qDebug();
                    xmlCoordinates.ResetPos();
                    if ( xmlCoordinates.FindElemByPath("IndexedFaceSet/coordIndex") ) {
                        attributeValue = xmlCoordinates.GetAttrib("value");
                        bodyShapePiece->setTriangles(attributeValue);
                    }
                    shape.geomPieces.push_back( bodyShapePiece );
                    shapeAttribFound = xmlShapeAttrib.FindElem("Shape");
                    if (!shapeAttribFound) {
                        autoshapeAttribFound = xmlShapeAttrib.FindElem("AutoShape");
                        //if (shFound)
                        //qDebug()<<"A new Autoshape was found? "<<shFound;
                    }
                    //qDebug()<<"A new shape was found? "<<gFound;
                }
                if (shapesCounter == 0) {
                    //qDebug()<<"No shape attribute was found in the xml file for filling the geometry of this body";
                    return false;
                }
                return true;
            }
            else
                transformFound = xmlFile.FindElem( "Transform" );
        }
        if ( !bodyShapeFound ) {
            //qDebug()<<"The body geometry for the body"<<this->name.data()<<"was not found in the xml file";
            return false;
        }
    }
    else {
        //qDebug()<<"File"<<xmlFileName.data()<<"not found";
        return false;
    }
    return true;
}

OperationalHandle* Body::getOperationalHandle(short int id)
{
    OperationalHandle *opHandleAux = 0;

    for( opHandles_iter = opHandles.begin(); opHandles_iter != opHandles.end(); opHandles_iter++ ) {
        if ( (*opHandles_iter)->getId() == id ) {
            opHandleAux = *opHandles_iter;
        }
    }
    return opHandleAux;
}

int Body::addOperationalHandle(const Vector3r &position)
{
    int id = opHandles.size();
    this->relativeHandlePosition.push_back(position);
    Vector3r handlePosition = this->frame.getPosition() + this->frame.getOrientation()*position;
    opHandles.push_back(new OperationalHandle(id,handlePosition));
    return id;
}

void Body::removeOperationalHandle(short int id)
{
    for( opHandles_iter = opHandles.begin(); opHandles_iter != opHandles.end(); opHandles_iter++ ) {
        if ( (*opHandles_iter)->getId() == id ) {
            opHandles.erase(opHandles_iter);
        }
    }
}

void Body::updateOperationalHandlesPose()
{
    for( opHandles_iter = opHandles.begin(); opHandles_iter != opHandles.end(); opHandles_iter++ ) {
        Vector3r handlePosition = this->frame.getPosition() +
                this->frame.getOrientation()*this->getRelativeHandlePosition((*opHandles_iter)->getId());
        (*opHandles_iter)->setPosition(handlePosition);
        Matrix3r handleOrientation = this->frame.getOrientation();
        (*opHandles_iter)->setOrientation(handleOrientation);
    }
}

bool Body::hasOperationalHandles()
{
    if( !opHandles.empty() ) return true;
    else return false;
}

void Body::createPQPBodyModel()
{
    PQP_REAL p1[3], p2[3], p3[3];
    GeometryPiece* geomPiece;
    Triangle* triangle;
    Eigen::Vector3f* point;

    pqpBodyModel = new PQP_Model();
    pqpBodyModel->BeginModel();
    for (unsigned int i=0; i < shape.geomPieces.size(); i++) {
        geomPiece = shape.geomPieces.at(i);
        for (unsigned int j=0; j<geomPiece->triangles.size(); j++) {
            triangle = geomPiece->getTriangle( j );
            point = triangle->getVertexA();
            p1[0] = (*point)(0);  p1[1] = (*point)(1);  p1[2] = (*point)(2);
            point = triangle->getVertexB();
            p2[0] = (*point)(0);  p2[1] = (*point)(1);  p2[2] = (*point)(2);
            point = triangle->getVertexC();
            p3[0] = (*point)(0);  p3[1] = (*point)(1);  p3[2] = (*point)(2);
            pqpBodyModel->AddTri(p1, p2, p3, j+i);
        }
    }
    pqpBodyModel->EndModel();
}

void Body::setPosition(const Vector3r &position)
{
    this->frame.setPosition(position);
    pqpTranslation[0] = position(0);
    pqpTranslation[1] = position(1);
    pqpTranslation[2] = position(2);
}

void Body::setOrientation( const Matrix3r &orientation )
{
    this->frame.setOrientation(orientation);
    pqpRotation[0][0] = orientation(0,0);   
    pqpRotation[0][1] = orientation(0,1);   
    pqpRotation[0][2] = orientation(0,2);
    pqpRotation[1][0] = orientation(1,0);   
    pqpRotation[1][1] = orientation(1,1);   
    pqpRotation[1][2] = orientation(1,2);
    pqpRotation[2][0] = orientation(2,0);   
    pqpRotation[2][1] = orientation(2,1);  
    pqpRotation[2][2] = orientation(2,2);
}

Body::~Body()
{
    //delete bodyShape;
}

} // end of namespace core
} // end of namespace hr
