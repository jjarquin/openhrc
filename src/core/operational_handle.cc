/*
 *
 * Copyright (C) 2015
 * Julio Jarquin <jjarquin.ct@gmail.com>, Gustavo Arechavaleta <garechav@cinvestav.edu.mx> , Gerardo Jarquin
 * CINVESTAV - Saltillo Campus
 *
 * This file is part of OpenHRC
 * OpenHRC is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * OpenHRC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/**
 *	\file src/core/operational_handle.cc
 *	\author Gerardo Jarquin, Gustavo Arechavaleta, Julio Jarquin.
 *	\version 1.0
 *	\date 2015
 *
 * OperationalHandle class implementation.
 */

#include "openhrc/core/operational_handle.h"

namespace hr {
namespace core{

OperationalHandle::OperationalHandle(const unsigned int id, const std::string &name) {
    this->id = id;
    this->name = name;
    this->position.setZero();
    this->orientation.setIdentity();
}

OperationalHandle::OperationalHandle(const unsigned int id, const Vector3r &pos,
                                     const std::string &name) {
    this->id = id;
    this->name = name;
    this->position = pos;
    this->orientation.setIdentity();
}

OperationalHandle::OperationalHandle(const unsigned int id, const std::string &name,
                                     const Vector3r &pos, const Matrix3r &rot) {
    this->id = id;
    this->name = name;
    this->position = pos;
    this->orientation = rot;
}

OperationalHandle::~OperationalHandle() {
    delete &id;
    delete &name;
    delete &position;
    delete &orientation;
}

} // end of namespace core
} // end of namespace hr

