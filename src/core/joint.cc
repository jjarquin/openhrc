/*
 *
 * Copyright (C) 2015
 * Julio Jarquin <jjarquin.ct@gmail.com>, Gustavo Arechavaleta <garechav@cinvestav.edu.mx> , Gerardo Jarquin
 * CINVESTAV - Saltillo Campus
 *
 * This file is part of OpenHRC
 * OpenHRC is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * OpenHRC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/**
 *	\file src/core/joint.cc
 *	\author Gerardo Jarquin, Gustavo Arechavaleta, Julio Jarquin.
 *	\version 1.0
 *	\date 2015
 *
 * Joint class implementation.
 */

#include "openhrc/core/joint.h"

namespace hr
{
namespace core
{
Joint::Joint( short int id )
{
    jointId = id;
    jointType = kNullJoint;
    q = 0;
    dq = 0.0;
    ddq = 0.0;
    actionAxis << 0,0,1;
}

Joint::Joint( JointType jType )
{
    jointId = 0;
    jointType = jType;
    q = 0.0;
    dq = 0.0;
    ddq = 0.0;
    actionAxis << 0,0,1;
}
}   // end of namespace core
}   // end of namespace hr

