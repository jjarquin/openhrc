/*
 *
 * Copyright (C) 2015
 * Julio Jarquin <jjarquin.ct@gmail.com>, Gustavo Arechavaleta <garechav@cinvestav.edu.mx>
 * CINVESTAV - Saltillo Campus
 *
 * This file is part of OpenHRC
 * OpenHRC is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;NAOther
 * version 2.1 of the License, or (at your option) any later version.
 *
 * OpenHRC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/**
 *	\file src/core/world.cc
 *	\author Julio Jarquin, Gustavo Arechavaleta, Julio Jarquin.
 *	\version 1.0
 *	\date 2015
 *
 *	World class implementation.
 */

#include "openhrc/core/world.h"
#include "openhrc/core/markup.h"

namespace hr{
namespace core{

boost::shared_ptr< MultiBody > World::getRobot( short int id )
{
    boost::shared_ptr< MultiBody > robotAux;

    for( robots_iter = robots.begin(); robots_iter != robots.end(); robots_iter++ ) {
        if ( (*robots_iter)->getId() == id )
            robotAux = *robots_iter;
    }
    return robotAux;
}

boost::shared_ptr< MultiBody > World::getRobot( std::string robotName )
{
    boost::shared_ptr< MultiBody > robotAux;

    for( robots_iter = robots.begin(); robots_iter != robots.end(); robots_iter++ ) {
        if ( (*robots_iter)->getName() == robotName )
            robotAux = *robots_iter;
    }
    return robotAux;
}

void World::loadMultiBody(std::string sFileName, int robotID, const RobotType robotType = kNAO){
    CMarkup xbotFile;
    boost::shared_ptr< MultiBody > robot;

    if ( xbotFile.Load(sFileName) ) {
        if ( xbotFile.FindElemByPath("Robot") ) {
            std::string sAttrib = xbotFile.GetAttrib("DEFName");
            if (robotType == kNAO)
                //robot = new MultiBodyNAO( robotID, sAttrib );
                robot = boost::make_shared<MultiBodyNAO>( robotID, sAttrib );
            else
                //robot = new MultiBodyHRP2( robotID, sAttrib );
                robot = boost::make_shared<MultiBodyHRP2>( robotID, sAttrib );
            sAttrib = xbotFile.GetAttrib("DoF");
            robot->setDoF( atoi( sAttrib.c_str() ) );
            sAttrib = xbotFile.GetAttrib("FileName");
            robot->loadTree(sAttrib, xbotFile.GetElemContent() );
        }
    }
    if (robot != 0) {
        robots.push_back( robot );
        std::cout<<"Multibody succesfully loaded"<< endl;
    }
    else std::cout<<"ERROR: Impossible to load the multibody"<< endl;
}

void World::collisionQuery()
{
    boost::shared_ptr< MultiBody > robot;

    for ( unsigned int i = 0; i < getRobotsVector()->size(); i++ ) {
        robot = getRobot(i);
        robot->autoCollisionQuery();
    }
}

} // end of namespace core
} // end of namespace hr
