/*
 *
 * Copyright (C) 2015
 * Julio Jarquin <jjarquin.ct@gmail.com>, Gustavo Arechavaleta <garechav@cinvestav.edu.mx> , Gerardo Jarquin
 * CINVESTAV - Saltillo Campus
 *
 * This file is part of OpenHRC
 * OpenHRC is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * OpenHRC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/**
 *	\file src/core/multibody.cc
 *	\author Gerardo Jarquin, Gustavo Arechavaleta, Julio Jarquin.
 *	\version 1.0
 *	\date 2015
 *
 * Multibody class implementation.
 */
#include "openhrc/core/multibody.h"
#include  <iostream>
#include "PQP/PQP.h"
#include "openhrc/core/markup.h"

namespace hr{
namespace core{

bool MultiBody::loadTree(const std::string &xmlFileName, const std::string &xbotFileName)
{
    CMarkup xmlGeo;
    Vector3r actionAxis;
    std::string jointType, jointPredecessor, jointSuccessor, minLimit, maxLimit;
    bool status;
    real_t ulim, llim;
    std::string maxVelocity;
    real_t uSpeed;
    ulim = 0;
    llim = 0;

    xmlGeo.SetDoc(xbotFileName);
    while ( xmlGeo.FindElem("Joint") ) {
        jointType = xmlGeo.GetAttrib("Type");
        actionAxis = stringToVector3r(status, xmlGeo.GetAttrib("Axis"), " ");
        minLimit = xmlGeo.GetAttrib("MinLimit");
        maxLimit = xmlGeo.GetAttrib("MaxLimit");
        maxVelocity = xmlGeo.GetAttrib("MaxVelocity");
        ulim = atof(maxLimit.c_str());
        llim = atof(minLimit.c_str());
        uSpeed = atof(maxVelocity.c_str());
        if (!status) {
            actionAxis<<0,0,1;
            std::cout<<"WARNNING: Undefined or incorrect action axis, it is set [0, 0, 1]"<< endl;
        }
        jointPredecessor = xmlGeo.GetAttrib("Predecessor");
        jointSuccessor = xmlGeo.GetAttrib("Successor");
        //qDebug()<< "New" <<jointType.data()<<"joint acting in the axis ["<<jointAxis.data()<<"] was found. The predecessor and successor bodies are "<<jointPredecessor.data()<<" and "<<jointSuccessor.data();
        if ( !newJoint( jointType, actionAxis, jointPredecessor, jointSuccessor, ulim, llim, uSpeed, xmlFileName ) ) {
            std::cout<<"ERROR: The tree was not loaded"<< endl;
            return false;
        }
    }
    this->DoF = this->joints.size();
    fillSubtreeBodies();
    computeForwardKinematics();
    setCoMHandles();
    printStructure();
    return true;
}

void MultiBody::fillSubtreeBodies()
{
    std::vector<short int> *childrenBodies;
    std::vector<short int> *newSubtreeBodies;
    std::vector<short int> *subtreeBodies;
    std::vector<short int>::iterator subtreeBodies_iter;
    std::vector<short int>::iterator childrenBodies_iter;

    for ( short int i = (this->bodies.size()-1); i >= 0; i-- ) {
        //qDebug()<<"Body "<<i<<": "<<bodies.at(i)->getName().data();
        childrenBodies = this->childrenBodies.at( i );
        //qDebug()<< childrenBodies->size()<<" children";
        if ( !childrenBodies->empty() ) {
            subtreeBodies = this->subtreeBodies.at(i);
            for ( childrenBodies_iter = childrenBodies->begin();  childrenBodies_iter != childrenBodies->end();
                  childrenBodies_iter++ ) {
                newSubtreeBodies = this->subtreeBodies.at( *childrenBodies_iter );
                for ( subtreeBodies_iter = newSubtreeBodies->begin(); subtreeBodies_iter != newSubtreeBodies->end();
                      subtreeBodies_iter++ )
                    subtreeBodies->push_back( *subtreeBodies_iter );
            }
        }
    }
}

Body* MultiBody::getBody( std::string bodyName )  //TODO 03/2015: Change to DynamicBody
{
    typename std::vector<Body*>::iterator bodies_iter;
    Body *bodyAux = 0;

    for( bodies_iter = bodies.begin(); bodies_iter != bodies.end(); bodies_iter++ ) {
        if ( (*bodies_iter)->getName() == bodyName )
            bodyAux = *bodies_iter;
    }
    return bodyAux;
}

Body* MultiBody::getBody( short int bodyId  )  //TODO 03/2015: Change to DynamicBody
{
    if (bodyId >= 0)
        return bodies.at(bodyId );
    else {
        //qDebug()<<"ERROR: Invalid body Id, only Id >= 0 are allowed. Body 0 will be obtained by default";
        return bodies.at(0);
    }
}

bool MultiBody::newBody(std::string newBodyName, const std::string &xmlFileName, short int parentBodyId )  //TODO 03/2015: Change to DynamicBody
{
    DynamicBody* newBody; //TODO: remove hardcoded DynamicBody
    std::vector<short int> predecessorJoints;
    std::vector<short int> *childrenBodies, *newSubtreeBodies;
    int newBodyId;
    Vector3r relativeBodyPosition;

    relativeBodyPosition.setZero();
    newBodyId = this->bodies.size();
    //qDebug()<<"There are "<<newBodyId<<" bodies and "<<this->joints.size()<<" joints in the multibody system";
    //qDebug()<<"Body"<<newBodyName.data()<<" with ID = "<<newBodyId<<" will be created";
    //Verify if there exists the joint i that connects the body i and its parent
    if ( this->joints.size() >= newBodyId  || ( parentBodyId == -1 && this->joints.size() == 1) ) {
        //qDebug()<<"Joint "<<this->joints.size()<< " already exists in the multibody system";
        //Create new body
        //newBody = new Body( newBodyId, newBodyName );  //TODO 03/2015: Change to DynamicBody
        newBody = new DynamicBody ( newBodyId, newBodyName);
        //Load its geometry
        if ( newBody->loadGeometry(xmlFileName, relativeBodyPosition) ) {
            //Create the PQP model
            //newBody->createPQPBodyModel();
        }
        this->relativeBodyPosition.push_back(relativeBodyPosition);
        //Store in bodies vector
        bodies.push_back(newBody);
        //Store the id of the body in the subtree of bodies
        newSubtreeBodies = new std::vector<short int>;
        newSubtreeBodies->push_back(newBodyId);
        this->subtreeBodies.push_back(newSubtreeBodies);
        //Store the id of the parent body of body i
        this->parent.push_back(parentBodyId);
        //Store the id of the set of joints between body i and the root
        if ( parentBodyId > 0 )
            predecessorJoints = getPredecessorJoints( parentBodyId );
        predecessorJoints.push_back( newBodyId );  //Always the body i and its parent are connected by the joint i, then newBodyId = jointId
        this->predecessorJoints.push_back(predecessorJoints);
        //Create a new vector of children bodies for the body i
        childrenBodies = new std::vector<short int>;
        this->childrenBodies.push_back(childrenBodies);
        //Add the body i in the set of child bodies of the parent
        if ( parentBodyId >= 0 ) {
            childrenBodies = this->childrenBodies.at(parentBodyId);
            //qDebug()<<"childrenBodies size before add the new child is "<<childrenBodies->size();
            childrenBodies->push_back(newBodyId);
            //childrenBodies = this->childrenBodies.at( parentBodyId );
            //qDebug()<<"childrenBodies size after add the new child is "<<childrenBodies->size();
        }
        //qDebug()<<"Body"<<newBodyName.data()<<"succesfully created";
    }
    else {
        //qDebug()<< "The joint "<<this->joints.size()<< "does not exist, it will be created with default settings before the body i is created";
        Vector3r actionAxis(0,0,1);
        Body* predecessorBody;   //TODO 03/2015: Change to DynamicBody
        predecessorBody = this->bodies.at(parentBodyId);
        if ( !newJoint( "NULL", actionAxis,  predecessorBody->getName(), newBodyName, 0, 0, xmlFileName ) ) {
            //qDebug()<<"ERROR: The tree was not loaded";
            return false;
        }
    }
    return true;
}

bool MultiBody::newJoint( std::string jointType, const Vector3r &actionAxis,
                          std::string jointPredecessor, std::string jointSuccessor,
                          real_t ulim, real_t llim,
                          const std::string &xmlFileName )
{
    Joint* joint;
    Body* predecessorBody;  //TODO 03/2015: Change to DynamicBody
    Body* successorBody;    //TODO 03/2015: Change to DynamicBody

    //Create the new joint
    joint = new Joint( this->joints.size()+1 );
    //Set the joint type (default value = revolute)
    if ( jointType != "NULL" && jointType != "" && ( jointType == "Revolute" || jointType == "Prismatic" ) ) {
        if (jointType == "Revolute")
            joint->setJointType( kRevoluteJoint );
        else if (jointType == "Prismatic")
            joint->setJointType( kPrismaticJoint );
    }
    else {
        //qDebug()<<"WARNNING: Undefined or incorrect joint type, it will be set REVOLUTE";
        joint->setJointType( kRevoluteJoint );
    }
    //Set the action axis of the joint
    joint->setActionAxis(actionAxis);
    joint->setLowLimit(llim);
    joint->setUpLimit(ulim);
    //Store the new joint
    this->joints.push_back( joint );
    //Set the predecessor body
    if ( jointPredecessor != "NULL" && jointPredecessor != "" ) {
        if ( this->joints.size() == 1 ) {
            //Create new body
            if ( !newBody(jointPredecessor, xmlFileName, -1 ) )
                return false;
        }
        predecessorBody = getBody(jointPredecessor);
        if (predecessorBody != 0)
            this->jointPredecessor.push_back( predecessorBody->getId() );
        else {
            //qDebug()<<"ERROR: The predecessor body was not found";
            return false;
        }
    }
    //Set the successor body
    if( jointSuccessor != "NULL" || jointSuccessor != "" ) {
        successorBody = getBody(jointSuccessor);
        if ( successorBody == 0 ) {
            //Create new body
            if ( newBody(jointSuccessor, xmlFileName, predecessorBody->getId() ) )
                successorBody = getBody(jointSuccessor);
            else return false;
        }
        this->jointSuccessor.push_back( successorBody->getId() );
    }
    return true;
}

bool MultiBody::newJoint( std::string jointType, const Vector3r &actionAxis,
                          std::string jointPredecessor, std::string jointSuccessor,
                          real_t ulim, real_t llim, real_t maxSpeed,
                          const std::string &xmlFileName )
{
    Joint* joint;
    Body* predecessorBody;  //TODO 03/2015: Change to DynamicBody
    Body* successorBody;    //TODO 03/2015: Change to DynamicBody

    //Create the new joint
    joint = new Joint( this->joints.size()+1 );
    //Set the joint type (default value = revolute)
    if ( jointType != "NULL" && jointType != "" && ( jointType == "Revolute" || jointType == "Prismatic" ) ) {
        if (jointType == "Revolute")
            joint->setJointType( kRevoluteJoint );
        else if (jointType == "Prismatic")
            joint->setJointType( kPrismaticJoint );
    }
    else {
        //qDebug()<<"WARNNING: Undefined or incorrect joint type, it will be set REVOLUTE";
        joint->setJointType( kRevoluteJoint );
    }
    //Set the action axis of the joint
    joint->setActionAxis(actionAxis);
    joint->setLowLimit(llim);
    joint->setUpLimit(ulim);
    joint->setUpSpeedLimit(maxSpeed);
    joint->setLowSpeedLimit(-maxSpeed);

    //Store the new joint
    this->joints.push_back( joint );
    //Set the predecessor body
    if ( jointPredecessor != "NULL" && jointPredecessor != "" ) {
        if ( this->joints.size() == 1 ) {
            //Create new body
            if ( !newBody(jointPredecessor, xmlFileName, -1 ) )
                return false;
        }
        predecessorBody = getBody(jointPredecessor);
        if (predecessorBody != 0)
            this->jointPredecessor.push_back( predecessorBody->getId() );
        else {
            //qDebug()<<"ERROR: The predecessor body was not found";
            return false;
        }
    }
    //Set the successor body
    if( jointSuccessor != "NULL" || jointSuccessor != "" ) {
        successorBody = getBody(jointSuccessor);
        if ( successorBody == 0 ) {
            //Create new body
            if ( newBody(jointSuccessor, xmlFileName, predecessorBody->getId() ) )
                successorBody = getBody(jointSuccessor);
            else return false;
        }
        this->jointSuccessor.push_back( successorBody->getId() );
    }
    return true;
}


Joint* MultiBody::getJoint( short int jointId  )
{
    if (jointId >= 1)
        return joints.at(jointId-1);
    else {
        //qDebug()<<"ERROR: Invalid joint Id, only Id > 0 are allowed. Joint 1 will be obtained by default";
        return joints.at( 0 );
    }
}

void MultiBody::clear()
{
    joints.clear();
    bodies.clear();
    jointPredecessor.clear();
    jointSuccessor.clear();
}

void MultiBody::printStructure()   //TODO 03/2015: Change to DynamicBody
{
    /**/
    std::cout<<"Multibody "<<multiBodyId<<" has "<<bodies.size()<<" bodies and "<<joints.size()<<" joints" << endl;
    for (unsigned int i = 0; i < bodies.size(); i++) {
        std::cout<<"     Body "<<getBody( i )->getId()<<": "<<getBody( i )->getName().data() << endl;
        if ( i>0 ) {
            std::cout<<"           Parent :"<<getBody( parent.at(i) )->getName().data() << endl;
            std::cout<<"          "<<predecessorJoints.at(i).size()<<"Predecessor Joints :" << endl;
            for (unsigned int j=0; j<predecessorJoints.at(i).size(); j++)
                std::cout<<"               Joint"<<predecessorJoints.at(i).at(j) << endl;
        }
        else {
            std::cout<<"           Parent : None"<< endl;
            std::cout<<"           0 Predecessor Joints :"<< endl;
        }
        std::cout<<"          "<<childrenBodies.at(i)->size()<<"Children Bodies :"<< endl;
        for (unsigned int j=0; j<childrenBodies.at(i)->size(); j++)
            std::cout<<"               "<<(getBody(childrenBodies.at(i)->at(j)))->getName().data()<< endl;
        std::cout<<"          "<<subtreeBodies.at(i)->size()<<"Bodies in the subtree:"<< endl;
        for (unsigned int j=0; j<subtreeBodies.at(i)->size(); j++)
            std::cout<<"               "<<(getBody(subtreeBodies.at(i)->at(j)))->getName().data()<< endl;
        std::cout<<"           Geometry :"<< endl;
        if ( getBody( i )->getGeometry()->geomPieces.size() > 0 )
            std::cout<<"               "<<getBody( i )->getGeometry()->geomPieces.size()<<"Pieces"<< endl;
        else
            std::cout<<"               None"<< endl;
        std::cout<<"          Position relative to its parent : [ "<<relativeBodyPosition.at(i)(0)<<", "<<relativeBodyPosition.at(i)(1)<<", "<<relativeBodyPosition.at(i)(2)<<" ]"<< endl;
        std::cout<<"          Absolute position : [ "<<getBody( i )->getPosition()(0)<<", "<<getBody( i )->getPosition()(1)<<", "<<getBody( i )->getPosition()(2)<<" ]"<< endl;
    }
    for (unsigned int i = 1; i <= joints.size(); i++) {
        if (getJoint( i )->getJointType() == 0)
            std::cout<<"     Revolute Joint "<<getJoint( i )->getId()<<" :"<< endl;
        else if (getJoint( i )->getJointType() == 1)
            std::cout<<"     Prismatic Joint "<<getJoint( i )->getId()<<" :"<< endl;
        std::cout<<"          Action Axis : [ "<<getJoint( i )->getActionAxis()(0)<<", "<<getJoint( i )->getActionAxis()(1)<<", "<<getJoint( i )->getActionAxis()(2)<<" ]"<< endl;
        std::cout<<"          Predecessor Body :"<<getBody( getPredecessorId(i) )->getName().data()<< endl;
        std::cout<<"          Successor Body :"<<getBody( getSuccessorId(i) )->getName().data()<< endl;
        std::cout<<"          MaxLim : "<<getJoint(i)->getUpLimit()<< endl;
        std::cout<<"          MinLim : "<<getJoint(i)->getLowLimit()<< endl;
        std::cout<<"          MaxSpeed : "<<getJoint(i)->getUpSpeedLimit()<< endl;
        std::cout<<"          MinSpeed : "<<getJoint(i)->getLowSpeedLimit()<< endl;

    }
    /**/
}

VectorXr MultiBody::getConfiguration()
{
    VectorXr q(DoF);
    q.setZero();
    typename std::vector< Joint* >::iterator jointIterator;
    int i = 0;
    for(jointIterator = joints.begin(); jointIterator != joints.end(); jointIterator++)
    {
        q(i) = (*jointIterator)->getConfiguration();
        i++;
    }
    return q;
}

void MultiBody::setConfiguration(const VectorXr &q)
{
    typename std::vector< Joint* >::iterator jointIterator;
    int i = 0;
    for(jointIterator = joints.begin(); jointIterator != joints.end(); jointIterator++)
    {
        (*jointIterator)->setConfiguration(q(i));
        i++;
    }
    computeForwardKinematics();
}

VectorXr MultiBody::getGeneralizedVelocity()
{
    VectorXr dq(DoF);
    dq.setZero();
    typename std::vector< Joint* >::iterator jointIterator;
    int i = 0;
    for(jointIterator = joints.begin(); jointIterator != joints.end(); jointIterator++)
    {
        dq(i) = (*jointIterator)->getVelocity();
        i++;
    }
    return dq;
}

void MultiBody::setGeneralizedVelocity(const VectorXr &dq)
{
    typename std::vector< Joint* >::iterator jointIterator;
    int i = 0;
    for(jointIterator = joints.begin(); jointIterator != joints.end(); jointIterator++)
    {
        (*jointIterator)->setVelocity(dq(i));
        i++;
    }
}

VectorXr MultiBody::getGeneralizedAcceleration()
{
    VectorXr ddq(DoF);
    ddq.setZero();
    typename std::vector< Joint* >::iterator jointIterator;
    int i = 0;
    for(jointIterator = joints.begin(); jointIterator != joints.end(); jointIterator++)
    {
        ddq(i) = (*jointIterator)->getAcceleration();
        i++;
    }
    return ddq;
}

void MultiBody::setGeneralizedAcceleration(const VectorXr &ddq)
{
    typename std::vector< Joint* >::iterator jointIterator;
    int i = 0;
    for(jointIterator = joints.begin(); jointIterator != joints.end(); jointIterator++)
    {
        (*jointIterator)->setAcceleration(ddq(i));
        i++;
    }
}

VectorXr MultiBody::getJointUpLimits(){
    VectorXr jointUpLimits(DoF);
    jointUpLimits.setZero();
    typename std::vector< Joint* >::iterator jointIterator;
    int i = 0;
    for(jointIterator = joints.begin(); jointIterator != joints.end(); jointIterator++)
    {
        jointUpLimits(i) = (*jointIterator)->getUpLimit();
        i++;
    }
    return jointUpLimits;
}

VectorXr MultiBody::getJointLowLimits(){
    VectorXr jointLowLimits(DoF);
    jointLowLimits.setZero();
    typename std::vector< Joint* >::iterator jointIterator;
    int i = 0;
    for(jointIterator = joints.begin(); jointIterator != joints.end(); jointIterator++)
    {
        jointLowLimits(i) = (*jointIterator)->getLowLimit();
        i++;
    }
    return jointLowLimits;
}

VectorXr MultiBody::getJointUpSpeedLimits(){
    VectorXr jointUpSpeedLimits(DoF);
    jointUpSpeedLimits.setZero();
    typename std::vector< Joint* >::iterator jointIterator;
    int i = 0;
    for(jointIterator = joints.begin(); jointIterator != joints.end(); jointIterator++)
    {
        jointUpSpeedLimits(i) = (*jointIterator)->getUpSpeedLimit();
        i++;
    }
    return jointUpSpeedLimits;
}

VectorXr MultiBody::getJointLowSpeedLimits(){
    VectorXr jointLowSpeedLimits(DoF);
    jointLowSpeedLimits.setZero();
    typename std::vector< Joint* >::iterator jointIterator;
    int i = 0;
    for(jointIterator = joints.begin(); jointIterator != joints.end(); jointIterator++)
    {
        jointLowSpeedLimits(i) = (*jointIterator)->getLowSpeedLimit();
        i++;
    }
    return jointLowSpeedLimits;
}



void MultiBody::computeForwardKinematics()
{
    computeChildrenForwardKinematics( getBody( 0 ) );
}

void MultiBody::computeSubtreeForwardKinematics(short int initialJoint)
{
    Body* body;
    Body* bodyParent;

    body = getBody( initialJoint );
    bodyParent = getBody( getPredecessorId( initialJoint ) );
    computeBodyPoseFromParent(body, bodyParent->getPosition(), bodyParent->getOrientation());
    computeChildrenForwardKinematics( body );
}

void MultiBody::computeBodyForwardKinematics(short int bodyId)
{
    if ( bodyId > 0 ){
        short int parentId = getParentId( bodyId );
        if ( parentId  > 0 )
            computeBodyForwardKinematics( parentId );
        computeBodyPoseFromParent(getBody( bodyId ), getBody( parentId )->getPosition(),
                                  getBody( parentId )->getOrientation());
    }
}

void MultiBody::computeChildrenForwardKinematics(Body* body)   //TODO 03/2015: Change to DynamicBody
{
    Body* childBody;
    std::vector<short int> bodyChildren;

    bodyChildren = getBodyChildren( body->getId() );
    for(unsigned int i=0; i < bodyChildren.size(); i++) {
        childBody = getBody( bodyChildren.at( i ) );
        computeBodyPoseFromParent(childBody, body->getPosition(), body->getOrientation());
        computeChildrenForwardKinematics(childBody);
    }
}

void MultiBody::computeBodyPoseFromParent(Body* body, const Vector3r &parentPosition,
                                          const Matrix3r &parentOrientation)  //TODO 03/2015: Change to DynamicBody
{
    Vector3r bodyPosition;
    Vector3r actionAxis;
    Vector3r relativePosition;
    Matrix3r bodyRotation;
    Joint* jointWithParent;
    real_t qRot=0;
    real_t qTrans=0;

    jointWithParent = getJoint( body->getId() );
    if (jointWithParent != 0)  {
        actionAxis = jointWithParent->getActionAxis().cast<real_t>();
        relativePosition = getRelativeBodyPosition( body->getId() );
        if ( jointWithParent->getJointType() == kPrismaticJoint ) {
            qTrans = jointWithParent->getConfiguration();
            relativePosition += actionAxis * qTrans;
            bodyRotation = parentOrientation;
        }
        else if (jointWithParent->getJointType() == kRevoluteJoint) {
            qRot = jointWithParent->getConfiguration();
            actionAxis.normalize();
            AngleAxisr angleAxisRotation(qRot, actionAxis);    //TODO: careful with real_t and float types
            bodyRotation = parentOrientation*angleAxisRotation.toRotationMatrix();
        }
        bodyPosition = parentPosition + parentOrientation*relativePosition;
        body->setPosition( bodyPosition );
        body->setOrientation( bodyRotation );
        body->updateOperationalHandlesPose();
    }
}

void MultiBody::autoCollisionQuery()  //TODO 03/2015: Change to DynamicBody
{
    Body* body;
    MultiBody* robotAux;
    Body* bodyAux;
    bool collisionDetected;
    unsigned int k;

    for ( unsigned int j = 0; j < this->getBodies()->size(); j++ ) {
        body = this->getBody(j);
        body->getGeometry()->setCollisionStatus(false);
        if ( body->getGeometry()->geomPieces.size() != 0 ) {
            k = 0;
            while ( k < this->getBodies()->size() ) {
                bodyAux = this->getBody(k);
                if ( bodyAux->getId() == body->getId()) {
                    k = this->getBodies()->size();
                }
                else {
                    if ( collisionQueryAllowed(body, bodyAux) )
                        collisionDetected = inCollision(body, bodyAux);
                    k++;
                }
            }
        }
    }
}

bool MultiBody::collisionQueryAllowed( Body* body1, Body* body2)  //TODO 03/2015: Change to DynamicBody
{
    //cout<<"in collisionQueryAllowed"<< endl;;
    if ( body1->getGeometry()->geomPieces.size() != 0 && body2->getGeometry()->geomPieces.size() !=0 ) {
        int body1_Id = body1->getId();
        int body2_Id = body2->getId();
        if (body1_Id != body2_Id) {
            //cout<<"bodies "<<body1_Id<<" and "<<body2_Id<<" are not the same body"<< endl;
            if ( this->getParentId( body1_Id ) !=  body2_Id ) {
                //cout<<"body "<<body2_Id<<" is not the parent body of body "<<body1_Id<< endl;
                for ( unsigned int i = 0; i < this->getBodyChildren( body1_Id ).size(); i++ ) {
                    if ( this->getBodyChildren( body1_Id ).at(i) == body2_Id)
                        return false;
                }
            }
            else return false;
            //cout<<"body "<<body2_Id<<" is not a child body of body "<<body1_Id<< endl;
            return true;
        }
        else return false;
    }
    else return false;
}

bool MultiBody::inCollision(Body* body1, Body* body2)  //TODO 03/2015: Change to DynamicBody
{
    PQP_CollideResult collideResult;
    PQP_Collide( &collideResult,body1->getPQProtation(),body1->getPQPtranslation(),
                 body1->getPQPBodyModel(), body2->getPQProtation(), body2->getPQPtranslation(),
                 body2->getPQPBodyModel(), PQP_FIRST_CONTACT );
    if ( collideResult.NumPairs() >  0 ) {
        //cout<<"Collision detected between bodies "<<body1->getName().data()<<" and "<<body2->getName().data()<< endl;
        body1->getGeometry()->setCollisionStatus(true);
        body2->getGeometry()->setCollisionStatus(true);
        return true;
    }
    return false;
}

Vector3r MultiBody::getBodyPosition(const short int &bodyId)  //TODO 03/2015: Change to DynamicBody
{
    Body *body = this->getBody(bodyId);
    return body->getPosition();
}

Matrix3r MultiBody::getBodyOrientation(const short int &bodyId) //TODO 03/2015: Change to DynamicBody
{
    Body *body = this->getBody(bodyId);
    return body->getOrientation();
}

Vector3r MultiBody::getOperationalHandlePosition(const short int &bodyId,
                                                        const short int &handleId)
{
    Body *body = this->getBody(bodyId);
    OperationalHandle *opHandle = body->getOperationalHandle(handleId);
    return opHandle->getPosition();
}

Matrix3r MultiBody::getOperationalHandleOrientation(const short int &bodyId,
                                                           const short int &handleId)
{
    Body *body = this->getBody(bodyId);
    OperationalHandle *opHandle = body->getOperationalHandle(handleId);
    return opHandle->getOrientation();
}

MatrixXr MultiBodyNAO::getJacobian(const JacobianType &JacobianType, const short int &bodyId,
                                          const short int &handleId)
{
    MatrixXr Jg_w(3,DoF);
    MatrixXr Jg_v(3,DoF);
    Vector3r r;
    Vector3r zi_1;
    Vector3r zi_1Crossr;
    //OperationalHandle handle;
    Joint* joint;
    Body* bodyAux;   //TODO 03/2015: Change to DynamicBody
    bool supportsBody;
    bool computeJw = false;
    bool computeJv = false;
    std::vector<short int> predecessorJoints;

    if ( JacobianType != kGeometricJacobian_Jv ) {
        computeJw = true;
        Jg_w.setZero();
    }
    if ( JacobianType != kGeometricJacobian_Jw ) {
        computeJv = true;
        Jg_v.setZero();
    }

    //handle = getBody(bodyId)->getOperationalHandle(handleId);
    predecessorJoints = getPredecessorJoints(bodyId);
    //std::cout << "predecessor joints number " <<  predecessorJoints.size() << std::endl;

    for ( short int i=1; i <= DoF; i++ ) {
        // Determine if the joint i supports the body
        supportsBody = false;

        for (short int j = 0; j < predecessorJoints.size(); j++) {
            if (predecessorJoints.at(j) == i) {
                //std::cout << "predecessor is joint " <<  i << std::endl;
                supportsBody = true;
                j = predecessorJoints.size();
            }
        }
        // If the joint supports the body and its type is revoulute, then compute Jw_i
        joint = getJoint(i);
        if ( supportsBody ) {
            //if( i > 19){
                bodyAux = getBody( getSuccessorId( i ));
            //}
            //else{
            //    bodyAux = getBody( getPredecessorId( i ) );
            //}


                   if ( joint->getActionAxis()(0) == 1 )
                zi_1 = bodyAux->getOrientation().col(0);
            else if (getJoint(i)->getActionAxis()(1) == 1 )
                zi_1 = bodyAux->getOrientation().col(1);
            else if (getJoint(i)->getActionAxis()(2) == 1 )
                zi_1 = bodyAux->getOrientation().col(2);
            else
                zi_1 = getJoint(i)->getActionAxis()(1)*bodyAux->getOrientation().col(1) +
                        getJoint(i)->getActionAxis()(2)*bodyAux->getOrientation().col(2);
            if ( joint->getJointType() == kRevoluteJoint) {
                if (computeJw)
                    Jg_w.col(i-1) = zi_1;
                if (computeJv) {
                    r = (getBody(bodyId)->getOperationalHandle(handleId))->getPosition() - bodyAux->getPosition();
                    zi_1Crossr = zi_1.cross(r);
                    Jg_v.col(i-1) = zi_1Crossr;
                }
            }
            else if ( joint->getJointType() == kPrismaticJoint && computeJv)
                Jg_v.col(i-1) = zi_1;
        }
    }
    //garechav NAO Jacobian begin
    MatrixXr Jg_vNAO(3,DoF-1);
    MatrixXr Jg_wNAO(3,DoF-1);
    if (bodyId > 24 ) {
        // Right foot
        Jg_vNAO.block(0,0,3,18) = Jg_v.block(0,0,3,18);
        Jg_vNAO.block(0,18,3,1) = Jg_v.block(0,24,3,1);
        Jg_vNAO.block(0,19,3,5) = Jg_v.block(0,19,3,5);
        Jg_vNAO.block(0,24,3,5) = Jg_v.block(0,25,3,5);
        Jg_wNAO.block(0,0,3,18) = Jg_w.block(0,0,3,18);
        Jg_wNAO.block(0,18,3,1) = Jg_w.block(0,24,3,1);
        Jg_wNAO.block(0,19,3,5) = Jg_w.block(0,19,3,5);
        Jg_wNAO.block(0,24,3,5) = Jg_w.block(0,25,3,5);
    } else {
        //Left foot
        Jg_vNAO.block(0,0,3,24) = Jg_v.block(0,0,3,24);
        Jg_vNAO.block(0,24,3,5) = Jg_v.block(0,25,3,5);
        Jg_wNAO.block(0,0,3,24) = Jg_w.block(0,0,3,24);
        Jg_wNAO.block(0,24,3,5) = Jg_w.block(0,25,3,5);
    }

    //Jg_vNAO.block(0,0,3,6) = hr::MatrixXr::Zero(3,6);
    //Jg_wNAO.block(0,0,3,6) = hr::MatrixXr::Zero(3,6);

    if (JacobianType == kGeometricJacobian_Jv) {
        return Jg_vNAO;
    } else if (JacobianType == kGeometricJacobian_Jw) {
        return Jg_wNAO;
    } else if (JacobianType == kGeometricJacobian) {
        MatrixXr Jg(6,DoF-1);
        Jg.block(0,0,3,DoF-1) = Jg_vNAO;
        Jg.block(3,0,3,DoF-1) = Jg_wNAO;
        return Jg;
    }
    //garechav NAO Jacobian end
}

void MultiBodyNAO::setCoMHandles(){

    hr::real_t tempMass;
    hr::real_t totalMass = 0.0;
    hr::Vector3r com;
    DynamicBody* tempBody;
    typename std::vector< Body* >::iterator bodyIterator;
    for(bodyIterator = bodies.begin(); bodyIterator != bodies.end(); bodyIterator++)
    {

        tempBody = (DynamicBody *)(*bodyIterator);
        tempMass = tempBody->getMass();
        totalMass += tempMass;
        int handleId = tempBody->addOperationalHandle(tempBody->getCoM());
        std::cout << "HandleId " << handleId << std::endl;
        std::cout << "Mass " << tempMass << std::endl;

    }
    std::cout << "Total Mass " << totalMass << std::endl;


}

Vector3r MultiBodyNAO::getCoM(){
    hr::Vector3r tempCoM;
    hr::Vector3r com;
    com.setZero();
    hr::real_t totalMass = 0.0;
    hr::real_t tempMass = 0.0;
    DynamicBody* tempBody;

    typename std::vector< Body* >::iterator bodyIterator;
    for(bodyIterator = bodies.begin(); bodyIterator != bodies.end(); bodyIterator++)
    {
        tempBody = (DynamicBody *)(*bodyIterator);
        tempMass = tempBody->getMass();
        totalMass += tempMass;
        OperationalHandle *opHandle =tempBody->getOperationalHandle(0);
        tempCoM = tempMass*opHandle->getPosition();
        com += tempCoM;
    }
    com = (1/totalMass)*com;
    return com;
}

Matrix3Xr MultiBodyNAO::getCoMJacobian(){
    hr::Matrix3Xr tempCoMJacobian;
    hr::Matrix3Xr comJacobian;
    tempCoMJacobian.resize(3,29);
    comJacobian.resize(3,29);
    comJacobian.setZero();
    hr::real_t totalMass = 0.0;
    hr::real_t tempMass = 0.0;
    DynamicBody* tempBody;
    int tempBodyId;
    typename std::vector< Body* >::iterator bodyIterator;
    for(bodyIterator = bodies.begin(); bodyIterator != bodies.end(); bodyIterator++)
    {
        tempBody = (DynamicBody *)(*bodyIterator);
        tempMass = tempBody->getMass();
        totalMass += tempMass;
        tempBodyId = tempBody->getId();
        tempCoMJacobian = getJacobian(kGeometricJacobian_Jv,tempBodyId,0);
        comJacobian += tempMass * tempCoMJacobian;

    }
    comJacobian = (1/totalMass)*comJacobian;
    return comJacobian;
}


void MultiBodyHRP2::setCoMHandles(){

}

Vector3r MultiBodyHRP2::getCoM(){
    hr::Vector3r com;
    com.setZero();
    return com;
}

Matrix3Xr MultiBodyHRP2::getCoMJacobian(){
    Matrix3Xr jacobian(3,30);

    //dummy code
    jacobian.setZero();
    return jacobian;
}


MatrixXr MultiBodyHRP2::getJacobian(const JacobianType &JacobianType, const short int &bodyId,
                                           const short int &handleId)
{
    MatrixXr Jg_w(3,DoF);
    MatrixXr Jg_v(3,DoF);
    Vector3r r;
    Vector3r zi_1;
    Vector3r zi_1Crossr;
    Joint* joint;
    Body* bodyAux;
    bool supportsBody;
    bool computeJw = false;
    bool computeJv = false;
    std::vector<short int> predecessorJoints;

    if ( JacobianType != kGeometricJacobian_Jv ) {
        computeJw = true;
        Jg_w.setZero();
    }
    if ( JacobianType != kGeometricJacobian_Jw ) {
        computeJv = true;
        Jg_v.setZero();
    }

    predecessorJoints = getPredecessorJoints(bodyId);
    for ( short int i=1; i <= DoF; i++ ) {
        // Determine if the joint i supports the body
        supportsBody = false;
        for (short int j = 0; j < predecessorJoints.size(); j++) {
            if (predecessorJoints.at(j) == i) {
                supportsBody = true;
                j = predecessorJoints.size();
            }
        }
        // If the joint supports the body and its type is revoulute, then compute Jw_i
        joint = getJoint(i);
        if ( supportsBody ) {
            bodyAux = getBody( getPredecessorId( i ) );
            if ( joint->getActionAxis()(0) == 1 )
                zi_1 = bodyAux->getOrientation().col(0);
            else if (getJoint(i)->getActionAxis()(1) == 1 )
                zi_1 = bodyAux->getOrientation().col(1);
            else if (getJoint(i)->getActionAxis()(2) == 1 )
                zi_1 = bodyAux->getOrientation().col(2);
            else
                zi_1 = getJoint(i)->getActionAxis()(0)*bodyAux->getOrientation().col(0) +
                        getJoint(i)->getActionAxis()(1)*bodyAux->getOrientation().col(1) +
                        getJoint(i)->getActionAxis()(2)*bodyAux->getOrientation().col(2);
            if ( joint->getJointType() == kRevoluteJoint) {
                if (computeJw)
                    Jg_w.col(i-1) = zi_1;
                if (computeJv) {
                    r = (getBody(bodyId)->getOperationalHandle(handleId))->getPosition() - bodyAux->getPosition();
                    zi_1Crossr = zi_1.cross(r);
                    Jg_v.col(i-1) = zi_1Crossr;
                }
            }
            else if ( joint->getJointType() == kPrismaticJoint && computeJv)
                Jg_v.col(i-1) = zi_1;
        }
    }
    if (JacobianType == kGeometricJacobian_Jv) {
        return Jg_v;
    } else if (JacobianType == kGeometricJacobian_Jw) {
        return Jg_w;
    } else if (JacobianType == kGeometricJacobian) {
        MatrixXr Jg(6,DoF);
        Jg.block(0,0,3,DoF) = Jg_v;
        Jg.block(3,0,3,DoF) = Jg_w;
        return Jg;
    }
}

} // end of namespace core
} // end of namesapce hr
