/*
 *
 * Copyright (C) 2015
 * Julio Jarquin <jjarquin.ct@gmail.com>, Gustavo Arechavaleta <garechav@cinvestav.edu.mx>
 * CINVESTAV - Saltillo Campus
 *
 * This file is part of OpenHRC
 * OpenHRC is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * OpenHRC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/**
 *	\file src/locomotion/bezier_curve.cc
 *	\author Julio Jarquin, Gustavo Arechavaleta
 *	\version 1.0
 *	\date 2015
 *
 *	Implementation of the BezierCurve class.
 */

#include "openhrc/locomotion/bezier_curve.h"

namespace hr{
namespace locomotion{
BezierCurve::BezierCurve()
{
    initialized = false;
}

void BezierCurve::init(const  VectorXr& p0, const VectorXr& p1,
                       const  VectorXr& p2,const VectorXr& p3){
    p_0 = p0;
    p_1 = p1;
    p_2 = p2;
    p_3 = p3;
    n_segments = 10;
    for(int i = 0 ; i != n_segments; ++i){
        VectorXr val = eval(real_t(i)/real_t(n_segments-1));
        p_segments.push_back(val);
    }
    initialized = true;
}
void BezierCurve::init(const  VectorXr& p0, const VectorXr& p1,
                       const  VectorXr& p2,const VectorXr& p3, const int & segments){
    p_0 = p0;
    p_1 = p1;
    p_2 = p2;
    p_3 = p3;
    n_segments = segments;

    for(int i = 0 ; i != segments; ++i){
        VectorXr val = eval(real_t(i)/real_t(segments-1));
        p_segments.push_back(val);
    }
    initialized = true;
}

VectorXr BezierCurve::eval(const real_t& t){
    //assert(initialized);
    using std::pow;
    return p_0*pow((1-t),3) + 3*p_1*t*pow((1-t),2) + 3*p_2*pow(t,2)*(1-t) + p_3*pow(t,3);
}

VectorXr BezierCurve::eval(const int& index){
    //assert(initialized);
    //assert(index < n_segments);

    if(index < p_segments.size())
        return p_segments[index];
    else
        return p_segments.back();
}

VectorXr BezierCurve::Eval(const real_t& t,const  VectorXr& p0, const VectorXr& p1,
                        const  VectorXr& p2,const VectorXr& p3){
 using std::pow;
 return p0*pow((1-t),3) + 3*p1*t*pow((1-t),2) + 3*p2*pow(t,2)*(1-t) + p3*pow(t,3);

}


VectorXr BezierCurve::eval(const real_t& t,const  VectorXr& p0, const VectorXr& p1,
                           const  VectorXr& p2,const VectorXr& p3){
    using std::pow;
    return p0*pow((1-t),3) + 3*p1*t*pow((1-t),2) + 3*p2*pow(t,2)*(1-t) + p3*pow(t,3);

}

VectorXr BezierCurve::eval(const int & index, const int& segments, const  VectorXr& p0, const VectorXr& p1,
                           const  VectorXr& p2,const VectorXr& p3){
    using std::pow;
    real_t  t = real_t(index)/real_t(segments);
    return p0*pow((1-t),3) + 3*p1*t*pow((1-t),2) + 3*p2*pow(t,2)*(1-t) + p3*pow(t,3);
}


VectorXr BezierCurve::eval(const real_t& t,const  VectorXr& p0, const VectorXr& p1,
                           const  VectorXr& p2,const VectorXr& p3, const bool & rewritePoints){
    if(rewritePoints){
        p_0 = p0;
        p_1 = p1;
        p_2 = p2;
        p_3 = p3;
    }
    using std::pow;
    return p0*pow((1-t),3) + 3*p1*t*pow((1-t),2) + 3*p2*pow(t,2)*(1-t) + p3*pow(t,3);

}

VectorXr BezierCurve::eval(const int & index, const int& segments, const  VectorXr& p0, const VectorXr& p1,
                           const  VectorXr& p2,const VectorXr& p3, const bool & rewritePoints){

    if(rewritePoints){
        p_0 = p0;
        p_1 = p1;
        p_2 = p2;
        p_3 = p3;

        using std::pow;

    }
    real_t  t = real_t(index)/real_t(segments);
    return p0*pow((1-t),3) + 3*p1*t*pow((1-t),2) + 3*p2*pow(t,2)*(1-t) + p3*pow(t,3);
}

BezierCurve::~BezierCurve(){

}

}
}
