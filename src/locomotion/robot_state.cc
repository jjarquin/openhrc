/*
 *
 * Copyright (C) 2015
 * Julio Jarquin <jjarquin.ct@gmail.com>, Gustavo Arechavaleta <garechav@cinvestav.edu.mx>
 * CINVESTAV - Saltillo Campus
 *
 * This file is part of OpenHRC
 * OpenHRC is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * OpenHRC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301  USA
 */

/**
 *	\file src/locomotion/robot_state.cc
 *	\author Julio Jarquin, Gustavo Arechavaleta
 *	\version 1.0
 *	\date 2015
 *
 *	Implementation of the RobotState class.
 */


#include "openhrc/locomotion/robot_state.h"

namespace hr{
namespace locomotion{


void RobotState::setX(const Vector3r& xk){
    (this)->segment(0,3) = xk;
}

void RobotState::setY(const Vector3r& yk){
    (this)->segment(3,3) = yk;
}

void RobotState::setTheta(const Vector3r& thetak){
    (this)->segment(6,3) = thetak;
}
void RobotState::setState(const Vector3r& xk,const Vector3r& yk,const Vector3r& thetak){
    setX(xk);
    setY(yk);
    setTheta(thetak);
}


}
}
