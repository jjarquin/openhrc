OpenHRC - Open Source Humanoid Robot Control Library

=========================================

OpenHRC is an object-oriented library for real-time control of Humanoid Robots. It has a modular architecture. 
It is written in C++, its main purpose is to provide methods and algorithms to develop Humanoid Robot controllers.


Authors
-------
Julio Jarquin - main developer (jjarquin.ct@gmail.com)
Gustavo Arechavaleta - main developer ()

Download
--------



Install
-------


Contribute
----------



