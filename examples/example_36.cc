// example 36 - Simple visual servo control using VMPC with Visual Constraints, The VisionModule and ALMotion


#include "openhrc/vision.h"
#include "qpOASES.hpp"

#include <boost/timer/timer.hpp>
#include <boost/shared_ptr.hpp>
#include "boost/date_time/posix_time/posix_time.hpp"

#include <alproxies/almotionproxy.h>
#include <alcommon/alproxy.h>
#include <alvalue/alvalue.h>
#include <iostream>
#include <fstream>

#include <vector>
#include <unistd.h>


namespace pt = boost::posix_time;

using boost::timer::cpu_timer;
using std::cout;
using std::endl;

int main(){

    std::ofstream file_current;
    std::ofstream file_desired;
    std::ofstream file_error;
    std::ofstream file_control;


    file_current.open("ex_current_features_36.dat");
    file_desired.open("ex_desired_features_36.dat");
    file_error.open("ex_error_features_36.dat");
    file_control.open("ex_control_36.dat");


    /** VMPC parameters */
    int N_horizon = 1;
    int n_camera_dof = 6;
    int n_features = 8;
    hr::real_t Ts = 0.1;
    hr::VectorXr uk(n_camera_dof*N_horizon);

    /** These parameters describe the camera */
    hr::Matrix3r kMatrix;
    hr::MatrixX6r interactionMatrix;
    hr::VectorXr currentPixelFeatures(n_features);
    hr::VectorXr desiredPixelFeatures(n_features);
    hr::VectorXr currentFeatures(n_features);
    hr::VectorXr desiredFeatures(n_features);
    hr::VectorXr desiredHorizonFeatures(n_features*N_horizon);

    hr::Matrix3r rotCamera;
    rotCamera << 0, 0, 1, -1, 0, 0, 0, -1, 0;

    hr::SpatialMatrix cameraTransformation;
    cameraTransformation << rotCamera, hr::Matrix3r::Zero(), hr::Matrix3r::Zero(), rotCamera;

    /** Initialization of the camera parameters */
    kMatrix << 531.712 , 0, 318.244, 0, 532.662, 250.698, 0,  0,  1;
    hr::vision::Camera camera(kMatrix);

    /** Nao proxies */
    AL::ALMotionProxy motionProxy = AL::ALMotionProxy("nao.local");
    //AL::ALMotionProxy motionProxy = AL::ALMotionProxy("127.0.0.1");

    AL::ALValue configuration;
    configuration.arraySetSize(1);
    configuration[0].arraySetSize(2);
    configuration[0][0] =  AL::ALValue("ENABLE_FOOT_CONTACT_PROTECTION");
    configuration[0][1] =   AL::ALValue(false);
    motionProxy.setMotionConfig(configuration);
    motionProxy.walkInit();

    sleep(1);//wait 1 second for the features to stabilize

    //! VisionModule
    AL::ALProxy visionProxy = AL::ALProxy("VisionModuleTest","nao.local",9559);

    //! Extract initial and target features
    std::vector<float> current = visionProxy.call<std::vector<float> >("getCurrentFeatures");
    std::vector<float> target = visionProxy.call<std::vector<float> >("getTargetFeatures");

    //! Convert pixel features to image features
    Eigen::VectorXf currentPixelFeats(n_features);
    Eigen::VectorXf targetPixelFeats(n_features);
    currentPixelFeats = Eigen::VectorXf::Map(current.data(),n_features);
    targetPixelFeats = Eigen::VectorXf::Map(target.data(),n_features);
    cout << "Desired features (m) " << targetPixelFeats.transpose() <<endl;
    cout << "Initial features (m) " << currentPixelFeats.transpose() <<endl;
    currentPixelFeatures = currentPixelFeats.cast<hr::real_t>();
    desiredPixelFeatures = targetPixelFeats.cast<hr::real_t>();

    //! Initialization of the VMPC
    hr::vision::VMPCData vmpcData = hr::vision::VMPCData(N_horizon,n_features,Ts,4.0,1.0); //

    camera.convertPoints(currentPixelFeatures,currentFeatures,hr::vision::kPixel2ImageCoordinates,true,hr::vision::k2DPoint);
    camera.convertPoints(desiredPixelFeatures,desiredFeatures,hr::vision::kPixel2ImageCoordinates,true,hr::vision::k2DPoint);

    for(int i = 0 ; i!= N_horizon ; i++){
        desiredHorizonFeatures.segment(i*n_features,n_features) = desiredFeatures;
      }
    interactionMatrix = camera.getInteractionMatrix(desiredFeatures, hr::vision::kImageCoordinates, hr::vision::k2DPoint,1.0);

    vmpcData.init(interactionMatrix);

    //! Control bounds and constraints

    hr::VectorXr lowerBounds(n_camera_dof);
    hr::VectorXr upperBounds(n_camera_dof);
    lowerBounds << -0.15, -1, -0.10, -1, -0.5, -1;
    upperBounds <<  0.15, 1,  0.10, 1,  0.5, 1;

    //Horizon bounds
    hr::VectorXr lBounds(n_camera_dof*N_horizon);
    hr::VectorXr uBounds(n_camera_dof*N_horizon);

    for(int i = 0 ; i!= N_horizon ; i++){
        lBounds.segment(i*n_camera_dof,n_camera_dof) = lowerBounds;
        uBounds.segment(i*n_camera_dof,n_camera_dof) = upperBounds;
      }

    hr::vision::VMPCConstraints vmpcConstraints;
    cout << "Constraints init " << endl;
    vmpcConstraints.init(vmpcData);
    cout << "initialization finished " << endl;
    //! Solver initialization

    int nWSR = 1000;
    qpOASES::real_t cpuTime = 1.0;

    int nV = n_camera_dof*N_horizon;
    int nC = 0;
    boost::shared_ptr<qpOASES::SQProblem> quadprog;
    qpOASES::Options options;
    options.setToMPC();
    options.printLevel = qpOASES::PL_NONE;
    quadprog.reset(new qpOASES::SQProblem(nV,nC));
    quadprog->setOptions(options);
    cout << "Solve qpoases" << endl;

    vmpcConstraints.setConstraints(vmpcData,currentFeatures);
    quadprog->init(vmpcData.getHessian().data(), vmpcData.getFvector(currentFeatures, desiredHorizonFeatures).data(),vmpcConstraints.getMatrixC().data(),lBounds.data(),uBounds.data(),NULL,vmpcConstraints.getVectord().data(),nWSR,&cpuTime);
    if(quadprog->getPrimalSolution(uk.data()) == qpOASES::RET_QP_NOT_SOLVED){
        cout << "failed to solve" << endl;
        return 0;
    }
    hr::VectorXr u(n_camera_dof);
    u = uk.head(n_camera_dof);
    cout << "Control output" << endl << u.transpose() << endl;
    hr::real_t x_speed = u(2);
    hr::real_t y_speed = -u(0);
    hr::real_t theta_speed = -u(4);

    //! Timing variables
    struct timespec time;

    pt::ptime current_date_microseconds = pt::microsec_clock::local_time();
    long milliseconds = current_date_microseconds.time_of_day().total_milliseconds();
    long initial_milliseconds = milliseconds;
    hr::real_t time_p = 0.0;




    hr::real_t T_sim = 30.0;
    int N = T_sim / Ts;
    for(int i = 0 ; i != N ; i ++ ){
        current_date_microseconds = pt::microsec_clock::local_time();
        milliseconds = current_date_microseconds.time_of_day().total_milliseconds() - initial_milliseconds;
        time_p = ((double) milliseconds)/1000.0;


        //! Get current features
        current = visionProxy.call<std::vector<float> >("getCurrentFeatures");
        currentPixelFeats = Eigen::VectorXf::Map(current.data(),n_features);
        currentPixelFeatures = currentPixelFeats.cast<hr::real_t>();
        camera.convertPoints(currentPixelFeatures,currentFeatures,hr::vision::kPixel2ImageCoordinates,true,hr::vision::k2DPoint);

        //! warmstart for the solver
        nWSR = 1000;
        cpuTime = 1.0;
        quadprog->setOptions(options);
        quadprog->hotstart(vmpcData.getFvector(currentFeatures, desiredHorizonFeatures).data(),lBounds.data(),uBounds.data(),NULL,NULL,nWSR,&cpuTime);
        if(quadprog->getPrimalSolution(uk.data()) == qpOASES::RET_QP_NOT_SOLVED){
            cout << "failed to solve" << endl;
            return 0;

        }
        //! Extract control
        u = uk.head(n_camera_dof);

        x_speed = 1.2*u(2);
        y_speed = -1.6*u(0);
        theta_speed = -u(4);

        //! Log data ---------------------------------------------------------------


        file_current << i*Ts << " " << time_p << " " << currentFeatures.transpose() << std::endl;
        file_desired <<  i*Ts << " " << time_p << " " << desiredFeatures.transpose() << std::endl;
        file_error <<  i*Ts << " " << time_p << " " << (currentFeatures - desiredFeatures).transpose() << std::endl;
        file_control <<  i*Ts << " " << time_p << " " << x_speed << " " <<  y_speed << " " << theta_speed << std::endl;

        //! end of Log data --


        cout << "Control output | x y theta | " << endl << x_speed << " " <<  y_speed << " " << theta_speed << endl;
        cout << "Squared norm error" << endl << (currentFeatures - desiredFeatures).norm() << endl;
        cout << "time_p " << time_p << endl;


        //! Move(Apply control) and Sleep for 90ms
        time.tv_sec = 0;
        time.tv_nsec = 90000000; //80ms

        motionProxy.move( (float) x_speed, (float) y_speed, (float) 0.0);

        nanosleep(&time,0);

    }

    std::cout << "Finished at time " << time_p << std::endl;

    //! Stop nao movement
    motionProxy.move(0.0,0.0,0.0);
    motionProxy.waitUntilMoveIsFinished();
    motionProxy.walkInit();

    file_current.close();
    file_desired.close();
    file_error.close();
    file_control.close();

}
