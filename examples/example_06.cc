// Example of use of the SimpleTask class

#include "openhrc/utils.h"
#include <iostream>



using std::cout;
using std::endl;

int main(){

    hr::utils::SimpleTask task_01;
    hr::MatrixXr A(2,10);
    hr::VectorXr bA(2);
    task_01.addRows(A,bA);
    hr::MatrixXr matrixTaskA = task_01.getMatrixA();
    hr::VectorXr vectorTaskb = task_01.getVectorb();
    cout<< "Matrix A: " << endl << matrixTaskA << endl;
    cout<< "Vector b: " << endl << vectorTaskb << endl;

    hr::MatrixXr B(2,10);
    B.setOnes();
    hr::VectorXr bB(2);
    bB.setConstant(1.5);
    task_01.addRows(B,bB);

    matrixTaskA = task_01.getMatrixA();
    vectorTaskb = task_01.getVectorb();
    cout<< "Matrix A: " << endl << matrixTaskA << endl;
    cout<< "Vector b: " << endl << vectorTaskb << endl;

    hr::MatrixXr C(2,10);
    hr::VectorXr bC(2);
    C.setConstant(2);
    bC.setConstant(2.5);
    task_01.addRows(C,bC);
    matrixTaskA = task_01.getMatrixA();
    vectorTaskb = task_01.getVectorb();
    cout<< "Matrix A: " << endl << matrixTaskA << endl;
    cout<< "Vector b: " << endl << vectorTaskb << endl;

}
