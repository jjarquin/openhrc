//Use of hr::vision::Camera

#include "openhrc/vision.h"
#include <iostream>
#include <boost/timer/timer.hpp>
using std::cout;
using std::endl;
using boost::timer::cpu_timer;

int main(){
    cpu_timer timer;
    hr::Matrix3r kMatrix;
    kMatrix << 531.712 , 0, 318.244, 0, 532.662, 250.698, 0,  0,  1;
    hr::VectorXr point3d(3);
    hr::VectorXr point4d(4);
    point3d << 0.0,0.0,0.5;
    point4d << 0.0,0.0,0.5,1.0;
    cout << "Camera matrix: " << endl << kMatrix << endl;
    cout << "Point3d " << point3d.transpose() << endl;
    cout << "Point4d " << point4d.transpose() << endl;

    hr::vision::Camera camera(kMatrix);

    hr::VectorXr destPoint3d(3);
    hr::VectorXr destPoint4d(4);

    camera.projectPoints(point3d,destPoint3d);
    camera.projectPoints(point4d,destPoint4d,hr::vision::k4DPoint);

    cout << "Projected Point3d " << destPoint3d.transpose() << endl;
    cout << "Projected Point4d " << destPoint4d.transpose() << endl;

    hr::VectorXr pixelPoints(2);
    hr::VectorXr imagePoints2d(2);
    hr::VectorXr imagePoints3d(3);
    pixelPoints << 640,480;
    timer.start();
    camera.convertPoints(pixelPoints,imagePoints3d,hr::vision::kPixel2ImageCoordinates);
    timer.stop();
    cout << "Execution time for 3d " << endl << timer.format() << endl;

    timer.start();
    camera.convertPoints(pixelPoints,imagePoints2d,hr::vision::kPixel2ImageCoordinates,true,hr::vision::k2DPoint);
    timer.stop();
    cout << "Execution time for 2d " << endl << timer.format() << endl;
    cout << "Projected imagePoints3d " << imagePoints3d.transpose() << endl;

    cout << "Projected imagePoints2d " << imagePoints2d.transpose() << endl;

    hr::MatrixX6r interactionMatrix;
    interactionMatrix = camera.getInteractionMatrix(imagePoints2d,hr::vision::kImageCoordinates,hr::vision::k2DPoint, 1.0);
    cout << "Interaction matrix: " << endl << interactionMatrix << endl;



    hr::VectorXr features(8);
    features << 224, 130, 341, 126, 348, 237, 229, 244;
    hr::VectorXr featuresInImageCoordinates(8);
    camera.convertPoints(features,featuresInImageCoordinates,hr::vision::kPixel2ImageCoordinates,true,hr::vision::k2DPoint);

    interactionMatrix = camera.getInteractionMatrix(featuresInImageCoordinates,hr::vision::kImageCoordinates,hr::vision::k2DPoint, 1.0);
    cout << "Interaction matrix: " << endl << interactionMatrix << endl;


}

