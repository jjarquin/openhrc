
//example 13, Generation of FootTrajectories

#include "openhrc/utils.h"
#include "openhrc/core.h"
#include "openhrc/kinematics.h"

#include <iostream>
#include <fstream>

using std::cout;
using std::endl;

int main(){
    std::ofstream myfile;
    myfile.open("example_16.dat");

    hr::SpatialVector xi;
    hr::SpatialVector xf;

    hr::SpatialVector x1;
    hr::SpatialVector x2;

    hr::SpatialVector dxi;
    hr::SpatialVector dxf;

    xi << 0, 0, 0.0, 0, 0, 0;
    xf << 0.0, 0, 0.0, 0, 0, 0;

    x1 << 0, 0, 0, 0, 0, 0;
    x2 << 0, 0, 0, 0, 0, 0;

    dxi << 0, 0, 0, 0, 0, 0;
    dxf << 0, 0, 0, 0, 0, 0;

    hr::real_t ti = 0.0;
    hr::real_t tf = 0.8;
    hr::real_t dt = 0.01;
    int N = 1.5*(tf)/dt;
    hr::real_t t = 0.0;

    hr::core::TrajectoryAbstract * traj;
    traj = new hr::core::FootTrajectory(ti,tf,xi,xf,hr::core::kFootCycloidal);
    //traj = new hr::core::PointTrajectory(ti,tf,xi,xf,hr::core::kQuinticPolynomial);

    hr::SpatialVector x;
    hr::SpatialVector dx;
    hr::SpatialVector dxN;

    hr::SpatialVector ddx;

    hr::real_t sys_y = 0.0;
    hr::real_t sys_dy = 0;
    hr::real_t lambda = 0.0;
    hr::real_t error = 0.0;


    hr::real_t int_x = xi(0);
    hr::real_t int_dx = (traj->getVelocity(ti))(0);

    hr::real_t int_z = xi(2);
    hr::real_t int_dz = (traj->getVelocity(ti))(2);

    for (int i = 0 ; i != N ; i ++){
        x = traj->getPose(t);
        dx = traj->getVelocity(t);
        dxN = traj->getVelocity(t-dt);
        ddx = traj->getAcceleration(t);
        cout << "Time: " <<  t << endl;

        cout << "Pose" << endl;
        cout <<  x.transpose() << endl;
        cout << "Twist" << endl;
        cout <<  dx.transpose() << endl;
        int_x = int_x + dt*dx(0);
        int_dx = int_dx + dt*ddx(0);


        error = sys_y - x(2);
        sys_dy = -lambda*error+dx(2);
        cout << "posexyz " << traj->getPose(t).head(3).transpose() << endl;
        // t x dx ddx  z dz ddz  ix idx  iz idz
        myfile << t << " " << x(0) << " " << dx(0) << " " << ddx(0) << " " << x(2) << " " << dx(2) << " " << ddx(2) << " " << int_x << " " << int_dx<< " " << sys_y << " " << sys_dy << endl;

        int_z = int_z + dt*dx(2);
        int_dz = int_dz + dt*ddx(2);
        sys_y = sys_y + dt*sys_dy;
        t = t + dt;
    }
}



