
//Example of use of KinematicTask and SimpleHierarchicalSolver class

#include "openhrc/utils.h"
#include "openhrc/core.h"
#include "openhrc/kinematics.h"
#include <alproxies/almotionproxy.h>
#include <alerror/alerror.h>
#include <iostream>

#ifdef  USE_LOCAL_XBOT //
    std::string naoFile = "../../../src/data/Nao_blue_noninertial.xbot";
#else
    std::string naoFile = "/home/nao/projects/hrData/Nao_blue_noninertial.xbot";
#endif

using std::cout;
using std::endl;

hr::VectorXr getNaoConfig(AL::ALMotionProxy & motionProxy);
bool setNaoConfig(AL::ALMotionProxy & motionProxy, const hr::VectorXr & q, hr::real_t time );

void hrToAl(const hr::VectorXr & q_hr , hr::VectorXr & q_al){
    const int kNAO_DOF = 26;
    int jointId[kNAO_DOF] = {7,8,9,10,11,12,13,-1,19,20,21,22,23,24,-1,26,27,28,29,30,14,15,16,17,18,-1};
    q_al.setZero();
    for (int i = 0 ; i < kNAO_DOF; i++) {
        if(jointId[i] != -1) {
            q_al(i) = q_hr(jointId[i]-1);
        }
    }
}

void alToHr(hr::VectorXr & q_hr , const hr::VectorXr & q_al){
    const int kNAO_DOF = 26;
    int jointId[kNAO_DOF] = {7,8,9,10,11,12,13,-1,19,20,21,22,23,24,-1,26,27,28,29,30,14,15,16,17,18,-1};
    q_hr.setZero();
    for (int i = 0 ; i < kNAO_DOF; i++) {
        if(jointId[i] != -1) {
            q_hr(jointId[i]-1) = q_al(i);
        }
    }

}


int main(){

    hr::core::World world = hr::core::World();
    std::string sFile = naoFile;
    int robotId = world.getRobotsVector()->size();
    world.loadMultiBody(sFile,robotId, hr::core::kNAO);
    boost::shared_ptr<hr::core::MultiBody> robot = world.getRobot(0);
    hr::VectorXr upSpeedLimit = robot->getJointUpSpeedLimits();
    hr::VectorXr lowSpeedLimit = robot->getJointLowSpeedLimits();
    hr::VectorXr lowLimit = robot->getJointLowLimits();
    hr::VectorXr upLimit = robot->getJointUpLimits();

    hr::VectorXr q = robot->getConfiguration();
    hr::VectorXr qtmp(q.rows()-1);

    cout << "Robot configuration (" << q.rows() <<"x1)" << endl << q << endl;

    AL::ALMotionProxy motionProxy = AL::ALMotionProxy("127.0.0.1");

    setNaoConfig(motionProxy,q,1);
    q = getNaoConfig(motionProxy);
    robot->setConfiguration(q);
    robot->computeForwardKinematics();

    cout << "Robot configuration" << endl << q << endl;

    hr::Vector3r rightFootHandle(0.0,0.01,-0.04);
    int rightFootBodyId = 30;
    int rightFootHandleId = robot->addOperationalHandle(rightFootBodyId,rightFootHandle);

    hr::Vector3r leftFootHandle(0.0,0.01,-0.04);
    int leftFootBodyId = 24;
    int leftFootHandleId = robot->addOperationalHandle(leftFootBodyId,leftFootHandle);

    hr::Vector3r torsoHandle(0.0,0.0,0.0);
    int torsoBodyId = 6;
    int torsoHandleId = robot->addOperationalHandle(torsoBodyId,torsoHandle);

    cout << "Left foot position: " << endl << robot->getBodyPosition(leftFootBodyId) << endl;
    cout << "Right foot position: " << endl << robot->getBodyPosition(rightFootBodyId) << endl;
    cout << "Torso position: " << endl << robot->getBodyPosition(torsoBodyId) << endl;

    hr::SpatialVector leftFootGoal;
    hr::SpatialVector rightFootGoal;
    hr::SpatialVector torsoGoal;

    leftFootGoal << 0.00 , 0.05, -0.33 , 0.0 , 0.0 , 0.0;
    rightFootGoal << 0.0 , -0.05, -0.33 , 0.0, 0.0, 0.0;
    torsoGoal << 0.0, 0.0, 0.0, 0.0, 0.0, 0.0;

    hr::core::KinematicTask task_01(rightFootHandleId,rightFootBodyId,0,robot);
    task_01.setGoal(rightFootGoal);
    hr::core::KinematicTask task_02(leftFootHandleId,leftFootBodyId,0,robot);
    task_02.setGoal(leftFootGoal);
    hr::core::KinematicTask task_03(torsoHandleId,torsoBodyId,0,robot, (hr::core::kXY | hr::core::kWY));
    task_03.setGoal(torsoGoal);

    hr::utils::SimpleHierarchicalSolver solver;

    hr::real_t integrationStepSize = 0.1;


    hr::VectorXr lb_ = (robot->getJointLowLimits()-q)/integrationStepSize;
    hr::VectorXr ub_ = (robot->getJointUpLimits()-q)/integrationStepSize;

    hr::VectorXr lb(29);
    hr::VectorXr ub(29);

    lb.segment(0,24) = lb_.segment(0,24);
    lb.segment(24,5) = lb_.segment(25,5);

    ub.segment(0,24) = ub_.segment(0,24);
    ub.segment(24,5) = ub_.segment(25,5);

    hr::VectorXr ld(29);
    hr::VectorXr ud(29);
    hr::MatrixXr C;

    hr::VectorXr ud_ = robot->getJointUpSpeedLimits();
    hr::VectorXr ld_ = robot->getJointLowSpeedLimits();

    int n = 29;
    C = hr::MatrixXr::Identity(n,n);

    ld.segment(0,24) = ld_.segment(0,24);
    ld.segment(24,5) = ld_.segment(25,5);

    ud.segment(0,24) = ud_.segment(0,24);
    ud.segment(24,5) = ud_.segment(25,5);



    for(int i = 0 ; i != 100 ; i++){
        task_01.update();
        task_02.update();
        task_03.update();

        hr::utils::SimpleTask feetTask;
        feetTask.addRows(task_01.getMatrixA(),task_01.getVectorb());
        feetTask.addRows(task_02.getMatrixA(),task_02.getVectorb());

        std::vector<hr::utils::TaskAbstract*> taskList;
        taskList.push_back(&feetTask);
        taskList.push_back(&task_03);


       cout<< "Error Task 1: " << endl << task_01.getVectorb() << endl;
       cout<< "Error Task 2: " << endl << task_02.getVectorb() << endl;
       cout<< "Error Task 3: " << endl << task_03.getVectorb() << endl;

        hr::VectorXr dq = solver.Solve(taskList,C,ld,ud,lb,ub);

        qtmp.segment(0,24) = q.segment(0,24);
        qtmp.segment(24,5) = q.segment(25,5);
        qtmp += dq*integrationStepSize;
        q.segment(0,24) = qtmp.segment(0,24);
        q(24) = qtmp(18);
        q.segment(25,5) = qtmp.segment(24,5);

        //q = q + integrationStepSize*dq;
        setNaoConfig(motionProxy,q, 0.1);
        cout << " q: " << endl << q << endl;
        robot->setConfiguration(q);
        robot->computeForwardKinematics();

        lb_ = (robot->getJointLowLimits()-q)/integrationStepSize;
        ub_ = (robot->getJointUpLimits()-q)/integrationStepSize;

        lb.segment(0,24) = lb_.segment(0,24);
        lb.segment(24,5) = lb_.segment(25,5);

        ub.segment(0,24) = ub_.segment(0,24);
        ub.segment(24,5) = ub_.segment(25,5);
    }

}


hr::VectorXr getNaoConfig(AL::ALMotionProxy & motionProxy){
    const int kNAO_DOF = 26;
    const bool useSensors = true;

    AL::ALValue jointNames;
    jointNames = AL::ALValue::array("Body");
    std::vector<float> sensorAngles = motionProxy.getAngles(jointNames, useSensors);
    Eigen::Map<Eigen::VectorXf> q_al(sensorAngles.data(),26);

    hr::VectorXr q_hr(30);
    alToHr(q_hr, q_al.cast<hr::real_t>());
    return q_hr;

}

bool setNaoConfig(AL::ALMotionProxy & motionProxy, const hr::VectorXr & q_hr, hr::real_t time ){
    const int kNAO_DOF = 26;
    const bool isAbsolute = true;

    AL::ALValue alAngles, timeLists;
    alAngles.clear();
    alAngles.arraySetSize(kNAO_DOF);

    timeLists.clear();
    timeLists.arraySetSize(kNAO_DOF);

    hr::VectorXr q_al(kNAO_DOF);

    hrToAl(q_hr,q_al);


    AL::ALValue jointNames;
    jointNames = AL::ALValue::array("Body");

    for (int i = 0; i!=kNAO_DOF; i++) {
        timeLists[i] = AL::ALValue::array((float) time);
        alAngles[i] = q_al(i);
    }

    motionProxy.angleInterpolation(jointNames, alAngles, timeLists, isAbsolute);

    return true;

}



