//Example 34 - Simple visual servo control using VMPC, The VisionModule and ALMotion


#include "openhrc/vision.h"
#include "qpOASES.hpp"
#include <boost/timer/timer.hpp>
#include <boost/shared_ptr.hpp>
#include <alproxies/almotionproxy.h>
#include <alcommon/alproxy.h>
#include <vector>
#include <unistd.h>
#include <alvalue/alvalue.h>
#include <fstream>

#include "boost/date_time/posix_time/posix_time.hpp"
namespace pt = boost::posix_time;

using boost::timer::cpu_timer;
using std::cout;
using std::endl;

int main(){

    std::ofstream file_current;
    std::ofstream file_desired;
    std::ofstream file_error;
    std::ofstream file_control;


    file_current.open("ex_current_features_34.dat");
    file_desired.open("ex_desired_features_34.dat");
    file_error.open("ex_error_features_34.dat");
    file_control.open("ex_control_34.dat");
    cpu_timer timer;
    /** VMPC parameters */
    int N_horizon = 2;
    int n_camera_dof = 6;
    int n_features = 8;
    hr::real_t Ts = 1.0;
    //hr::real_t Ts = 0.8;
    //hr::real_t Ts = 1.6;
    hr::VectorXr uk;      ///Control vector
    uk.resize(n_camera_dof*N_horizon);
    uk.setZero();

    /** These parameters describe the camera */
    hr::Matrix3r kMatrix;
    hr::MatrixX6r interactionMatrix;
    hr::VectorXr currentPixelFeatures(n_features);
    hr::VectorXr desiredPixelFeatures(n_features);
    hr::VectorXr currentFeatures(n_features);
    hr::VectorXr desiredFeatures(n_features);
    hr::VectorXr errorFeatures(n_features);
    hr::VectorXr desiredHorizonFeatures(n_features*N_horizon);

    hr::Matrix3r rotCamera;
    rotCamera << 0, 0, 1, -1, 0, 0, 0, -1, 0;

    hr::SpatialMatrix cameraTransformation;
    cameraTransformation << rotCamera, hr::Matrix3r::Zero(), hr::Matrix3r::Zero(), rotCamera;

    /** Initialization of the camera parameters */
    kMatrix << 531.712 , 0, 318.244, 0, 532.662, 250.698, 0,  0,  1;
    hr::vision::Camera camera(kMatrix);

    AL::ALMotionProxy motionProxy = AL::ALMotionProxy("nao.local");
    //AL::ALMotionProxy motionProxy = AL::ALMotionProxy("127.0.0.1");

    AL::ALValue configuration;
    configuration.arraySetSize(1);
    configuration[0].arraySetSize(2);
    configuration[0][0] =  AL::ALValue("ENABLE_FOOT_CONTACT_PROTECTION");
    configuration[0][1] =   AL::ALValue(false);
    motionProxy.setMotionConfig(configuration);
    motionProxy.walkInit();

    sleep(1);//wait 1 second for the features to stabilize

    AL::ALProxy visionProxy = AL::ALProxy("VisionModuleTest","nao.local",9559);

    std::vector<float> current = visionProxy.call<std::vector<float> >("getCurrentFeatures");
    std::vector<float> target = visionProxy.call<std::vector<float> >("getTargetFeatures");

    Eigen::VectorXf currentPixelFeats(n_features);
    Eigen::VectorXf targetPixelFeats(n_features);
    currentPixelFeats = Eigen::VectorXf::Map(current.data(),8);
    targetPixelFeats = Eigen::VectorXf::Map(target.data(),8);
    cout << "Desired features (m) " << targetPixelFeats.transpose() <<endl;
    cout << "Current features (m) " << currentPixelFeats.transpose() <<endl;
    currentPixelFeatures = currentPixelFeats.cast<hr::real_t>();
    desiredPixelFeatures = targetPixelFeats.cast<hr::real_t>();

    /** Initialization of the VMPC */
    //hr::vision::VMPCData vmpcData = hr::vision::VMPCData(N_horizon,n_features,Ts,4.0,1.0); // 0.0005  , 1.0
    hr::vision::VMPCData vmpcData = hr::vision::VMPCData(N_horizon,n_features,Ts,0.05,1.0); // 0.0005  , 1.0
    camera.convertPoints(currentPixelFeatures,currentFeatures,hr::vision::kPixel2ImageCoordinates,true,hr::vision::k2DPoint);
    camera.convertPoints(desiredPixelFeatures,desiredFeatures,hr::vision::kPixel2ImageCoordinates,true,hr::vision::k2DPoint);

    for(int i = 0 ; i!= N_horizon ; i++){
        desiredHorizonFeatures.segment(i*n_features,n_features) = desiredFeatures;
    }

    interactionMatrix = camera.getInteractionMatrix(desiredFeatures, hr::vision::kImageCoordinates, hr::vision::k2DPoint,0.5);

    // interactionMatrix is of size n_features x n_camera_dof
    Eigen::HouseholderQR<hr::MatrixXr> J(interactionMatrix);
    hr::MatrixXr Q = J.householderQ();
    hr::MatrixXr Ra = Q.transpose() * interactionMatrix;
    hr::MatrixXr R = Ra.block(0,0,n_camera_dof,n_camera_dof);
    hr::MatrixXr Rt = Ra.transpose();
    Rt.block(0,0,n_camera_dof,n_camera_dof) = (R).inverse();
    cout << "got here" << endl;
    hr::MatrixXr pInv = Rt * Q.transpose();
    cout << "did this" << endl;


    vmpcData.init(interactionMatrix);


    std::cout << "Interaction matrix" << std::endl << interactionMatrix <<std::endl;


    hr::VectorXr lowerBounds(n_camera_dof);
    hr::VectorXr upperBounds(n_camera_dof);
    //lowerBounds << -0.15, 0, -0.10, 0, -0.5, 0;
    //upperBounds <<  0.15, 0,  0.10, 0,  0.5, 0;

//    lowerBounds << -1, -1, -1, -1, -1, -1;
//    upperBounds <<  1,  1,  1,  1,  1,  1;

    lowerBounds << -0.15, -1, -0.10, -1, -0.5, -1;
    upperBounds <<  0.15, 1,  0.10, 1,  0.5, 1;


    hr::VectorXr lBounds(n_camera_dof*N_horizon);
    hr::VectorXr uBounds(n_camera_dof*N_horizon);

    for(int i = 0 ; i!= N_horizon ; i++){
        lBounds.segment(i*n_camera_dof,n_camera_dof) = lowerBounds;
        uBounds.segment(i*n_camera_dof,n_camera_dof) = upperBounds;
      }

    int nWSR = 1000;
    qpOASES::real_t cpuTime = 1.0;

    int nV = n_camera_dof*N_horizon;
    int nC = 0;
    boost::shared_ptr<qpOASES::SQProblem> quadprog;
    qpOASES::Options options;
    options.setToMPC();
    options.printLevel = qpOASES::PL_NONE;
    quadprog.reset(new qpOASES::SQProblem(nV,nC));
    quadprog->setOptions(options);
    cout << "Solve qpoases" << endl;

    timer.start();
    quadprog->init(vmpcData.getHessian().data(), vmpcData.getFvector(currentFeatures, desiredHorizonFeatures).data(),NULL,lBounds.data(),uBounds.data(),NULL,NULL,nWSR,&cpuTime);
    if(quadprog->getPrimalSolution(uk.data()) == qpOASES::RET_QP_NOT_SOLVED){
        return 0;
        cout << "failed to solve init" << endl;
    }
    timer.stop();

    hr::VectorXr u(n_camera_dof);
    hr::VectorXr u_pInv(n_camera_dof);
    u = uk.head(n_camera_dof);

    cout << "Control output" << endl << u.transpose() << endl;
    hr::real_t gain = 1.0;
    hr::real_t x_speed = gain*u(2);
    hr::real_t y_speed = -gain*u(0);
    hr::real_t theta_speed = -gain*u(4);
    struct timespec time;

    pt::ptime current_date_microseconds = pt::microsec_clock::local_time();
    long milliseconds = current_date_microseconds.time_of_day().total_milliseconds();
    long initial_milliseconds = milliseconds;
    hr::real_t time_p = 0.0;

    motionProxy.move(0.0,0.0,0.0);
    for(int i = 0 ; i != 200 ; i ++ ){
        current_date_microseconds = pt::microsec_clock::local_time();
        milliseconds = current_date_microseconds.time_of_day().total_milliseconds() - initial_milliseconds;
        time_p = ((double) milliseconds)/1000.0;

        timer.start();
        //visionProxy.callVoid("processImage");  //a thread must be running
        current = visionProxy.call<std::vector<float> >("getCurrentFeatures");
        currentPixelFeats = Eigen::VectorXf::Map(current.data(),n_features);
        currentPixelFeatures = currentPixelFeats.cast<hr::real_t>();
        camera.convertPoints(currentPixelFeatures,currentFeatures,hr::vision::kPixel2ImageCoordinates,true,hr::vision::k2DPoint);

        nWSR = 1000;
        cpuTime = 1.0;
        quadprog->setOptions(options);
        //quadprog->hotstart(vmpcData.getHessian().data(), vmpcData.getFvector(currentFeatures, desiredHorizonFeatures).data(),NULL,lBounds.data(),uBounds.data(),NULL,NULL,nWSR,&cpuTime);
        quadprog->hotstart(vmpcData.getFvector(currentFeatures, desiredHorizonFeatures).data(),lBounds.data(),uBounds.data(),NULL,NULL,nWSR,&cpuTime);
        if(quadprog->getPrimalSolution(uk.data()) == qpOASES::RET_QP_NOT_SOLVED){

            cout << "failed to solve" << endl;
            time.tv_sec = 0;
            time.tv_nsec = 90000000; //90ms
            nanosleep(&time,0);

            //continue;
        }
        u = uk.head(n_camera_dof);
        errorFeatures = currentFeatures - desiredFeatures;
        u_pInv = - pInv*errorFeatures;
        x_speed = gain*u(2);
        y_speed = -gain*u(0);
        theta_speed = -gain*u(4);


        x_speed = gain*u_pInv(2);
        y_speed = -gain*u_pInv(0);
        theta_speed = -0.5*gain*u_pInv(4);

        //! Log data ---------------------------------------------------------------


        file_current << i*Ts << " " << time_p << " " << currentFeatures.transpose() << std::endl;
        file_desired <<  i*Ts << " " << time_p << " " << desiredFeatures.transpose() << std::endl;
        file_error <<  i*Ts << " " << time_p << " " << (currentFeatures - desiredFeatures).transpose() << std::endl;
        file_control <<  i*Ts << " " << time_p << " " << x_speed << " " <<  y_speed << " " << theta_speed << std::endl;

        //! end of Log data --

        timer.stop();

//        cout << "Current features (pixel) " << currentPixelFeatures.transpose() << endl;
//        cout << "Desired features (m) " << desiredFeatures.transpose() << endl;
//        cout << "Desired Horizon (m) " << desiredHorizonFeatures.transpose() << endl;

//        cout << "Current features (m) " << currentFeatures.transpose() << endl;
        cout << "Control output" << endl << u.transpose() << endl;
        cout << "Control output with pseudoinverse" << endl << u_pInv.transpose() << endl;
        cout << "Squared norm error" << endl << (currentFeatures - desiredFeatures).norm() << endl;

//        cout << "Execution time " << endl << timer.format()<<endl;
        std::cout << "time_p " << time_p << std::endl;

        time.tv_sec = 0;
        //time.tv_nsec = 100000000; //100ms
        time.tv_nsec = 90000000; //90ms

        motionProxy.move( (float) x_speed, (float) y_speed, (float) theta_speed);

        nanosleep(&time,0);
    }
    std::cout << "Finished at time " << time_p << std::endl;

    motionProxy.move(0.0,0.0,0.0);
    motionProxy.waitUntilMoveIsFinished();
    motionProxy.walkInit();



    file_current.close();
    file_desired.close();
    file_error.close();
    file_control.close();



    //    struct timespec time;
//    time.tv_sec = 1;
//    time.tv_nsec = 0;
//    sleep(1);
//    cout << "1" <<endl;
//    nanosleep(&time,0);
//    cout << "2" <<endl;
//    sleep(1);
//    cout << "3" <<endl;


}
