
//Example of use of KinematicTask and SimpleHierarchicalSolver to track a a circle using angleInterpolation from ALMotion

#include "openhrc/utils.h"
#include "openhrc/core.h"
#include "openhrc/kinematics.h"
#include <alproxies/almotionproxy.h>
#include <alerror/alerror.h>
#include <iostream>

#ifdef  USE_LOCAL_XBOT //
    std::string naoFile = "../../../src/data/Nao_blue_noninertial.xbot";
#else
    std::string naoFile = "/home/nao/projects/hrData/Nao_blue_noninertial.xbot";
#endif

using std::cout;
using std::endl;

hr::VectorXr getNaoConfig(AL::ALMotionProxy & motionProxy);
bool setNaoConfig(AL::ALMotionProxy & motionProxy, const hr::VectorXr & q, hr::real_t time );

void hrToAl(const hr::VectorXr & q_hr , hr::VectorXr & q_al){
    const int kNAO_DOF = 26;
    int jointId[kNAO_DOF] = {7,8,9,10,11,12,13,-1,19,20,21,22,23,24,25,26,27,28,29,30,14,15,16,17,18,-1};
    q_al.setZero();
    for (int i = 0 ; i < kNAO_DOF; i++) {
        if(jointId[i] != -1) {
            q_al(i) = q_hr(jointId[i]-1);
        }
    }

}

void alToHr(hr::VectorXr & q_hr , const hr::VectorXr & q_al){
    const int kNAO_DOF = 26;
    int jointId[kNAO_DOF] = {7,8,9,10,11,12,13,-1,19,20,21,22,23,24,-1,26,27,28,29,30,14,15,16,17,18,-1};
    q_hr.setZero();
    for (int i = 0 ; i < kNAO_DOF; i++) {
        if(jointId[i] != -1) {
            q_hr(jointId[i]-1) = q_al(i);
        }
    }
    q_hr(24) = q_hr(18);

}


int main(){

    hr::core::World world = hr::core::World();
    std::string sFile = naoFile;
    int robotId = world.getRobotsVector()->size();
    world.loadMultiBody(sFile,robotId, hr::core::kNAO);
    boost::shared_ptr<hr::core::MultiBody> robot = world.getRobot(0);
    hr::VectorXr upSpeedLimit = robot->getJointUpSpeedLimits();
    hr::VectorXr lowSpeedLimit = robot->getJointLowSpeedLimits();
    hr::VectorXr lowLimit = robot->getJointLowLimits();
    hr::VectorXr upLimit = robot->getJointUpLimits();

    hr::VectorXr q = robot->getConfiguration();
    hr::VectorXr qtmp(q.rows()-1);

    cout << "Robot configuration (" << q.rows() <<"x1)" << endl << q << endl;

    AL::ALMotionProxy motionProxy = AL::ALMotionProxy("127.0.0.1");
    //AL::ALMotionProxy motionProxy = AL::ALMotionProxy("169.254.200.114");
    //setNaoConfig(motionProxy,q,1);
    q = getNaoConfig(motionProxy);
    robot->setConfiguration(q);
    robot->computeForwardKinematics();

    cout << "Robot configuration" << endl << q << endl;

    hr::Vector3r rightFootHandle(0.0,0.0,-0.04);
    int rightFootBodyId = 30;
    int rightFootHandleId = robot->addOperationalHandle(rightFootBodyId,rightFootHandle);

    hr::Vector3r leftFootHandle(0.0,0.0,-0.04);
    int leftFootBodyId = 24;
    int leftFootHandleId = robot->addOperationalHandle(leftFootBodyId,leftFootHandle);
//Absolute position : [ 0, 0, 0.1265 ] TORSO IS 6 AND HEAD IS 8
    hr::Vector3r torsoHandle(0.0,0.0,0.0);
    int torsoBodyId = 6;
    int torsoHandleId = robot->addOperationalHandle(torsoBodyId,torsoHandle);

    hr::Vector3r rightHandHandle(0.08,0.0,0.0);
    int rightHandBodyId = 18;
    int rightHandHandleId = robot->addOperationalHandle(rightHandBodyId,rightHandHandle);

    //cout << "Left foot position: " << endl << robot->getOperationalHandlePosition(leftFootBodyId,leftFootHandleId) << endl;
    //cout << "Right foot position: " << endl << robot->getOperationalHandlePosition(rightFootBodyId,rightFootHandleId) << endl;
    //cout << "Torso position: " << endl << robot->getBodyPosition(torsoBodyId) << endl;


    //cout << "Left foot orientation: " << endl << robot->getBodyOrientation(leftFootBodyId).eulerAngles(0,1,2) << endl;
    //cout << "Right foot orientation: " << endl << robot->getBodyOrientation(rightFootBodyId).eulerAngles(0,1,2) << endl;
    //cout << "Torso orientation: " << endl << robot->getBodyOrientation(torsoBodyId).eulerAngles(0,1,2) << endl;



    hr::SpatialVector leftFootGoal;
    hr::SpatialVector rightFootGoal;
    hr::SpatialVector torsoGoal;
    hr::SpatialVector rightHandGoal;
    //fot is at -0.3279 in Z
    leftFootGoal << 0.0, 0.05, -0.3279 , 0.0 , 0.0 , 0.0;
    rightFootGoal << 0.0, -0.05, -0.3279 , 0.0, 0.0, 0.0;
    torsoGoal << 0.0, 0.0, -0.1, 0.0, 0.0, 0.0;

    hr::Vector3r rightHandPosition = robot->getOperationalHandlePosition(rightHandBodyId,rightHandHandleId);
    hr::Vector3r rightHandOrientation = robot->getOperationalHandleOrientation(rightHandBodyId,rightHandHandleId).eulerAngles(0,1,2);
    //cout << "Right hand position "<< endl << rightHandPosition << endl;
    //cout << "Right hand orientation "<< endl << rightHandOrientation << endl;


    rightHandGoal << 0.15, -0.15, 0.0, 0.0, 0.0,0.0;

    hr::core::KinematicTask task_01(rightFootHandleId,rightFootBodyId,0,robot, hr::core::kPositionOrientation);
    task_01.setGoal(rightFootGoal);
    hr::core::KinematicTask task_02(leftFootHandleId,leftFootBodyId,0,robot, hr::core::kPositionOrientation);
    task_02.setGoal(leftFootGoal);
    hr::core::KinematicTask task_03(torsoHandleId,torsoBodyId,0,robot, (  hr::core::kWXY | hr::core::kXY ));
    task_03.setGoal(torsoGoal);
    hr::core::KinematicTask task_04(rightHandHandleId,rightHandBodyId,0,robot, (hr::core::kXZ | hr::core::kWXZ ));
    task_04.setGoal(rightHandGoal);

    hr::utils::SimpleHierarchicalSolver solver;

    hr::real_t integrationStepSize = 0.06;


    hr::VectorXr lb_ = (robot->getJointLowLimits()-q)/integrationStepSize;
    hr::VectorXr ub_ = (robot->getJointUpLimits()-q)/integrationStepSize;


    hr::VectorXr lb(29);
    hr::VectorXr ub(29);

    lb.segment(0,24) = lb_.segment(0,24);
    lb.segment(24,5) = lb_.segment(25,5);

    ub.segment(0,24) = ub_.segment(0,24);
    ub.segment(24,5) = ub_.segment(25,5);
    //lb.setConstant(-1.15);
    //ub.setConstant(1.15);
    hr::VectorXr ld(29);
    hr::VectorXr ud(29);
    hr::MatrixXr C;

    hr::VectorXr ud_ = robot->getJointUpSpeedLimits();
    hr::VectorXr ld_ = robot->getJointLowSpeedLimits();

    int n = 29;
    C = hr::MatrixXr::Identity(n,n);

    ld.segment(0,24) = ld_.segment(0,24);
    ld.segment(24,5) = ld_.segment(25,5);

    ud.segment(0,24) = ud_.segment(0,24);
    ud.segment(24,5) = ud_.segment(25,5);


    hr::real_t t = 0.0;

    //cout << "Hip max vel is" << endl << ud(18) << endl;
    //cout << "Hip low vel is" << endl << ld(18) << endl;
    //return 0; /// >>>>>>>>>>>

    for(int i = 0; i != 200; i++){
        task_01.update();
        task_02.update();
        task_03.update();

        // begin circle
            hr::real_t x = 0.15;
            hr::real_t dx = 0.0;
            hr::real_t z = 0.0;
            hr::real_t dz = 0.0;
            //x = 0.2+0.1*cos(t/(integrationStepSize*20)*2*3.1415);
            //dx = -1.0/(integrationStepSize*20)*2*3.1415*0.10*sin(t/(integrationStepSize*20)*2*3.1415);
            z =  0.0+0.20*sin(t/(integrationStepSize*50)*2*3.1415);
            dz =  1.0/(integrationStepSize*50)*2*3.1415*0.20*cos(t/(integrationStepSize*50)*2*3.1415);
            rightHandGoal(0) = x;
            rightHandGoal(2) = z;
            //cout << "Z: " << z << endl;
            hr::Vector3r rightHandPosition = robot->getOperationalHandlePosition(rightHandBodyId,rightHandHandleId);

            //out << "Z total: " << rightHandPosition(2)<< endl;
            task_04.setGoal(rightHandGoal);
            task_04.update();

        // end circle

        hr::utils::SimpleTask feetTask;
        feetTask.addRows(task_01.getMatrixA(),task_01.getVectorb());
        feetTask.addRows(task_02.getMatrixA(),task_02.getVectorb());
        //cout << "jacobian torso" << endl << task_03.getMatrixA().transpose() << endl;


        hr::utils::SimpleTask handTask;

        hr::VectorXr handError = task_04.getVectorb();
        handError(0) = dx + handError(0);
        handError(1) = dz + handError(1);
        handTask.addRows(task_04.getMatrixA(),handError);

        //cout << "jacobian right foot" << endl << task_01.getMatrixA().transpose() << endl;

        std::vector<hr::utils::TaskAbstract*> taskList;
        taskList.push_back(&feetTask);
        taskList.push_back(&task_03);
        //taskList.push_back(&task_02);
        taskList.push_back(&handTask);


       cout<< "Error Task 1: " << endl << task_01.getVectorb() << endl;
       cout<< "Error Task 2: " << endl << task_02.getVectorb() << endl;
       cout<< "Error Task 3: " << endl << task_03.getVectorb() << endl;
       cout<< "Error Task 4: " << endl << task_04.getVectorb() << endl;


        hr::VectorXr dq = solver.Solve(taskList,C,ld,ud,lb,ub);

        qtmp.segment(0,24) = q.segment(0,24);
        qtmp.segment(24,5) = q.segment(25,5);
        qtmp += dq*integrationStepSize;
        t = t + integrationStepSize;
        q.segment(0,24) = qtmp.segment(0,24);
        q(24) = qtmp(18);
        q.segment(25,5) = qtmp.segment(24,5);

        //cout << "dq is" << endl <<dq.transpose() << endl;
        //q = q + integrationStepSize*dq;
        setNaoConfig(motionProxy,q, integrationStepSize);
        //q = getNaoConfig(motionProxy);


        //cout << " q: " << endl << q << endl;
        robot->setConfiguration(q);
        robot->computeForwardKinematics();

        lb_ = (robot->getJointLowLimits()-q)/integrationStepSize;
        ub_ = (robot->getJointUpLimits()-q)/integrationStepSize;

        lb.segment(0,24) = lb_.segment(0,24);
        lb.segment(24,5) = lb_.segment(25,5);

        ub.segment(0,24) = ub_.segment(0,24);
        ub.segment(24,5) = ub_.segment(25,5);

        //cout << "ub " << endl << ub.transpose() << endl;
        //cout << "lb " << endl << lb.transpose() << endl;


        //cout << "ud " << endl << ud.transpose() << endl;
        //cout << "ld " << endl << ld.transpose() << endl;
    }

}


hr::VectorXr getNaoConfig(AL::ALMotionProxy & motionProxy){
    const int kNAO_DOF = 26;
    const bool useSensors = true;

    AL::ALValue jointNames;
    jointNames = AL::ALValue::array("Body");
    std::vector<float> sensorAngles = motionProxy.getAngles(jointNames, useSensors);
    Eigen::Map<Eigen::VectorXf> q_al(sensorAngles.data(),26);

    hr::VectorXr q_hr(30);
    alToHr(q_hr, q_al.cast<hr::real_t>());
    return q_hr;

}

bool setNaoConfig(AL::ALMotionProxy & motionProxy, const hr::VectorXr & q_hr, hr::real_t time ){
    const int kNAO_DOF = 26;
    const bool isAbsolute = true;

    AL::ALValue alAngles, timeLists;
    alAngles.clear();
    alAngles.arraySetSize(kNAO_DOF);

    timeLists.clear();
    timeLists.arraySetSize(kNAO_DOF);

    hr::VectorXr q_al(kNAO_DOF);

    hrToAl(q_hr,q_al);


    AL::ALValue jointNames;
    jointNames = AL::ALValue::array("Body");
    for (int i = 0; i!=kNAO_DOF; i++) {
        timeLists[i] = AL::ALValue::array((float) time);
        alAngles[i] = q_al(i);
    }

    motionProxy.angleInterpolation(jointNames, alAngles, timeLists, isAbsolute);

    return true;

}




