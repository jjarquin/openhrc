
// example 39 Visual Driven Walking Pattern Generator without Inverse Kinematics


#include <iostream>
#include <fstream>
#include <vector>
#include <unistd.h>

#include "openhrc/utils.h"
#include "openhrc/core.h"
#include "openhrc/kinematics.h"
#include "openhrc/locomotion.h"
#include "openhrc/vision.h"

#include "qpOASES.hpp"


#include <alproxies/almotionproxy.h>
#include <alproxies/almemoryproxy.h>
#include <alcommon/alproxy.h>
#include <alvalue/alvalue.h>
#include <alerror/alerror.h>

#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <boost/timer/timer.hpp>
#include "boost/date_time/posix_time/posix_time.hpp"

namespace pt = boost::posix_time;

#ifdef  USE_LOCAL_XBOT //
std::string naoFile = "../../../src/data/Nao_blue_noninertial.xbot";
#else
std::string naoFile = "/home/nao/projects/hrData/Nao_blue_noninertial.xbot";
#endif

using boost::timer::cpu_timer;
using std::cout;
using std::endl;


int main(){
    cpu_timer timer;

    std::ofstream myfile;
    myfile.open ("example_26.dat");
    std::ofstream myfile_joints;
    myfile_joints.open ("example_26_joints.dat");
    std::ofstream myfile_feet;
    myfile_feet.open ("example_26_feet.dat");
    std::ofstream myfile_in;
    myfile_in.open ("example_26_in.dat");

    /** MPC parameters */
    int m_steps = 2;
    int n_periods_step = 8; //15
    int N_horizon = m_steps*n_periods_step;
    hr::real_t T_sampling = 0.1;
    hr::real_t com_height = 0.265 ;


    //    hr::real_t jerk_gain = 0.001; // alpha
    //    hr::real_t velocity_gain = 0.001; // beta
    //    hr::real_t zmp_gain = 1; // gama

    //    hr::real_t jerk_gain = 0.0001; // alpha
    //    hr::real_t velocity_gain = 1; // beta
    //    hr::real_t zmp_gain = 4; // gama

    hr::real_t jerk_gain = 0.1; // alpha
    hr::real_t velocity_gain = 100; // beta
    hr::real_t zmp_gain = 1; // gama


    /** VMPC parameters */
    int n_features = 8;

    /** These parameters describe the camera */
    hr::Matrix3r kMatrix;
    hr::MatrixX6r interactionMatrix;
    hr::VectorXr currentPixelFeatures(n_features);
    hr::VectorXr desiredPixelFeatures(n_features);
    hr::VectorXr currentFeatures(n_features);
    hr::VectorXr desiredFeatures(n_features);
    hr::VectorXr desiredHorizonFeatures(n_features*N_horizon);

    //    hr::Matrix3r rotCamera;
    //    rotCamera << 0, 0, 1, -1, 0, 0, 0, -1, 0;

    //    hr::SpatialMatrix cameraTransformation;
    //    cameraTransformation << rotCamera, hr::Matrix3r::Zero(), hr::Matrix3r::Zero(), rotCamera;

    /** Initialization of the camera parameters */
    kMatrix << 531.712 , 0, 318.244, 0, 532.662, 250.698, 0,  0,  1;
    hr::vision::Camera camera(kMatrix);
    hr::SpatialMatrix matrixTransform;
    matrixTransform.setZero();
    matrixTransform.col(0) << 0, 0, 1, 0, 0, 0;
    matrixTransform.col(1) << -1, 0, 0, 0, 0, 0;


    //! VisionModule
    AL::ALProxy visionProxy = AL::ALProxy("VisionModuleTest","nao.local",9559);

    //! Extract initial and target features
    std::vector<float> current = visionProxy.call<std::vector<float> >("getCurrentFeatures");
    std::vector<float> target = visionProxy.call<std::vector<float> >("getTargetFeatures");

    //! Convert pixel features to image features
    Eigen::VectorXf currentPixelFeats(n_features);
    Eigen::VectorXf targetPixelFeats(n_features);
    currentPixelFeats = Eigen::VectorXf::Map(current.data(),n_features);
    targetPixelFeats = Eigen::VectorXf::Map(target.data(),n_features);
    cout << "Desired features (m) " << targetPixelFeats.transpose() <<endl;
    cout << "Initial features (m) " << currentPixelFeats.transpose() <<endl;
    currentPixelFeatures = currentPixelFeats.cast<hr::real_t>();
    desiredPixelFeatures = targetPixelFeats.cast<hr::real_t>();




    /** These parameters describe the state of the robot */
    hr::real_t z_foot = -0.32;
    hr::Vector3r footState;    //Member for the position of the current foot on the ground
    hr::Vector3r nextFootState;    //Member for the position of the current foot on the ground
    hr::locomotion::RobotState robotState;        //Member for the augmented CoM robot state
    hr::Vector3r xk;              //Variable for the CoM robot state in X
    hr::Vector3r yk;              //Variable for the CoM robot state in Y
    hr::Vector3r thetak;          //Variable for the CoM robot state in Theta

    hr::core::FootStatus footStatus = hr::core::kRightFootOnGround;
    hr::core::FootStatus mpcFootStatus = hr::core::kRightFootOnGround;
    hr::Vector3r jerkXYTheta; //Jerk holder variable
    hr::VectorXr uk;      ///Jerk vector
    uk.resize(2*(N_horizon+m_steps));
    uk.setZero();
    /** Initialization of the robot parameters */

    cout << "Init robot parameters" << endl;
    footState << 0.0, -0.05 , -0.32;   //For this example the left foot is the starting foot.  //TODO: 04/2015: I would prefer footPosition instead of state
    nextFootState << 0 , 0 ,z_foot;
    xk << 0.0, 0.0, 0.0;
    yk << -0.05, 0.0, 0.0;

    thetak << 0.0, 0.0 ,0.0;
    robotState.setState(xk,yk,thetak);
    jerkXYTheta << 0.0,0.0,0.0;




    //! Extract points

    camera.convertPoints(currentPixelFeatures,currentFeatures,hr::vision::kPixel2ImageCoordinates,true,hr::vision::k2DPoint);
    camera.convertPoints(desiredPixelFeatures,desiredFeatures,hr::vision::kPixel2ImageCoordinates,true,hr::vision::k2DPoint);

    //! Debuggin features
    //currentFeatures << -0.0400, -0.0400, -0.0400, 0.0400, 0.0400, 0.0400,  0.0400, -0.0400;
    //desiredFeatures << -0.2000, -0.2000, -0.2000, 0.2000, 0.2000, 0.2000, 0.2000, -0.2000;

//    currentFeatures << -0.2000, -0.2000, -0.2000, 0.2000, 0.2000, 0.2000, 0.2000, -0.2000;
//    desiredFeatures << -0.0400, -0.0400, -0.0400, 0.0400, 0.0400, 0.0400,  0.0400, -0.0400;

    currentFeatures << -0.4000, -0.0667, -0.4000, 0.0667, -0.2667, 0.0667,-0.2667, -0.0667;
    desiredFeatures << -0.0667, -0.0667, -0.0667, 0.0667, 0.0667,  0.0667, 0.0667, -0.0667;

    for(int i = 0 ; i!= N_horizon ; i++){
        desiredHorizonFeatures.segment(i*n_features,n_features) = desiredFeatures;
    }
    interactionMatrix = camera.getInteractionMatrix(desiredFeatures, hr::vision::kImageCoordinates, hr::vision::k2DPoint,1.0);


    /** Initialization of the MPC */

    cout << "Create mpcOject" << endl;

    hr::vision::VWMPCData mpcData = hr::vision::VWMPCData(com_height,T_sampling,m_steps,n_periods_step,jerk_gain,velocity_gain,zmp_gain);
    hr::vision::VWMPCConstraints constraints;
    hr::locomotion::RobotSystem system;

    cout << "Init mpc" << endl;

    mpcData.init(interactionMatrix,matrixTransform);
    constraints.init(mpcData);
    system.init(com_height,T_sampling);

    system.nextState(jerkXYTheta,robotState);
    constraints.setConstraints(mpcData,mpcFootStatus, footState, robotState);

    int nWSR = 1000;
    qpOASES::real_t cpuTime = 1.0;

    int nV = 2*(N_horizon+m_steps);
    int nC = constraints.getbupVector().rows();
    qpOASES::SQProblem quad;
    boost::shared_ptr<qpOASES::SQProblem> quadprog;
    qpOASES::Options options;
    options.setToMPC();
    options.printLevel = qpOASES::PL_NONE;
    quadprog.reset(new qpOASES::SQProblem(nV,nC));
    quadprog->setOptions(options);
    timer.start();
    quadprog->init(mpcData.getHessian().data(), mpcData.getFvector(robotState,footState,currentFeatures,desiredHorizonFeatures).data(),constraints.getAMatrix().data(),NULL,NULL,NULL,constraints.getbupVector().data(),nWSR,&cpuTime);
    if(quadprog->getPrimalSolution(uk.data()) == qpOASES::RET_QP_NOT_SOLVED){
        return hr::FAILED_INIT;
        cout << "failed to solve" << endl;
    }
    timer.stop();
    cout << "qpoases solved, time:" << endl;
    cout << timer.format() << endl;




    //! Timing variables
    struct timespec time;

    pt::ptime current_date_microseconds = pt::microsec_clock::local_time();
    long milliseconds = current_date_microseconds.time_of_day().total_milliseconds();
    long initial_milliseconds = milliseconds;
    hr::real_t time_p = 0.0;

    double integrationStepSize = 0.01;
    double log_time = 0.0;

    int t = 0.0;
    double T = 10.02; //MPCTIME
    int N = T/integrationStepSize;

    for(int i = 0; i != N; i++){


        current_date_microseconds = pt::microsec_clock::local_time();
        milliseconds = current_date_microseconds.time_of_day().total_milliseconds() - initial_milliseconds;
        time_p = ((double) milliseconds)/1000.0;

        //! Log data ---------------------------------------------------------------
        myfile << log_time << " " << robotState(0)  << " " << robotState(3) << " " << system.getZx_k()  << " " << system.getZy_k()  << " " << footState(0) << " " << footState(1) << std::endl;
        log_time += integrationStepSize;
        //! end of Log data --------------------------------------------------------


        //! closed-loop
        //robotState(0) = comPosition(0);
        //robotState(3) = comPosition(1);

        if( i % 10 == 0){



            //! Get current features
            current = visionProxy.call<std::vector<float> >("getCurrentFeatures");
            currentPixelFeats = Eigen::VectorXf::Map(current.data(),n_features);
            currentPixelFeatures = currentPixelFeats.cast<hr::real_t>();
            camera.convertPoints(currentPixelFeatures,currentFeatures,hr::vision::kPixel2ImageCoordinates,true,hr::vision::k2DPoint);

            //currentFeatures << -0.0400, -0.0400, -0.0400, 0.0400, 0.0400, 0.0400,  0.0400, -0.0400;
            //currentFeatures << -0.2000, -0.2000, -0.2000, 0.2000, 0.2000, 0.2000, 0.2000, -0.2000;
            currentFeatures << -0.4000, -0.0667, -0.4000, 0.0667, -0.2667, 0.0667,-0.2667, -0.0667;

            std::cout <<  "currentFeatures" << std::endl;
            std::cout <<  currentFeatures << std::endl;

            std::cout <<  "desiredFeatures" << std::endl;
            std::cout <<  desiredFeatures << std::endl;

            //cout << "iteration>>>>>>>>>>>>>>>>>>" << endl;
            //cout << "Foot is: " << footStatus << endl;
            //cout << "mpcData at " << mpcData.getStage()<< endl ;

            nWSR = 1000;
            cpuTime = 1.0;
            quadprog->setOptions(options);
            quadprog->hotstart(mpcData.getHessian().data(), mpcData.getFvector(robotState,footState,currentFeatures,desiredHorizonFeatures).data(),constraints.getAMatrix().data(),NULL,NULL,NULL,constraints.getbupVector().data(),nWSR,&cpuTime);
            if(quadprog->getPrimalSolution(uk.data()) == qpOASES::RET_QP_NOT_SOLVED){
                return hr::FAILED_INIT;
                cout << "failed to solve" << endl;
            }
            //cout << "cputime in ms" << endl << cpuTime*1000 << endl;

            jerkXYTheta(0) = uk(0);
            jerkXYTheta(1) = uk(N_horizon+m_steps);
            nextFootState(0) = uk(N_horizon);
            nextFootState(1) = uk(N_horizon+m_steps+N_horizon);
            nextFootState(2) = z_foot;


            //cout << "Uk size:" << endl << uk.rows() << endl;
            //cout << "Uk:" << endl << uk << endl;

            //

            //cout << "Before Robot State" << endl << robotState << endl;
            system.nextState(jerkXYTheta,robotState);
            //cout << "New Robot State" << endl << robotState << endl;
            cout << "Current foot:" << endl << footState << endl;
            cout << "Next foot:" << endl << nextFootState << endl;


            if( (i+10) % (n_periods_step*10) == 0){
                //cout << "step foot mpc-----------------------------------------------------" << endl;

                if(mpcFootStatus == hr::core::kLeftFootOnGround){
                    mpcFootStatus = hr::core::kRightFootOnGround;
                    footState = nextFootState;
                }
                else{
                    mpcFootStatus = hr::core::kLeftFootOnGround;
                    footState = nextFootState;
                }



            }

            mpcData.next();

            constraints.setConstraints(mpcData,mpcFootStatus, footState, robotState);


            if( i % (n_periods_step*10) == 0){  //Foot trajectory
                if(mpcFootStatus == hr::core::kLeftFootOnGround){
                    //rightFoot trajectory

                }
                else{
                    //leftFoot trajectory

                }
            }

            //torso trajectory

            //cout << "Torso Pose " << torsoPose.transpose() << endl;
            //cout << "Torso Goal " <<  torsoGoal.transpose() << endl;



        }



        if( (i + 1) % (n_periods_step*10) == 0){
            //cout << "step foot status-----------------------------------------------------" << endl;

            if(footStatus == hr::core::kLeftFootOnGround){
                footStatus = hr::core::kRightFootOnGround;
            }
            else{
                footStatus = hr::core::kLeftFootOnGround;

            }

        }


        //! Move(Apply control) and Sleep for 5ms
        time.tv_sec = 0;
        time.tv_nsec = 10000000; //10ms
        nanosleep(&time,0);

    }

    myfile.close();
    myfile_joints.close();
    myfile_feet.close();
}





