//Example 28 Locomotion test


#include "openhrc/locomotion.h"
#include "qpOASES.hpp"
#include <iostream>
#include <boost/timer/timer.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <cstdlib>
#include <fstream>

#include "boost/date_time/posix_time/posix_time.hpp"
namespace pt = boost::posix_time;

using boost::timer::cpu_timer;
using std::cout;
using std::endl;


int main(int argc, char** argv){

    if(argc != 3)
    {
      std::cerr << "Wrong number of arguments!" << std::endl;
      std::cerr << "Usage: example_28 step_numbers x_vel" << std::endl;
      exit(2);
    }


    int steps_to = atoi(argv[1]);
    float xspeed = atof(argv[2]);

    std::cout << "Running for " << steps_to << " steps" << std::endl;


    cpu_timer timer;

    std::ofstream myfile;
    myfile.open ("example_26.dat");
    std::ofstream myfile_joints;
    myfile_joints.open ("example_26_joints.dat");
    std::ofstream myfile_feet;
    myfile_feet.open ("example_26_feet.dat");

    /** MPC parameters */
    int m_steps = 2;
    int n_periods_step = steps_to;
    int N_horizon = m_steps*n_periods_step;
    hr::real_t T_sampling = 0.1;
    hr::real_t com_height = 0.265 ;

//    hr::real_t zmp_gain = 1;
//    hr::real_t jerk_gain = 10e-6;
//    //hr::real_t jerk_gain = 1.0;
//    hr::real_t velocity_gain = 1;

//    hr::real_t zmp_gain = 1;
    hr::real_t zmp_gain = 20;
    hr::real_t jerk_gain = 10e-6;
    //hr::real_t jerk_gain = 1.0;
    hr::real_t velocity_gain = 1;

    /** These parameters describe the state of the robot */

    hr::real_t z_foot = -0.32;
    hr::Vector3r footState;    //Member for the position of the current foot on the ground
    hr::Vector3r nextFootState;    //Member for the position of the current foot on the ground
    hr::locomotion::RobotState robotState;        //Member for the augmented CoM robot state
    hr::Vector3r xk;              //Variable for the CoM robot state in X
    hr::Vector3r yk;              //Variable for the CoM robot state in Y
    hr::Vector3r thetak;          //Variable for the CoM robot state in Theta
    hr::VectorXr x_vel(N_horizon);        //Reference velocity in X direction     //TODO: Time list trajectories similar to ALDCM
    hr::VectorXr y_vel(N_horizon);        //Reference velocity in Y direction     //TODO
    hr::core::FootStatus footStatus = hr::core::kRightFootOnGround;
    hr::core::FootStatus mpcFootStatus = hr::core::kRightFootOnGround;
    hr::Vector3r jerkXYTheta; //Jerk holder variable
    hr::VectorXr uk;      ///Jerk vector
    uk.resize(2*(N_horizon+m_steps));
    uk.setZero();
    /** Initialization of the robot parameters */

    cout << "Init robot parameters" << endl;
    footState << 0.0, -0.05 , -0.32;   //For this example the left foot is the starting foot.  //TODO: 04/2015: I would prefer footPosition instead of state
    nextFootState << 0 , 0 ,z_foot;
    xk << 0.0, 0.0, 0.0;
    yk << -0.05, 0.0, 0.0;

    thetak << 0.0, 0.0 ,0.0;
    robotState.setState(xk,yk,thetak);
    jerkXYTheta << 0.0,0.0,0.0;
    x_vel.setOnes();
    y_vel.setOnes();
    x_vel = x_vel*xspeed;
    y_vel = -y_vel*0.16;


    /** Initialization of the MPC */
    //hr::locomotion::MPCData mpcData = hr::locomotion::MPCData(com_height,T_sampling,m_steps,n_periods_step);
    //hr::locomotion::MPCData mpcData = hr::locomotion::MPCData(com_height,T_sampling,m_steps,n_periods_step,jerk_gain,velocity_gain,zmp_gain);
    boost::shared_ptr<hr::locomotion::MPCData> mpcData;
    mpcData = boost::make_shared<hr::locomotion::MPCData>(com_height,T_sampling,m_steps,n_periods_step,jerk_gain,velocity_gain,zmp_gain);
    hr::locomotion::MPCConstraints constraints;
    hr::locomotion::RobotSystem system;

    //mpcData.init();
    mpcData->init();
    constraints.init(*mpcData);
    system.init(com_height,T_sampling);

    constraints.setConstraints(*mpcData,mpcFootStatus, footState, robotState);

    int nWSR = 1000;
    qpOASES::real_t cpuTime = 1.0;

    int nV = 2*(N_horizon+m_steps);
    int nC = constraints.getbupVector().rows();
    boost::shared_ptr<qpOASES::SQProblem> quadprog;
    qpOASES::Options options;
    options.setToMPC();
    options.printLevel = qpOASES::PL_NONE;
    quadprog.reset(new qpOASES::SQProblem(nV,nC));
    quadprog->setOptions(options);
    quadprog->init(mpcData->getHessian().data(), mpcData->getFvector(robotState,footState,x_vel,y_vel).data(),constraints.getAMatrix().data(),NULL,NULL,NULL,constraints.getbupVector().data(),nWSR,&cpuTime);
    if(quadprog->getPrimalSolution(uk.data()) == qpOASES::RET_QP_NOT_SOLVED){
        return hr::FAILED_INIT;
        cout << "failed to solve" << endl;
    }
    cout << "qpoases init" << endl;





    pt::ptime current_date_microseconds = pt::microsec_clock::local_time();
    long milliseconds = current_date_microseconds.time_of_day().total_milliseconds();
    long initial_milliseconds = milliseconds;
    hr::real_t time_p = 0.0;


    double integrationStepSize = 0.01;
    double log_time = 0.0;
    int t = 0.0;
    double T = 10.02; //MPCTIME
    int N = T/integrationStepSize;
    for(int i = 0; i != N; i++){


        current_date_microseconds = pt::microsec_clock::local_time();
        milliseconds = current_date_microseconds.time_of_day().total_milliseconds() - initial_milliseconds;
        time_p = ((double) milliseconds)/1000.0;

        timer.start();

        //! Log data ---------------------------------------------------------------
        myfile << log_time << " " << robotState(0)  << " " << robotState(3) << " " << system.getZx_k()  << " " << system.getZy_k()  << " " << footState(0) << " " << footState(1) << std::endl;
        log_time += integrationStepSize;
        //! end of Log data --------------------------------------------------------

        if( i % 10 == 0){

            nWSR = 100;
            cpuTime = 0.002;
            //quadprog->setOptions(options);
            quadprog->hotstart(mpcData->getHessian().data(), mpcData->getFvector(robotState,footState,x_vel,y_vel).data(),constraints.getAMatrix().data(),NULL,NULL,NULL,constraints.getbupVector().data(),nWSR,&cpuTime);
            if(quadprog->getPrimalSolution(uk.data()) == qpOASES::RET_QP_NOT_SOLVED){
                return hr::FAILED_INIT;
                cout << "failed to solve" << endl;
            }
            //cout << "cputime in ms" << endl << cpuTime*1000 << endl;

            jerkXYTheta(0) = uk(0);
            jerkXYTheta(1) = uk(N_horizon+m_steps);
            nextFootState(0) = uk(N_horizon);
            nextFootState(1) = uk(N_horizon+m_steps+N_horizon);
            nextFootState(2) = z_foot;

            system.nextState(jerkXYTheta,robotState);

            if( (i+10) % (n_periods_step*10) == 0){
                cout << "step foot mpc-----------------------------------------------------" << endl;

                if(mpcFootStatus == hr::core::kLeftFootOnGround){
                    mpcFootStatus = hr::core::kRightFootOnGround;
                    footState = nextFootState;
                    //footState << rightFootPosition(0),rightFootPosition(1), z_foot;
                }
                else{
                    mpcFootStatus = hr::core::kLeftFootOnGround;
                    footState = nextFootState;
                    //footState << leftFootPosition(0),leftFootPosition(1), z_foot;
               }

            }


            mpcData->next();

            constraints.setConstraints(*mpcData,mpcFootStatus, footState, robotState);

            if( i % (n_periods_step*10) == 0){  //Foot trajectory
                if(mpcFootStatus == hr::core::kLeftFootOnGround){
                    //rightFoot trajectory

                }
                else{
                    //leftFoot trajectory

              }
            }

            //torso trajectory
        }



        if( (i + 1) % (n_periods_step*10) == 0){
            cout << "step foot status-----------------------------------------------------" << endl;

            if(footStatus == hr::core::kLeftFootOnGround){
                footStatus = hr::core::kRightFootOnGround;
            }
            else{
                footStatus = hr::core::kLeftFootOnGround;

            }

        }

        timer.stop();
        cout << "Iteration time"<< endl;
        cout << timer.format()<< endl;

    }

    myfile.close();
    myfile_joints.close();
    myfile_feet.close();
}




