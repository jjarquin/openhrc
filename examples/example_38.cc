// example 38 Visual Driven Walking Pattern Generator with Inverse Kinematics


#include <iostream>
#include <fstream>
#include <vector>
#include <unistd.h>

#include "openhrc/utils.h"
#include "openhrc/core.h"
#include "openhrc/kinematics.h"
#include "openhrc/locomotion.h"
#include "openhrc/vision.h"

#include "qpOASES.hpp"


#include <alproxies/almotionproxy.h>
#include <alproxies/almemoryproxy.h>
#include <alcommon/alproxy.h>
#include <alvalue/alvalue.h>
#include <alerror/alerror.h>

#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <boost/timer/timer.hpp>
#include "boost/date_time/posix_time/posix_time.hpp"

namespace pt = boost::posix_time;

#ifdef  USE_LOCAL_XBOT //
std::string naoFile = "../../../src/data/Nao_blue_noninertial.xbot";
#else
std::string naoFile = "/home/nao/projects/hrData/Nao_blue_noninertial.xbot";
#endif

using boost::timer::cpu_timer;
using std::cout;
using std::endl;

hr::VectorXr getNaoConfig(AL::ALMotionProxy & motionProxy);
bool setNaoConfig(AL::ALMotionProxy & motionProxy, const hr::VectorXr & q, hr::real_t time );

void hrToAl(const hr::VectorXr & q_hr , hr::VectorXr & q_al){
    const int kNAO_DOF = 26;
    int jointId[kNAO_DOF] = {7,8,9,10,11,12,13,-1,19,20,21,22,23,24,25,26,27,28,29,30,14,15,16,17,18,-1};
    q_al.setZero();
    for (int i = 0 ; i < kNAO_DOF; i++) {
        if(jointId[i] != -1) {
            q_al(i) = q_hr(jointId[i]-1);
        }
    }

}

void alToHr(hr::VectorXr & q_hr , const hr::VectorXr & q_al){
    const int kNAO_DOF = 26;
    int jointId[kNAO_DOF] = {7,8,9,10,11,12,13,-1,19,20,21,22,23,24,-1,26,27,28,29,30,14,15,16,17,18,-1};
    q_hr.setZero();
    for (int i = 0 ; i < kNAO_DOF; i++) {
        if(jointId[i] != -1) {
            q_hr(jointId[i]-1) = q_al(i);
        }
    }
    q_hr(24) = q_hr(18);

}


int main(){
    cpu_timer timer;

    std::ofstream myfile;
    myfile.open ("example_26.dat");
    std::ofstream myfile_joints;
    myfile_joints.open ("example_26_joints.dat");
    std::ofstream myfile_feet;
    myfile_feet.open ("example_26_feet.dat");
    std::ofstream myfile_in;
    myfile_in.open ("example_26_in.dat");

    hr::real_t log_time = 0.0;


    //! init hr::core:multibody
    hr::core::World world = hr::core::World();
    std::string sFile = naoFile;
    int robotId = world.getRobotsVector()->size();
    world.loadMultiBody(sFile,robotId, hr::core::kNAO);
    boost::shared_ptr<hr::core::MultiBody> robot = world.getRobot(0);

    hr::VectorXr q = robot->getConfiguration();
    hr::VectorXr qtmp(q.rows()-1);

    std::string robot_ip ="127.0.0.1";
//    std::string robot_ip ="nao.local";

    //! Nao Proxies
    AL::ALMotionProxy motionProxy = AL::ALMotionProxy(robot_ip);







    //! Load current configuration for the multibody
    q = getNaoConfig(motionProxy);
    robot->setConfiguration(q);
    robot->computeForwardKinematics();

    hr::real_t const integrationStepSize = 0.01;
    hr::real_t t = 0.0;
    hr::real_t T = 3.0;
    int N = T/integrationStepSize;

    //! Define the end-effector handlers, the pose variables and their kinematic tasks
    hr::Vector3r rightFootHandle(0.015,0.0,-0.04);
    int rightFootBodyId = hr::core::nao::kRightFoot;
    int rightFootHandleId = robot->addOperationalHandle(rightFootBodyId,rightFootHandle);

    hr::Vector3r leftFootHandle(0.015,0.0,-0.04);
    int leftFootBodyId = hr::core::nao::kLeftFoot;
    int leftFootHandleId = robot->addOperationalHandle(leftFootBodyId,leftFootHandle);

    hr::Vector3r rightHandHandle(0.08,0.0,0.0);
    int rightHandBodyId = hr::core::nao::kRightHand;
    int rightHandHandleId = robot->addOperationalHandle(rightHandBodyId,rightHandHandle);

    hr::Vector3r leftHandHandle(0.08,0.0,0.0);
    int leftHandBodyId = hr::core::nao::kLeftHand;
    int leftHandHandleId = robot->addOperationalHandle(leftHandBodyId,leftHandHandle);

    hr::Vector3r torsoHandle(0.0,0.0,0.0);
    int torsoBodyId = hr::core::nao::kTorso;
    int torsoHandleId = robot->addOperationalHandle(torsoBodyId,torsoHandle);

    hr::Vector3r headHandle(0.0,0.0,0.0);
    int headBodyId = hr::core::nao::kHead;
    int headHandleId = robot->addOperationalHandle(headBodyId,headHandle);

    hr::SpatialVector comPose;
    hr::SpatialVector comGoal;

    hr::SpatialVector rightFootPose;
    hr::SpatialVector leftFootPose;

    hr::SpatialVector rightHandPose;
    hr::SpatialVector leftHandPose;

    hr::SpatialVector torsoPose;
    hr::SpatialVector headPose;

    hr::SpatialVector rightFootGoal;
    hr::SpatialVector leftFootGoal;

    hr::SpatialVector rightHandGoal;
    hr::SpatialVector leftHandGoal;

    hr::SpatialVector torsoGoal;
    hr::SpatialVector headGoal;

    hr::Vector3r rightFootPosition = robot->getOperationalHandlePosition(rightFootBodyId,rightFootHandleId);
    hr::Vector3r rightFootOrientation = robot->getBodyOrientation(rightFootBodyId).eulerAngles(0,1,2);

    hr::Vector3r leftFootPosition = robot->getOperationalHandlePosition(leftFootBodyId,leftFootHandleId);
    hr::Vector3r leftFootOrientation = robot->getBodyOrientation(leftFootBodyId).eulerAngles(0,1,2);

    hr::Vector3r rightHandPosition = robot->getOperationalHandlePosition(rightHandBodyId,rightHandHandleId);
    hr::Vector3r rightHandOrientation = robot->getBodyOrientation(rightHandBodyId).eulerAngles(0,1,2);

    hr::Vector3r leftHandPosition = robot->getOperationalHandlePosition(leftHandBodyId,leftHandHandleId);
    hr::Vector3r leftHandOrientation = robot->getBodyOrientation(leftHandBodyId).eulerAngles(0,1,2);

    hr::Vector3r torsoPosition = robot->getOperationalHandlePosition(torsoBodyId,torsoHandleId);
    hr::Vector3r torsoOrientation = robot->getBodyOrientation(torsoBodyId).eulerAngles(0,1,2);

    hr::Vector3r headPosition = robot->getOperationalHandlePosition(headBodyId,headHandleId);
    hr::Vector3r headOrientation = robot->getBodyOrientation(headBodyId).eulerAngles(0,1,2);

    hr::Vector3r comPosition = robot->getCoM();

    rightFootPose << rightFootPosition , rightFootOrientation;
    leftFootPose << leftFootPosition, leftFootOrientation;
    torsoPose << torsoPosition, torsoOrientation;
    rightHandPose << rightHandPosition , rightHandOrientation;
    leftHandPose << leftHandPosition , leftHandOrientation;
    headPose << headPosition, headOrientation;

    rightFootGoal << rightFootPosition , 0.0 , 0.0, 0.0;
    leftFootGoal << leftFootPosition, 0.0 , 0.0 , 0.0;

    torsoGoal << torsoPosition(0)+0.02,torsoPosition(1),torsoPosition(2), 0,0,0;
    rightHandGoal << rightHandPosition , rightHandOrientation;
    leftHandGoal << leftHandPosition , leftHandOrientation;
    headGoal << headPosition, headOrientation;

    comPose << comPosition , 0.0,0.0,0.0;
    comGoal << rightFootPosition(0), rightFootPosition(1), comPosition(2), 0.0,0.0,0.0;

    hr::core::KinematicTask rightFootTask(rightFootHandleId,rightFootBodyId,0,robot, hr::core::kPositionOrientation);
    rightFootTask.setGoal(rightFootGoal);
    rightFootTask.setGain(1.0);

    hr::core::KinematicTask leftFootTask(leftFootHandleId,leftFootBodyId,0,robot, hr::core::kPositionOrientation);
    leftFootTask.setGoal(leftFootGoal);
    leftFootTask.setGain(1.0);

    hr::core::KinematicTask rightHandTask(rightHandHandleId,rightHandBodyId,0,robot, hr::core::kPositionOrientation);
    rightHandTask.setGoal(rightHandGoal);
    rightHandTask.setGain(1.0);

    hr::core::KinematicTask leftHandTask(leftHandHandleId,leftHandBodyId,0,robot, hr::core::kPositionOrientation);
    leftHandTask.setGoal(leftHandGoal);
    leftHandTask.setGain(1.0);

    hr::core::KinematicTask torsoTask(torsoHandleId,torsoBodyId,0,robot, hr::core::kWXYZ );
    torsoTask.setGoal(torsoGoal);
    torsoTask.setGain(1.0);

    hr::core::KinematicTask torsoZTask(torsoHandleId,torsoBodyId,0,robot,  hr::core::kZ );
    torsoZTask.setGoal(torsoGoal);
    torsoZTask.setGain(1.0);

    hr::core::KinematicTask headTask(headHandleId,headBodyId,0,robot,hr::core::kWYZ );
    headTask.setGoal(headPose);
    headTask.setGain(3.0);

    hr::core::CoMTask comTask(0,robot, hr::core::kXY);
    comTask.setGoal(comGoal);
    comTask.setGain(0.5);

    leftHandGoal(0) = rightFootPosition(0);
    rightHandGoal(0) = rightFootPosition(0);

    hr::core::KinematicTask rightHandXTask(rightHandHandleId,rightHandBodyId,0,robot, hr::core::kX);
    rightHandXTask.setGoal(rightHandGoal);
    rightHandXTask.setGain(1.0);
    hr::core::KinematicTask leftHandXTask(leftHandHandleId,leftHandBodyId,0,robot, hr::core::kX);
    leftHandXTask.setGoal(leftHandGoal);
    leftHandXTask.setGain(1.0);

    hr::utils::SimpleHierarchicalSolver solver;


    hr::VectorXr lb_ = (robot->getJointLowLimits()-q)/integrationStepSize;
    hr::VectorXr ub_ = (robot->getJointUpLimits()-q)/integrationStepSize;

    hr::VectorXr lb(29);
    hr::VectorXr ub(29);

    lb.segment(0,24) = lb_.segment(0,24);
    lb.segment(24,5) = lb_.segment(25,5);

    ub.segment(0,24) = ub_.segment(0,24);
    ub.segment(24,5) = ub_.segment(25,5);

    hr::VectorXr ld(29);
    hr::VectorXr ud(29);
    hr::MatrixXr C;

    hr::VectorXr ud_ = robot->getJointUpSpeedLimits();
    hr::VectorXr ld_ = robot->getJointLowSpeedLimits();

    int n = 29;
    C = hr::MatrixXr::Identity(n,n);

    ld.segment(0,24) = ld_.segment(0,24);
    ld.segment(24,5) = ld_.segment(25,5);

    ud.segment(0,24) = ud_.segment(0,24);
    ud.segment(24,5) = ud_.segment(25,5);

    q = getNaoConfig(motionProxy);
    hr::VectorXr q_tmp = q;
    robot->setConfiguration(q);
    robot->computeForwardKinematics();

    t = 0.0;
    T = 0.8;
    N = T/integrationStepSize;

    //move hands
    torsoTask.setTaskSpace(hr::core::kPositionOrientation);
    for(int i = 0; i != N ; i++){

        q_tmp = getNaoConfig(motionProxy);
        q_tmp.segment(0,6) = q.segment(0,6);

        robot->setConfiguration(q_tmp);
        robot->computeForwardKinematics();

        //! Log data ---------------------------------------------------------------
        comPosition = robot->getCoM();
        torsoPosition = robot->getOperationalHandlePosition(torsoBodyId,torsoHandleId);
        rightFootPosition = robot->getOperationalHandlePosition(rightFootBodyId,rightFootHandleId);
        //        myfile << log_time << " " << comPosition(0)  << " " << comPosition(1) << " " << torsoPosition(0)  << " " << torsoPosition(1) << " " << rightFootPosition(0) << " " << rightFootPosition(1) << std::endl;
        //        log_time += integrationStepSize;
        //! end of Log data --------------------------------------------------------

        //Calculate the new joint speed limit
        lb_ = (robot->getJointLowLimits()-q)/integrationStepSize;
        ub_ = (robot->getJointUpLimits()-q)/integrationStepSize;

        lb.segment(0,24) = lb_.segment(0,24);
        lb.segment(24,5) = lb_.segment(25,5);

        ub.segment(0,24) = ub_.segment(0,24);
        ub.segment(24,5) = ub_.segment(25,5);

        rightFootTask.update(t);
        leftFootTask.update(t);
        rightHandTask.update(t);
        leftHandTask.update(t);

        rightHandXTask.update(t);
        leftHandXTask.update(t);

        torsoTask.update(t);
        torsoZTask.update(t);
        headTask.update(t);
        comTask.update(t);

        std::vector<hr::utils::TaskAbstract*> taskList;
        hr::utils::SimpleTask feetTask;
        hr::utils::SimpleTask handsXTask;
        hr::VectorXr zeros(6);
        zeros.setZero();

        //Fix hands wrt to torso
        handsXTask.addRows(rightHandXTask.getMatrixA(), rightHandXTask.getVectorb());
        handsXTask.addRows(leftHandXTask.getMatrixA(), leftHandXTask.getVectorb());

        //

        feetTask.addRows(leftFootTask.getMatrixA(),leftFootTask.getVectorb());
        feetTask.addRows(rightFootTask.getMatrixA(),rightFootTask.getVectorb());

        taskList.push_back(&feetTask);
        taskList.push_back(&torsoTask);
        taskList.push_back(&handsXTask);


        hr::VectorXr dq = solver.Solve(taskList,C,ld,ud,lb,ub);

        std::cout << "t " << t << std::endl;


        // Integrate
        qtmp.segment(0,24) = q.segment(0,24);
        qtmp.segment(24,5) = q.segment(25,5);
        qtmp += dq*integrationStepSize;
        t = t + integrationStepSize;
        q.segment(0,24) = qtmp.segment(0,24);
        q(24) = qtmp(18);
        q.segment(25,5) = qtmp.segment(24,5);
        // End of integration


        //Set the new configuration
        setNaoConfig(motionProxy,q, integrationStepSize);

    }
    q_tmp = getNaoConfig(motionProxy);
    q_tmp.segment(0,6) = q.segment(0,6);

    robot->setConfiguration(q_tmp);
    robot->computeForwardKinematics();

    torsoTask.setTaskSpace(hr::core::kOrientation);

    comGoal << rightFootPosition(0),rightFootPosition(1)+0.02, comPosition(2), 0,0,0;
    boost::shared_ptr<hr::core::PointTrajectory> comTraj = boost::make_shared<hr::core::PointTrajectory>(0.0,0.8,comPose,comGoal,hr::core::kQuinticPolynomial);
    comTask.setTrajectory(comTraj);
    comTask.setGain(0.5);

    t = 0.0;
    T = 0.8;
    N = T/integrationStepSize;

    //rest CoM on right foot

    for(int i = 0; i != N ; i++){

        q_tmp = getNaoConfig(motionProxy);
        q_tmp.segment(0,6) = q.segment(0,6);

        robot->setConfiguration(q_tmp);
        robot->computeForwardKinematics();

        //! Log data ---------------------------------------------------------------
        comPosition = robot->getCoM();
        torsoPosition = robot->getOperationalHandlePosition(torsoBodyId,torsoHandleId);
        rightFootPosition = robot->getOperationalHandlePosition(rightFootBodyId,rightFootHandleId);
        //        myfile << log_time << " " << comPosition(0)  << " " << comPosition(1) << " " << torsoPosition(0)  << " " << torsoPosition(1) << " " << rightFootPosition(0) << " " << rightFootPosition(1) << std::endl;
        //        log_time += integrationStepSize;
        //! end of Log data --------------------------------------------------------

        //Calculate the new joint speed limit
        lb_ = (robot->getJointLowLimits()-q)/integrationStepSize;
        ub_ = (robot->getJointUpLimits()-q)/integrationStepSize;

        lb.segment(0,24) = lb_.segment(0,24);
        lb.segment(24,5) = lb_.segment(25,5);

        ub.segment(0,24) = ub_.segment(0,24);
        ub.segment(24,5) = ub_.segment(25,5);

        rightFootTask.update(t);
        leftFootTask.update(t);
        rightHandTask.update(t);
        leftHandTask.update(t);

        torsoTask.update(t);
        torsoZTask.update(t);
        headTask.update(t);
        comTask.update(t);

        std::vector<hr::utils::TaskAbstract*> taskList;
        hr::utils::SimpleTask feetTask;
        hr::utils::SimpleTask handsTask;
        hr::VectorXr zeros(6);
        zeros.setZero();

        //Fix hands wrt to torso
        hr::MatrixXr righHandJacobian = rightHandTask.getMatrixA();
        hr::MatrixXr leftHandJacobian = leftHandTask.getMatrixA();
        righHandJacobian.leftCols(6).setZero();
        leftHandJacobian.leftCols(6).setZero();
        handsTask.addRows(righHandJacobian,zeros);
        handsTask.addRows(leftHandJacobian,zeros);
        //

        feetTask.addRows(leftFootTask.getMatrixA(),leftFootTask.getVectorb());
        feetTask.addRows(rightFootTask.getMatrixA(),rightFootTask.getVectorb());

        taskList.push_back(&feetTask);
        taskList.push_back(&handsTask);
        taskList.push_back(&torsoZTask);
        taskList.push_back(&headTask);
        taskList.push_back(&comTask);
        taskList.push_back(&torsoTask);

        hr::VectorXr dq = solver.Solve(taskList,C,ld,ud,lb,ub);

        std::cout << "t " << t << std::endl;


        // Integrate
        qtmp.segment(0,24) = q.segment(0,24);
        qtmp.segment(24,5) = q.segment(25,5);
        qtmp += dq*integrationStepSize;
        t = t + integrationStepSize;
        q.segment(0,24) = qtmp.segment(0,24);
        q(24) = qtmp(18);
        q.segment(25,5) = qtmp.segment(24,5);
        // End of integration


        //Set the new configuration
        setNaoConfig(motionProxy,q, integrationStepSize);

    }
    q_tmp = getNaoConfig(motionProxy);
    q_tmp.segment(0,6) = q.segment(0,6);

    robot->setConfiguration(q_tmp);
    robot->computeForwardKinematics();

    //! Log data ---------------------------------------------------------------
    comPosition = robot->getCoM();
    torsoPosition = robot->getOperationalHandlePosition(torsoBodyId,torsoHandleId);
    rightFootPosition = robot->getOperationalHandlePosition(rightFootBodyId,rightFootHandleId);
    //    myfile << log_time << " " << comPosition(0)  << " " << comPosition(1) << " " << torsoPosition(0)  << " " << torsoPosition(1) << " " << rightFootPosition(0) << " " << rightFootPosition(1) << std::endl;
    //    log_time += integrationStepSize;
    //    myfile << std::endl;
    //! end of Log data --------------------------------------------------------






    /** MPC parameters */
    int m_steps = 2;
    int n_periods_step = 8; //15
    int N_horizon = m_steps*n_periods_step;
    hr::real_t T_sampling = 0.1;
    hr::real_t com_height = 0.265 ;


//    hr::real_t jerk_gain = 0.001; // alpha
//    hr::real_t velocity_gain = 0.001; // beta
//    hr::real_t zmp_gain = 1; // gama

    hr::real_t jerk_gain = 0.1; // alpha
    hr::real_t velocity_gain = 1.0; // beta
    hr::real_t zmp_gain = 0.0001; // gama

//    hr::real_t jerk_gain = 10e-6; // alpha
//    hr::real_t velocity_gain = 1.0; // beta
//    hr::real_t zmp_gain = 20.0; // gama


//    hr::real_t jerk_gain = 0.1; // alpha
//    hr::real_t velocity_gain = 20.0; // beta
//    hr::real_t zmp_gain = 20.0; // gama


    /** VMPC parameters */
    int n_camera_dof = 6;
    int n_features = 8;
    hr::real_t Ts = 0.1;

    /** These parameters describe the camera */
    hr::Matrix3r kMatrix;
    hr::MatrixX6r interactionMatrix;
    hr::VectorXr currentPixelFeatures(n_features);
    hr::VectorXr desiredPixelFeatures(n_features);
    hr::VectorXr currentFeatures(n_features);
    hr::VectorXr desiredFeatures(n_features);
    hr::VectorXr desiredHorizonFeatures(n_features*N_horizon);

//    hr::Matrix3r rotCamera;
//    rotCamera << 0, 0, 1, -1, 0, 0, 0, -1, 0;

//    hr::SpatialMatrix cameraTransformation;
//    cameraTransformation << rotCamera, hr::Matrix3r::Zero(), hr::Matrix3r::Zero(), rotCamera;

    /** Initialization of the camera parameters */
    kMatrix << 531.712 , 0, 318.244, 0, 532.662, 250.698, 0,  0,  1;
    hr::vision::Camera camera(kMatrix);
    hr::SpatialMatrix matrixTransform;
    matrixTransform.setZero();
    matrixTransform.col(0) << 0, 0, 1, 0, 0, 0;
    matrixTransform.col(1) << -1, 0, 0, 0, 0, 0;


    //! VisionModule
    AL::ALProxy visionProxy = AL::ALProxy("VisionModuleTest","nao.local",9559);

    //! Extract initial and target features
    std::vector<float> current = visionProxy.call<std::vector<float> >("getCurrentFeatures");
    std::vector<float> target = visionProxy.call<std::vector<float> >("getTargetFeatures");

    //! Convert pixel features to image features
    Eigen::VectorXf currentPixelFeats(n_features);
    Eigen::VectorXf targetPixelFeats(n_features);
    currentPixelFeats = Eigen::VectorXf::Map(current.data(),n_features);
    targetPixelFeats = Eigen::VectorXf::Map(target.data(),n_features);
    cout << "Desired features (m) " << targetPixelFeats.transpose() <<endl;
    cout << "Initial features (m) " << currentPixelFeats.transpose() <<endl;
    currentPixelFeatures = currentPixelFeats.cast<hr::real_t>();
    desiredPixelFeatures = targetPixelFeats.cast<hr::real_t>();




    /** These parameters describe the state of the robot */
    hr::real_t z_foot = rightFootPosition(2);
    //hr::real_t z_foot = -0.3115;
    hr::Vector3r footState;    //Member for the position of the current foot on the ground
    hr::Vector3r nextFootState;    //Member for the position of the current foot on the ground
    hr::locomotion::RobotState robotState;        //Member for the augmented CoM robot state
    hr::Vector3r xk;              //Variable for the CoM robot state in X
    hr::Vector3r yk;              //Variable for the CoM robot state in Y
    hr::Vector3r thetak;          //Variable for the CoM robot state in Theta

    hr::core::FootStatus footStatus = hr::core::kRightFootOnGround;
    hr::core::FootStatus mpcFootStatus = hr::core::kRightFootOnGround;
    hr::Vector3r jerkXYTheta; //Jerk holder variable
    hr::VectorXr uk;      ///Jerk vector
    uk.resize(2*(N_horizon+m_steps));
    uk.setZero();
    /** Initialization of the robot parameters */

    cout << "Init robot parameters" << endl;
    footState << rightFootPosition(0), rightFootPosition(1) , z_foot;   //For this example the left foot is the starting foot.  //TODO: 04/2015: I would prefer footPosition instead of state
    nextFootState << 0 , 0 ,z_foot;
    xk << comPosition(0), 0.0, 0.0;
    yk << comPosition(1), 0.0, 0.0;
    hr::real_t com_z = comPosition(2);

    thetak << 0.0, 0.0 ,0.0;
    robotState.setState(xk,yk,thetak);
    jerkXYTheta << 0.0,0.0,0.0;


    //! Extract points

    camera.convertPoints(currentPixelFeatures,currentFeatures,hr::vision::kPixel2ImageCoordinates,true,hr::vision::k2DPoint);
    camera.convertPoints(desiredPixelFeatures,desiredFeatures,hr::vision::kPixel2ImageCoordinates,true,hr::vision::k2DPoint);


    //! Debuggin features
    //currentFeatures << -0.0400, -0.0400, -0.0400, 0.0400, 0.0400, 0.0400,  0.0400, -0.0400;
    //desiredFeatures << -0.2000, -0.2000, -0.2000, 0.2000, 0.2000, 0.2000, 0.2000, -0.2000;

//    currentFeatures << -0.4000, -0.0667, -0.4000, 0.0667, -0.2667, 0.0667,-0.2667, -0.0667;
//    desiredFeatures << -0.0667, -0.0667, -0.0667, 0.0667, 0.0667,  0.0667, 0.0667, -0.0667;

//    desiredFeatures << -0.4000, -0.0667, -0.4000, 0.0667, -0.2667, 0.0667,-0.2667, -0.0667;
//    currentFeatures << -0.0667, -0.0667, -0.0667, 0.0667, 0.0667,  0.0667, 0.0667, -0.0667;


//    currentFeatures << -0.0400, -0.0400, -0.0400, 0.0400, 0.0400, 0.0400,  0.0400, -0.0400;
//    desiredFeatures << -0.0400, -0.0400, -0.0400, 0.0400, 0.0400, 0.0400,  0.0400, -0.0400;


    std::cout << "currentFeatures" << std::endl;
    std::cout << "desiredFeatures" << std::endl;

    std::cout << currentFeatures << std::endl;
    std::cout << desiredFeatures << std::endl;

    for(int i = 0 ; i!= N_horizon ; i++){
        desiredHorizonFeatures.segment(i*n_features,n_features) = desiredFeatures;
    }
    interactionMatrix = camera.getInteractionMatrix(desiredFeatures, hr::vision::kImageCoordinates, hr::vision::k2DPoint,1.0);


    /** Initialization of the MPC */

    cout << "Create mpcOject" << endl;

    hr::vision::VWMPCData mpcData = hr::vision::VWMPCData(com_height,T_sampling,m_steps,n_periods_step,jerk_gain,velocity_gain,zmp_gain);
    hr::vision::VWMPCConstraints constraints;
    hr::locomotion::RobotSystem system;

    cout << "Init mpc" << endl;

    mpcData.init(interactionMatrix,matrixTransform);
    constraints.init(mpcData);
    system.init(com_height,T_sampling);

    constraints.setConstraints(mpcData,mpcFootStatus, footState, robotState);



    int nWSR = 1000;
    qpOASES::real_t cpuTime = 1.0;

    int nV = 2*(N_horizon+m_steps);
    int nC = constraints.getbupVector().rows();
    qpOASES::SQProblem quad;
    boost::shared_ptr<qpOASES::SQProblem> quadprog;
    qpOASES::Options options;
    options.setToMPC();
    options.printLevel = qpOASES::PL_NONE;
    quadprog.reset(new qpOASES::SQProblem(nV,nC));
    quadprog->setOptions(options);
    timer.start();
    quadprog->init(mpcData.getHessian().data(), mpcData.getFvector(robotState,footState,currentFeatures,desiredHorizonFeatures).data(),constraints.getAMatrix().data(),NULL,NULL,NULL,constraints.getbupVector().data(),nWSR,&cpuTime);
    if(quadprog->getPrimalSolution(uk.data()) == qpOASES::RET_QP_NOT_SOLVED){
        return hr::FAILED_INIT;
        cout << "failed to solve" << endl;
    }
    timer.stop();
    cout << "qpoases solved, time:" << endl;
    cout << timer.format() << endl;




    //! Timing variables
    struct timespec time;

    pt::ptime current_date_microseconds = pt::microsec_clock::local_time();
    long milliseconds = current_date_microseconds.time_of_day().total_milliseconds();
    long initial_milliseconds = milliseconds;
    hr::real_t time_p = 0.0;

    hr::SpatialVector initialCoMPose;
    hr::SpatialVector finalCoMPose;
    hr::SpatialVector initialCoMSpeed;
    hr::SpatialVector finalCoMSpeed;
    hr::SpatialVector initialCoMAccel;
    hr::SpatialVector finalCoMAccel;
    initialCoMPose << comPosition , 0,0,0;
    finalCoMPose << comPosition , 0,0,0;
    initialCoMSpeed.setZero();
    initialCoMAccel.setZero();
    finalCoMSpeed.setZero();
    finalCoMAccel.setZero();



    t = 0.0;
    T = 12.02; //MPCTIME
    N = T/integrationStepSize;
    bool send = true;
    for(int i = 0; i != N; i++){


        current_date_microseconds = pt::microsec_clock::local_time();
        milliseconds = current_date_microseconds.time_of_day().total_milliseconds() - initial_milliseconds;
        time_p = ((double) milliseconds)/1000.0;

        timer.start();

        q_tmp = getNaoConfig(motionProxy);
        q_tmp.segment(0,6) = q.segment(0,6);

        robot->setConfiguration(q_tmp);
        robot->computeForwardKinematics();

        std::string pName = "Body";
        int pSpace = 0; // FRAME_TORSO
        bool pUseSensors = true;
        std::vector<float> pos = motionProxy.getCOM(pName, pSpace, pUseSensors);
        Eigen::Map<Eigen::VectorXf> com(pos.data(),3);
        //cout << "CoM" << endl << com.transpose() << endl;

        //! Log data ---------------------------------------------------------------
        comPosition = robot->getCoM();
        torsoPosition = robot->getOperationalHandlePosition(torsoBodyId,torsoHandleId);
        rightFootPosition = robot->getOperationalHandlePosition(rightFootBodyId,rightFootHandleId);
        myfile << log_time << " " << comPosition(0)  << " " << comPosition(1) << " " << system.getZx_k()  << " " << system.getZy_k()  << " " << footState(0) << " " << footState(1) << " " << com(0)+torsoPosition(0)<< " " << com(1)+torsoPosition(1) << std::endl;
        myfile_joints << t << " " << q_tmp(19) << " " << q(19) << " " << q_tmp(25) << " " << q(25) << std::endl;
        myfile_feet << t << " " << leftFootPosition(0) << " " << leftFootPosition(2) << " " << rightFootPosition(0) << " " << rightFootPosition(2) << " " << comPosition(2) << " " << torsoPosition(2) << std::endl;
        log_time += integrationStepSize;
        //! end of Log data --------------------------------------------------------

        //Calculate the new joint speed limit
        lb_ = (robot->getJointLowLimits()-q)/integrationStepSize;
        ub_ = (robot->getJointUpLimits()-q)/integrationStepSize;

        lb.segment(0,24) = lb_.segment(0,24);
        lb.segment(24,5) = lb_.segment(25,5);

        ub.segment(0,24) = ub_.segment(0,24);
        ub.segment(24,5) = ub_.segment(25,5);



        rightFootPosition = robot->getOperationalHandlePosition(rightFootBodyId,rightFootHandleId);
        rightFootOrientation = robot->getBodyOrientation(rightFootBodyId).eulerAngles(0,1,2);

        leftFootPosition = robot->getOperationalHandlePosition(leftFootBodyId,leftFootHandleId);
        leftFootOrientation = robot->getBodyOrientation(leftFootBodyId).eulerAngles(0,1,2);

        rightHandPosition = robot->getOperationalHandlePosition(rightHandBodyId,rightHandHandleId);
        rightHandOrientation = robot->getBodyOrientation(rightHandBodyId).eulerAngles(0,1,2);

        leftHandPosition = robot->getOperationalHandlePosition(leftHandBodyId,leftHandHandleId);
        leftHandOrientation = robot->getBodyOrientation(leftHandBodyId).eulerAngles(0,1,2);

        torsoPosition = robot->getOperationalHandlePosition(torsoBodyId,torsoHandleId);
        torsoOrientation = robot->getBodyOrientation(torsoBodyId).eulerAngles(0,1,2);

        headPosition = robot->getOperationalHandlePosition(headBodyId,headHandleId);
        headOrientation = robot->getBodyOrientation(headBodyId).eulerAngles(0,1,2);

        comPosition = robot->getCoM();

        rightFootPose << rightFootPosition , rightFootOrientation;
        leftFootPose << leftFootPosition, leftFootOrientation;
        torsoPose << torsoPosition, torsoOrientation;
        rightHandPose << rightHandPosition , rightHandOrientation;
        leftHandPose << leftHandPosition , leftHandOrientation;
        headPose << headPosition, headOrientation;
        comPose << comPosition, 0,0,0;

        //! closed-loop
        //robotState(0) = comPosition(0);
        //robotState(3) = comPosition(1);

        if( i % 10 == 0){

            //! Get current features
            current = visionProxy.call<std::vector<float> >("getCurrentFeatures");
            currentPixelFeats = Eigen::VectorXf::Map(current.data(),n_features);
            currentPixelFeatures = currentPixelFeats.cast<hr::real_t>();
            camera.convertPoints(currentPixelFeatures,currentFeatures,hr::vision::kPixel2ImageCoordinates,true,hr::vision::k2DPoint);


            //! Debuggin features
//            currentFeatures << -0.0400, -0.0400, -0.0400, 0.0400, 0.0400, 0.0400,  0.0400, -0.0400; // move to the front
//            currentFeatures << -0.4000, -0.0667, -0.4000, 0.0667, -0.2667, 0.0667,-0.2667, -0.0667;  // move to the left
//            currentFeatures << -0.0667, -0.0667, -0.0667, 0.0667, 0.0667,  0.0667, 0.0667, -0.0667;  //move to the right



            //cout << "iteration>>>>>>>>>>>>>>>>>>" << endl;
            //cout << "Foot is: " << footStatus << endl;
            //cout << "mpcData at " << mpcData.getStage()<< endl ;

            nWSR = 1000;
            cpuTime = 1.0;
            quadprog->setOptions(options);
            quadprog->hotstart(mpcData.getHessian().data(), mpcData.getFvector(robotState,footState,currentFeatures,desiredHorizonFeatures).data(),constraints.getAMatrix().data(),NULL,NULL,NULL,constraints.getbupVector().data(),nWSR,&cpuTime);
            if(quadprog->getPrimalSolution(uk.data()) == qpOASES::RET_QP_NOT_SOLVED){
                return hr::FAILED_INIT;
                cout << "failed to solve" << endl;
            }
            //cout << "cputime in ms" << endl << cpuTime*1000 << endl;

            jerkXYTheta(0) = uk(0);
            jerkXYTheta(1) = uk(N_horizon+m_steps);
            nextFootState(0) = uk(N_horizon);
            nextFootState(1) = uk(N_horizon+m_steps+N_horizon);
            nextFootState(2) = z_foot;


            //cout << "Uk size:" << endl << uk.rows() << endl;
            //cout << "Uk:" << endl << uk << endl;

            //

            //cout << "Before Robot State" << endl << robotState << endl;
            system.nextState(jerkXYTheta,robotState);
            //cout << "New Robot State" << endl << robotState << endl;
            cout << "Current foot:" << endl << footState << endl;
            cout << "Next foot:" << endl << nextFootState << endl;


            if( (i+10) % (n_periods_step*10) == 0){
                //cout << "step foot mpc-----------------------------------------------------" << endl;

                if(mpcFootStatus == hr::core::kLeftFootOnGround){
                    mpcFootStatus = hr::core::kRightFootOnGround;
                    footState = nextFootState;
                    //footState << rightFootPosition(0),rightFootPosition(1), z_foot;
                    hr::SpatialVector zgain;
                    zgain << 0.1 , 0.1 , 1.0 , 0.1 , 0.1 , 0.1;
                    rightFootTask.setGain(zgain);

                }
                else{
                    mpcFootStatus = hr::core::kLeftFootOnGround;
                    footState = nextFootState;
                    //footState << leftFootPosition(0),leftFootPosition(1), z_foot;
                    hr::SpatialVector zgain;
                    zgain << 0.1 , 0.1 , 1.0 , 0.1 , 0.1 , 0.1;
                    leftFootTask.setGain(zgain);

                }



            }



            mpcData.next();

            constraints.setConstraints(mpcData,mpcFootStatus, footState, robotState);


            if( i % (n_periods_step*10) == 0){  //Foot trajectory
                if(mpcFootStatus == hr::core::kLeftFootOnGround){
                    rightFootGoal << nextFootState, 0.0, 0.0, 0.0;

                    //cout << "Right Foot Pose " << rightFootPose.transpose() << endl;
                    //cout << "Right Foot Goal " << rightFootGoal.transpose() << endl;

                    //boost::shared_ptr<hr::core::FootTrajectory> rightFootTraj = boost::make_shared<hr::core::FootTrajectory>(t+2*T_sampling,t+((double)n_periods_step/10.0)-T_sampling,rightFootPose,rightFootGoal,hr::core::kFootCycloidal);
                    boost::shared_ptr<hr::core::FootTrajectory> rightFootTraj = boost::make_shared<hr::core::FootTrajectory>(t+0.05,t+0.6,rightFootPose,rightFootGoal,hr::core::kFootBezier,hr::core::kFixed,0.025);
                    rightFootTask.setTrajectory(rightFootTraj);
                    rightFootTask.setGain(0.8);
                    hr::SpatialVector zgain;
                    zgain << 0 , 0 , 0.8 , 0.0 , 0.0 , 0.0;
                    leftFootTask.setGain(zgain);

                }
                else{

                    leftFootGoal << nextFootState, 0.0, 0.0, 0.0;

                    //cout << "Left Foot Pose " << leftFootPose.transpose() << endl;
                    //cout << "Left Foot Goal " <<  leftFootGoal.transpose() << endl;

//                    boost::shared_ptr<hr::core::FootTrajectory> leftFootTraj = boost::make_shared<hr::core::FootTrajectory>(t+2*T_sampling,t+((double)n_periods_step/10.0)-T_sampling,leftFootPose,leftFootGoal,hr::core::kFootCycloidal);
                    boost::shared_ptr<hr::core::FootTrajectory> leftFootTraj = boost::make_shared<hr::core::FootTrajectory>(t+0.05,t+0.6,leftFootPose,leftFootGoal,hr::core::kFootBezier,hr::core::kFixed,0.025);
                    leftFootTask.setTrajectory(leftFootTraj);
                    leftFootTask.setGain(0.8);
                    hr::SpatialVector zgain;
                    zgain << 0 , 0 , 0.8 , 0.0 , 0.0 , 0.0;
                    rightFootTask.setGain(zgain);
                }
            }

            //torso trajectory

            //cout << "Torso Pose " << torsoPose.transpose() << endl;
            //cout << "Torso Goal " <<  torsoGoal.transpose() << endl;

            comGoal << robotState(0), robotState(3), com_z, 0.0, 0.0, 0.0;

            //initialCoMPose = comPose; //Pose feedback
            initialCoMPose = finalCoMPose;
            finalCoMPose = comGoal;

            initialCoMSpeed = finalCoMSpeed;
            initialCoMAccel = finalCoMAccel;



            finalCoMSpeed(0) = robotState(1);
            finalCoMAccel(0) = robotState(2);

            finalCoMSpeed(1) = robotState(4);
            finalCoMAccel(1) = robotState(5);
            boost::shared_ptr<hr::core::PointTrajectory> comTraj = boost::make_shared<hr::core::PointTrajectory>(t,t+0.1,initialCoMPose,finalCoMPose,initialCoMSpeed,finalCoMSpeed, initialCoMAccel, finalCoMAccel,hr::core::kQuinticPolynomial);
            comTask.setTrajectory(comTraj);
            comTask.setGain(1.0);

        }



        if( (i + 1) % (n_periods_step*10) == 0){
            //cout << "step foot status-----------------------------------------------------" << endl;

            if(footStatus == hr::core::kLeftFootOnGround){
                footStatus = hr::core::kRightFootOnGround;
            }
            else{
                footStatus = hr::core::kLeftFootOnGround;

            }

        }

        //if(t > 0.9) break;

        rightFootTask.update(t);
        leftFootTask.update(t);
        rightHandTask.update(t);
        leftHandTask.update(t);
        torsoTask.update(t);
        torsoZTask.update(t);
        headTask.update(t);
        comTask.update(t);




        std::vector<hr::utils::TaskAbstract*> taskList;
        hr::utils::SimpleTask swingFootTask;
        hr::utils::SimpleTask feetTask;
        hr::utils::SimpleTask handsTask;
        hr::utils::SimpleTask torsoOrientationTask;
        hr::utils::SimpleTask handsTorsoHeadTask;

        hr::VectorXr zeros(6);
        zeros.setZero();

        //Fix hands wrt to torso
        hr::MatrixXr righHandJacobian = rightHandTask.getMatrixA();
        hr::MatrixXr leftHandJacobian = leftHandTask.getMatrixA();
        righHandJacobian.leftCols(6).setZero();
        leftHandJacobian.leftCols(6).setZero();
        handsTask.addRows(righHandJacobian,zeros);
        handsTask.addRows(leftHandJacobian,zeros);
        //

        if(footStatus == hr::core::kBothFeetOnGround){
            cout << "BothFeet" << endl;

            feetTask.addRows(leftFootTask.getMatrixA(),leftFootTask.getVectorb());
            feetTask.addRows(rightFootTask.getMatrixA(),rightFootTask.getVectorb());

            taskList.push_back(&feetTask);
            taskList.push_back(&handsTask);
            taskList.push_back(&comTask);
            taskList.push_back(&torsoTask);
            taskList.push_back(&headTask);

        }

        else if(footStatus == hr::core::kRightFootOnGround){
            //cout << "RightFoot" << endl;
            feetTask.addRows(leftFootTask.getMatrixA(),leftFootTask.getVectorb());
            feetTask.addRows(rightFootTask.getMatrixA(),rightFootTask.getVectorb());

            swingFootTask.addRows(leftFootTask.getMatrixA(),leftFootTask.getVectorb());
            hr::Vector3r torsoOrientationError;
            hr::Vector3r torsoWXYZ;
            torsoWXYZ << 0.0*comTask.getVectorb()(1), 0, 0;
            torsoOrientationError = torsoWXYZ;

            torsoOrientationTask.addRows(torsoTask.getMatrixA(),torsoTask.getVectorb());
            taskList.push_back(&feetTask);
//            taskList.push_back(&rightFootTask);
//            taskList.push_back(&swingFootTask);


            handsTorsoHeadTask.addRows(handsTask.getMatrixA(),handsTask.getVectorb());
            handsTorsoHeadTask.addRows(headTask.getMatrixA(),headTask.getVectorb());
            handsTorsoHeadTask.addRows(torsoZTask.getMatrixA(),torsoZTask.getVectorb());

            taskList.push_back(&handsTorsoHeadTask);

            //taskList.push_back(&handsTask);
            //taskList.push_back(&headTask);
            //taskList.push_back(&torsoZTask);
            taskList.push_back(&comTask);
            taskList.push_back(&torsoOrientationTask);

        }
        else if(footStatus == hr::core::kLeftFootOnGround){
            //cout << "LeftFoot" << endl;
            feetTask.addRows(leftFootTask.getMatrixA(),leftFootTask.getVectorb());
            feetTask.addRows(rightFootTask.getMatrixA(),rightFootTask.getVectorb());

            swingFootTask.addRows(rightFootTask.getMatrixA(),rightFootTask.getVectorb());
            hr::Vector3r torsoOrientationError;
            hr::Vector3r torsoWXYZ;
            torsoWXYZ << 0.0*comTask.getVectorb()(1), 0, 0;
            torsoOrientationError = torsoWXYZ;

            torsoOrientationTask.addRows(torsoTask.getMatrixA(),torsoTask.getVectorb());

            taskList.push_back(&feetTask);
//            taskList.push_back(&leftFootTask);
//            taskList.push_back(&swingFootTask);


            handsTorsoHeadTask.addRows(handsTask.getMatrixA(),handsTask.getVectorb());
            handsTorsoHeadTask.addRows(headTask.getMatrixA(),headTask.getVectorb());
            handsTorsoHeadTask.addRows(torsoZTask.getMatrixA(),torsoZTask.getVectorb());

            taskList.push_back(&handsTorsoHeadTask);

            //taskList.push_back(&handsTask);
            //taskList.push_back(&headTask);
            //taskList.push_back(&torsoZTask);
            taskList.push_back(&comTask);
            taskList.push_back(&torsoOrientationTask);


        }


        hr::VectorXr dq = solver.Solve(taskList,C,ld,ud,lb,ub);

//        std::cout << "t " << t << std::endl;
        std::cout << "time_p " << time_p << std::endl;

        // Integrate
        qtmp.segment(0,24) = q.segment(0,24);
        qtmp.segment(24,5) = q.segment(25,5);
        qtmp += dq*integrationStepSize;
        t = t + integrationStepSize;
        q.segment(0,24) = qtmp.segment(0,24);
        q(24) = qtmp(18);
        q.segment(25,5) = qtmp.segment(24,5);
        // End of integration

        timer.stop();
//        cout << "Iteration time"<< endl;
//        cout << timer.format()<< endl;
        //Set the new configuration
        setNaoConfig(motionProxy,q, integrationStepSize);

        //! Move(Apply control) and Sleep for 5ms
        time.tv_sec = 0;
        time.tv_nsec = 10000000; //10ms
        nanosleep(&time,0);

    }

    myfile.close();
    myfile_joints.close();
    myfile_feet.close();
}


hr::VectorXr getNaoConfig(AL::ALMotionProxy & motionProxy){
    const int kNAO_DOF = 26;
    const bool useSensors = true;

    AL::ALValue jointNames;
    jointNames = AL::ALValue::array("Body");
    std::vector<float> sensorAngles = motionProxy.getAngles(jointNames, useSensors);
    Eigen::Map<Eigen::VectorXf> q_al(sensorAngles.data(),26);

    hr::VectorXr q_hr(30);
    alToHr(q_hr, q_al.cast<hr::real_t>());
    return q_hr;

}

bool setNaoConfig(AL::ALMotionProxy & motionProxy, const hr::VectorXr & q_hr, hr::real_t time ){
    const int kNAO_DOF = 26;
    const bool isAbsolute = true;

    AL::ALValue alAngles, timeLists;
    alAngles.clear();
    alAngles.arraySetSize(kNAO_DOF);

    timeLists.clear();
    timeLists.arraySetSize(kNAO_DOF);

    hr::VectorXr q_al(kNAO_DOF);

    hrToAl(q_hr,q_al);


    AL::ALValue jointNames;
    jointNames = AL::ALValue::array("Body");
    for (int i = 0; i!=kNAO_DOF; i++) {
        timeLists[i] = AL::ALValue::array((float) 5*time);
        alAngles[i] = q_al(i);
    }
    motionProxy.setAngles(jointNames,alAngles,1.0);

    //motionProxy.angleInterpolation(jointNames, alAngles, timeLists, isAbsolute);

    return true;

}




