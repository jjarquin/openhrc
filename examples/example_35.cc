// example 35, a pthread
#include <iostream>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>

#include <signal.h>
#include <time.h>

void * printMessage(void * ptr);
void * executeFunction( void * fnPtr);
void doThis(int number){
    std::cout << "Doing this " << number << std::endl;
}

struct arg_struct{
    void (*fn)(int);
    int n;
    bool run;
};


int main(){

    pthread_t thread_1, thread_2;
    const std::string message1 = "Thread 1";
    const std::string message2 = "Thread 2";

    int iret1, iret2;
    struct arg_struct args;
    args.fn = &doThis;
    args.n = 2;
    args.run = true;

    iret1 = pthread_create(&thread_1, NULL, printMessage, (void *) &args );
    if(iret1){
        std::cout << "Failed creating of thread 1" << std::endl;
        exit(EXIT_FAILURE);

    }

    std::cout << "pthread_create() for thread 1 returns: " << iret1 << std::endl;

    sleep(10);
    args.run = false;

    pthread_join( thread_1, NULL);


    exit(EXIT_SUCCESS);
}


void * printMessage(void *ptr){
    struct arg_struct * args = ( struct arg_struct *) ptr;
    struct timespec time;
    for(int i = 0 ; i!= 100000000000000000 ; i++){
        time.tv_sec = 1;
        time.tv_nsec = 0;
        args->fn(args->n);
        //nanosleep(&time,0);
        std::cout << "is " << (args->run ? "true" : "false" ) << std::endl;

        if(!args->run){
            break;
        }

    }
    //std::cout << "ok"  << std::endl;
}

