//Example of use of KinematicTask and ConvexHierarchicalSolver FootTrajectory to make a step in StaticWalkingMode
//The robot torso must be above the right foot

#include "openhrc/utils.h"
#include "openhrc/core.h"
#include "openhrc/kinematics.h"
#include <alproxies/almotionproxy.h>
#include <alerror/alerror.h>
#include <iostream>
#include <boost/timer/timer.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <fstream>

#ifdef  USE_LOCAL_XBOT //
std::string naoFile = "../../../src/data/Nao_blue_noninertial.xbot";
#else
std::string naoFile = "/home/nao/projects/hrData/Nao_blue_noninertial.xbot";
#endif

using boost::timer::cpu_timer;
using std::cout;
using std::endl;

hr::VectorXr getNaoConfig(AL::ALMotionProxy & motionProxy);
bool setNaoConfig(AL::ALMotionProxy & motionProxy, const hr::VectorXr & q, hr::real_t time );

void hrToAl(const hr::VectorXr & q_hr , hr::VectorXr & q_al){
    const int kNAO_DOF = 26;
    int jointId[kNAO_DOF] = {7,8,9,10,11,12,13,-1,19,20,21,22,23,24,25,26,27,28,29,30,14,15,16,17,18,-1};
    q_al.setZero();
    for (int i = 0 ; i < kNAO_DOF; i++) {
        if(jointId[i] != -1) {
            q_al(i) = q_hr(jointId[i]-1);
        }
    }

}

void alToHr(hr::VectorXr & q_hr , const hr::VectorXr & q_al){
    const int kNAO_DOF = 26;
    int jointId[kNAO_DOF] = {7,8,9,10,11,12,13,-1,19,20,21,22,23,24,-1,26,27,28,29,30,14,15,16,17,18,-1};
    q_hr.setZero();
    for (int i = 0 ; i < kNAO_DOF; i++) {
        if(jointId[i] != -1) {
            q_hr(jointId[i]-1) = q_al(i);
        }
    }
    q_hr(24) = q_hr(18);

}


int main(){
    cpu_timer timer;
    cpu_timer timer_ik;
    std::ofstream myfile;
    myfile.open ("example_17.dat");

    hr::core::World world = hr::core::World();
    std::string sFile = naoFile;
    int robotId = world.getRobotsVector()->size();
    world.loadMultiBody(sFile,robotId, hr::core::kNAO);
    boost::shared_ptr<hr::core::MultiBody> robot = world.getRobot(0);

    hr::VectorXr q = robot->getConfiguration();
    hr::VectorXr qtmp(q.rows()-1);

    cout << "Robot configuration (" << q.rows() <<"x1)" << endl << q << endl;

    AL::ALMotionProxy motionProxy = AL::ALMotionProxy("127.0.0.1");
    //AL::ALMotionProxy motionProxy = AL::ALMotionProxy("nao.local");

    q = getNaoConfig(motionProxy);
    robot->setConfiguration(q);
    robot->computeForwardKinematics();

    cout << "Robot configuration" << endl << q << endl;

    hr::Vector3r rightFootHandle(0.0,0.0,-0.04);
    int rightFootBodyId = hr::core::nao::kRightFoot;
    int rightFootHandleId = robot->addOperationalHandle(rightFootBodyId,rightFootHandle);

    hr::Vector3r leftFootHandle(0.0,0.0,-0.04);
    int leftFootBodyId = hr::core::nao::kLeftFoot;
    int leftFootHandleId = robot->addOperationalHandle(leftFootBodyId,leftFootHandle);


    hr::Vector3r rightHandHandle(0.08,0.0,0.0);
    int rightHandBodyId = hr::core::nao::kRightHand;
    int rightHandHandleId = robot->addOperationalHandle(rightHandBodyId,rightHandHandle);

    hr::Vector3r leftHandHandle(0.08,0.0,0.0);
    int leftHandBodyId = hr::core::nao::kLeftHand;
    int leftHandHandleId = robot->addOperationalHandle(leftHandBodyId,leftHandHandle);


    hr::Vector3r torsoHandle(0.0,0.0,0.0);
    int torsoBodyId = hr::core::nao::kTorso;
    int torsoHandleId = robot->addOperationalHandle(torsoBodyId,torsoHandle);

    hr::Vector3r headHandle(0.0,0.0,0.0);
    int headBodyId = hr::core::nao::kHead;
    int headHandleId = robot->addOperationalHandle(headBodyId,headHandle);

    hr::SpatialVector rightFootPose;
    hr::SpatialVector leftFootPose;

    hr::SpatialVector rightHandPose;
    hr::SpatialVector leftHandPose;

    hr::SpatialVector torsoPose;
    hr::SpatialVector headPose;


    hr::SpatialVector rightFootGoal;
    hr::SpatialVector leftFootGoal;

    hr::SpatialVector rightHandGoal;
    hr::SpatialVector leftHandGoal;

    hr::SpatialVector torsoGoal;
    hr::SpatialVector headGoal;

    //fot is at -0.3279 in Z
    rightFootGoal << 0.0, -0.05, -0.3279 , 0.0, 0.0, 0.0;
    leftFootGoal << 0.0, 0.05, -0.3279 , 0.0 , 0.0 , 0.0;

    //rightHandGoal << 0.15, -0.15, 0.0, 0.0, 0.0,0.0;
    //rightHandGoal << 0.052854, 0.058631,-0.0129944, 0.629214,0.705425,0.52007;
    rightHandGoal << 0.1, -0.1, 0.0, 0.0, 0.0, 0.0;
    //leftHandGoal << 0.15, 0.15, 0.0, 0.0, 0.0,0.0;
    //leftHandGoal << 0.0506791,-0.0604368,-0.0133796,-0.532732,0.694599,-0.601439;
    leftHandGoal << 0.1, 0.1, 0.0, 0.0, 0.0,0.0;

    torsoGoal << 0.0, 0.0, 0.0, 0.0, 0.0, 0.0;
    headGoal << 0.0,0.0,0.0, 0.0, 0.0,0.0;


    hr::Vector3r rightFootPosition = robot->getOperationalHandlePosition(rightFootBodyId,rightFootHandleId);
    hr::Vector3r rightFootOrientation = robot->getBodyOrientation(rightFootBodyId).eulerAngles(0,1,2);

    hr::Vector3r leftFootPosition = robot->getOperationalHandlePosition(leftFootBodyId,leftFootHandleId);
    hr::Vector3r leftFootOrientation = robot->getBodyOrientation(leftFootBodyId).eulerAngles(0,1,2);

    hr::Vector3r rightHandPosition = robot->getOperationalHandlePosition(rightHandBodyId,rightHandHandleId);
    hr::Vector3r rightHandOrientation = robot->getBodyOrientation(rightHandBodyId).eulerAngles(0,1,2);

    hr::Vector3r leftHandPosition = robot->getOperationalHandlePosition(leftHandBodyId,leftHandHandleId);
    hr::Vector3r leftHandOrientation = robot->getBodyOrientation(leftHandBodyId).eulerAngles(0,1,2);

    hr::Vector3r torsoPosition = robot->getOperationalHandlePosition(torsoBodyId,torsoHandleId);
    hr::Vector3r torsoOrientation = robot->getBodyOrientation(torsoBodyId).eulerAngles(0,1,2);

    hr::Vector3r headPosition = robot->getOperationalHandlePosition(headBodyId,headHandleId);
    hr::Vector3r headOrientation = robot->getBodyOrientation(headBodyId).eulerAngles(0,1,2);



    rightFootPose << rightFootPosition , rightFootOrientation;
    leftFootPose << leftFootPosition, leftFootOrientation;
    torsoPose << torsoPosition, torsoOrientation;
    rightHandPose << rightHandPosition , rightHandOrientation;
    leftHandPose << leftHandPosition , leftHandOrientation;
    headPose << headPosition, headOrientation;

    rightFootGoal << rightFootPosition , 0 , 0, 0.0;
    leftFootGoal << leftFootPosition(0)+0.8,leftFootPosition(1),leftFootPosition(2), 0, 0 , 0.3;  //Step goal
    //leftFootGoal << leftFootPosition(0),leftFootPosition(1), leftFootPosition(2)+0.15, 0, 0 , 0;  //Lift foot goal

    boost::shared_ptr<hr::core::FootTrajectory> leftFootTraj = boost::make_shared<hr::core::FootTrajectory>(0.0,1.0,leftFootPose,leftFootGoal,hr::core::kFootCycloidal);
    //boost::shared_ptr<hr::core::PointTrajectory> leftFootTraj = boost::make_shared<hr::core::PointTrajectory>(0,3.0,leftFootPose,leftFootGoal,hr::core::kQuinticPolynomial);
    hr::core::KinematicTask task_01(rightFootHandleId,rightFootBodyId,0,robot, hr::core::kPositionOrientation);
    task_01.setGoal(rightFootGoal);
    task_01.setGain(0.5);
    hr::core::KinematicTask task_02(leftFootHandleId,leftFootBodyId,0,robot, hr::core::kPositionOrientation);
    task_02.setTrajectory(leftFootTraj);
    cout << "Left foot pose " <<  leftFootPose.transpose() << endl;
    task_02.setGain(0.5);
    hr::core::KinematicTask task_03(rightHandHandleId,rightHandBodyId,0,robot, (hr::core::kPosition ));
    task_03.setGoal(rightHandGoal);
    hr::core::KinematicTask task_04(leftHandHandleId,leftHandBodyId,0,robot, (hr::core::kPosition));
    task_04.setGoal(leftHandGoal);
    hr::core::KinematicTask task_05(torsoHandleId,torsoBodyId,0,robot, (  hr::core::kWXYZ ));
    task_05.setGoal(torsoGoal);
    hr::core::KinematicTask task_06(headHandleId,headBodyId,0,robot, hr::core::kWYZ);
    task_06.setGoal(headGoal);


    hr::utils::ConstrainedHierarchicalSolver solver;

    hr::real_t integrationStepSize = 0.05;


    hr::VectorXr lb_ = (robot->getJointLowLimits()-q)/integrationStepSize;
    hr::VectorXr ub_ = (robot->getJointUpLimits()-q)/integrationStepSize;

    hr::VectorXr lb(29);
    hr::VectorXr ub(29);

    lb.segment(0,24) = lb_.segment(0,24);
    lb.segment(24,5) = lb_.segment(25,5);

    ub.segment(0,24) = ub_.segment(0,24);
    ub.segment(24,5) = ub_.segment(25,5);

    hr::VectorXr ld(29);
    hr::VectorXr ud(29);
    hr::MatrixXr C;

    hr::VectorXr ud_ = robot->getJointUpSpeedLimits();
    hr::VectorXr ld_ = robot->getJointLowSpeedLimits();

    int n = 29;
    C = hr::MatrixXr::Identity(n,n);

    ld.segment(0,24) = ld_.segment(0,24);
    ld.segment(24,5) = ld_.segment(25,5);

    ud.segment(0,24) = ud_.segment(0,24);
    ud.segment(24,5) = ud_.segment(25,5);

    Eigen::VectorXd q_tmp = getNaoConfig(motionProxy);
    robot->setConfiguration(q_tmp);
    robot->computeForwardKinematics();

    hr::core::nao::SupportBaseConstraint constraint;
    hr::Vector3r rightFootXYTheta;
    hr::Vector3r leftFootXYTheta;
    hr::Vector2r comXY;
    hr::Matrix2Xr comXYJacobian;

    hr::real_t t = 0.0;

    for(int i = 0; i != 150; i++){
        timer.start();

        hr::VectorXr q_last = q_tmp;

        q_tmp = getNaoConfig(motionProxy);
        q_tmp.segment(0,6) = q.segment(0,6);

        cout << "q" << endl<< q.transpose() << endl;
        cout << "q_tmp" << endl<< q_tmp.transpose() << endl;

        hr::VectorXr dq_got = (q_tmp - q_last)/integrationStepSize;

        //robot->setConfiguration(q);
        robot->setConfiguration(q);
        robot->computeForwardKinematics();

        //Calculate the new joint speed limit
        lb_ = (robot->getJointLowLimits()-q)/integrationStepSize;
        ub_ = (robot->getJointUpLimits()-q)/integrationStepSize;

        lb.segment(0,24) = lb_.segment(0,24);
        lb.segment(24,5) = lb_.segment(25,5);

        ub.segment(0,24) = ub_.segment(0,24);
        ub.segment(24,5) = ub_.segment(25,5);


        task_01.update();
        task_02.update(t);
        task_03.update(t);
        task_04.update();
        task_05.update();
        task_06.update();



        rightFootXYTheta << rightFootPosition(0),rightFootPosition(1), rightFootOrientation(2);
        leftFootXYTheta << leftFootPosition(0),leftFootPosition(1), leftFootOrientation(2);

        comXY << torsoPosition(0), torsoPosition(1);
        hr::MatrixXr comJacobian = robot->getJacobian(hr::core::kGeometricJacobian,hr::core::nao::kTorso,torsoHandleId);
        comXYJacobian = comJacobian.topRows(2);

        constraint.setConstraint(comXYJacobian,comXY,leftFootXYTheta,rightFootXYTheta,hr::core::kRightFootOnGround,0.1);

        std::string pName = "Body";
        int pSpace = 0; // FRAME_TORSO
        bool pUseSensors = true;
        std::vector<float> pos = motionProxy.getCOM(pName, pSpace, pUseSensors);
        Eigen::Map<Eigen::VectorXf> com(pos.data(),3);
        cout << "CoM" << endl << com.transpose() << endl;

        hr::utils::SimpleTask feetTask;
        feetTask.addRows(task_01.getMatrixA(),task_01.getVectorb());
        feetTask.addRows(task_02.getMatrixA(),task_02.getVectorb());

        hr::utils::SimpleTask swingFootTask;

        swingFootTask.addRows(task_02.getMatrixA(),task_02.getVectorb());
        swingFootTask.addConstraintRows(constraint.getMatrixC(),constraint.getVectord());

        hr::utils::SimpleTask handTask;

        handTask.addRows(task_03.getMatrixA(),task_03.getVectorb());
        handTask.addRows(task_04.getMatrixA(),task_04.getVectorb());

        std::vector<hr::utils::TaskAbstract*> taskList;
        //taskList.push_back(&feetTask);
        taskList.push_back(&task_01); //rightFootTask task_01
        taskList.push_back(&swingFootTask); //leftFootTask task_02
        //taskList.push_back(&task_05); //torsoTask  task_05
        //taskList.push_back(&handTask);
        //taskList.push_back(&headTask); //headTask  task_06

        cout<< "Error Task 1: " << endl << task_01.getVectorb() << endl;
        cout<< "Error Task 2: " << endl << task_02.getVectorb() << endl;
        cout<< "Error Task 3: " << endl << task_03.getVectorb() << endl;
        cout<< "Error Task 4: " << endl << task_04.getVectorb() << endl;
        cout<< "Error Task 5: " << endl << task_05.getVectorb() << endl;
        cout<< "Error Task 6: " << endl << task_06.getVectorb() << endl;


        timer_ik.start();
        hr::VectorXr dq = solver.Solve(taskList,C,ld,ud,lb,ub);
        timer_ik.stop();

        std::cout << "t " << t << std::endl;

        torsoPosition = robot->getOperationalHandlePosition(torsoBodyId,torsoHandleId);
        leftFootPosition = robot->getOperationalHandlePosition(leftFootBodyId,leftFootHandleId);


        //print results to a file
        myfile << t << " " << torsoPosition(0) << " " << torsoPosition(1) << " " << torsoPosition(2)  << std::endl;

        // Integrate
        qtmp.segment(0,24) = q.segment(0,24);
        qtmp.segment(24,5) = q.segment(25,5);
        qtmp += dq*integrationStepSize;
        t = t + integrationStepSize;
        q.segment(0,24) = qtmp.segment(0,24);
        q(24) = qtmp(18);
        q.segment(25,5) = qtmp.segment(24,5);
        // End of integration



        timer.stop();
        cout << "Iteration time" << endl;
        cout << timer.format() << endl;
        cout << "InverseKinematics time" << endl;
        cout << timer_ik.format() << endl;

        //Set the new configuration
        setNaoConfig(motionProxy,q, integrationStepSize);
    }

    myfile.close();
}


hr::VectorXr getNaoConfig(AL::ALMotionProxy & motionProxy){
    const int kNAO_DOF = 26;
    const bool useSensors = false;

    AL::ALValue jointNames;
    jointNames = AL::ALValue::array("Body");
    std::vector<float> sensorAngles = motionProxy.getAngles(jointNames, useSensors);
    Eigen::Map<Eigen::VectorXf> q_al(sensorAngles.data(),26);

    hr::VectorXr q_hr(30);
    alToHr(q_hr, q_al.cast<hr::real_t>());
    return q_hr;

}

bool setNaoConfig(AL::ALMotionProxy & motionProxy, const hr::VectorXr & q_hr, hr::real_t time ){
    const int kNAO_DOF = 26;
    const bool isAbsolute = true;

    AL::ALValue alAngles, timeLists;
    alAngles.clear();
    alAngles.arraySetSize(kNAO_DOF);

    timeLists.clear();
    timeLists.arraySetSize(kNAO_DOF);

    hr::VectorXr q_al(kNAO_DOF);

    hrToAl(q_hr,q_al);


    AL::ALValue jointNames;
    jointNames = AL::ALValue::array("Body");
    for (int i = 0; i!=kNAO_DOF; i++) {
        timeLists[i] = AL::ALValue::array((float) time);
        alAngles[i] = q_al(i);
    }

    motionProxy.angleInterpolation(jointNames, alAngles, timeLists, isAbsolute);

    return true;

}




