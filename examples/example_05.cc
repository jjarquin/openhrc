//Example of use of the SimpleHierarchicalSolver class


#include "openhrc/core.h"
#include "openhrc/locomotion.h"
#include "openhrc/utils.h"
#include <alproxies/almotionproxy.h>
#include <alerror/alerror.h>
#include <iostream>

#ifdef  USE_LOCAL_XBOT //
    std::string naoFile = "../../../src/data/Nao_blue_noninertial.xbot";
#else
    std::string naoFile = "/home/nao/projects/hrData/Nao_blue_noninertial.xbot";
#endif

using std::cout;
using std::endl;

hr::VectorXr getNaoConfig(AL::ALMotionProxy & motionProxy);
bool setNaoConfig(AL::ALMotionProxy & motionProxy, const hr::VectorXr & q, hr::real_t time );

void hrToAl(const hr::VectorXr & q_hr , hr::VectorXr & q_al){
    const int kNAO_DOF = 26;
    int jointId[kNAO_DOF] = {7,8,9,10,11,12,13,-1,19,20,21,22,23,24,-1,26,27,28,29,30,14,15,16,17,18,-1};
    q_al.setZero();
    for (int i = 0 ; i < kNAO_DOF; i++) {
        if(jointId[i] != -1) {
            q_al(i) = q_hr(jointId[i]-1);
        }
    }
}

void alToHr(hr::VectorXr & q_hr , const hr::VectorXr & q_al){
    const int kNAO_DOF = 26;
    int jointId[kNAO_DOF] = {7,8,9,10,11,12,13,-1,19,20,21,22,23,24,-1,26,27,28,29,30,14,15,16,17,18,-1};
    q_hr.setZero();
    for (int i = 0 ; i < kNAO_DOF; i++) {
        if(jointId[i] != -1) {
            q_hr(jointId[i]-1) = q_al(i);
        }
    }

}

hr::MatrixXr geometricToAnalytical(hr::MatrixXr jacobian_w, hr::Vector3r handleOrientation){
    //Compute the analytical Jacobian
    Eigen::Vector3d eulerAngles;
    Eigen::Matrix3d R1;
    Eigen::Matrix3d R2;
    Eigen::Matrix3d R3;
    Eigen::Matrix3d Jtheta;
    Eigen::Vector3d axis;
    Eigen::Vector3i eulerConvention;
    eulerConvention << 0,1,2;

    axis<<1,0,0;
    Eigen::AngleAxisd r1(handleOrientation(0),axis);
    axis<<0,1,0;
    Eigen::AngleAxisd r2(handleOrientation(1),axis);
    axis<<0,0,1;
    Eigen::AngleAxisd r3(handleOrientation(2),axis);

    R1 = r1.toRotationMatrix();
    R2 = R1*r2.toRotationMatrix();
    R3 = R2*r3.toRotationMatrix();

    Jtheta.col(0) = R1.col(eulerConvention(0));
    Jtheta.col(1) = R2.col(eulerConvention(1));
    Jtheta.col(2) = R3.col(eulerConvention(2));

    return Jtheta.inverse() * jacobian_w;

}

int main(){

    hr::core::World world = hr::core::World();
    std::string sFile = naoFile;
    int robotId = world.getRobotsVector()->size();
    world.loadMultiBody(sFile,robotId, hr::core::kNAO);
    boost::shared_ptr<hr::core::MultiBody> robot = world.getRobot(0);
    hr::VectorXr upSpeedLimit = robot->getJointUpSpeedLimits();
    hr::VectorXr lowSpeedLimit = robot->getJointLowSpeedLimits();
    hr::VectorXr lowLimit = robot->getJointLowLimits();
    hr::VectorXr upLimit = robot->getJointUpLimits();

    hr::VectorXr q = robot->getConfiguration();
    hr::VectorXr qtmp(q.rows()-1);

    cout << "Robot configuration (" << q.rows() <<"x1)" << endl << q << endl;

    AL::ALMotionProxy motionProxy = AL::ALMotionProxy("127.0.0.1");

    setNaoConfig(motionProxy,q,1);
    q = getNaoConfig(motionProxy);
    robot->setConfiguration(q);
    robot->computeForwardKinematics();

    cout << "Robot configuration" << endl << q << endl;

    hr::Vector3r rightFootHandle(0.0,0.01,-0.04);
    int rightFootBodyId = 30;
    int rightFootHandleId = robot->addOperationalHandle(rightFootBodyId,rightFootHandle);

    hr::Vector3r leftFootHandle(0.0,0.01,-0.04);
    int leftFootBodyId = 24;
    int leftFootHandleId = robot->addOperationalHandle(leftFootBodyId,leftFootHandle);

    hr::Vector3r torsoHandle(0.0,0.0,0.0);
    int torsoBodyId = 6;
    int torsoHandleId = robot->addOperationalHandle(torsoBodyId,torsoHandle);

    cout << "Left foot position: " << endl << robot->getBodyPosition(leftFootBodyId) << endl;
    cout << "Right foot position: " << endl << robot->getBodyPosition(rightFootBodyId) << endl;
    cout << "Torso position: " << endl << robot->getBodyPosition(torsoBodyId) << endl;

    // A simple test to move the CoM towards the left foot. -- 10 iterations
    hr::MatrixXr leftFootJacobian;
    hr::MatrixXr rightFootJacobian;
    hr::MatrixXr torsoJacobian;

    hr::MatrixXr leftFootJacobian_w;
    hr::MatrixXr rightFootJacobian_w;
    hr::MatrixXr torsoJacobian_w;

    hr::VectorXr leftFootError;
    hr::VectorXr rightFootError;
    hr::VectorXr torsoError;

    hr::VectorXr leftFootErrorOrientation;
    hr::VectorXr rightFootErrorOrientation;
    hr::VectorXr torsoErrorOrientation;

    hr::Vector3r leftFootGoal;
    hr::Vector3r leftFootGoalOrientation;

    leftFootGoal << 0.00 , 0.05, -0.33; //, 0.0 , 0.0 , 0.0;
    leftFootGoalOrientation << 0.0 , 0.0 , 0.2;

    hr::Vector3r rightFootGoal;
    hr::Vector3r rightFootGoalOrientation;

    rightFootGoal << 0.0 , -0.05, -0.33; //, 0.0, 0.0, 0.0;
    rightFootGoalOrientation << 0.0 , 0.0 , -0.2;

    hr::Vector3r torsoGoal;
    hr::Vector3r torsoGoalOrientation;

    torsoGoal << 0.0, 0.0, 0.0;
    torsoGoalOrientation << 0.0, 0.0, 0.0;

    hr::Vector3r leftFootPosition;
    hr::Vector3r rightFootPosition;
    hr::Vector3r torsoPosition;


    hr::Vector3r leftFootOrientation;
    hr::Vector3r rightFootOrientation;
    hr::Vector3r torsoOrientation;


    hr::utils::SimpleHierarchicalSolver solver;

    hr::real_t integrationStepSize = 0.2;


    hr::VectorXr lb_ = (robot->getJointLowLimits()-q)/integrationStepSize;
    hr::VectorXr ub_ = (robot->getJointUpLimits()-q)/integrationStepSize;

    hr::VectorXr lb(29);
    hr::VectorXr ub(29);

    lb.segment(0,24) = lb_.segment(0,24);
    lb.segment(24,5) = lb_.segment(25,5);

    ub.segment(0,24) = ub_.segment(0,24);
    ub.segment(24,5) = ub_.segment(25,5);

    hr::VectorXr ld(29);
    hr::VectorXr ud(29);
    hr::MatrixXr C;

    hr::VectorXr ud_ = robot->getJointUpSpeedLimits();
    hr::VectorXr ld_ = robot->getJointLowSpeedLimits();

    int n = 29;
    C = hr::MatrixXr::Identity(n,n);

    ld.segment(0,24) = ld_.segment(0,24);
    ld.segment(24,5) = ld_.segment(25,5);

    ud.segment(0,24) = ud_.segment(0,24);
    ud.segment(24,5) = ud_.segment(25,5);


//    ud.resize(n,1);
//    ud.setOnes();
//    ud = 3.5*ud;

 //   ld.resize(n,1);
 //   ld.setOnes();
 //   ld = -3.5*ld;



    for(int i = 0 ; i != 100 ; i++){
        leftFootJacobian = robot->getJacobian(hr::core::kGeometricJacobian_Jv, leftFootBodyId, leftFootHandleId);
        rightFootJacobian = robot->getJacobian(hr::core::kGeometricJacobian_Jv, rightFootBodyId, rightFootHandleId);
        torsoJacobian = robot->getJacobian(hr::core::kGeometricJacobian_Jv, torsoBodyId, torsoHandleId);

        leftFootJacobian_w = robot->getJacobian(hr::core::kGeometricJacobian_Jw, leftFootBodyId, leftFootHandleId);
        rightFootJacobian_w = robot->getJacobian(hr::core::kGeometricJacobian_Jw, rightFootBodyId, rightFootHandleId);
        torsoJacobian_w = robot->getJacobian(hr::core::kGeometricJacobian_Jw, torsoBodyId, torsoHandleId);

        //error = desiredValue - actualValue
        leftFootPosition = robot->getOperationalHandlePosition(leftFootBodyId,leftFootHandleId);
        leftFootError = leftFootGoal - leftFootPosition;

        rightFootPosition = robot->getOperationalHandlePosition(rightFootBodyId,rightFootHandleId);
        rightFootError = rightFootGoal - rightFootPosition;

        torsoPosition = robot->getOperationalHandlePosition(torsoBodyId,torsoHandleId);
        torsoError = (torsoGoal - torsoPosition);

        leftFootOrientation = robot->getOperationalHandleOrientation(leftFootBodyId,leftFootHandleId).eulerAngles(0,1,2);
        rightFootOrientation = robot->getOperationalHandleOrientation(rightFootBodyId,rightFootHandleId).eulerAngles(0,1,2);
        cout << "LeftO = " << endl << leftFootOrientation << endl;
        cout << "RightO = " << endl << rightFootOrientation << endl;
        cout << "TorsoeftO = " << endl << torsoOrientation << endl;

        leftFootErrorOrientation = leftFootGoalOrientation - leftFootOrientation;
        rightFootErrorOrientation = rightFootGoalOrientation - rightFootOrientation;
        torsoErrorOrientation = torsoGoalOrientation - torsoOrientation;

        hr::MatrixXr leftJ_w = geometricToAnalytical(leftFootJacobian_w,leftFootOrientation);
        hr::MatrixXr rightJ_w = geometricToAnalytical(rightFootJacobian_w,rightFootOrientation);
        hr::MatrixXr torsoJ_w = geometricToAnalytical(torsoJacobian_w,torsoOrientation);

        hr::utils::SimpleTask task_01;
        hr::utils::SimpleTask task_02;
        hr::utils::SimpleTask task_03;

        task_01.addRows(leftFootJacobian,leftFootError);
        task_01.addRows(leftJ_w,leftFootErrorOrientation);
        task_01.addRows(rightFootJacobian,rightFootError);
        task_01.addRows(rightJ_w,rightFootErrorOrientation);

        ////torso task

        hr::MatrixXr torsoJselect(6,29);
        hr::VectorXr torsobselect(6);

        torsoJselect.row(0) = torsoJacobian.row(0);
        torsoJselect.row(1) = torsoJacobian.row(1);
        torsoJselect.row(2) = torsoJacobian.row(2);
        torsoJselect.row(3) = torsoJ_w.row(0);
        torsoJselect.row(4) = torsoJ_w.row(1);
        torsoJselect.row(5) = torsoJ_w.row(2);


        torsobselect(0) = torsoError(0);
        torsobselect(1) = torsoError(1);
        torsobselect(2) = torsoError(2);
        torsobselect(3) = torsoErrorOrientation(0);
        torsobselect(4) = torsoErrorOrientation(1);
        torsobselect(5) = torsoErrorOrientation(2);



   /* The good one

        hr::MatrixXr torsoJselect(3,29);
        hr::VectorXr torsobselect(3);



        torsoJselect.row(0) = torsoJacobian.row(0);
        torsoJselect.row(1) = torsoJacobian.row(1);
        torsoJselect.row(2) = torsoJ_w.row(1);

        torsobselect(0) = torsoError(0);
        torsobselect(1) = torsoError(1);
        torsobselect(2) = torsoErrorOrientation(1);
  */

        ///solved


        task_03.addRows(torsoJselect,torsobselect);

        std::vector<hr::utils::TaskAbstract*> taskList;
        taskList.push_back(&task_01);

//        taskList.push_back(&task_02);
        taskList.push_back(&task_03);



        cout << "Torso jacobian" << endl <<torsoJacobian << endl;
//       cout<< "Jacobian Task 1 ("<< task_01.getMatrixA().rows() << " x "<< task_01.getMatrixA().cols()  <<" ): " << endl << task_01.getMatrixA() << endl;
       cout<< "Error Task 1: " << endl << task_01.getVectorb() << endl;
       cout<< "Error Task 2: " << endl << task_02.getVectorb() << endl;
       cout<< "Error Task 3: " << endl << task_03.getVectorb() << endl;

//        cout<< "Jacobian Task 2: " << endl << task_02.getMatrixA() << endl;
//        cout<< "Error Task 2: " << endl << task_02.getVectorb() << endl;
        hr::VectorXr dq = solver.Solve(taskList,C,ld,ud,lb,ub);
        //cout << "q BEFORE (" << dq.rows() << "x1) = :" << endl << dq<< endl;

        qtmp.segment(0,24) = q.segment(0,24);
        qtmp.segment(24,5) = q.segment(25,5);
        qtmp += dq*integrationStepSize;
        q.segment(0,24) = qtmp.segment(0,24);
        q(24) = qtmp(18);
        q.segment(25,5) = qtmp.segment(24,5);

        //q = q + integrationStepSize*dq;
        setNaoConfig(motionProxy,q, integrationStepSize);
        cout << " q: " << endl << q << endl;
        robot->setConfiguration(q);
        robot->computeForwardKinematics();

        lb_ = (robot->getJointLowLimits()-q)/integrationStepSize;
        ub_ = (robot->getJointUpLimits()-q)/integrationStepSize;

        lb.segment(0,24) = lb_.segment(0,24);
        lb.segment(24,5) = lb_.segment(25,5);

        ub.segment(0,24) = ub_.segment(0,24);
        ub.segment(24,5) = ub_.segment(25,5);
    }

}


hr::VectorXr getNaoConfig(AL::ALMotionProxy & motionProxy){
    const int kNAO_DOF = 26;
    const bool useSensors = true;

    AL::ALValue jointNames;
    jointNames = AL::ALValue::array("Body");
    std::vector<float> sensorAngles = motionProxy.getAngles(jointNames, useSensors);
    Eigen::Map<Eigen::VectorXf> q_al(sensorAngles.data(),26);

    hr::VectorXr q_hr(30);
    alToHr(q_hr, q_al.cast<hr::real_t>());
    return q_hr;

}

bool setNaoConfig(AL::ALMotionProxy & motionProxy, const hr::VectorXr & q_hr, hr::real_t time ){
    const int kNAO_DOF = 26;
    const bool isAbsolute = true;

    AL::ALValue alAngles, timeLists;
    alAngles.clear();
    alAngles.arraySetSize(kNAO_DOF);

    timeLists.clear();
    timeLists.arraySetSize(kNAO_DOF);

    hr::VectorXr q_al(kNAO_DOF);

    hrToAl(q_hr,q_al);


    AL::ALValue jointNames;
    jointNames = AL::ALValue::array("Body");

    for (int i = 0; i!=kNAO_DOF; i++) {
        timeLists[i] = AL::ALValue::array((float) time);
        alAngles[i] = q_al(i);
    }
    motionProxy.setAngles(jointNames,alAngles,1.0);
    //motionProxy.angleInterpolation(jointNames, alAngles, timeLists, isAbsolute);

    return true;

}



