//Example of the hr::locomotion module and the hr::core module

#include "openhrc/core.h"
#include "openhrc/locomotion.h"
#include "qpOASES.hpp"
#include <boost/shared_ptr.hpp>
#include <alproxies/almotionproxy.h>
#include <alerror/alerror.h>
#include <boost/timer/timer.hpp>
#include <iostream>

#ifdef  USE_LOCAL_XBOT //
    std::string naoFile = "../../../src/data/Nao_blue_noninertial.xbot";
#else
    std::string naoFile = "/home/nao/projects/hrData/Nao_blue_noninertial.xbot";
#endif

using std::cout;
using std::endl;
using boost::timer::cpu_timer;


hr::VectorXr getNaoConfig(AL::ALMotionProxy & motionProxy);
bool setNaoConfig(AL::ALMotionProxy & motionProxy, const hr::VectorXr & q, hr::real_t time );

void hrToAl(const hr::VectorXr & q_hr , hr::VectorXr & q_al){
    const int kNAO_DOF = 26;
    int jointId[kNAO_DOF] = {7,8,9,10,11,12,13,-1,19,20,21,22,23,24,-1,26,27,28,29,30,14,15,16,17,18,-1};
    q_al.setZero();
    for (int i = 0 ; i < kNAO_DOF; i++) {
        if(jointId[i] != -1) {
            q_al(i) = q_hr(jointId[i]-1);
        }
    }
}

void alToHr(hr::VectorXr & q_hr , const hr::VectorXr & q_al){
    const int kNAO_DOF = 26;
    int jointId[kNAO_DOF] = {7,8,9,10,11,12,13,-1,19,20,21,22,23,24,-1,26,27,28,29,30,14,15,16,17,18,-1};
    q_hr.setZero();
    for (int i = 0 ; i < kNAO_DOF; i++) {
        if(jointId[i] != -1) {
            q_hr(jointId[i]-1) = q_al(i);
        }
    }

}

int main(){

    hr::core::World world = hr::core::World();
    std::string sFile = naoFile;
    int robotId = world.getRobotsVector()->size();
    world.loadMultiBody(sFile,robotId, hr::core::kNAO);
    boost::shared_ptr<hr::core::MultiBody> robot = world.getRobot(0);
    hr::VectorXr upSpeedLimit = robot->getJointUpSpeedLimits();
    hr::VectorXr lowSpeedLimit = robot->getJointLowSpeedLimits();
    hr::VectorXr lowLimit = robot->getJointLowLimits();
    hr::VectorXr upLimit = robot->getJointUpLimits();

    hr::VectorXr q = robot->getConfiguration();


    cout << "Robot configuration (" << q.rows() <<"x1)" << endl << q << endl;

    AL::ALMotionProxy motionProxy = AL::ALMotionProxy("127.0.0.1");

    setNaoConfig(motionProxy,q,1);
    q = getNaoConfig(motionProxy);
    robot->setConfiguration(q);
    robot->computeForwardKinematics();

    cout << "Robot configuration" << endl << q << endl;

    hr::Vector3r rightFootHandle(0.0,0.0,-0.04);
    int rightFootBodyId = 30;
    int rightFootHandleId = robot->addOperationalHandle(rightFootBodyId,rightFootHandle);

    hr::Vector3r leftFootHandle(0.0,0.0,-0.04);
    int leftFootBodyId = 24;
    int leftFootHandleId = robot->addOperationalHandle(leftFootBodyId,leftFootHandle);

    hr::Vector3r torsoHandle(0.0,0.05,0.0);
    int torsoBodyId = 6;
    int torsoHandleId = robot->addOperationalHandle(torsoBodyId,torsoHandle);

    hr::Vector3r leftFootPosition = robot->getOperationalHandlePosition(leftFootBodyId,leftFootHandleId);
    hr::Vector3r rightFootPosition = robot->getOperationalHandlePosition(rightFootBodyId,rightFootHandleId);
    hr::Vector3r torsoPosition = robot->getOperationalHandlePosition(torsoBodyId,torsoHandleId);
    cout << "Left foot position: " << endl << leftFootPosition << endl;
    cout << "Right foot position: " << endl << rightFootPosition << endl;
    cout << "Torso position: " << endl << torsoPosition << endl;



    cout << "Starting locomotion test-part" << endl;
    cpu_timer timer;

    /** MPC parameters */
    int m_steps = 2;
    int n_periods_step = 8;
    int N_horizon = m_steps*n_periods_step;
    hr::real_t T_sampling = 0.1;
    hr::real_t com_height = 0.3 ;

       /** These parameters describe the state of the robot */

    hr::Vector3r footState;    //Member for the position of the current foot on the ground
    hr::Vector3r nextFootState;    //Member for the position of the current foot on the ground
    hr::locomotion::RobotState robotState;        //Member for the augmented CoM robot state
    hr::Vector3r xk;              //Variable for the CoM robot state in X
    hr::Vector3r yk;              //Variable for the CoM robot state in Y
    hr::Vector3r thetak;          //Variable for the CoM robot state in Theta
    hr::VectorXr x_vel(N_horizon);        //Reference velocity in X direction     //TODO: Time list trajectories similar to ALDCM
    hr::VectorXr y_vel(N_horizon);        //Reference velocity in Y direction     //TODO
    hr::core::FootStatus footStatus = hr::core::kLeftFootOnGround;
    hr::Vector3r jerkXYTheta; //Jerk holder variable
    hr::VectorXr uk;      ///Jerk vector
    uk.resize(2*(N_horizon+m_steps));
    uk.setZero();
    /** Initialization of the robot parameters */

    cout << "Init robot parameters" << endl;
    footState << leftFootPosition(0), leftFootPosition(1),0.0;   //For this example the left foot is the starting foot.  //TODO: 04/2015: I would prefer footPosition instead of state
    xk << 0.0 , 0.0, 0.0;
    yk << 0.05 , 0.0, 0.0;
    thetak << 0.0, 0.0 ,0.0;
    robotState.setState(xk,yk,thetak);
    jerkXYTheta << 0.0,0.,0.;
    x_vel.setOnes();
    y_vel.setOnes();
    x_vel = x_vel*1.00;
    y_vel = y_vel*0.00;

    /** Initialization of the MPC */
    hr::locomotion::MPCData mpcData = hr::locomotion::MPCData(com_height,T_sampling,m_steps,n_periods_step);
    hr::locomotion::MPCConstraints constraints;
    hr::locomotion::RobotSystem system;

    mpcData.init();
    constraints.init(mpcData);
    system.init(com_height,T_sampling);

    constraints.setConstraints(mpcData,footStatus, footState, robotState);

    int nWSR = 1000;
    qpOASES::real_t cpuTime = 1.0;

    int nV = 2*(N_horizon+m_steps);
    int nC = constraints.getbupVector().rows();
    qpOASES::SQProblem quad;
    boost::shared_ptr<qpOASES::SQProblem> quadprog;
    qpOASES::Options options;
    options.setToMPC();
    options.printLevel = qpOASES::PL_NONE;
    quadprog.reset(new qpOASES::SQProblem(nV,nC));
    quadprog->setOptions(options);
    timer.start();
    quadprog->init(mpcData.getHessian().data(), mpcData.getFvector(robotState,footState,x_vel,y_vel).data(),constraints.getAMatrix().data(),NULL,NULL,NULL,constraints.getbupVector().data(),nWSR,&cpuTime);
    if(quadprog->getPrimalSolution(uk.data()) == qpOASES::RET_QP_NOT_SOLVED){
        return hr::FAILED_INIT;
        cout << "failed to solve" << endl;
    }
    timer.stop();
    cout << "qpoases solved, time:" << endl;
    cout << timer.format() << endl;

    nWSR = 1000;
    cpuTime = 0.003;
    timer.start();
    quadprog->hotstart(mpcData.getHessian().data(), mpcData.getFvector(robotState,footState,x_vel,y_vel).data(),constraints.getAMatrix().data(),NULL,NULL,NULL,constraints.getbupVector().data(),nWSR,&cpuTime);
    if(quadprog->getPrimalSolution(uk.data()) == qpOASES::RET_QP_NOT_SOLVED){
        return hr::FAILED_INIT;
        cout << "failed to solve" << endl;
    }
    timer.stop();
    cout << "hot qpoases solved, warm solving time:" << endl;
    cout << timer.format() << endl;
    cout << "cputime in ms" << endl << cpuTime*1000 << endl;

        jerkXYTheta(0) = uk(0);
        jerkXYTheta(1) = uk(N_horizon+m_steps);
        nextFootState(0) = uk(N_horizon);
        nextFootState(1) = uk(N_horizon+m_steps+N_horizon);


    cout << "Uk size:" << endl << uk.rows() << endl;
    cout << "Uk:" << endl << uk << endl;

    //

    cout << "Everything seems to be working fine..." << endl;
    cout << "Before Robot State" << endl << robotState << endl;
    system.nextState(jerkXYTheta,robotState);
    cout <<  "Applied Jerk" << endl << jerkXYTheta << endl;
    cout << "New Robot State" << endl << robotState << endl;
    cout << "Current foot:" << endl << footState << endl;
    cout << "Next foot:" << endl << nextFootState << endl;




}


hr::VectorXr getNaoConfig(AL::ALMotionProxy & motionProxy){
    const int kNAO_DOF = 26;
    const bool useSensors = true;

    AL::ALValue jointNames;
    jointNames = AL::ALValue::array("Body");
    std::vector<float> sensorAngles = motionProxy.getAngles(jointNames, useSensors);
    Eigen::Map<Eigen::VectorXf> q_al(sensorAngles.data(),26);

    hr::VectorXr q_hr(30);
    alToHr(q_hr, q_al.cast<hr::real_t>());
    return q_hr;

}

bool setNaoConfig(AL::ALMotionProxy & motionProxy, const hr::VectorXr & q_hr, hr::real_t time ){
    const int kNAO_DOF = 26;
    const bool isAbsolute = true;

    AL::ALValue alAngles, timeLists;
    alAngles.clear();
    alAngles.arraySetSize(kNAO_DOF);

    timeLists.clear();
    timeLists.arraySetSize(kNAO_DOF);

    hr::VectorXr q_al(kNAO_DOF);

    hrToAl(q_hr,q_al);


    AL::ALValue jointNames;
    jointNames = AL::ALValue::array("Body");

    for (int i = 0; i!=kNAO_DOF; i++) {
        timeLists[i] = AL::ALValue::array((float) time);
        alAngles[i] = q_al(i);
    }

    motionProxy.angleInterpolation(jointNames, alAngles, timeLists, isAbsolute);

    return true;

}


