// Example 30

#include <iostream>
#include <fstream>

#include "openhrc/core.h"
#include "openhrc/utils.h"
#include "openhrc/kinematics.h"

int main(){

    std::ofstream myfile;
    myfile.open ("example_30.dat");
    hr::core::TrapezoidalFunction fn_1 = hr::core::TrapezoidalFunction(0.2,5.1,5.5,5.7,5.8);
    hr::core::TrapezoidalFunction fn_2 = hr::core::TrapezoidalFunction(2.0,1.0,5.0,7.0,8.0);

    hr::real_t const integrationStepSize = 0.01;
    hr::real_t t = 0.0;
    hr::real_t T = 10.0;
    int N = T/integrationStepSize;

    hr::real_t x = 0.0;

    for(int i = 0; i != N ; i++){
        x = fn_1.getValue(t) + fn_2.getValue(t);
        myfile << t << " " << x << std::endl;
        t +=integrationStepSize;
    }
    myfile.close();

}
