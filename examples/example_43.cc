//example 43 Kalman Filter for the CoM (position/velocity/acceleration).

#include "openhrc/core.h"
#include "openhrc/kinematics.h"
#include <iostream>

void kalmanFilter(hr::VectorXr & median, hr::MatrixXr & covariance, const hr::VectorXr & control, const hr::VectorXr & measure){

    hr::VectorXr nextState;

}

int main(){

    hr::MatrixXr cov_x(3,3);
    hr::MatrixXr cov_y(3,3);
    hr::MatrixXr A(3,3);
    hr::MatrixXr B(3,1);
    hr::MatrixXr C(2,3);
    hr::MatrixXr R(3,3);
    hr::MatrixXr Q(2,2);

    hr::real_t alpha  = 0.1;
    hr::real_t beta   = 3.0;
    hr::real_t gamma  = 9.0;
    hr::real_t sensorDeviation = 2;

    hr::VectorXr state_x(3);
    hr::VectorXr control_x(2);
    hr::VectorXr nextState_x(3);
    hr::VectorXr nextPrediction_x(2);

    hr::VectorXr state_y(3);
    hr::VectorXr control_y(2);
    hr::VectorXr nextState_y(3);
    hr::VectorXr nextPrediction_y(2);

    state_x << 0,0,0;
    state_y << 0,0,0;
    control_x << 0.1;
    control_y << -0.5;

    hr::real_t T = 0.01; //Sampling time

    cov_x << sqrt(alpha), alpha*beta, alpha*gamma,
            alpha*beta , sqrt(beta) , beta*gamma,
            alpha*gamma , beta*gamma , sqrt(gamma);

    cov_y = cov_x;

    R(0,0) = sqrt(alpha);
    R(1,1) = sqrt(beta);
    R(2,2) = sqrt(gamma);

    Q(0,0) = sqrt(sensorDeviation);
    Q(1,1) = sqrt(sensorDeviation);

    A << 1, T, 0.5*T*T,
         0, 1, T,
         0, 0, 1;

    B << (1.0/6.0)*T*T*T,
         0.5*T*T,
         T;

    C << 1, 0, 0,
         0, 0, 1;



    std::cout << "example 43 kalman filter full state" << std::endl;
    std::cout << "A" << std::endl << A << std::endl;
    std::cout << "B" << std::endl << B << std::endl;
    std::cout << "C" << std::endl << C << std::endl;

    std::cout << "Q" << std::endl << Q << std::endl;
    std::cout << "R" << std::endl << R << std::endl;

    std::cout << "cov X" << std::endl << cov_x << std::endl;
    std::cout << "State X" << std::endl << state_x << std::endl;
    std::cout << "Control X" << std::endl << control_x << std::endl;

    std::cout << "cov Y" << std::endl << cov_y << std::endl;
    std::cout << "State Y" << std::endl << state_y << std::endl;
    std::cout << "Control Y" << std::endl << control_y << std::endl;

    nextState_x = A*state_x+B*control_x;
    nextPrediction_x = C*nextState_x;

    std::cout << "Next State" << std::endl << nextState_x << std::endl;
    std::cout << "Prediction" << std::endl << nextPrediction_x << std::endl;



    hr::VectorXr bState_x(3);
    hr::VectorXr rState_x(3);
    hr::VectorXr wState_x(3);
    hr::VectorXr error_x(3);

    hr::VectorXr filteredState_x(3);
    hr::VectorXr measurement_x(2);
    hr::MatrixXr bCov_x(3,3);
    hr::MatrixXr kalmanGain_x(3,2);


    hr::VectorXr bState_y(3);
    hr::VectorXr rState_y(3);
    hr::VectorXr wState_y(3);
    hr::VectorXr error_y(3);

    hr::VectorXr filteredState_y(3);
    hr::VectorXr measurement_y(2);
    hr::MatrixXr bCov_y(3,3);
    hr::MatrixXr kalmanGain_y(3,2);


    hr::MatrixXr eye(3,3);
    eye = hr::MatrixXr::Identity(3,3);

    int N = 200;

    rState_x = state_x;
    wState_x = state_x;

    for(int k = 0 ; k <= N ; k++){

        // state / covariance / control / measurement
        error_x = 0.05*hr::MatrixXr::Random(3,1);
        bState_x = A*state_x+B*control_x + error_x;
        wState_x = A*wState_x+B*control_x + error_x;
        rState_x = A*rState_x+B*control_x;

        measurement_x = C*rState_x + 0.05*hr::MatrixXr::Random(2,1);

        bCov_x = A*cov_x*A.transpose() + R;
        hr::MatrixXr inner_x = C*bCov_x*C.transpose() + Q;
        kalmanGain_x = bCov_x*C.transpose()*inner_x.inverse();
        filteredState_x = bState_x + kalmanGain_x*(measurement_x - C*bState_x);
        cov_x =(eye - kalmanGain_x*C)*bCov_x;


        // state / covariance / control / measurement
        error_y = 0.05*hr::MatrixXr::Random(3,1);
        bState_y = A*state_y+B*control_y + error_y;
        wState_y = A*wState_y+B*control_y + error_y;
        rState_y = A*rState_y+B*control_y;

        measurement_y = C*rState_y + 0.05*hr::MatrixXr::Random(2,1);

        bCov_y = A*cov_y*A.transpose() + R;
        hr::MatrixXr inner_y = C*bCov_y*C.transpose() + Q;
        kalmanGain_y = bCov_y*C.transpose()*inner_y.inverse();
        filteredState_y = bState_y + kalmanGain_y*(measurement_y - C*bState_y);
        cov_y =(eye - kalmanGain_y*C)*bCov_y;



        std::cout << "Iteration X" << std::endl << k << std::endl;
        std::cout << "Expected State" << std::endl << C*bState_x << std::endl;
        std::cout << "Measured State" << std::endl << measurement_x << std::endl;
        std::cout << "Filtered State" << std::endl << C*filteredState_x << std::endl;
        std::cout << "Real State" << std::endl << C*rState_x << std::endl;
        std::cout << "Non-filtered State" << std::endl << C*wState_x << std::endl;

        std::cout << "Iteration Y" << std::endl << k << std::endl;
        std::cout << "Expected State" << std::endl << C*bState_y << std::endl;
        std::cout << "Measured State" << std::endl << measurement_y << std::endl;
        std::cout << "Filtered State" << std::endl << C*filteredState_y << std::endl;
        std::cout << "Real State" << std::endl << C*rState_y << std::endl;
        std::cout << "Non-filtered State" << std::endl << C*wState_y << std::endl;




        state_x = filteredState_x;
        state_y = filteredState_y;

    }





    return 0;
}


