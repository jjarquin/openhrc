//Example 29 Walking pattern generator , Use of PointTrajectory, FootTrajectory and CoMTask

#include "openhrc/utils.h"
#include "openhrc/core.h"
#include "openhrc/kinematics.h"
#include "openhrc/locomotion.h"
#include "qpOASES.hpp"
#include <alproxies/almotionproxy.h>
#include <alerror/alerror.h>
#include <iostream>
#include <boost/timer/timer.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <fstream>

#include "boost/date_time/posix_time/posix_time.hpp"
namespace pt = boost::posix_time;

#ifdef  USE_LOCAL_XBOT //
std::string naoFile = "../../../src/data/Nao_blue_noninertial.xbot";
#else
std::string naoFile = "/home/nao/projects/hrData/Nao_blue_noninertial.xbot";
#endif

using boost::timer::cpu_timer;
using std::cout;
using std::endl;

hr::VectorXr getNaoConfig(AL::ALMotionProxy & motionProxy);
bool setNaoConfig(AL::ALMotionProxy & motionProxy, const hr::VectorXr & q, hr::real_t time );

void hrToAl(const hr::VectorXr & q_hr , hr::VectorXr & q_al){
    const int kNAO_DOF = 26;
    int jointId[kNAO_DOF] = {7,8,9,10,11,12,13,-1,19,20,21,22,23,24,25,26,27,28,29,30,14,15,16,17,18,-1};
    q_al.setZero();
    for (int i = 0 ; i < kNAO_DOF; i++) {
        if(jointId[i] != -1) {
            q_al(i) = q_hr(jointId[i]-1);
        }
    }

}

void alToHr(hr::VectorXr & q_hr , const hr::VectorXr & q_al){
    const int kNAO_DOF = 26;
    int jointId[kNAO_DOF] = {7,8,9,10,11,12,13,-1,19,20,21,22,23,24,-1,26,27,28,29,30,14,15,16,17,18,-1};
    q_hr.setZero();
    for (int i = 0 ; i < kNAO_DOF; i++) {
        if(jointId[i] != -1) {
            q_hr(jointId[i]-1) = q_al(i);
        }
    }
    q_hr(24) = q_hr(18);

}


int main(){

    hr::core::World world = hr::core::World();
    std::string sFile = naoFile;
    int robotId = world.getRobotsVector()->size();
    world.loadMultiBody(sFile,robotId, hr::core::kNAO);
    boost::shared_ptr<hr::core::MultiBody> robot = world.getRobot(0);

    hr::VectorXr q = robot->getConfiguration();
    hr::VectorXr qtmp(q.rows()-1);

    AL::ALMotionProxy motionProxy = AL::ALMotionProxy("127.0.0.1");
    //AL::ALMotionProxy motionProxy = AL::ALMotionProxy("nao.local");
    //AL::ALMotionProxy motionProxy = AL::ALMotionProxy("seia.local");

    q = getNaoConfig(motionProxy);
    robot->setConfiguration(q);
    robot->computeForwardKinematics();

    hr::real_t const integrationStepSize = 0.01;
    hr::real_t t = 0.0;
    hr::real_t T = 3.0;
    int N = T/integrationStepSize;

    hr::Vector3r rightFootHandle(0.0,0.0,-0.04);
    int rightFootBodyId = hr::core::nao::kRightFoot;
    int rightFootHandleId = robot->addOperationalHandle(rightFootBodyId,rightFootHandle);

    hr::Vector3r leftFootHandle(0.0,0.0,-0.04);
    int leftFootBodyId = hr::core::nao::kLeftFoot;
    int leftFootHandleId = robot->addOperationalHandle(leftFootBodyId,leftFootHandle);

    hr::Vector3r rightHandHandle(0.08,0.0,0.0);
    int rightHandBodyId = hr::core::nao::kRightHand;
    int rightHandHandleId = robot->addOperationalHandle(rightHandBodyId,rightHandHandle);

    hr::Vector3r leftHandHandle(0.08,0.0,0.0);
    int leftHandBodyId = hr::core::nao::kLeftHand;
    int leftHandHandleId = robot->addOperationalHandle(leftHandBodyId,leftHandHandle);

    hr::Vector3r torsoHandle(0.0,0.0,0.0);
    int torsoBodyId = hr::core::nao::kTorso;
    int torsoHandleId = robot->addOperationalHandle(torsoBodyId,torsoHandle);

    hr::Vector3r headHandle(0.0,0.0,0.0);
    int headBodyId = hr::core::nao::kHead;
    int headHandleId = robot->addOperationalHandle(headBodyId,headHandle);

    hr::SpatialVector comPose;
    hr::SpatialVector comGoal;

    hr::SpatialVector rightFootPose;
    hr::SpatialVector leftFootPose;

    hr::SpatialVector rightHandPose;
    hr::SpatialVector leftHandPose;

    hr::SpatialVector torsoPose;
    hr::SpatialVector headPose;

    hr::SpatialVector rightFootGoal;
    hr::SpatialVector leftFootGoal;

    hr::SpatialVector rightHandGoal;
    hr::SpatialVector leftHandGoal;

    hr::SpatialVector torsoGoal;
    hr::SpatialVector headGoal;

    hr::Vector3r rightFootPosition = robot->getOperationalHandlePosition(rightFootBodyId,rightFootHandleId);
    hr::Vector3r rightFootOrientation = robot->getBodyOrientation(rightFootBodyId).eulerAngles(0,1,2);

    hr::Vector3r leftFootPosition = robot->getOperationalHandlePosition(leftFootBodyId,leftFootHandleId);
    hr::Vector3r leftFootOrientation = robot->getBodyOrientation(leftFootBodyId).eulerAngles(0,1,2);

    hr::Vector3r rightHandPosition = robot->getOperationalHandlePosition(rightHandBodyId,rightHandHandleId);
    hr::Vector3r rightHandOrientation = robot->getBodyOrientation(rightHandBodyId).eulerAngles(0,1,2);

    hr::Vector3r leftHandPosition = robot->getOperationalHandlePosition(leftHandBodyId,leftHandHandleId);
    hr::Vector3r leftHandOrientation = robot->getBodyOrientation(leftHandBodyId).eulerAngles(0,1,2);

    hr::Vector3r torsoPosition = robot->getOperationalHandlePosition(torsoBodyId,torsoHandleId);
    hr::Vector3r torsoOrientation = robot->getBodyOrientation(torsoBodyId).eulerAngles(0,1,2);

    hr::Vector3r headPosition = robot->getOperationalHandlePosition(headBodyId,headHandleId);
    hr::Vector3r headOrientation = robot->getBodyOrientation(headBodyId).eulerAngles(0,1,2);

    hr::Vector3r comPosition = robot->getCoM();

    rightFootPose << rightFootPosition , rightFootOrientation;
    leftFootPose << leftFootPosition, leftFootOrientation;
    torsoPose << torsoPosition, torsoOrientation;
    rightHandPose << rightHandPosition , rightHandOrientation;
    leftHandPose << leftHandPosition , leftHandOrientation;
    headPose << headPosition, headOrientation;

    rightFootGoal << rightFootPosition , rightFootOrientation;
    leftFootGoal << leftFootPosition, leftFootOrientation;

    torsoGoal << torsoPosition(0),torsoPosition(1),torsoPosition(2), torsoOrientation;
    rightHandGoal << rightHandPosition , rightHandOrientation;
    leftHandGoal << leftHandPosition , leftHandOrientation;
    headGoal << headPosition, headOrientation;

    comPose << comPosition , 0.0,0.0,0.0;
    comGoal << rightFootPosition(0), rightFootPosition(1), comPosition(2), 0.0,0.0,0.0;

    hr::core::KinematicTask rightFootTask(rightFootHandleId,rightFootBodyId,0,robot, hr::core::kPositionOrientation);
    rightFootTask.setGoal(rightFootGoal);
    rightFootTask.setGain(1.0);

    hr::core::KinematicTask leftFootTask(leftFootHandleId,leftFootBodyId,0,robot, hr::core::kPositionOrientation);
    leftFootTask.setGoal(leftFootGoal);
    leftFootTask.setGain(1.0);

    hr::core::KinematicTask rightHandTask(rightHandHandleId,rightHandBodyId,0,robot, hr::core::kPosition);
    rightHandTask.setGoal(rightHandGoal);
    rightHandTask.setGain(1.0);

    hr::core::KinematicTask leftHandTask(leftHandHandleId,leftHandBodyId,0,robot, hr::core::kPosition);
    leftHandTask.setGoal(leftHandGoal);
    leftHandTask.setGain(1.0);


    hr::core::KinematicTask rightHandFullTask(rightHandHandleId,rightHandBodyId,0,robot, hr::core::kPosition);
    rightHandFullTask.setGoal(rightHandGoal);
    rightHandFullTask.setGain(1.0);

    hr::core::KinematicTask leftHandFullTask(leftHandHandleId,leftHandBodyId,0,robot, hr::core::kPosition);
    leftHandFullTask.setGoal(leftHandGoal);
    leftHandFullTask.setGain(1.0);


    hr::core::KinematicTask torsoTask(torsoHandleId,torsoBodyId,0,robot, hr::core::kPositionOrientation );
    torsoTask.setGoal(torsoGoal);
    torsoTask.setGain(1.0);

    hr::core::KinematicTask torsoZTask(torsoHandleId,torsoBodyId,0,robot,  hr::core::kZ );
    torsoZTask.setGoal(torsoGoal);
    torsoZTask.setGain(1.0);

    hr::core::KinematicTask headTask(headHandleId,headBodyId,0,robot,hr::core::kWYZ );
    headTask.setGoal(headPose);
    headTask.setGain(3.0);

    hr::core::CoMTask comTask(0,robot, hr::core::kXY);
    comTask.setGoal(comGoal);
    comTask.setGain(1.0);

    hr::utils::SimpleHierarchicalSolver solver;

    hr::VectorXr lb_ = (robot->getJointLowLimits()-q)/integrationStepSize;
    hr::VectorXr ub_ = (robot->getJointUpLimits()-q)/integrationStepSize;

    hr::VectorXr lb(29);
    hr::VectorXr ub(29);

    lb.segment(0,24) = lb_.segment(0,24);
    lb.segment(24,5) = lb_.segment(25,5);

    ub.segment(0,24) = ub_.segment(0,24);
    ub.segment(24,5) = ub_.segment(25,5);

    hr::VectorXr ld(29);
    hr::VectorXr ud(29);
    hr::MatrixXr C;

    hr::VectorXr ud_ = robot->getJointUpSpeedLimits();
    hr::VectorXr ld_ = robot->getJointLowSpeedLimits();

    int n = 29;
    C = hr::MatrixXr::Identity(n,n);

    ld.segment(0,24) = ld_.segment(0,24);
    ld.segment(24,5) = ld_.segment(25,5);

    ud.segment(0,24) = ud_.segment(0,24);
    ud.segment(24,5) = ud_.segment(25,5);

    q = getNaoConfig(motionProxy);
    hr::VectorXr q_tmp = q;
    robot->setConfiguration(q);
    robot->computeForwardKinematics();

    t = 0.0;
    T = 30.0;
    N = T/integrationStepSize;

    for(int i = 0; i != N ; i++){

        q_tmp = getNaoConfig(motionProxy);
        q_tmp.segment(0,6) = q.segment(0,6);

        robot->setConfiguration(q_tmp);
        robot->computeForwardKinematics();

        //Calculate the new joint speed limit
        lb_ = (robot->getJointLowLimits()-q)/integrationStepSize;
        ub_ = (robot->getJointUpLimits()-q)/integrationStepSize;

        lb.segment(0,24) = lb_.segment(0,24);
        lb.segment(24,5) = lb_.segment(25,5);

        ub.segment(0,24) = ub_.segment(0,24);
        ub.segment(24,5) = ub_.segment(25,5);

        rightFootTask.update(t);
        leftFootTask.update(t);
        rightHandTask.update(t);
        leftHandTask.update(t);

        rightHandFullTask.update(t);
        leftHandFullTask.update(t);

        torsoTask.update(t);
        torsoZTask.update(t);
        headTask.update(t);
        comTask.update(t);

        std::vector<hr::utils::TaskAbstract*> taskList;
        hr::utils::SimpleTask feetTask;
        hr::utils::SimpleTask handsTask;

        //Fix hands wrt to torso
        handsTask.addRows(rightHandTask.getMatrixA(), rightHandTask.getVectorb());
        handsTask.addRows(leftHandTask.getMatrixA(), leftHandTask.getVectorb());

        feetTask.addRows(leftFootTask.getMatrixA(),leftFootTask.getVectorb());
        feetTask.addRows(rightFootTask.getMatrixA(),rightFootTask.getVectorb());

        taskList.push_back(&feetTask);
        taskList.push_back(&torsoTask);
        taskList.push_back(&handsTask);
        taskList.push_back(&headTask);

        hr::VectorXr dq = solver.Solve(taskList,C,ld,ud,lb,ub);

        std::cout << "t " << t << std::endl;

        // Integrate
        qtmp.segment(0,24) = q.segment(0,24);
        qtmp.segment(24,5) = q.segment(25,5);
        qtmp += dq*integrationStepSize;
        t = t + integrationStepSize;
        q.segment(0,24) = qtmp.segment(0,24);
        q(24) = qtmp(18);
        q.segment(25,5) = qtmp.segment(24,5);
        // End of integration

        //Set the new configuration
        setNaoConfig(motionProxy,q, integrationStepSize);

    }
    q_tmp = getNaoConfig(motionProxy);
    q_tmp.segment(0,6) = q.segment(0,6);

    robot->setConfiguration(q_tmp);
    robot->computeForwardKinematics();
}


hr::VectorXr getNaoConfig(AL::ALMotionProxy & motionProxy){
    const int kNAO_DOF = 26;
    const bool useSensors = true;

    AL::ALValue jointNames;
    jointNames = AL::ALValue::array("Body");
    std::vector<float> sensorAngles = motionProxy.getAngles(jointNames, useSensors);
    Eigen::Map<Eigen::VectorXf> q_al(sensorAngles.data(),26);

    hr::VectorXr q_hr(30);
    alToHr(q_hr, q_al.cast<hr::real_t>());
    return q_hr;

}

bool setNaoConfig(AL::ALMotionProxy & motionProxy, const hr::VectorXr & q_hr, hr::real_t time ){
    const int kNAO_DOF = 26;
    const bool isAbsolute = true;

    AL::ALValue alAngles, timeLists;
    alAngles.clear();
    alAngles.arraySetSize(kNAO_DOF);

    timeLists.clear();
    timeLists.arraySetSize(kNAO_DOF);

    hr::VectorXr q_al(kNAO_DOF);

    hrToAl(q_hr,q_al);


    AL::ALValue jointNames;
    jointNames = AL::ALValue::array("Body");
    for (int i = 0; i!=kNAO_DOF; i++) {
        timeLists[i] = AL::ALValue::array((float) 5*time);
        alAngles[i] = q_al(i);
    }
    motionProxy.setAngles(jointNames,alAngles,1.0);

    //motionProxy.angleInterpolation(jointNames, alAngles, timeLists, isAbsolute);

    return true;

}




