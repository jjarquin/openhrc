//example 13, Generation of Trajectories

#include "openhrc/utils.h"
#include "openhrc/core.h"
#include "openhrc/kinematics.h"

#include <iostream>
#include <fstream>

using std::cout;
using std::endl;

int main(){
    std::ofstream myfile;
    myfile.open("example_13.dat");

    hr::SpatialVector xi;
    hr::SpatialVector xf;

    hr::SpatialVector x1;
    hr::SpatialVector x2;

    hr::SpatialVector dxi;
    hr::SpatialVector dxf;

    xi << -3, 0, 0, 0, 0, 0;
    xf << 7, 0, 0, 0, 0, 0;

    x1 << -2, 0, 0, 0, 0, 0;
    x2 << 6, 0, 0, 0, 0, 0;

    dxi << 0, 0, 0, 0, 0, 0;
    dxf << 0, 0, 0, 0, 0, 0;

    hr::real_t ti = 1.6;
    hr::real_t tf = 4.8;
    hr::real_t dt = 0.001;
    int N = 1.5*(tf)/dt;
    hr::real_t t = 0.0;

    hr::core::TrajectoryAbstract * traj;
    traj = new hr::core::PointTrajectory(ti,tf,xi,xf,hr::core::kCycloidal);
    //traj = new hr::core::BezierTrajectory(ti,tf,xi,x1,x2,xf);
    //traj = new hr::core::PointTrajectory(ti,tf,xi,xf,dxi,dxf);
    //traj = new hr::core::PointTrajectory(ti,tf,xi,xf,hr::core::kCubicPolynomial);
    //traj = new hr::core::PointTrajectory(ti,tf,xi,xf,dxi,dxf,hr::core::kQuinticPolynomial);

    hr::SpatialVector x;
    hr::SpatialVector dx;
    hr::SpatialVector ddx;
    hr::real_t int_x = xi(0);
    hr::real_t int_dx = (traj->getVelocity(ti))(0);

    for (int i = 0 ; i != N ; i ++){
        x = traj->getPose(t);
        dx = traj->getVelocity(t);
        ddx = traj->getAcceleration(t);
        int_x = int_x + dt*dx(0);
        int_dx = int_dx + dt*ddx(0);
        myfile << t << " " << x(0) << " " << dx(0) << " " << ddx(0) << " " << int_x << " " << int_dx << endl;
        t = t + dt;
    }
}



