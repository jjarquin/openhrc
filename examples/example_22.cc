//Example 22 Walking pattern generator , Use of point and foot trajectory.

#include "openhrc/utils.h"
#include "openhrc/core.h"
#include "openhrc/kinematics.h"
#include "openhrc/locomotion.h"
#include "qpOASES.hpp"
#include <alproxies/almotionproxy.h>
#include <alerror/alerror.h>
#include <iostream>
#include <boost/timer/timer.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <fstream>

#ifdef  USE_LOCAL_XBOT //
std::string naoFile = "../../../src/data/Nao_blue_noninertial.xbot";
#else
std::string naoFile = "/home/nao/projects/hrData/Nao_blue_noninertial.xbot";
#endif

using boost::timer::cpu_timer;
using std::cout;
using std::endl;

hr::VectorXr getNaoConfig(AL::ALMotionProxy & motionProxy);
bool setNaoConfig(AL::ALMotionProxy & motionProxy, const hr::VectorXr & q, hr::real_t time );

void hrToAl(const hr::VectorXr & q_hr , hr::VectorXr & q_al){
    const int kNAO_DOF = 26;
    int jointId[kNAO_DOF] = {7,8,9,10,11,12,13,-1,19,20,21,22,23,24,25,26,27,28,29,30,14,15,16,17,18,-1};
    q_al.setZero();
    for (int i = 0 ; i < kNAO_DOF; i++) {
        if(jointId[i] != -1) {
            q_al(i) = q_hr(jointId[i]-1);
        }
    }

}

void alToHr(hr::VectorXr & q_hr , const hr::VectorXr & q_al){
    const int kNAO_DOF = 26;
    int jointId[kNAO_DOF] = {7,8,9,10,11,12,13,-1,19,20,21,22,23,24,-1,26,27,28,29,30,14,15,16,17,18,-1};
    q_hr.setZero();
    for (int i = 0 ; i < kNAO_DOF; i++) {
        if(jointId[i] != -1) {
            q_hr(jointId[i]-1) = q_al(i);
        }
    }
    q_hr(24) = q_hr(18);

}


int main(){
    cpu_timer timer;

    std::ofstream myfile;
    myfile.open ("example_22.dat");
    std::ofstream myfile_joints;
    myfile_joints.open ("example_22_joints.dat");

    hr::real_t log_time = 0.0;

    hr::core::World world = hr::core::World();
    std::string sFile = naoFile;
    int robotId = world.getRobotsVector()->size();
    world.loadMultiBody(sFile,robotId, hr::core::kNAO);
    boost::shared_ptr<hr::core::MultiBody> robot = world.getRobot(0);

    hr::VectorXr q = robot->getConfiguration();
    hr::VectorXr qtmp(q.rows()-1);

    AL::ALMotionProxy motionProxy = AL::ALMotionProxy("127.0.0.1");
    //AL::ALMotionProxy motionProxy = AL::ALMotionProxy("nao.local");
    //AL::ALMotionProxy motionProxy = AL::ALMotionProxy("seia.local");

    q = getNaoConfig(motionProxy);
    robot->setConfiguration(q);
    robot->computeForwardKinematics();

    hr::real_t const integrationStepSize = 0.01;
    hr::real_t t = 0.0;
    hr::real_t T = 3.0;
    int N = T/integrationStepSize;

    int n_joints = q.rows();

    hr::VectorXr StandInit(n_joints);
    hr::VectorXr StandZero(n_joints);

    StandInit << 0 ,0, 0, 0, 0, 0, 0, 0, 1.39969, 0.300946, -1.39107, -1.01314, 0, 1.39969, -0.300946, 1.39107, 1.01314, 0, 0, 0, -0.45, 0.7, -0.35, 0, 0, 0, -0.45, 0.7, -0.35, 0;
    StandZero <<  0, 0, 0, 0, 0, 0, 0, 0, 0,  0.0087, 0,  0, 0, 0, -0.0087, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0;

    hr::core::JointTrajectory jointTraj( 0, 3.0 ,q, StandInit);

    /*
    //rest CoM on right foot
    for(int i = 0; i != N ; i++){
        robot->setConfiguration(q);
        robot->computeForwardKinematics();
        //Calculate the new joint speed limit
        q = jointTraj.getJointConfiguration(t);
        t = t + integrationStepSize;
        //Set the new configuration
        setNaoConfig(motionProxy,q, integrationStepSize);

    }

    */
    robot->setConfiguration(q);
    robot->computeForwardKinematics();

    cout << "Robot configuration" << endl << q << endl;

    hr::Vector3r rightFootHandle(0.0,0.0,-0.04);
    int rightFootBodyId = hr::core::nao::kRightFoot;
    int rightFootHandleId = robot->addOperationalHandle(rightFootBodyId,rightFootHandle);

    hr::Vector3r leftFootHandle(0.0,0.0,-0.04);
    int leftFootBodyId = hr::core::nao::kLeftFoot;
    int leftFootHandleId = robot->addOperationalHandle(leftFootBodyId,leftFootHandle);


    hr::Vector3r rightHandHandle(0.08,0.0,0.0);
    int rightHandBodyId = hr::core::nao::kRightHand;
    int rightHandHandleId = robot->addOperationalHandle(rightHandBodyId,rightHandHandle);

    hr::Vector3r leftHandHandle(0.08,0.0,0.0);
    int leftHandBodyId = hr::core::nao::kLeftHand;
    int leftHandHandleId = robot->addOperationalHandle(leftHandBodyId,leftHandHandle);

    hr::real_t torsoHandle_x = 0.0;
    hr::Vector3r torsoHandle(torsoHandle_x,0.0,0.0);
    int torsoBodyId = hr::core::nao::kTorso;
    int torsoHandleId = robot->addOperationalHandle(torsoBodyId,torsoHandle);

    hr::Vector3r headHandle(0.0,0.0,0.0);
    int headBodyId = hr::core::nao::kHead;
    int headHandleId = robot->addOperationalHandle(headBodyId,headHandle);

    hr::SpatialVector rightFootPose;
    hr::SpatialVector leftFootPose;

    hr::SpatialVector rightHandPose;
    hr::SpatialVector leftHandPose;

    hr::SpatialVector torsoPose;
    hr::SpatialVector headPose;

    hr::SpatialVector rightFootGoal;
    hr::SpatialVector leftFootGoal;

    hr::SpatialVector rightHandGoal;
    hr::SpatialVector leftHandGoal;

    hr::SpatialVector torsoGoal;
    hr::SpatialVector headGoal;

    hr::Vector3r rightFootPosition = robot->getOperationalHandlePosition(rightFootBodyId,rightFootHandleId);
    hr::Vector3r rightFootOrientation = robot->getBodyOrientation(rightFootBodyId).eulerAngles(0,1,2);

    hr::Vector3r leftFootPosition = robot->getOperationalHandlePosition(leftFootBodyId,leftFootHandleId);
    hr::Vector3r leftFootOrientation = robot->getBodyOrientation(leftFootBodyId).eulerAngles(0,1,2);

    hr::Vector3r rightHandPosition = robot->getOperationalHandlePosition(rightHandBodyId,rightHandHandleId);
    hr::Vector3r rightHandOrientation = robot->getBodyOrientation(rightHandBodyId).eulerAngles(0,1,2);

    hr::Vector3r leftHandPosition = robot->getOperationalHandlePosition(leftHandBodyId,leftHandHandleId);
    hr::Vector3r leftHandOrientation = robot->getBodyOrientation(leftHandBodyId).eulerAngles(0,1,2);

    hr::Vector3r torsoPosition = robot->getOperationalHandlePosition(torsoBodyId,torsoHandleId);
    hr::Vector3r torsoOrientation = robot->getBodyOrientation(torsoBodyId).eulerAngles(0,1,2);

    hr::Vector3r headPosition = robot->getOperationalHandlePosition(headBodyId,headHandleId);
    hr::Vector3r headOrientation = robot->getBodyOrientation(headBodyId).eulerAngles(0,1,2);

    cout << "Left hand position: " << endl << leftHandPosition << endl;
    cout << "Right hand position: " << endl << rightHandPosition << endl;
    cout << "Head position: " << endl << headPosition << endl;
    cout << "Torso position: " << endl << torsoPosition << endl;
    cout << "Left foot position: " << endl << leftFootPosition << endl;
    cout << "Right foot position: " << endl << rightFootPosition << endl;

    cout << "Left hand orientation: " << endl << leftHandOrientation << endl;
    cout << "Right hand orientation: " << endl << rightHandOrientation << endl;
    cout << "Head orientation: " << endl << headOrientation << endl;
    cout << "Torso orientation: " << endl << torsoOrientation << endl;
    cout << "Left foot orientation: " << endl << leftFootOrientation << endl;
    cout << "Right foot orientation: " << endl << rightFootOrientation << endl;


    rightFootPose << rightFootPosition , rightFootOrientation;
    leftFootPose << leftFootPosition, leftFootOrientation;
    torsoPose << torsoPosition, torsoOrientation;
    rightHandPose << rightHandPosition , rightHandOrientation;
    leftHandPose << leftHandPosition , leftHandOrientation;
    headPose << headPosition, headOrientation;

    rightFootGoal << rightFootPosition , 0.0 , 0.0, 0.0;
    leftFootGoal << leftFootPosition, 0.0 , 0.0 , 0.0;

    torsoGoal << torsoPosition, torsoOrientation;
    rightHandGoal << rightHandPosition , rightHandOrientation;
    leftHandGoal << leftHandPosition , leftHandOrientation;
    headGoal << headPosition, headOrientation;



    hr::core::KinematicTask task_01(rightFootHandleId,rightFootBodyId,0,robot, hr::core::kPositionOrientation);
    task_01.setGoal(rightFootGoal);
    task_01.setGain(1.0);
    hr::core::KinematicTask task_02(leftFootHandleId,leftFootBodyId,0,robot, hr::core::kPositionOrientation);
    task_02.setGoal(leftFootGoal);
    task_02.setGain(1.0);
    hr::core::KinematicTask task_03(rightHandHandleId,rightHandBodyId,0,robot, hr::core::kPosition);
    task_03.setGoal(rightHandGoal);
    //task_03.setGain(1.0);
    hr::core::KinematicTask task_04(leftHandHandleId,leftHandBodyId,0,robot, hr::core::kPosition);
    task_04.setGoal(leftHandGoal);
    //task_04.setGain(1.0);
    hr::core::KinematicTask task_05(torsoHandleId,torsoBodyId,0,robot, (  hr::core::kWXYZ | hr::core::kXYZ ));
    task_05.setGoal(torsoGoal);
    //task_05.setGain(1.0);
    hr::core::KinematicTask task_06(headHandleId,headBodyId,0,robot, hr::core::kWYZ);
    task_06.setGoal(headGoal);
    //task_06.setGain(1.0);






    hr::utils::SimpleHierarchicalSolver solver;


    hr::VectorXr lb_ = (robot->getJointLowLimits()-q)/integrationStepSize;
    hr::VectorXr ub_ = (robot->getJointUpLimits()-q)/integrationStepSize;

    hr::VectorXr lb(29);
    hr::VectorXr ub(29);

    lb.segment(0,24) = lb_.segment(0,24);
    lb.segment(24,5) = lb_.segment(25,5);

    ub.segment(0,24) = ub_.segment(0,24);
    ub.segment(24,5) = ub_.segment(25,5);

    hr::VectorXr ld(29);
    hr::VectorXr ud(29);
    hr::MatrixXr C;

    hr::VectorXr ud_ = robot->getJointUpSpeedLimits();
    hr::VectorXr ld_ = robot->getJointLowSpeedLimits();

    int n = 29;
    C = hr::MatrixXr::Identity(n,n);

    ld.segment(0,24) = ld_.segment(0,24);
    ld.segment(24,5) = ld_.segment(25,5);

    ud.segment(0,24) = ud_.segment(0,24);
    ud.segment(24,5) = ud_.segment(25,5);

    q = getNaoConfig(motionProxy);
    hr::VectorXr q_tmp = q;
    robot->setConfiguration(q);
    robot->computeForwardKinematics();





    hr::core::nao::SupportBaseConstraint supportBaseConstraint;
    hr::Vector3r rightFootXYTheta;
    hr::Vector3r leftFootXYTheta;
    hr::Vector2r comXY;
    hr::Matrix2Xr comXYJacobian;



    torsoGoal << rightFootPosition(0),rightFootPosition(1)+0.015, torsoPosition(2), torsoOrientation;
    boost::shared_ptr<hr::core::PointTrajectory> torsoTraj = boost::make_shared<hr::core::PointTrajectory>(0.0,0.8,torsoPose,torsoGoal,hr::core::kQuinticPolynomial);
    task_05.setTrajectory(torsoTraj);
    task_05.setGain(2.0);



    t = 0.0;
    T = 1.0;
    N = T/integrationStepSize;

    //rest CoM on right foot
    for(int i = 0; i != N ; i++){
        q_tmp = getNaoConfig(motionProxy);
        q_tmp.segment(0,6) = q.segment(0,6);

        robot->setConfiguration(q_tmp);
        robot->computeForwardKinematics();

        //! Log data ---------------------------------------------------------------
        torsoPosition = robot->getOperationalHandlePosition(torsoBodyId,torsoHandleId);
        rightFootPosition = robot->getOperationalHandlePosition(rightFootBodyId,rightFootHandleId);
        myfile << log_time << " " << torsoPosition(0)  << " " << torsoPosition(1) << " " << torsoPosition(0)  << " " << torsoPosition(1) << " " << rightFootPosition(0) << " " << rightFootPosition(1) << std::endl;
        log_time += integrationStepSize;
        //! end of Log data --------------------------------------------------------

        //Calculate the new joint speed limit
        lb_ = (robot->getJointLowLimits()-q)/integrationStepSize;
        ub_ = (robot->getJointUpLimits()-q)/integrationStepSize;

        lb.segment(0,24) = lb_.segment(0,24);
        lb.segment(24,5) = lb_.segment(25,5);

        ub.segment(0,24) = ub_.segment(0,24);
        ub.segment(24,5) = ub_.segment(25,5);

        task_01.update(t);
        task_02.update(t);
        task_03.update(t);
        task_04.update(t);
        task_05.update(t);
        task_06.update(t);

        std::vector<hr::utils::TaskAbstract*> taskList;
        hr::utils::SimpleTask feetTask;

        feetTask.addRows(task_01.getMatrixA(),task_01.getVectorb());
        feetTask.addRows(task_02.getMatrixA(),task_02.getVectorb());

        taskList.push_back(&feetTask);
        taskList.push_back(&task_05);
        hr::VectorXr dq = solver.Solve(taskList,C,ld,ud,lb,ub);

        std::cout << "t " << t << std::endl;

        // Integrate
        qtmp.segment(0,24) = q.segment(0,24);
        qtmp.segment(24,5) = q.segment(25,5);
        qtmp += dq*integrationStepSize;
        t = t + integrationStepSize;
        q.segment(0,24) = qtmp.segment(0,24);
        q(24) = qtmp(18);
        q.segment(25,5) = qtmp.segment(24,5);
        // End of integration


        //Set the new configuration
        setNaoConfig(motionProxy,q, integrationStepSize);

    }
    q_tmp = getNaoConfig(motionProxy);
    q_tmp.segment(0,6) = q.segment(0,6);

    robot->setConfiguration(q_tmp);
    robot->computeForwardKinematics();

    //! Log data ---------------------------------------------------------------
    torsoPosition = robot->getOperationalHandlePosition(torsoBodyId,torsoHandleId);
    rightFootPosition = robot->getOperationalHandlePosition(rightFootBodyId,rightFootHandleId);
    myfile << log_time << " " << torsoPosition(0)  << " " << torsoPosition(1) << " " << rightFootPosition(0) << " " << rightFootPosition(1) << std::endl;
    log_time += integrationStepSize;
    myfile << std::endl;
    //! end of Log data --------------------------------------------------------


    rightFootPosition = robot->getOperationalHandlePosition(rightFootBodyId,rightFootHandleId);
    torsoPosition = robot->getOperationalHandlePosition(torsoBodyId,torsoHandleId);




    /** MPC parameters */
    int m_steps = 2;
    int n_periods_step = 12;
    int N_horizon = m_steps*n_periods_step;
    hr::real_t T_sampling = 0.1;
    hr::real_t com_height = 0.315 ;

    hr::real_t zmp_gain = 100000;
    hr::real_t jerk_gain = 0.1;
    hr::real_t velocity_gain = 1000;

    /** These parameters describe the state of the robot */
    hr::real_t z_foot = rightFootPosition(2);
    hr::Vector3r footState;    //Member for the position of the current foot on the ground
    hr::Vector3r nextFootState;    //Member for the position of the current foot on the ground
    hr::locomotion::RobotState robotState;        //Member for the augmented CoM robot state
    hr::Vector3r xk;              //Variable for the CoM robot state in X
    hr::Vector3r yk;              //Variable for the CoM robot state in Y
    hr::Vector3r thetak;          //Variable for the CoM robot state in Theta
    hr::VectorXr x_vel(N_horizon);        //Reference velocity in X direction     //TODO: Time list trajectories similar to ALDCM
    hr::VectorXr y_vel(N_horizon);        //Reference velocity in Y direction     //TODO
    hr::core::FootStatus footStatus = hr::core::kRightFootOnGround;
    hr::core::FootStatus mpcFootStatus = hr::core::kRightFootOnGround;
    hr::Vector3r jerkXYTheta; //Jerk holder variable
    hr::VectorXr uk;      ///Jerk vector
    uk.resize(2*(N_horizon+m_steps));
    uk.setZero();
    /** Initialization of the robot parameters */

    cout << "Init robot parameters" << endl;
    footState << rightFootPosition(0), rightFootPosition(1) , z_foot;   //For this example the left foot is the starting foot.  //TODO: 04/2015: I would prefer footPosition instead of state
    nextFootState << 0 , 0 ,z_foot;
    xk << torsoPosition(0), 0.0, 0.0;
    yk << torsoPosition(1), 0.0, 0.0;
    hr::real_t torso_heigth = torsoPosition(2);

    thetak << 0.0, 0.0 ,0.0;
    robotState.setState(xk,yk,thetak);
    jerkXYTheta << 0.0,0.0,0.0;
    x_vel.setOnes();
    y_vel.setOnes();
    x_vel = x_vel*0.04;
    y_vel = y_vel*0.0;


    /** Initialization of the MPC */
    //hr::locomotion::MPCData mpcData = hr::locomotion::MPCData(com_height,T_sampling,m_steps,n_periods_step);
    hr::locomotion::MPCData mpcData = hr::locomotion::MPCData(com_height,T_sampling,m_steps,n_periods_step,jerk_gain,velocity_gain,zmp_gain);
    hr::locomotion::MPCConstraints constraints;
    hr::locomotion::RobotSystem system;

    mpcData.init();
    constraints.init(mpcData);
    system.init(com_height,T_sampling);

    constraints.setConstraints(mpcData,mpcFootStatus, footState, robotState);

    int nWSR = 1000;
    qpOASES::real_t cpuTime = 1.0;

    int nV = 2*(N_horizon+m_steps);
    int nC = constraints.getbupVector().rows();
    boost::shared_ptr<qpOASES::SQProblem> quadprog;
    qpOASES::Options options;
    options.setToMPC();
    options.printLevel = qpOASES::PL_NONE;
    quadprog.reset(new qpOASES::SQProblem(nV,nC));
    quadprog->setOptions(options);
    quadprog->init(mpcData.getHessian().data(), mpcData.getFvector(robotState,footState,x_vel,y_vel).data(),constraints.getAMatrix().data(),NULL,NULL,NULL,constraints.getbupVector().data(),nWSR,&cpuTime);
    if(quadprog->getPrimalSolution(uk.data()) == qpOASES::RET_QP_NOT_SOLVED){
        return hr::FAILED_INIT;
        cout << "failed to solve" << endl;
    }
    cout << "qpoases init" << endl;








    t = 0.0;
    T = 3.62;
    N = T/integrationStepSize;

    for(int i = 0; i != N; i++){
        timer.start();

        q_tmp = getNaoConfig(motionProxy);
        q_tmp.segment(0,6) = q.segment(0,6);

        robot->setConfiguration(q_tmp);
        robot->computeForwardKinematics();

        std::string pName = "Body";
        int pSpace = 0; // FRAME_TORSO
        bool pUseSensors = true;
        std::vector<float> pos = motionProxy.getCOM(pName, pSpace, pUseSensors);
        Eigen::Map<Eigen::VectorXf> com(pos.data(),3);
        //cout << "CoM" << endl << com.transpose() << endl;

        //! Log data ---------------------------------------------------------------
        torsoPosition = robot->getOperationalHandlePosition(torsoBodyId,torsoHandleId);
        myfile << log_time << " " << torsoPosition(0)  << " " << torsoPosition(1)  << " " << system.getZx_k()  << " " << system.getZy_k()  << " " << footState(0) << " " << footState(1) << " " << com(0)+torsoPosition(0)-torsoHandle_x << " " << com(1)+torsoPosition(1) << std::endl;
        myfile_joints << t << " " << q_tmp(19) << " " << q(19) << " " << q_tmp(25) << " " << q(25) << std::endl;
        log_time += integrationStepSize;
        //! end of Log data --------------------------------------------------------


        //Calculate the new joint speed limit
        lb_ = (robot->getJointLowLimits()-q)/integrationStepSize;
        ub_ = (robot->getJointUpLimits()-q)/integrationStepSize;

        lb.segment(0,24) = lb_.segment(0,24);
        lb.segment(24,5) = lb_.segment(25,5);

        ub.segment(0,24) = ub_.segment(0,24);
        ub.segment(24,5) = ub_.segment(25,5);



        rightFootPosition = robot->getOperationalHandlePosition(rightFootBodyId,rightFootHandleId);
        rightFootOrientation = robot->getBodyOrientation(rightFootBodyId).eulerAngles(0,1,2);

        leftFootPosition = robot->getOperationalHandlePosition(leftFootBodyId,leftFootHandleId);
        leftFootOrientation = robot->getBodyOrientation(leftFootBodyId).eulerAngles(0,1,2);

        rightHandPosition = robot->getOperationalHandlePosition(rightHandBodyId,rightHandHandleId);
        rightHandOrientation = robot->getBodyOrientation(rightHandBodyId).eulerAngles(0,1,2);

        leftHandPosition = robot->getOperationalHandlePosition(leftHandBodyId,leftHandHandleId);
        leftHandOrientation = robot->getBodyOrientation(leftHandBodyId).eulerAngles(0,1,2);

        torsoPosition = robot->getOperationalHandlePosition(torsoBodyId,torsoHandleId);
        torsoOrientation = robot->getBodyOrientation(torsoBodyId).eulerAngles(0,1,2);

        headPosition = robot->getOperationalHandlePosition(headBodyId,headHandleId);
        headOrientation = robot->getBodyOrientation(headBodyId).eulerAngles(0,1,2);



        rightFootXYTheta << rightFootPosition(0),rightFootPosition(1), rightFootOrientation(2);
        leftFootXYTheta << leftFootPosition(0),leftFootPosition(1), leftFootOrientation(2);
        comXY << torsoPosition(0), torsoPosition(1);

        hr::MatrixXr comJacobian = robot->getJacobian(hr::core::kGeometricJacobian,hr::core::nao::kTorso,torsoHandleId);
        comXYJacobian = comJacobian.topRows(2);

        supportBaseConstraint.setConstraint(comXYJacobian, comXY, leftFootXYTheta, rightFootXYTheta, footStatus, 0.1);

        rightFootPose << rightFootPosition , rightFootOrientation;
        leftFootPose << leftFootPosition, leftFootOrientation;
        torsoPose << torsoPosition, torsoOrientation;
        rightHandPose << rightHandPosition , rightHandOrientation;
        leftHandPose << leftHandPosition , leftHandOrientation;
        headPose << headPosition, headOrientation;

        if( i % 10 == 0){
            //cout << "iteration>>>>>>>>>>>>>>>>>>" << endl;
            //cout << "Foot is: " << footStatus << endl;
            //cout << "mpcData at " << mpcData.getStage()<< endl ;

            nWSR = 1000;
            cpuTime = 0.003;
            quadprog->setOptions(options);
            quadprog->hotstart(mpcData.getHessian().data(), mpcData.getFvector(robotState,footState,x_vel,y_vel).data(),constraints.getAMatrix().data(),NULL,NULL,NULL,constraints.getbupVector().data(),nWSR,&cpuTime);
            if(quadprog->getPrimalSolution(uk.data()) == qpOASES::RET_QP_NOT_SOLVED){
                return hr::FAILED_INIT;
                cout << "failed to solve" << endl;
            }
            //cout << "cputime in ms" << endl << cpuTime*1000 << endl;

            jerkXYTheta(0) = uk(0);
            jerkXYTheta(1) = uk(N_horizon+m_steps);
            nextFootState(0) = uk(N_horizon);
            nextFootState(1) = uk(N_horizon+m_steps+N_horizon);


            //cout << "Uk size:" << endl << uk.rows() << endl;
            //cout << "Uk:" << endl << uk << endl;

            //

            //cout << "Before Robot State" << endl << robotState << endl;
            system.nextState(jerkXYTheta,robotState);
            //cout << "New Robot State" << endl << robotState << endl;
            //cout << "Current foot:" << endl << footState << endl;
            //cout << "Next foot:" << endl << nextFootState << endl;


            if( (i+10) % (n_periods_step*10) == 0){
                cout << "step foot mpc-----------------------------------------------------" << endl;

                if(mpcFootStatus == hr::core::kLeftFootOnGround){
                    mpcFootStatus = hr::core::kRightFootOnGround;
                    footState = nextFootState;
                    //footState << rightFootPosition(0),rightFootPosition(1), z_foot;

                }
                else{
                    mpcFootStatus = hr::core::kLeftFootOnGround;
                    footState = nextFootState;
                    //footState << leftFootPosition(0),leftFootPosition(1), z_foot;

                }



            }



            mpcData.next();

            constraints.setConstraints(mpcData,mpcFootStatus, footState, robotState);


            if( i % (n_periods_step*10) == 0){  //Foot trajectory
                if(mpcFootStatus == hr::core::kLeftFootOnGround){
                    rightFootGoal << nextFootState, 0.0, 0.0, 0.0;

                    //cout << "Right Foot Pose " << rightFootPose.transpose() << endl;
                    //cout << "Right Foot Goal " << rightFootGoal.transpose() << endl;

                    boost::shared_ptr<hr::core::FootTrajectory> rightFootTraj = boost::make_shared<hr::core::FootTrajectory>(t,t+((double)n_periods_step/10.0),rightFootPose,rightFootGoal,hr::core::kFootCycloidal);
                    task_01.setTrajectory(rightFootTraj);
                    task_01.setGain(2.0);
                }
                else{

                    leftFootGoal << nextFootState, 0.0, 0.0, 0.0;

                    //cout << "Left Foot Pose " << leftFootPose.transpose() << endl;
                    //cout << "Left Foot Goal " <<  leftFootGoal.transpose() << endl;

                    boost::shared_ptr<hr::core::FootTrajectory> leftFootTraj = boost::make_shared<hr::core::FootTrajectory>(t,t+((double)n_periods_step/10.0),leftFootPose,leftFootGoal,hr::core::kFootCycloidal);
                    task_02.setTrajectory(leftFootTraj);
                    task_02.setGain(2.0);
                }
            }

            //torso trajectory
            torsoGoal << robotState(0), robotState(3), torso_heigth, 0.0, 0.0, 0.0;
            //cout << "Torso Pose " << torsoPose.transpose() << endl;
            //cout << "Torso Goal " <<  torsoGoal.transpose() << endl;
            boost::shared_ptr<hr::core::PointTrajectory> torsoTraj = boost::make_shared<hr::core::PointTrajectory>(t,t+0.1,torsoPose,torsoGoal,hr::core::kQuinticPolynomial);
            task_05.setTrajectory(torsoTraj);
            task_05.setGain(1.0);

        }



        if( (i + 1) % (n_periods_step*10) == 0){
            cout << "step foot status-----------------------------------------------------" << endl;

            if(footStatus == hr::core::kLeftFootOnGround){
                footStatus = hr::core::kRightFootOnGround;
            }
            else{
                footStatus = hr::core::kLeftFootOnGround;

            }

        }

        //if(t > 0.9) break;

        task_01.update(t);
        task_02.update(t);
        task_03.update(t);
        task_04.update(t);
        task_05.update(t);
        task_06.update(t);



        std::vector<hr::utils::TaskAbstract*> taskList;
        hr::utils::SimpleTask swingFootTask;
        hr::utils::SimpleTask feetTask;

        if(footStatus == hr::core::kBothFeetOnGround){
            cout << "BothFeet" << endl;
            feetTask.addRows(task_01.getMatrixA(),task_01.getVectorb());
            feetTask.addRows(task_02.getMatrixA(),task_02.getVectorb());

            taskList.push_back(&feetTask);
            taskList.push_back(&task_05);

        }

        else if(footStatus == hr::core::kRightFootOnGround){
            cout << "RightFoot" << endl;
            swingFootTask.addRows(task_02.getMatrixA(),task_02.getVectorb());
            taskList.push_back(&task_01);
            taskList.push_back(&task_05);
            taskList.push_back(&swingFootTask);

        }
        else if(footStatus == hr::core::kLeftFootOnGround){
            cout << "LeftFoot" << endl;
            swingFootTask.addRows(task_01.getMatrixA(),task_01.getVectorb());
            taskList.push_back(&task_02);
            taskList.push_back(&task_05);
            taskList.push_back(&swingFootTask);
        }


        hr::VectorXr dq = solver.Solve(taskList,C,ld,ud,lb,ub);

        std::cout << "t " << t << std::endl;

        // Integrate
        qtmp.segment(0,24) = q.segment(0,24);
        qtmp.segment(24,5) = q.segment(25,5);
        qtmp += dq*integrationStepSize;
        t = t + integrationStepSize;
        q.segment(0,24) = qtmp.segment(0,24);
        q(24) = qtmp(18);
        q.segment(25,5) = qtmp.segment(24,5);
        // End of integration

        timer.stop();
        cout << "Iteration time"<< endl;
        cout << timer.format()<< endl;
        //Set the new configuration
        setNaoConfig(motionProxy,q, integrationStepSize);
    }

    myfile.close();
}


hr::VectorXr getNaoConfig(AL::ALMotionProxy & motionProxy){
    const int kNAO_DOF = 26;
    const bool useSensors = false;

    AL::ALValue jointNames;
    jointNames = AL::ALValue::array("Body");
    std::vector<float> sensorAngles = motionProxy.getAngles(jointNames, useSensors);
    Eigen::Map<Eigen::VectorXf> q_al(sensorAngles.data(),26);

    hr::VectorXr q_hr(30);
    alToHr(q_hr, q_al.cast<hr::real_t>());
    return q_hr;

}

bool setNaoConfig(AL::ALMotionProxy & motionProxy, const hr::VectorXr & q_hr, hr::real_t time ){
    const int kNAO_DOF = 26;
    const bool isAbsolute = true;

    AL::ALValue alAngles, timeLists;
    alAngles.clear();
    alAngles.arraySetSize(kNAO_DOF);

    timeLists.clear();
    timeLists.arraySetSize(kNAO_DOF);

    hr::VectorXr q_al(kNAO_DOF);

    hrToAl(q_hr,q_al);


    AL::ALValue jointNames;
    jointNames = AL::ALValue::array("Body");
    for (int i = 0; i!=kNAO_DOF; i++) {
        timeLists[i] = AL::ALValue::array((float) 5*time);
        alAngles[i] = q_al(i);
    }
    motionProxy.setAngles(jointNames,alAngles,1.0);

    //motionProxy.angleInterpolation(jointNames, alAngles, timeLists, isAbsolute);

    return true;

}




