//example 40, Smooth transition gains

#include "openhrc/utils.h"
#include "openhrc/core.h"
#include "openhrc/kinematics.h"
#include "openhrc/locomotion.h"
#include "openhrc/vision.h"

#include <iostream>
#include <fstream>

using std::cout;
using std::endl;

int main(){
    std::ofstream myfile;
    myfile.open("example_40.dat");


    hr::real_t activation_ti = 3.0;
    hr::real_t activation_tf = 5.0;
    hr::real_t gainActivation = 2.0;

    hr::real_t deactivation_ti = 2.0;
    hr::real_t deactivation_tf = 4.0;
    hr::real_t gainDeactivation = 3.0;

    hr::core::ActivationGain activationGain(activation_ti,activation_tf,gainActivation);
    hr::core::DeactivationGain deactivationGain(deactivation_ti,deactivation_tf,gainDeactivation);
    hr::core::GainAbstract * abstractActivationGain;
    hr::core::GainAbstract * abstractDeactivationGain;

    abstractActivationGain = &activationGain;
    abstractDeactivationGain = &deactivationGain;

    hr::real_t t = 0.0;
    hr::real_t T = 10.0;
    hr::real_t dt = 0.01;
    int N = T/dt;
    for (int i = 0 ; i != N ; i ++){

        myfile << t << " " << (*abstractActivationGain)(t)<< " " << (*abstractDeactivationGain)(t)  <<endl;

        t = t + dt;
    }
}


