//Example 20 Static Walk , Use of trajectory definitions

#include "openhrc/utils.h"
#include "openhrc/core.h"
#include "openhrc/kinematics.h"
#include <alproxies/almotionproxy.h>
#include <alerror/alerror.h>
#include <iostream>
#include <boost/timer/timer.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <fstream>

#ifdef  USE_LOCAL_XBOT //
std::string naoFile = "../../../src/data/Nao_blue_noninertial.xbot";
#else
std::string naoFile = "/home/nao/projects/hrData/Nao_blue_noninertial.xbot";
#endif

using boost::timer::cpu_timer;
using std::cout;
using std::endl;

hr::VectorXr getNaoConfig(AL::ALMotionProxy & motionProxy);
bool setNaoConfig(AL::ALMotionProxy & motionProxy, const hr::VectorXr & q, hr::real_t time );

void hrToAl(const hr::VectorXr & q_hr , hr::VectorXr & q_al){
    const int kNAO_DOF = 26;
    int jointId[kNAO_DOF] = {7,8,9,10,11,12,13,-1,19,20,21,22,23,24,25,26,27,28,29,30,14,15,16,17,18,-1};
    q_al.setZero();
    for (int i = 0 ; i < kNAO_DOF; i++) {
        if(jointId[i] != -1) {
            q_al(i) = q_hr(jointId[i]-1);
        }
    }

}

void alToHr(hr::VectorXr & q_hr , const hr::VectorXr & q_al){
    const int kNAO_DOF = 26;
    int jointId[kNAO_DOF] = {7,8,9,10,11,12,13,-1,19,20,21,22,23,24,-1,26,27,28,29,30,14,15,16,17,18,-1};
    q_hr.setZero();
    for (int i = 0 ; i < kNAO_DOF; i++) {
        if(jointId[i] != -1) {
            q_hr(jointId[i]-1) = q_al(i);
        }
    }
    q_hr(24) = q_hr(18);

}


int main(){

    std::ofstream myfile;
    myfile.open ("example_20.dat");

    hr::core::World world = hr::core::World();
    std::string sFile = naoFile;
    int robotId = world.getRobotsVector()->size();
    world.loadMultiBody(sFile,robotId, hr::core::kNAO);
    boost::shared_ptr<hr::core::MultiBody> robot = world.getRobot(0);

    hr::VectorXr q = robot->getConfiguration();
    hr::VectorXr qtmp(q.rows()-1);

    AL::ALMotionProxy motionProxy = AL::ALMotionProxy("127.0.0.1");
    //AL::ALMotionProxy motionProxy = AL::ALMotionProxy("nao.local");
    //AL::ALMotionProxy motionProxy = AL::ALMotionProxy("seia.local");

    q = getNaoConfig(motionProxy);
    robot->setConfiguration(q);
    robot->computeForwardKinematics();

    cout << "Robot configuration" << endl << q << endl;

    hr::Vector3r rightFootHandle(0.0,0.0,-0.04);
    int rightFootBodyId = hr::core::nao::kRightFoot;
    int rightFootHandleId = robot->addOperationalHandle(rightFootBodyId,rightFootHandle);

    hr::Vector3r leftFootHandle(0.0,0.0,-0.04);
    int leftFootBodyId = hr::core::nao::kLeftFoot;
    int leftFootHandleId = robot->addOperationalHandle(leftFootBodyId,leftFootHandle);


    hr::Vector3r rightHandHandle(0.08,0.0,0.0);
    int rightHandBodyId = hr::core::nao::kRightHand;
    int rightHandHandleId = robot->addOperationalHandle(rightHandBodyId,rightHandHandle);

    hr::Vector3r leftHandHandle(0.08,0.0,0.0);
    int leftHandBodyId = hr::core::nao::kLeftHand;
    int leftHandHandleId = robot->addOperationalHandle(leftHandBodyId,leftHandHandle);


    hr::Vector3r torsoHandle(0.0,0.0,0.0);
    int torsoBodyId = hr::core::nao::kTorso;
    int torsoHandleId = robot->addOperationalHandle(torsoBodyId,torsoHandle);

    hr::Vector3r headHandle(0.0,0.0,0.0);
    int headBodyId = hr::core::nao::kHead;
    int headHandleId = robot->addOperationalHandle(headBodyId,headHandle);

    hr::SpatialVector rightFootPose;
    hr::SpatialVector leftFootPose;

    hr::SpatialVector rightHandPose;
    hr::SpatialVector leftHandPose;

    hr::SpatialVector torsoPose;
    hr::SpatialVector headPose;

    hr::SpatialVector rightFootGoal;
    hr::SpatialVector leftFootGoal;

    hr::SpatialVector rightHandGoal;
    hr::SpatialVector leftHandGoal;

    hr::SpatialVector torsoGoal;
    hr::SpatialVector headGoal;

    hr::Vector3r rightFootPosition = robot->getOperationalHandlePosition(rightFootBodyId,rightFootHandleId);
    hr::Vector3r rightFootOrientation = robot->getBodyOrientation(rightFootBodyId).eulerAngles(0,1,2);

    hr::Vector3r leftFootPosition = robot->getOperationalHandlePosition(leftFootBodyId,leftFootHandleId);
    hr::Vector3r leftFootOrientation = robot->getBodyOrientation(leftFootBodyId).eulerAngles(0,1,2);

    hr::Vector3r rightHandPosition = robot->getOperationalHandlePosition(rightHandBodyId,rightHandHandleId);
    hr::Vector3r rightHandOrientation = robot->getBodyOrientation(rightHandBodyId).eulerAngles(0,1,2);

    hr::Vector3r leftHandPosition = robot->getOperationalHandlePosition(leftHandBodyId,leftHandHandleId);
    hr::Vector3r leftHandOrientation = robot->getBodyOrientation(leftHandBodyId).eulerAngles(0,1,2);

    hr::Vector3r torsoPosition = robot->getOperationalHandlePosition(torsoBodyId,torsoHandleId);
    hr::Vector3r torsoOrientation = robot->getBodyOrientation(torsoBodyId).eulerAngles(0,1,2);

    hr::Vector3r headPosition = robot->getOperationalHandlePosition(headBodyId,headHandleId);
    hr::Vector3r headOrientation = robot->getBodyOrientation(headBodyId).eulerAngles(0,1,2);

    cout << "Left hand position: " << endl << leftHandPosition << endl;
    cout << "Right hand position: " << endl << rightHandPosition << endl;
    cout << "Head position: " << endl << headPosition << endl;
    cout << "Torso position: " << endl << torsoPosition << endl;
    cout << "Left foot position: " << endl << leftFootPosition << endl;
    cout << "Right foot position: " << endl << rightFootPosition << endl;

    cout << "Left hand orientation: " << endl << leftHandOrientation << endl;
    cout << "Right hand orientation: " << endl << rightHandOrientation << endl;
    cout << "Head orientation: " << endl << headOrientation << endl;
    cout << "Torso orientation: " << endl << torsoOrientation << endl;
    cout << "Left foot orientation: " << endl << leftFootOrientation << endl;
    cout << "Right foot orientation: " << endl << rightFootOrientation << endl;


    rightFootPose << rightFootPosition , rightFootOrientation;
    leftFootPose << leftFootPosition, leftFootOrientation;
    torsoPose << torsoPosition, torsoOrientation;
    rightHandPose << rightHandPosition , rightHandOrientation;
    leftHandPose << leftHandPosition , leftHandOrientation;
    headPose << headPosition, headOrientation;

    rightFootGoal << rightFootPosition , 0.0 , 0.0, 0.0;
    leftFootGoal << leftFootPosition, 0.0 , 0.0 , 0.0;

    torsoGoal << torsoPosition, torsoOrientation;
    rightHandGoal << rightHandPosition , rightHandOrientation;
    leftHandGoal << leftHandPosition , leftHandOrientation;
    headGoal << headPosition, headOrientation;



    hr::core::KinematicTask task_01(rightFootHandleId,rightFootBodyId,0,robot, hr::core::kPositionOrientation);
    task_01.setGoal(rightFootGoal);
    task_01.setGain(1.0);
    hr::core::KinematicTask task_02(leftFootHandleId,leftFootBodyId,0,robot, hr::core::kPositionOrientation);
    task_02.setGoal(leftFootGoal);
    task_02.setGain(1.0);
    hr::core::KinematicTask task_03(rightHandHandleId,rightHandBodyId,0,robot, hr::core::kPosition);
    task_03.setGoal(rightHandGoal);
    //task_03.setGain(1.0);
    hr::core::KinematicTask task_04(leftHandHandleId,leftHandBodyId,0,robot, hr::core::kPosition);
    task_04.setGoal(leftHandGoal);
    //task_04.setGain(1.0);
    hr::core::KinematicTask task_05(torsoHandleId,torsoBodyId,0,robot, (  hr::core::kWXYZ | hr::core::kXYZ ));
    task_05.setGoal(torsoGoal);
    //task_05.setGain(1.0);
    hr::core::KinematicTask task_06(headHandleId,headBodyId,0,robot, hr::core::kWYZ);
    task_06.setGoal(headGoal);
    //task_06.setGain(1.0);

    hr::utils::SimpleHierarchicalSolver solver;

    hr::real_t integrationStepSize = 0.01;

    hr::VectorXr lb_ = (robot->getJointLowLimits()-q)/integrationStepSize;
    hr::VectorXr ub_ = (robot->getJointUpLimits()-q)/integrationStepSize;

    hr::VectorXr lb(29);
    hr::VectorXr ub(29);

    lb.segment(0,24) = lb_.segment(0,24);
    lb.segment(24,5) = lb_.segment(25,5);

    ub.segment(0,24) = ub_.segment(0,24);
    ub.segment(24,5) = ub_.segment(25,5);

    hr::VectorXr ld(29);
    hr::VectorXr ud(29);
    hr::MatrixXr C;

    hr::VectorXr ud_ = robot->getJointUpSpeedLimits();
    hr::VectorXr ld_ = robot->getJointLowSpeedLimits();

    int n = 29;
    C = hr::MatrixXr::Identity(n,n);

    ld.segment(0,24) = ld_.segment(0,24);
    ld.segment(24,5) = ld_.segment(25,5);

    ud.segment(0,24) = ud_.segment(0,24);
    ud.segment(24,5) = ud_.segment(25,5);

    q = getNaoConfig(motionProxy);
    hr::VectorXr q_tmp = q;
    robot->setConfiguration(q);
    robot->computeForwardKinematics();
/*
    hr::core::nao::SupportBaseConstraint constraint;
    hr::Vector3r rightFootXYTheta;
    hr::Vector3r leftFootXYTheta;
    hr::Vector2r comXY;
    hr::Matrix2Xr comXYJacobian;
    */
    hr::core::FootStatus footStatus = hr::core::kBothFeetOnGround;


    hr::real_t t = 0.0;
    hr::real_t T = 10.0;
    int N = T/integrationStepSize;

    for(int i = 0; i != N; i++){

        q_tmp = getNaoConfig(motionProxy);
        q_tmp.segment(0,6) = q.segment(0,6);

        robot->setConfiguration(q);
        robot->computeForwardKinematics();

        //Calculate the new joint speed limit
        lb_ = (robot->getJointLowLimits()-q)/integrationStepSize;
        ub_ = (robot->getJointUpLimits()-q)/integrationStepSize;

        lb.segment(0,24) = lb_.segment(0,24);
        lb.segment(24,5) = lb_.segment(25,5);

        ub.segment(0,24) = ub_.segment(0,24);
        ub.segment(24,5) = ub_.segment(25,5);



        rightFootPosition = robot->getOperationalHandlePosition(rightFootBodyId,rightFootHandleId);
        rightFootOrientation = robot->getBodyOrientation(rightFootBodyId).eulerAngles(0,1,2);

        leftFootPosition = robot->getOperationalHandlePosition(leftFootBodyId,leftFootHandleId);
        leftFootOrientation = robot->getBodyOrientation(leftFootBodyId).eulerAngles(0,1,2);

        rightHandPosition = robot->getOperationalHandlePosition(rightHandBodyId,rightHandHandleId);
        rightHandOrientation = robot->getBodyOrientation(rightHandBodyId).eulerAngles(0,1,2);

        leftHandPosition = robot->getOperationalHandlePosition(leftHandBodyId,leftHandHandleId);
        leftHandOrientation = robot->getBodyOrientation(leftHandBodyId).eulerAngles(0,1,2);

        torsoPosition = robot->getOperationalHandlePosition(torsoBodyId,torsoHandleId);
        torsoOrientation = robot->getBodyOrientation(torsoBodyId).eulerAngles(0,1,2);

        headPosition = robot->getOperationalHandlePosition(headBodyId,headHandleId);
        headOrientation = robot->getBodyOrientation(headBodyId).eulerAngles(0,1,2);


        /*
        rightFootXYTheta << rightFootPosition(0),rightFootPosition(1), rightFootOrientation(2);
        leftFootXYTheta << leftFootPosition(0),leftFootPosition(1), leftFootOrientation(2);
        comXY << torsoPosition(0), torsoPosition(1);

        hr::MatrixXr comJacobian = robot->getJacobian(hr::core::kGeometricJacobian,hr::core::nao::kTorso,torsoHandleId);
        comXYJacobian = comJacobian.topRows(2);

        constraint.setConstraint(comXYJacobian, comXY, leftFootXYTheta, rightFootXYTheta, footStatus, 0.1);

*/


        rightFootPose << rightFootPosition , rightFootOrientation;
        leftFootPose << leftFootPosition, leftFootOrientation;
        torsoPose << torsoPosition, torsoOrientation;
        rightHandPose << rightHandPosition , rightHandOrientation;
        leftHandPose << leftHandPosition , leftHandOrientation;
        headPose << headPosition, headOrientation;


        if(t > -0.001 && t < 0.0001){
            cout << "Torso Task" << endl;

            torsoGoal << rightFootPosition(0), -0.06, torsoPosition(2), 0.0, 0.0, 0.0;
            cout << "Torso Pose " << torsoPose.transpose() << endl;
            cout << "Torso Goal " <<  torsoGoal.transpose() << endl;

            boost::shared_ptr<hr::core::PointTrajectory> torsoTraj = boost::make_shared<hr::core::PointTrajectory>(0.0,1.0,torsoPose,torsoGoal,hr::core::kQuinticPolynomial);
            task_05.setTrajectory(torsoTraj);
            task_05.setGain(1.0);
            footStatus = hr::core::kBothFeetOnGround;
        }
        if(t > 0.9499 && t < 0.9501){
            cout << "Left Foot Traj" << endl;
            leftFootGoal << leftFootPosition(0)+0.04, 0.05, leftFootPosition(2), 0.0, 0.0, 0.0;

            cout << "Left Foot Pose " << leftFootPose.transpose() << endl;
            cout << "Left Foot Goal " <<  leftFootGoal.transpose() << endl;

            boost::shared_ptr<hr::core::FootTrajectory> leftFootTraj = boost::make_shared<hr::core::FootTrajectory>(1.0,1.8,leftFootPose,leftFootGoal,hr::core::kFootCycloidal);
            task_02.setTrajectory(leftFootTraj);
            task_02.setGain(2.0);
            footStatus = hr::core::kRightFootOnGround;
        }

        if(t > 1.9499 && t < 1.9501){
            cout << "Torso Task" << endl;

            torsoGoal << leftFootPosition(0), 0.06, torsoPosition(2), 0.0, 0.0, 0.0;
            cout << "Torso Pose " << torsoPose.transpose() << endl;
            cout << "Torso Goal " <<  torsoGoal.transpose() << endl;

            boost::shared_ptr<hr::core::PointTrajectory> torsoTraj = boost::make_shared<hr::core::PointTrajectory>(2.0,3.0,torsoPose,torsoGoal,hr::core::kQuinticPolynomial);
            task_05.setTrajectory(torsoTraj);
            task_05.setGain(1.0);
            footStatus = hr::core::kBothFeetOnGround;
        }
        if(t > 2.9499 && t < 2.9501){
            cout << "Right Foot Traj" << endl;
            rightFootGoal << rightFootPosition(0)+0.04, -0.05, rightFootPosition(2), 0.0, 0.0, 0.0;

            cout << "Right Foot Pose " << rightFootPose.transpose() << endl;
            cout << "Right Foot Goal " << rightFootGoal.transpose() << endl;

            boost::shared_ptr<hr::core::FootTrajectory> rightFootTraj = boost::make_shared<hr::core::FootTrajectory>(3.0,3.8,rightFootPose,rightFootGoal,hr::core::kFootCycloidal);
            task_01.setTrajectory(rightFootTraj);
            task_01.setGain(2.0);
            footStatus = hr::core::kLeftFootOnGround;
        }



        if(t > 3.9499 && t < 3.9501){
            cout << "Torso Task" << endl;

            torsoGoal << rightFootPosition(0), -0.06, torsoPosition(2), 0.0, 0.0, 0.0;
            cout << "Torso Pose " << torsoPose.transpose() << endl;
            cout << "Torso Goal " <<  torsoGoal.transpose() << endl;

            boost::shared_ptr<hr::core::PointTrajectory> torsoTraj = boost::make_shared<hr::core::PointTrajectory>(4.0,5.0,torsoPose,torsoGoal,hr::core::kQuinticPolynomial);
            task_05.setTrajectory(torsoTraj);
            task_05.setGain(1.0);
            footStatus = hr::core::kBothFeetOnGround;
        }
        if(t > 4.9499 && t < 4.9501){
            cout << "Left Foot Traj" << endl;
            leftFootGoal << leftFootPosition(0)+0.04, 0.05, leftFootPosition(2), 0.0, 0.0, 0.0;

            cout << "Left Foot Pose " << leftFootPose.transpose() << endl;
            cout << "Left Foot Goal " <<  leftFootGoal.transpose() << endl;

            boost::shared_ptr<hr::core::FootTrajectory> leftFootTraj = boost::make_shared<hr::core::FootTrajectory>(5.0,5.8,leftFootPose,leftFootGoal,hr::core::kFootCycloidal);
            task_02.setTrajectory(leftFootTraj);
            task_02.setGain(2.0);
            footStatus = hr::core::kRightFootOnGround;
        }

        if(t > 5.9499 && t < 5.9501){
            cout << "Torso Task" << endl;

            torsoGoal << leftFootPosition(0), 0.06, torsoPosition(2), 0.0, 0.0, 0.0;
            cout << "Torso Pose " << torsoPose.transpose() << endl;
            cout << "Torso Goal " <<  torsoGoal.transpose() << endl;

            boost::shared_ptr<hr::core::PointTrajectory> torsoTraj = boost::make_shared<hr::core::PointTrajectory>(6.0,7.0,torsoPose,torsoGoal,hr::core::kQuinticPolynomial);
            task_05.setTrajectory(torsoTraj);
            task_05.setGain(1.0);
            footStatus = hr::core::kBothFeetOnGround;
        }
        if(t > 6.9499 && t < 6.9501){
            cout << "Right Foot Traj" << endl;
            rightFootGoal << rightFootPosition(0)+0.04, -0.05, rightFootPosition(2), 0.0, 0.0, 0.0;

            cout << "Right Foot Pose " << rightFootPose.transpose() << endl;
            cout << "Right Foot Goal " << rightFootGoal.transpose() << endl;

            boost::shared_ptr<hr::core::FootTrajectory> rightFootTraj = boost::make_shared<hr::core::FootTrajectory>(7.0,7.8,rightFootPose,rightFootGoal,hr::core::kFootCycloidal);
            task_01.setTrajectory(rightFootTraj);
            task_01.setGain(2.0);
            footStatus = hr::core::kLeftFootOnGround;
        }

        if(t > 7.9499 && t < 7.9501){
            cout << "Torso Task" << endl;

            torsoGoal << rightFootPosition(0), 0.00, torsoPosition(2), 0.0, 0.0, 0.0;
            cout << "Torso Pose " << torsoPose.transpose() << endl;
            cout << "Torso Goal " <<  torsoGoal.transpose() << endl;

            boost::shared_ptr<hr::core::PointTrajectory> torsoTraj = boost::make_shared<hr::core::PointTrajectory>(8.0,9.0,torsoPose,torsoGoal,hr::core::kQuinticPolynomial);
            task_05.setTrajectory(torsoTraj);
            task_05.setGain(1.0);
            footStatus = hr::core::kBothFeetOnGround;
        }




        task_01.update(t);
        task_02.update(t);
        task_03.update(t);
        task_04.update(t);
        task_05.update(t);
        task_06.update(t);



        std::vector<hr::utils::TaskAbstract*> taskList;
        hr::utils::SimpleTask swingFootTask;
        hr::utils::SimpleTask feetTask;

        if(footStatus == hr::core::kBothFeetOnGround){
            cout << "BothFeet" << endl;
            feetTask.addRows(task_01.getMatrixA(),task_01.getVectorb());
            feetTask.addRows(task_02.getMatrixA(),task_02.getVectorb());

            taskList.push_back(&feetTask);
            taskList.push_back(&task_05);

        }

        else if(footStatus == hr::core::kRightFootOnGround){
            cout << "RightFoot" << endl;
            swingFootTask.addRows(task_02.getMatrixA(),task_02.getVectorb());
            taskList.push_back(&task_01);
            taskList.push_back(&task_05);
            taskList.push_back(&swingFootTask);

        }
        else if(footStatus == hr::core::kLeftFootOnGround){
            cout << "LeftFoot" << endl;
            swingFootTask.addRows(task_01.getMatrixA(),task_01.getVectorb());
            taskList.push_back(&task_02);
            taskList.push_back(&task_05);
            taskList.push_back(&swingFootTask);
        }


        hr::VectorXr dq = solver.Solve(taskList,C,ld,ud,lb,ub);

        std::cout << "t " << t << std::endl;

        // Integrate
        qtmp.segment(0,24) = q.segment(0,24);
        qtmp.segment(24,5) = q.segment(25,5);
        qtmp += dq*integrationStepSize;
        t = t + integrationStepSize;
        q.segment(0,24) = qtmp.segment(0,24);
        q(24) = qtmp(18);
        q.segment(25,5) = qtmp.segment(24,5);
        // End of integration


        //Set the new configuration
        setNaoConfig(motionProxy,q, integrationStepSize);
    }

    myfile.close();
}


hr::VectorXr getNaoConfig(AL::ALMotionProxy & motionProxy){
    const int kNAO_DOF = 26;
    const bool useSensors = false;

    AL::ALValue jointNames;
    jointNames = AL::ALValue::array("Body");
    std::vector<float> sensorAngles = motionProxy.getAngles(jointNames, useSensors);
    Eigen::Map<Eigen::VectorXf> q_al(sensorAngles.data(),26);

    hr::VectorXr q_hr(30);
    alToHr(q_hr, q_al.cast<hr::real_t>());
    return q_hr;

}

bool setNaoConfig(AL::ALMotionProxy & motionProxy, const hr::VectorXr & q_hr, hr::real_t time ){
    const int kNAO_DOF = 26;
    const bool isAbsolute = true;

    AL::ALValue alAngles, timeLists;
    alAngles.clear();
    alAngles.arraySetSize(kNAO_DOF);

    timeLists.clear();
    timeLists.arraySetSize(kNAO_DOF);

    hr::VectorXr q_al(kNAO_DOF);

    hrToAl(q_hr,q_al);


    AL::ALValue jointNames;
    jointNames = AL::ALValue::array("Body");
    for (int i = 0; i!=kNAO_DOF; i++) {
        timeLists[i] = AL::ALValue::array((float) time);
        alAngles[i] = q_al(i);
    }
    motionProxy.setAngles(jointNames,alAngles,1.0);

    //motionProxy.angleInterpolation(jointNames, alAngles, timeLists, isAbsolute);

    return true;

}




