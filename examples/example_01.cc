//Example of the locomotion controller and timing tests

#include <iostream>
#include <boost/shared_ptr.hpp>
#include "openhrc/locomotion.h"
#include "Eigen/Dense"
#include "qpOASES.hpp"
#include <boost/timer/timer.hpp>


using boost::timer::cpu_timer;
using std::cout;
using std::endl;

int main(){

    cpu_timer timer;

    cout << "Starting test" << endl;

    /** MPC parameters */
    int m_steps = 2;
    int n_periods_step = 8;
    int N_horizon = m_steps*n_periods_step;
    hr::real_t T_sampling = 0.1;
    hr::real_t com_height = 0.3 ;

       /** These parameters describe the state of the robot */

    hr::Vector3r footState;    //Member for the position of the current foot on the ground
    hr::Vector3r nextFootState;    //Member for the position of the current foot on the ground
    hr::locomotion::RobotState robotState;        //Member for the augmented CoM robot state
    hr::Vector3r xk;              //Variable for the CoM robot state in X
    hr::Vector3r yk;              //Variable for the CoM robot state in Y
    hr::Vector3r thetak;          //Variable for the CoM robot state in Theta
    hr::VectorXr x_vel(N_horizon);        //Reference velocity in X direction     //TODO: Time list trajectories similar to ALDCM
    hr::VectorXr y_vel(N_horizon);        //Reference velocity in Y direction     //TODO
    hr::core::FootStatus footStatus = hr::core::kLeftFootOnGround;
    hr::Vector3r jerkXYTheta; //Jerk holder variable
    hr::VectorXr uk;      ///Jerk vector
    uk.resize(2*(N_horizon+m_steps));
    uk.setZero();
    /** Initialization of the robot parameters */

    cout << "Init robot paramters" << endl;
    footState << 0.0,0.05,0.0;
    xk << 0.0 , 0.0, 0.0;
    yk << 0.05 , 0.0, 0.0;
    thetak << 0.0, 0.0 ,0.0;
    robotState.setState(xk,yk,thetak);
    jerkXYTheta << 0.0,0.,0.;
    x_vel.setOnes();
    y_vel.setOnes();
    x_vel = x_vel*0.00;
    y_vel = y_vel*1.00;

     timer.start();
    /** Initialization of the MPC */
    hr::locomotion::MPCData mpcData = hr::locomotion::MPCData(com_height,T_sampling,m_steps,n_periods_step);
    hr::locomotion::MPCConstraints constraints;
    hr::locomotion::RobotSystem system;

    mpcData.init();
    constraints.init(mpcData);
    system.init(com_height,T_sampling);

    constraints.setConstraints(mpcData,footStatus, footState, robotState);

    timer.stop();
    cout << "Initialization time" << endl;
    cout << timer.format() << endl;

    cout << "Constraints Hessian" << endl << constraints.getAMatrix().trace() << endl;                                       //TODO: Review version with different parameters for m and steps
    cout << "Constraints Bounds" << endl << constraints.getbupVector() << endl;     //TODO: Review version with different parameters for m and steps


    ///This part of the code should not be here, but this is a simple test

    int nWSR = 1000;
    qpOASES::real_t cpuTime = 1.0;

    int nV = 2*(N_horizon+m_steps);
    int nC = constraints.getbupVector().rows();
    qpOASES::SQProblem quad;
    boost::shared_ptr<qpOASES::SQProblem> quadprog;
    qpOASES::Options options;
    options.setToMPC();
    options.printLevel = qpOASES::PL_NONE;
    quadprog.reset(new qpOASES::SQProblem(nV,nC));
    quadprog->setOptions(options);
    timer.start();
    quadprog->init(mpcData.getHessian().data(), mpcData.getFvector(robotState,footState,x_vel,y_vel).data(),constraints.getAMatrix().data(),NULL,NULL,NULL,constraints.getbupVector().data(),nWSR,&cpuTime);
    if(quadprog->getPrimalSolution(uk.data()) == qpOASES::RET_QP_NOT_SOLVED){
        return hr::FAILED_INIT;
        cout << "failed to solve" << endl;
    }
    timer.stop();
    cout << "qpoases solved, time:" << endl;
    cout << timer.format() << endl;

    nWSR = 1000;
    cpuTime = 0.003;
    timer.start();
    quadprog->hotstart(mpcData.getHessian().data(), mpcData.getFvector(robotState,footState,x_vel,y_vel).data(),constraints.getAMatrix().data(),NULL,NULL,NULL,constraints.getbupVector().data(),nWSR,&cpuTime);
    if(quadprog->getPrimalSolution(uk.data()) == qpOASES::RET_QP_NOT_SOLVED){
        return hr::FAILED_INIT;
        cout << "failed to solve" << endl;
    }
    timer.stop();
    cout << "hot qpoases solved, warm solving time:" << endl;
    cout << timer.format() << endl;
    cout << "cputime in ms" << endl << cpuTime*1000 << endl;

        jerkXYTheta(0) = uk(0);
        jerkXYTheta(1) = uk(N_horizon+m_steps);
        nextFootState(0) = uk(N_horizon);
        nextFootState(1) = uk(N_horizon+m_steps+N_horizon);


    cout << "Uk size:" << endl << uk.rows() << endl;
    cout << "Uk:" << endl << uk << endl;

    //

    cout << "Everything seems to be working fine..." << endl;
    cout << "Before Robot State" << endl << robotState << endl;
    system.nextState(jerkXYTheta,robotState);
    cout <<  "Applied Jerk" << endl << jerkXYTheta << endl;
    cout << "New Robot State" << endl << robotState << endl;
    cout << "Current foot:" << endl << footState << endl;
    cout << "Next foot:" << endl << nextFootState << endl;

    cout << "Calling non init method" << endl;






    //! Second iteration
    cout << "Second iteration" << endl;
    timer.start();
    mpcData.next();
    constraints.setConstraints(mpcData,footStatus, footState, robotState);
    timer.stop();
    cout << "Constraints setting time:" << endl;
    cout << timer.format() << endl;

    nWSR = 1000;
    cpuTime = 0.003;
    timer.start();
    quadprog->hotstart(mpcData.getHessian().data(), mpcData.getFvector(robotState,footState,x_vel,y_vel).data(),constraints.getAMatrix().data(),NULL,NULL,NULL,constraints.getbupVector().data(),nWSR,&cpuTime);
    if(quadprog->getPrimalSolution(uk.data()) == qpOASES::RET_QP_NOT_SOLVED){
        return hr::FAILED_INIT;
        cout << "failed to solve" << endl;
    }
    timer.stop();
    cout << "hot qpoases solved, warm solving time:" << endl;
    cout << timer.format() << endl;
    cout << "cputime in ms" << endl << cpuTime*1000 << endl;


        jerkXYTheta(0) = uk(0);
        jerkXYTheta(1) = uk(N_horizon+m_steps);
        nextFootState(0) = uk(N_horizon);
        nextFootState(1) = uk(N_horizon+m_steps+N_horizon);


    cout << "Uk size:" << endl << uk.rows() << endl;
    cout << "Uk:" << endl << uk << endl;

    //

    cout << "Everything seems to be working fine..." << endl;
    cout << "Before Robot State" << endl << robotState << endl;
    system.nextState(jerkXYTheta,robotState);
    cout <<  "Applied Jerk" << endl << jerkXYTheta << endl;
    cout << "New Robot State" << endl << robotState << endl;
    cout << "Current foot:" << endl << footState << endl;
    cout << "Next foot:" << endl << nextFootState << endl;

    cout << "Calling non init method" << endl;



    //! Third iteration
    cout << "Third iteration" << endl;
    timer.start();
    mpcData.next();
    constraints.setConstraints(mpcData,footStatus, footState, robotState);
    timer.stop();
    cout << "Constraints setting time:" << endl;
    cout << timer.format() << endl;


    nWSR = 1000;
    cpuTime = 0.003;
    timer.start();
    quadprog->hotstart(mpcData.getHessian().data(), mpcData.getFvector(robotState,footState,x_vel,y_vel).data(),constraints.getAMatrix().data(),NULL,NULL,NULL,constraints.getbupVector().data(),nWSR,&cpuTime);
    if(quadprog->getPrimalSolution(uk.data()) == qpOASES::RET_QP_NOT_SOLVED){
        return hr::FAILED_INIT;
        cout << "failed to solve" << endl;
    }
    timer.stop();
    cout << "hot qpoases solved, warm solving time:" << endl;
    cout << timer.format() << endl;
        cout << "cputime in ms" << endl << cpuTime*1000 << endl;


        jerkXYTheta(0) = uk(0);
        jerkXYTheta(1) = uk(N_horizon+m_steps);
        nextFootState(0) = uk(N_horizon);
        nextFootState(1) = uk(N_horizon+m_steps+N_horizon);


    cout << "Uk size:" << endl << uk.rows() << endl;
    cout << "Uk:" << endl << uk << endl;

    //

    cout << "Everything seems to be working fine..." << endl;
    cout << "Before Robot State" << endl << robotState << endl;
    system.nextState(jerkXYTheta,robotState);
    cout <<  "Applied Jerk" << endl << jerkXYTheta << endl;
    cout << "New Robot State" << endl << robotState << endl;
    cout << "Current foot:" << endl << footState << endl;
    cout << "Next foot:" << endl << nextFootState << endl;

    cout << "Calling non init method" << endl;


    //! Fourth iteration
    cout << "Fourth iteration" << endl;

    timer.start();
    mpcData.next();
    constraints.setConstraints(mpcData,footStatus, footState, robotState);
    timer.stop();
    cout << "Constraints setting time:" << endl;
    cout << timer.format() << endl;

    nWSR = 1000;
    cpuTime = 0.003;
    timer.start();
    quadprog->hotstart(mpcData.getHessian().data(), mpcData.getFvector(robotState,footState,x_vel,y_vel).data(),constraints.getAMatrix().data(),NULL,NULL,NULL,constraints.getbupVector().data(),nWSR,&cpuTime);
    if(quadprog->getPrimalSolution(uk.data()) == qpOASES::RET_QP_NOT_SOLVED){
        return hr::FAILED_INIT;
        cout << "failed to solve" << endl;
    }
    timer.stop();
    cout << "hot qpoases solved, warm solving time:" << endl;
    cout << timer.format() << endl;
    cout << "cputime in ms" << endl << cpuTime*1000 << endl;

        jerkXYTheta(0) = uk(0);
        jerkXYTheta(1) = uk(N_horizon+m_steps);
        nextFootState(0) = uk(N_horizon);
        nextFootState(1) = uk(N_horizon+m_steps+N_horizon);


    cout << "Uk size:" << endl << uk.rows() << endl;
    cout << "Uk:" << endl << uk << endl;

    //

    cout << "Everything seems to be working fine..." << endl;
    cout << "Before Robot State" << endl << robotState << endl;
    system.nextState(jerkXYTheta,robotState);
    cout <<  "Applied Jerk" << endl << jerkXYTheta << endl;
    cout << "New Robot State" << endl << robotState << endl;
    cout << "Current foot:" << endl << footState << endl;
    cout << "Next foot:" << endl << nextFootState << endl;

    cout << "Calling non init method" << endl;


}
