
//example 19, Convex hull and constraint matrix computation

#include "openhrc/utils.h"
#include "openhrc/core.h"
#include "openhrc/kinematics.h"
#include <vector>
#include <iostream>
#include <fstream>

using std::cout;
using std::endl;

hr::real_t isLeft(hr::Vector2r P0, hr::Vector2r P1, hr::Vector2r P2){
    return (P1(0) - P0(0))*(P2(1)-P0(1))-(P2(0)-P0(0))*(P1(1)-P0(1));
}

std::vector<int> HULL( std::vector<hr::Vector2r> V){
    std::vector<int> index;
    int n = V.size() - 1;
    index.push_back(0);
    for(int i = 1 ; i != n ; i ++){
        if(isLeft(V[index.back()],V[i],V[i+1]) >= 0){
            continue;
        }
        else{
            index.push_back(i);
        }
    }
    return index;
}

std::vector<int> HULL2( std::vector<hr::Vector2r> V){
    std::vector<int> index;
    int n = V.size() - 1;
    index.push_back(0);
    bool exit = false;
    for(int i = 1 ; i != n ; i ++){
        for(int k = i + 1 ; k <= n ; k++){
            if(isLeft(V[index.back()],V[i],V[k]) > 0){
                exit = true;
                break;
            }
            else{

            }
        }
        if(exit){
            exit = false;
            continue;
        }
        else{
            index.push_back(i);
        }
    }
    return index;
}



int main(){
    std::ofstream myfile;
    std::ofstream myfile_result;
    myfile.open("convex_hull.dat");
    myfile_result.open("convex_hull_result.dat");

    hr::real_t x_offset =  0.01;
    hr::real_t y_offset =  -0.09;
    hr::real_t leftFootAngle =  0.1;
    hr::real_t rightFootAngle = -0.1;

    hr::Vector2r offset;
    offset << x_offset , y_offset;

    hr::Matrix2r rotLeft;
    rotLeft << std::cos(leftFootAngle), - std::sin(leftFootAngle), std::sin(leftFootAngle), std::cos(leftFootAngle);

    hr::Matrix2r rotRight;
    rotRight << std::cos(rightFootAngle), - std::sin(rightFootAngle), std::sin(rightFootAngle), std::cos(rightFootAngle);


    std::vector<hr::Vector2r> A;
    std::vector<hr::Vector2r> B;
    std::vector<hr::Vector2r> C;
    std::vector<hr::Vector2r> C_hull;


    hr::Vector2r LP0;
    hr::Vector2r LP1;
    hr::Vector2r LP2;
    hr::Vector2r LP3;

    hr::Vector2r RP0;
    hr::Vector2r RP1;
    hr::Vector2r RP2;
    hr::Vector2r RP3;

    //LP0 <<-0.02 , 0.07;
    //LP1 << 0.02 , 0.07;
    //LP2 << 0.02 ,-0.03;
    //LP3 <<-0.02 ,-0.03;
    LP0 << 0.07 , 0.03;
    LP1 << 0.07 , -0.02;
    LP2 <<-0.03 ,-0.02;
    LP3 <<-0.03 , 0.03;

    RP0 << 0.07 , 0.02;
    RP1 << 0.07 ,-0.03;
    RP2 <<-0.03 ,-0.03;
    RP3 <<-0.03 , 0.02;



    RP0 = rotRight*RP0+offset;
    RP1 = rotRight*RP1+offset;
    RP2 = rotRight*RP2+offset;
    RP3 = rotRight*RP3+offset;

    cout << " Right Points"<< endl;

    LP0 = rotLeft*LP0;
    LP1 = rotLeft*LP1;
    LP2 = rotLeft*LP2;
    LP3 = rotLeft*LP3;

    cout << " Left Points "<< endl;

    A.push_back(LP0);
    A.push_back(LP1);
    A.push_back(LP2);
    A.push_back(LP3);

    A.push_back(LP0);

    B.push_back(RP0);
    B.push_back(RP1);
    B.push_back(RP2);
    B.push_back(RP3);

    B.push_back(RP0);

    C.push_back(LP0);
    C.push_back(LP1);
    C.push_back(RP0);
    C.push_back(RP1);

    C.push_back(RP2);
    C.push_back(RP3);
    C.push_back(LP2);
    C.push_back(LP3);
    C.push_back(LP0);

    std::vector<int> index = HULL2(C);
    for(int k = 0; k != index.size(); k++){
        C_hull.push_back(C[index[k]]);
    }
    C_hull.push_back(C[0]);
    for(int i = 0 ; i != C_hull.size(); i++){
        myfile_result << C_hull[i](0) << " " << C_hull[i](1) << endl;
    }

    //Output data for gnuplot
    for(int i = 0 ; i != A.size(); i++){
        myfile << A[i](0) << " " << A[i](1)<< " " << B[i](0) << " " << B[i](1) << endl;
    }
    hr::MatrixXr CMatrix(index.size(),2);
    hr::VectorXr dVector(index.size());

    for(int k = 0; k != index.size(); k++){
        CMatrix(k,0) = -(C_hull[k+1](1)-C_hull[k](1));
        CMatrix(k,1) = (C_hull[k+1](0)-C_hull[k](0));
        dVector(k) = (C_hull[k+1](0)-C_hull[k](0))*C_hull[k+1](1)-(C_hull[k+1](1)-C_hull[k](1))*C_hull[k+1](0);
    }
    cout << "CMatrix" << endl << CMatrix << endl;
    cout << "dVector" << endl << dVector << endl;

    //

    myfile_result.open("convex_hull_result.dat");
    myfile.close();


}

