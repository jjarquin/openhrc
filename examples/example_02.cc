//Example of the usage of the module hr:core

#include <iostream>
#include <vector>
#include <string>
#include "openhrc/core.h"

#ifdef  USE_LOCAL_XBOT //
    std::string naoFile = "../../../src/data/Nao_blue_noninertial.xbot";
#else
    std::string naoFile = "/home/nao/projects/hrData/Nao_blue_noninertial.xbot";
#endif

using std::cout;
using std::endl;


int main(){
    hr::core::World world = hr::core::World();
    std::string sFile = naoFile;
    int robotId = world.getRobotsVector()->size();
    world.loadMultiBody(sFile,robotId, hr::core::kNAO);
    boost::shared_ptr<hr::core::MultiBody> robot = world.getRobot(0);
    hr::VectorXr upSpeedLimit = robot->getJointUpSpeedLimits();
    hr::VectorXr lowSpeedLimit = robot->getJointLowSpeedLimits();
    hr::VectorXr lowLimit = robot->getJointLowLimits();
    hr::VectorXr upLimit = robot->getJointUpLimits();

    cout << "UpSpeedLimits: size " << upSpeedLimit.rows()<< "x1" << endl << upSpeedLimit << endl;
    cout << "LowSpeedLimits: size " << lowSpeedLimit.rows()<< "x1" << endl << lowSpeedLimit << endl;
    cout << "UpLimits: size " << upLimit.rows()<< "x1" << endl << upLimit << endl;
    cout << "LowLimits: size " << lowLimit.rows()<< "x1" << endl << lowLimit << endl;
    return 0;
}
