// example 37 Visual Driven Walking Pattern Generator

#include <iostream>
#include <vector>
#include <unistd.h>

#include "Eigen/Dense"

#include "openhrc/vision.h"
#include "openhrc/locomotion.h"

#include "qpOASES.hpp"

#include <boost/timer/timer.hpp>
#include <boost/shared_ptr.hpp>
#include "boost/date_time/posix_time/posix_time.hpp"

#include <alproxies/almotionproxy.h>
#include <alcommon/alproxy.h>
#include <alvalue/alvalue.h>



namespace pt = boost::posix_time;

using boost::timer::cpu_timer;
using std::cout;
using std::endl;

int main(){
    cpu_timer timer;
    /** MPC parameters */
    int m_steps = 2;
    int n_periods_step = 8;
    int N_horizon = m_steps*n_periods_step;
    hr::real_t T_sampling = 0.1;
    hr::real_t com_height = 0.3 ;

    /** VMPC parameters */
    int n_camera_dof = 6;
    int n_features = 8;
    hr::real_t Ts = 0.1;
    //hr::VectorXr uk(n_camera_dof*N_horizon);

    /** These parameters describe the camera */
    hr::Matrix3r kMatrix;
    hr::MatrixX6r interactionMatrix;
    hr::VectorXr currentPixelFeatures(n_features);
    hr::VectorXr desiredPixelFeatures(n_features);
    hr::VectorXr currentFeatures(n_features);
    hr::VectorXr desiredFeatures(n_features);
    hr::VectorXr desiredHorizonFeatures(n_features*N_horizon);

    hr::Matrix3r rotCamera;
    rotCamera << 0, 0, 1, -1, 0, 0, 0, -1, 0;

    hr::SpatialMatrix cameraTransformation;
    cameraTransformation << rotCamera, hr::Matrix3r::Zero(), hr::Matrix3r::Zero(), rotCamera;

    /** Initialization of the camera parameters */
    kMatrix << 531.712 , 0, 318.244, 0, 532.662, 250.698, 0,  0,  1;
    hr::vision::Camera camera(kMatrix);
    hr::SpatialMatrix matrixTransform;
    matrixTransform.setZero();
    matrixTransform.col(0) << 0, 0, 1, 0, 0, 0;
    matrixTransform.col(1) << -1, 0, 0, 0, 0, 0;

    /** Nao proxies */
    //AL::ALMotionProxy motionProxy = AL::ALMotionProxy("nao.local");
    AL::ALMotionProxy motionProxy = AL::ALMotionProxy("127.0.0.1");

    AL::ALValue configuration;
    configuration.arraySetSize(1);
    configuration[0].arraySetSize(2);
    configuration[0][0] =  AL::ALValue("ENABLE_FOOT_CONTACT_PROTECTION");
    configuration[0][1] =   AL::ALValue(false);
    motionProxy.setMotionConfig(configuration);
    motionProxy.walkInit();

    sleep(1);//wait 1 second for the features to stabilize

    //! VisionModule
    AL::ALProxy visionProxy = AL::ALProxy("VisionModuleTest","nao.local",9559);

    //! Extract initial and target features
    std::vector<float> current = visionProxy.call<std::vector<float> >("getCurrentFeatures");
    std::vector<float> target = visionProxy.call<std::vector<float> >("getTargetFeatures");

    //! Convert pixel features to image features
    Eigen::VectorXf currentPixelFeats(n_features);
    Eigen::VectorXf targetPixelFeats(n_features);
    currentPixelFeats = Eigen::VectorXf::Map(current.data(),n_features);
    targetPixelFeats = Eigen::VectorXf::Map(target.data(),n_features);
    cout << "Desired features (m) " << targetPixelFeats.transpose() <<endl;
    cout << "Initial features (m) " << currentPixelFeats.transpose() <<endl;
    currentPixelFeatures = currentPixelFeats.cast<hr::real_t>();
    desiredPixelFeatures = targetPixelFeats.cast<hr::real_t>();


    /** These parameters describe the state of the robot */

    hr::Vector3r footState;    //Member for the position of the current foot on the ground
    hr::Vector3r nextFootState;    //Member for the position of the current foot on the ground
    hr::locomotion::RobotState robotState;        //Member for the augmented CoM robot state
    hr::Vector3r xk;              //Variable for the CoM robot state in X
    hr::Vector3r yk;              //Variable for the CoM robot state in Y
    hr::Vector3r thetak;          //Variable for the CoM robot state in Theta
    hr::VectorXr x_vel(N_horizon);        //Reference velocity in X direction     //TODO: Time list trajectories similar to ALDCM
    hr::VectorXr y_vel(N_horizon);        //Reference velocity in Y direction     //TODO
    hr::core::FootStatus footStatus = hr::core::kRightFootOnGround;
    hr::Vector3r jerkXYTheta; //Jerk holder variable
    hr::VectorXr uk;      ///Jerk vector
    uk.resize(2*(N_horizon+m_steps));
    uk.setZero();
    /** Initialization of the robot parameters */

    cout << "Init robot paramters" << endl;
    footState << 0.0,-0.05,0.0;
    xk << 0.0 , 0.0, 0.0;
    yk << -0.05 , 0.0, 0.0;
    thetak << 0.0, 0.0 ,0.0;
    robotState.setState(xk,yk,thetak);
    jerkXYTheta << 0.0,0.,0.;
    x_vel.setOnes();
    y_vel.setOnes();
    x_vel = x_vel*0.00;
    y_vel = y_vel*1.00;



    //! Extract points

    camera.convertPoints(currentPixelFeatures,currentFeatures,hr::vision::kPixel2ImageCoordinates,true,hr::vision::k2DPoint);
    camera.convertPoints(desiredPixelFeatures,desiredFeatures,hr::vision::kPixel2ImageCoordinates,true,hr::vision::k2DPoint);

    for(int i = 0 ; i!= N_horizon ; i++){
        desiredHorizonFeatures.segment(i*n_features,n_features) = desiredFeatures;
    }
    interactionMatrix = camera.getInteractionMatrix(desiredFeatures, hr::vision::kImageCoordinates, hr::vision::k2DPoint,1.0);


    /** Initialization of the MPC */
    cout << "Create mpcOject" << endl;

    hr::vision::VWMPCData mpcData = hr::vision::VWMPCData(com_height,T_sampling,m_steps,n_periods_step);
    hr::vision::VWMPCConstraints constraints;
    hr::locomotion::RobotSystem system;

    cout << "Init mpc" << endl;

    mpcData.init(interactionMatrix,cameraTransformation);
    constraints.init(mpcData);
    system.init(com_height,T_sampling);

    constraints.setConstraints(mpcData,footStatus, footState, robotState);



    int nWSR = 1000;
    qpOASES::real_t cpuTime = 1.0;

    int nV = 2*(N_horizon+m_steps);
    int nC = constraints.getbupVector().rows();
    qpOASES::SQProblem quad;
    boost::shared_ptr<qpOASES::SQProblem> quadprog;
    qpOASES::Options options;
    options.setToMPC();
    options.printLevel = qpOASES::PL_NONE;
    quadprog.reset(new qpOASES::SQProblem(nV,nC));
    quadprog->setOptions(options);
    timer.start();
    quadprog->init(mpcData.getHessian().data(), mpcData.getFvector(robotState,footState,currentFeatures,desiredHorizonFeatures).data(),constraints.getAMatrix().data(),NULL,NULL,NULL,constraints.getbupVector().data(),nWSR,&cpuTime);
    if(quadprog->getPrimalSolution(uk.data()) == qpOASES::RET_QP_NOT_SOLVED){
        return hr::FAILED_INIT;
        cout << "failed to solve" << endl;
    }
    timer.stop();
    cout << "qpoases solved, time:" << endl;
    cout << timer.format() << endl;

    nWSR = 1000;
    cpuTime = 0.003;
    timer.start();
    quadprog->hotstart(mpcData.getHessian().data(), mpcData.getFvector(robotState,footState,currentFeatures,desiredHorizonFeatures).data(),constraints.getAMatrix().data(),NULL,NULL,NULL,constraints.getbupVector().data(),nWSR,&cpuTime);
    if(quadprog->getPrimalSolution(uk.data()) == qpOASES::RET_QP_NOT_SOLVED){
        return hr::FAILED_INIT;
        cout << "failed to solve" << endl;
    }
    timer.stop();
    cout << "hot qpoases solved, warm solving time:" << endl;
    cout << timer.format() << endl;
    cout << "cputime in ms" << endl << cpuTime*1000 << endl;

        jerkXYTheta(0) = uk(0);
        jerkXYTheta(1) = uk(N_horizon+m_steps);
        nextFootState(0) = uk(N_horizon);
        nextFootState(1) = uk(N_horizon+m_steps+N_horizon);


    cout << "Uk size:" << endl << uk.rows() << endl;
    cout << "Uk:" << endl << uk << endl;

    //

    cout << "Everything seems to be working fine..." << endl;
    cout << "Before Robot State" << endl << robotState << endl;
    system.nextState(jerkXYTheta,robotState);
    cout <<  "Applied Jerk" << endl << jerkXYTheta << endl;
    cout << "New Robot State" << endl << robotState << endl;
    cout << "Current foot:" << endl << footState << endl;
    cout << "Next foot:" << endl << nextFootState << endl;


}
