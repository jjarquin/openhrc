#include "openhrc/utils.h"
#include <vector>
#include <iostream>
#include "unsupported/Eigen/Splines"

int main(){
  Eigen::Spline3f spline;

 hr::VectorXr Ur(11);
 std::vector<hr::real_t> B; // Basis functions
 std::vector<hr::real_t> U; // knot vector
 hr::real_t u = 1.5;
 int i = 4; // index of u
 int p = 3; // polynomial degree

 Ur << 0,0,0,0,1,2,4,7,7,7,7;
 U.resize(11);
 U.assign(Ur.data(),Ur.data()+11);

 for(int i = 0; i != U.size() ; i ++){
     std::cout << U[i] << std::endl;
 }
 hr::utils::BasisFn(i,u,p,U,B);

 for(int i = 0; i != B.size() ; i ++){
     std::cout << B[i] << std::endl;
 }
 return 0;
}
