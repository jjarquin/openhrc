//Example 24 CoM and comJacobian task with SimpleTask

#include "openhrc/utils.h"
#include "openhrc/core.h"
#include "openhrc/kinematics.h"
#include "openhrc/locomotion.h"
#include "qpOASES.hpp"
#include <alproxies/almotionproxy.h>
#include <alerror/alerror.h>
#include <iostream>
#include <boost/timer/timer.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <fstream>

#ifdef  USE_LOCAL_XBOT //
std::string naoFile = "../../../src/data/Nao_blue_noninertial.xbot";
#else
std::string naoFile = "/home/nao/projects/hrData/Nao_blue_noninertial.xbot";
#endif

using boost::timer::cpu_timer;
using std::cout;
using std::endl;

hr::VectorXr getNaoConfig(AL::ALMotionProxy & motionProxy);
bool setNaoConfig(AL::ALMotionProxy & motionProxy, const hr::VectorXr & q, hr::real_t time );

void hrToAl(const hr::VectorXr & q_hr , hr::VectorXr & q_al){
    const int kNAO_DOF = 26;
    int jointId[kNAO_DOF] = {7,8,9,10,11,12,13,-1,19,20,21,22,23,24,25,26,27,28,29,30,14,15,16,17,18,-1};
    q_al.setZero();
    for (int i = 0 ; i < kNAO_DOF; i++) {
        if(jointId[i] != -1) {
            q_al(i) = q_hr(jointId[i]-1);
        }
    }

}

void alToHr(hr::VectorXr & q_hr , const hr::VectorXr & q_al){
    const int kNAO_DOF = 26;
    int jointId[kNAO_DOF] = {7,8,9,10,11,12,13,-1,19,20,21,22,23,24,-1,26,27,28,29,30,14,15,16,17,18,-1};
    q_hr.setZero();
    for (int i = 0 ; i < kNAO_DOF; i++) {
        if(jointId[i] != -1) {
            q_hr(jointId[i]-1) = q_al(i);
        }
    }
    q_hr(24) = q_hr(18);

}


int main(){
    cpu_timer timer;

    std::ofstream myfile;
    myfile.open ("example_24.dat");

    hr::core::World world = hr::core::World();
    std::string sFile = naoFile;
    int robotId = world.getRobotsVector()->size();
    world.loadMultiBody(sFile,robotId, hr::core::kNAO);
    boost::shared_ptr<hr::core::MultiBody> robot = world.getRobot(0);

    hr::VectorXr q = robot->getConfiguration();
    hr::VectorXr qtmp(q.rows()-1);
    int n_joints = q.rows();

    AL::ALMotionProxy motionProxy = AL::ALMotionProxy("127.0.0.1");
    //AL::ALMotionProxy motionProxy = AL::ALMotionProxy("nao.local");

    hr::VectorXr StandInit(n_joints);
    hr::VectorXr StandZero(n_joints);

    StandInit << 0 ,0, 0, 0, 0, 0, 0, 0, 1.39969, 0.300946, -1.39107, -1.01314, 0, 1.39969, -0.300946, 1.39107, 1.01314, 0, 0, 0, -0.45, 0.7, -0.35, 0, 0, 0, -0.45, 0.7, -0.35, 0;
    StandZero <<  0, 0, 0, 0, 0, 0, 0, 0, 0,  0.0087, 0,  0, 0, 0, -0.0087, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0;

    q = getNaoConfig(motionProxy);
    robot->setConfiguration(q);
    robot->computeForwardKinematics();

    cout << "Robot configuration" << endl << q.transpose() << endl;

    hr::core::JointTrajectory jointTraj( 0, 3.0 ,q, StandInit);


    robot->setConfiguration(q);
    robot->computeForwardKinematics();

    hr::Vector3r rightFootHandle(0.0,0.0,-0.04);
    int rightFootBodyId = hr::core::nao::kRightFoot;
    int rightFootHandleId = robot->addOperationalHandle(rightFootBodyId,rightFootHandle);

    hr::Vector3r leftFootHandle(0.0,0.0,-0.04);
    int leftFootBodyId = hr::core::nao::kLeftFoot;
    int leftFootHandleId = robot->addOperationalHandle(leftFootBodyId,leftFootHandle);


    hr::Vector3r rightHandHandle(0.08,0.0,0.0);
    int rightHandBodyId = hr::core::nao::kRightHand;
    int rightHandHandleId = robot->addOperationalHandle(rightHandBodyId,rightHandHandle);

    hr::Vector3r leftHandHandle(0.08,0.0,0.0);
    int leftHandBodyId = hr::core::nao::kLeftHand;
    int leftHandHandleId = robot->addOperationalHandle(leftHandBodyId,leftHandHandle);

    hr::Vector3r torsoHandle(0.0,0.0,0.0);
    int torsoBodyId = hr::core::nao::kTorso;
    int torsoHandleId = robot->addOperationalHandle(torsoBodyId,torsoHandle);

    hr::Vector3r headHandle(0.0,0.0,0.0);
    int headBodyId = hr::core::nao::kHead;
    int headHandleId = robot->addOperationalHandle(headBodyId,headHandle);

    hr::SpatialVector rightFootPose;
    hr::SpatialVector leftFootPose;

    hr::SpatialVector rightHandPose;
    hr::SpatialVector leftHandPose;

    hr::SpatialVector torsoPose;
    hr::SpatialVector headPose;

    hr::SpatialVector rightFootGoal;
    hr::SpatialVector leftFootGoal;

    hr::SpatialVector rightHandGoal;
    hr::SpatialVector leftHandGoal;

    hr::SpatialVector torsoGoal;
    hr::SpatialVector headGoal;

    hr::Vector3r rightFootPosition = robot->getOperationalHandlePosition(rightFootBodyId,rightFootHandleId);
    hr::Vector3r rightFootOrientation = robot->getBodyOrientation(rightFootBodyId).eulerAngles(0,1,2);

    hr::Vector3r leftFootPosition = robot->getOperationalHandlePosition(leftFootBodyId,leftFootHandleId);
    hr::Vector3r leftFootOrientation = robot->getBodyOrientation(leftFootBodyId).eulerAngles(0,1,2);

    hr::Vector3r rightHandPosition = robot->getOperationalHandlePosition(rightHandBodyId,rightHandHandleId);
    hr::Vector3r rightHandOrientation = robot->getBodyOrientation(rightHandBodyId).eulerAngles(0,1,2);

    hr::Vector3r leftHandPosition = robot->getOperationalHandlePosition(leftHandBodyId,leftHandHandleId);
    hr::Vector3r leftHandOrientation = robot->getBodyOrientation(leftHandBodyId).eulerAngles(0,1,2);

    hr::Vector3r torsoPosition = robot->getOperationalHandlePosition(torsoBodyId,torsoHandleId);
    hr::Vector3r torsoOrientation = robot->getBodyOrientation(torsoBodyId).eulerAngles(0,1,2);

    hr::Vector3r headPosition = robot->getOperationalHandlePosition(headBodyId,headHandleId);
    hr::Vector3r headOrientation = robot->getBodyOrientation(headBodyId).eulerAngles(0,1,2);


    rightFootPose << rightFootPosition , rightFootOrientation;
    leftFootPose << leftFootPosition, leftFootOrientation;
    torsoPose << torsoPosition, torsoOrientation;
    rightHandPose << rightHandPosition , rightHandOrientation;
    leftHandPose << leftHandPosition , leftHandOrientation;
    headPose << headPosition, headOrientation;

    rightFootGoal << rightFootPosition , 0.0 , 0.0, 0.0;
    leftFootGoal << leftFootPosition, 0.0 , 0.0 , 0.0;

    torsoGoal << torsoPosition, torsoOrientation;
    rightHandGoal << rightHandPosition , rightHandOrientation;
    leftHandGoal << leftHandPosition , leftHandOrientation;
    headGoal << headPosition, headOrientation;



    hr::core::KinematicTask task_01(rightFootHandleId,rightFootBodyId,0,robot, hr::core::kPositionOrientation);
    task_01.setGoal(rightFootGoal);
    task_01.setGain(1.0);
    hr::core::KinematicTask task_02(leftFootHandleId,leftFootBodyId,0,robot, hr::core::kPositionOrientation);
    task_02.setGoal(leftFootGoal);
    task_02.setGain(1.0);
    hr::core::KinematicTask task_03(rightHandHandleId,rightHandBodyId,0,robot, hr::core::kPositionOrientation);
    task_03.setGoal(rightHandGoal);
    //task_03.setGain(1.0);
    hr::core::KinematicTask task_04(leftHandHandleId,leftHandBodyId,0,robot, hr::core::kPositionOrientation);
    task_04.setGoal(leftHandGoal);
    //task_04.setGain(1.0);
    hr::core::KinematicTask task_05(torsoHandleId,torsoBodyId,0,robot, (  hr::core::kWXZ ));
    task_05.setGoal(torsoGoal);
    //task_05.setGain(1.0);
//    hr::core::KinematicTask torsoTask(torsoHandleId,torsoBodyId,0,robot, hr::core::kOrientation);
//    hr::SpatialVector torsoOrientationGoal;
//    torsoOrientationGoal.setZero();
//    torsoTask.setGoal(torsoOrientationGoal);

    hr::core::KinematicTask task_06(headHandleId,headBodyId,0,robot, hr::core::kWXYZ);
    task_06.setGoal(headGoal);
    //task_06.setGain(1.0);



    hr::real_t integrationStepSize = 0.01;

    hr::real_t t = 0.0;
    hr::real_t T = 5.0;
    int N = T/integrationStepSize;


    hr::utils::SimpleHierarchicalSolver solver;


    hr::VectorXr lb_ = (robot->getJointLowLimits()-q)/integrationStepSize;
    hr::VectorXr ub_ = (robot->getJointUpLimits()-q)/integrationStepSize;

    hr::VectorXr lb(29);
    hr::VectorXr ub(29);

    lb.segment(0,24) = lb_.segment(0,24);
    lb.segment(24,5) = lb_.segment(25,5);

    ub.segment(0,24) = ub_.segment(0,24);
    ub.segment(24,5) = ub_.segment(25,5);

    hr::VectorXr ld(29);
    hr::VectorXr ud(29);
    hr::MatrixXr C;

    hr::VectorXr ud_ = robot->getJointUpSpeedLimits();
    hr::VectorXr ld_ = robot->getJointLowSpeedLimits();

    int n = 29;
    C = hr::MatrixXr::Identity(n,n);

    ld.segment(0,24) = ld_.segment(0,24);
    ld.segment(24,5) = ld_.segment(25,5);

    ud.segment(0,24) = ud_.segment(0,24);
    ud.segment(24,5) = ud_.segment(25,5);

    q = getNaoConfig(motionProxy);
    hr::VectorXr q_tmp = q;
    robot->setConfiguration(q);
    robot->computeForwardKinematics();

    hr::Vector3r robot_com;
    hr::Vector3r comGoal;
    hr::Vector3r comError;
    hr::Matrix3Xr comJacobian;
    comGoal << 0.022,-0.05,0.0;
    for(int i = 0; i != N; i++){

        q_tmp = getNaoConfig(motionProxy);
        q_tmp.segment(0,6) = q.segment(0,6);

        robot->setConfiguration(q_tmp);
        robot->computeForwardKinematics();

        //Calculate the new joint speed limit
        lb_ = (robot->getJointLowLimits()-q)/integrationStepSize;
        ub_ = (robot->getJointUpLimits()-q)/integrationStepSize;

        lb.segment(0,24) = lb_.segment(0,24);
        lb.segment(24,5) = lb_.segment(25,5);

        ub.segment(0,24) = ub_.segment(0,24);
        ub.segment(24,5) = ub_.segment(25,5);


        task_01.update(t);
        task_02.update(t);
        task_03.update(t);
        task_04.update(t);
        task_05.update(t);
        task_06.update(t);


        std::string pName = "Body";
        int pSpace = 0; // FRAME_TORSO
        bool pUseSensors = true;
        std::vector<float> pos = motionProxy.getCOM(pName, pSpace, pUseSensors);
        Eigen::Map<Eigen::VectorXf> com(pos.data(),3);
        cout << "CoM" << endl << com.transpose() << endl;

        cout << "TorsoPosition " << torsoPosition.transpose() << endl;
        cout << "TorsoOrientation " << torsoOrientation.transpose() << endl;

        robot_com = robot->getCoM();
        comJacobian = robot->getCoMJacobian();
        cout << "robot_com " << robot_com.transpose() << endl;

        //! Log data ---------------------------------------------------------------
        torsoPosition = robot->getOperationalHandlePosition(torsoBodyId,torsoHandleId);
        myfile << t << " " << torsoPosition(0)  << " " << torsoPosition(1) << " " << com(0)+torsoPosition(0)<< " " << com(1)+torsoPosition(1) << std::endl;
        //! end of Log data --------------------------------------------------------


        std::vector<hr::utils::TaskAbstract*> taskList;
        hr::utils::SimpleTask feetTask;
        hr::utils::SimpleTask handsTask;
        hr::VectorXr zeros(6);
        zeros.setZero();

        hr::MatrixXr righHandJacobian = task_03.getMatrixA();
        hr::MatrixXr leftHandJacobian = task_04.getMatrixA();
        righHandJacobian.leftCols(6).setZero();
        leftHandJacobian.leftCols(6).setZero();

        handsTask.addRows(righHandJacobian,zeros);
        handsTask.addRows(leftHandJacobian,zeros);

        hr::utils::SimpleTask comTask;
        comError = comGoal - robot_com;
        hr::Vector2r comErrorXY = comError.head(2);
        hr::Matrix2Xr comJacobianXY = comJacobian.topRows(2);
        comTask.addRows(comJacobianXY,comErrorXY);



        feetTask.addRows(task_01.getMatrixA(),task_01.getVectorb());
        feetTask.addRows(task_02.getMatrixA(),task_02.getVectorb());

        taskList.push_back(&feetTask);
        taskList.push_back(&handsTask);
        taskList.push_back(&comTask);
        taskList.push_back(&task_05);



        hr::VectorXr dq = solver.Solve(taskList,C,ld,ud,lb,ub);
        //hr::VectorXr dq(n);
        //dq.setZero();
        std::cout << "t " << t << std::endl;

        t = t + integrationStepSize;


        // Integrate
        qtmp.segment(0,24) = q.segment(0,24);
        qtmp.segment(24,5) = q.segment(25,5);
        qtmp += dq*integrationStepSize;
        q.segment(0,24) = qtmp.segment(0,24);
        q(24) = qtmp(18);
        q.segment(25,5) = qtmp.segment(24,5);
        // End of integration


        //Set the new configuration
        setNaoConfig(motionProxy,q, integrationStepSize);
    }

    myfile.close();
}


hr::VectorXr getNaoConfig(AL::ALMotionProxy & motionProxy){
    const int kNAO_DOF = 26;
    const bool useSensors = false;

    AL::ALValue jointNames;
    jointNames = AL::ALValue::array("Body");
    std::vector<float> sensorAngles = motionProxy.getAngles(jointNames, useSensors);
    Eigen::Map<Eigen::VectorXf> q_al(sensorAngles.data(),26);

    hr::VectorXr q_hr(30);
    alToHr(q_hr, q_al.cast<hr::real_t>());
    return q_hr;

}

bool setNaoConfig(AL::ALMotionProxy & motionProxy, const hr::VectorXr & q_hr, hr::real_t time ){
    const int kNAO_DOF = 26;
    const bool isAbsolute = true;

    AL::ALValue alAngles, timeLists;
    alAngles.clear();
    alAngles.arraySetSize(kNAO_DOF);

    timeLists.clear();
    timeLists.arraySetSize(kNAO_DOF);

    hr::VectorXr q_al(kNAO_DOF);

    hrToAl(q_hr,q_al);


    AL::ALValue jointNames;
    jointNames = AL::ALValue::array("Body");
    for (int i = 0; i!=kNAO_DOF; i++) {
        timeLists[i] = AL::ALValue::array((float) time);
        alAngles[i] = q_al(i);
    }
    motionProxy.setAngles(jointNames,alAngles,1.0);
    return true;

}




