//example 41 Kalman Filter for the CoM velocity.

#include "openhrc/core.h"
#include "openhrc/kinematics.h"
#include <iostream>

void kalmanFilter(hr::VectorXr & median, hr::MatrixXr & covariance, const hr::VectorXr & control, const hr::VectorXr & measure){

    hr::VectorXr nextState;

}

int main(){

    hr::MatrixXr cov(4,4);
    hr::MatrixXr A(4,4);
    hr::MatrixXr B(4,2);
    hr::MatrixXr C(2,4);
    hr::MatrixXr R(4,4);
    hr::MatrixXr Q(2,2);

    hr::real_t alpha  = 0.1;
    hr::real_t beta   = 0.1;
    hr::real_t gamma  = 3.0;
    hr::real_t lambda = 3.0;

    hr::real_t sensorDeviation = 2;

    hr::VectorXr state(4);
    hr::VectorXr control(2);
    hr::VectorXr nextState(4);
    hr::VectorXr nextPrediction(2);

    state << 0,0,0,0;
    control << 0,0;

    hr::real_t T = 0.01; //Sampling time

    cov << sqrt(alpha), 0 , alpha*gamma , 0,
            0, sqrt(beta) , 0  , beta * lambda,
            alpha*gamma, 0 , sqrt(gamma) , 0,
            0 , beta*lambda, 0 , sqrt(lambda);

    R(0,0) = sqrt(alpha);
    R(1,1) = sqrt(beta);
    R(2,2) = sqrt(gamma);
    R(3,3) = sqrt(lambda);

    Q(0,0) = sqrt(sensorDeviation);
    Q(1,1) = sqrt(sensorDeviation);

    A << 1, 0, T, 0,
         0, 1, 0, T,
         0, 0, 1, 0,
         0, 0, 0, 1;

    B << 0.5*T*T, 0,
         0, 0.5*T*T,
         T , 0,
         0 , T;

    C << 1, 0, 0, 0,
         0, 1, 0, 0;



    std::cout << "example 41" << std::endl;
    std::cout << "A" << std::endl << A << std::endl;
    std::cout << "B" << std::endl << B << std::endl;
    std::cout << "C" << std::endl << C << std::endl;

    std::cout << "Q" << std::endl << Q << std::endl;
    std::cout << "R" << std::endl << R << std::endl;

    std::cout << "cov" << std::endl << cov << std::endl;

    std::cout << "State" << std::endl << state << std::endl;
    std::cout << "Control" << std::endl << control << std::endl;

    control(0) = 1.0;

    nextState = A*state+B*control;
    nextPrediction = C*nextState;

    std::cout << "Next State" << std::endl << nextState << std::endl;
    std::cout << "Prediction" << std::endl << nextPrediction << std::endl;



    hr::VectorXr bState(4);
    hr::VectorXr rState(4);
    hr::VectorXr wState(4);
    hr::VectorXr error(2);

    hr::VectorXr filteredState(4);
    hr::VectorXr measurement(2);
    hr::MatrixXr bCov(4,4);
    hr::MatrixXr kalmanGain(4,2);
    hr::MatrixXr eye(4,4);
    eye = hr::MatrixXr::Identity(4,4);

    int N = 200;

    rState = state;
    wState = state;

    for(int k = 0 ; k <= N ; k++){

        // state / covariance / control / measurement
        error = 0.05*hr::MatrixXr::Random(4,1);
        bState = A*state+B*control + error;
        wState = A*wState+B*control + error;
        rState = A*rState+B*control;

        measurement = C*rState + 0.05*hr::MatrixXr::Random(2,1);

        bCov = A*cov*A.transpose() + R;
        hr::MatrixXr inner = C*bCov*C.transpose() + Q;
        kalmanGain = bCov*C.transpose()*inner.inverse();
        filteredState = bState + kalmanGain*(measurement - C*bState);
        cov =(eye - kalmanGain*C)*bCov;

        std::cout << "Iteration" << std::endl << k << std::endl;
        std::cout << "Expected State" << std::endl << C*bState << std::endl;
        std::cout << "Measured State" << std::endl << measurement << std::endl;
        std::cout << "Filtered State" << std::endl << C*filteredState << std::endl;
        std::cout << "Real State" << std::endl << C*rState << std::endl;
        std::cout << "Non-filtered State" << std::endl << C*wState << std::endl;




        state = filteredState;
    }




    return 0;
}


