//Example 27 CoM inertial unir

#include "openhrc/utils.h"
#include "openhrc/core.h"
#include "openhrc/kinematics.h"
#include "openhrc/locomotion.h"
#include "qpOASES.hpp"

#include <alproxies/almotionproxy.h>
#include <alproxies/almemoryproxy.h>
#include <alerror/alerror.h>

#include <iostream>
#include <boost/timer/timer.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <fstream>

#ifdef  USE_LOCAL_XBOT //
std::string naoFile = "../../../src/data/Nao_blue_noninertial.xbot";
#else
std::string naoFile = "/home/nao/projects/hrData/Nao_blue_noninertial.xbot";
#endif

using boost::timer::cpu_timer;
using std::cout;
using std::endl;

hr::VectorXr getNaoConfig(AL::ALMotionProxy & motionProxy);
bool setNaoConfig(AL::ALMotionProxy & motionProxy, const hr::VectorXr & q, hr::real_t time );

void hrToAl(const hr::VectorXr & q_hr , hr::VectorXr & q_al){
    const int kNAO_DOF = 26;
    int jointId[kNAO_DOF] = {7,8,9,10,11,12,13,-1,19,20,21,22,23,24,25,26,27,28,29,30,14,15,16,17,18,-1};
    q_al.setZero();
    for (int i = 0 ; i < kNAO_DOF; i++) {
        if(jointId[i] != -1) {
            q_al(i) = q_hr(jointId[i]-1);
        }
    }

}

void alToHr(hr::VectorXr & q_hr , const hr::VectorXr & q_al){
    const int kNAO_DOF = 26;
    int jointId[kNAO_DOF] = {7,8,9,10,11,12,13,-1,19,20,21,22,23,24,-1,26,27,28,29,30,14,15,16,17,18,-1};
    q_hr.setZero();
    for (int i = 0 ; i < kNAO_DOF; i++) {
        if(jointId[i] != -1) {
            q_hr(jointId[i]-1) = q_al(i);
        }
    }
    q_hr(24) = q_hr(18);

}


int main(){
    cpu_timer timer;

    std::ofstream myfile;
    myfile.open ("example_27.dat");

    hr::core::World world = hr::core::World();
    std::string sFile = naoFile;
    int robotId = world.getRobotsVector()->size();
    world.loadMultiBody(sFile,robotId, hr::core::kNAO);
    boost::shared_ptr<hr::core::MultiBody> robot = world.getRobot(0);

    hr::VectorXr q = robot->getConfiguration();
    hr::VectorXr qtmp(q.rows()-1);
    int n_joints = q.rows();

    std::string robot_ip ="127.0.0.1";
    //std::string robot_ip ="nao.local";

    AL::ALMotionProxy motionProxy = AL::ALMotionProxy(robot_ip);

    AL::ALMemoryProxy memoryProxy = AL::ALMemoryProxy(robot_ip);

    std::string keyAngleX = "Device/SubDeviceList/InertialSensor/AngleX/Sensor/Value";
    std::string keyAngleY ="Device/SubDeviceList/InertialSensor/AngleY/Sensor/Value";
    float angleX;
    float angleY;

    q = getNaoConfig(motionProxy);
    angleX = (float) memoryProxy.getData(keyAngleX);
    angleY = (float) memoryProxy.getData(keyAngleY);
    q(3) = (double) angleX;
    q(4) = (double) angleX;


    robot->setConfiguration(q);
    robot->computeForwardKinematics();


    cout << "Robot configuration" << endl << q.transpose() << endl;
    cout << "Robot angles (X,Y): " << angleX << ", " << angleY << endl;

    robot->setConfiguration(q);
    robot->computeForwardKinematics();

    hr::Vector3r rightFootHandle(0.0,0.0,-0.04);
    int rightFootBodyId = hr::core::nao::kRightFoot;
    int rightFootHandleId = robot->addOperationalHandle(rightFootBodyId,rightFootHandle);

    hr::Vector3r leftFootHandle(0.0,0.0,-0.04);
    int leftFootBodyId = hr::core::nao::kLeftFoot;
    int leftFootHandleId = robot->addOperationalHandle(leftFootBodyId,leftFootHandle);

    hr::Vector3r rightHandHandle(0.08,0.0,0.0);
    int rightHandBodyId = hr::core::nao::kRightHand;
    int rightHandHandleId = robot->addOperationalHandle(rightHandBodyId,rightHandHandle);

    hr::Vector3r leftHandHandle(0.08,0.0,0.0);
    int leftHandBodyId = hr::core::nao::kLeftHand;
    int leftHandHandleId = robot->addOperationalHandle(leftHandBodyId,leftHandHandle);

    hr::Vector3r torsoHandle(0.0,0.0,0.0);
    int torsoBodyId = hr::core::nao::kTorso;
    int torsoHandleId = robot->addOperationalHandle(torsoBodyId,torsoHandle);

    hr::Vector3r headHandle(0.01,0.0,0.01);
    int headBodyId = hr::core::nao::kHead;
    int headHandleId = robot->addOperationalHandle(headBodyId,headHandle);

    hr::SpatialVector comPose;
    hr::SpatialVector comGoal;

    hr::SpatialVector rightFootPose;
    hr::SpatialVector leftFootPose;

    hr::SpatialVector rightHandPose;
    hr::SpatialVector leftHandPose;

    hr::SpatialVector torsoPose;
    hr::SpatialVector headPose;

    hr::SpatialVector rightFootGoal;
    hr::SpatialVector leftFootGoal;

    hr::SpatialVector rightHandGoal;
    hr::SpatialVector leftHandGoal;

    hr::SpatialVector torsoGoal;
    hr::SpatialVector headGoal;

    hr::Vector3r rightFootPosition = robot->getOperationalHandlePosition(rightFootBodyId,rightFootHandleId);
    hr::Vector3r rightFootOrientation = robot->getBodyOrientation(rightFootBodyId).eulerAngles(0,1,2);

    hr::Vector3r leftFootPosition = robot->getOperationalHandlePosition(leftFootBodyId,leftFootHandleId);
    hr::Vector3r leftFootOrientation = robot->getBodyOrientation(leftFootBodyId).eulerAngles(0,1,2);

    hr::Vector3r rightHandPosition = robot->getOperationalHandlePosition(rightHandBodyId,rightHandHandleId);
    hr::Vector3r rightHandOrientation = robot->getBodyOrientation(rightHandBodyId).eulerAngles(0,1,2);

    hr::Vector3r leftHandPosition = robot->getOperationalHandlePosition(leftHandBodyId,leftHandHandleId);
    hr::Vector3r leftHandOrientation = robot->getBodyOrientation(leftHandBodyId).eulerAngles(0,1,2);

    hr::Vector3r torsoPosition = robot->getOperationalHandlePosition(torsoBodyId,torsoHandleId);
    hr::Vector3r torsoOrientation = robot->getBodyOrientation(torsoBodyId).eulerAngles(0,1,2);

    hr::Vector3r headPosition = robot->getOperationalHandlePosition(headBodyId,headHandleId);
    hr::Vector3r headOrientation = robot->getBodyOrientation(headBodyId).eulerAngles(0,1,2);

    hr::Vector3r comPosition = robot->getCoM();

    rightFootPose << rightFootPosition , rightFootOrientation;
    leftFootPose << leftFootPosition, leftFootOrientation;
    torsoPose << torsoPosition, torsoOrientation;
    rightHandPose << rightHandPosition , rightHandOrientation;
    leftHandPose << leftHandPosition , leftHandOrientation;
    headPose << headPosition, headOrientation;

    rightFootGoal << rightFootPosition , rightFootOrientation;
    leftFootGoal << leftFootPosition, rightFootOrientation;

    torsoGoal << torsoPosition, 0,0,0;
    rightHandGoal << rightHandPosition , rightHandOrientation;
    leftHandGoal << leftHandPosition , leftHandOrientation;
    headGoal << headPosition, headOrientation;

    comPose << comPosition , 0.0,0.0,0.0;
    comGoal << comPosition(0), comPosition(1), comPosition(2), 0.0,0.0,0.0;

    cout << "Left hand position: " << endl << leftHandPosition << endl;
    cout << "Right hand position: " << endl << rightHandPosition << endl;
    cout << "Head position: " << endl << headPosition << endl;
    cout << "Torso position: " << endl << torsoPosition << endl;
    cout << "Left foot position: " << endl << leftFootPosition << endl;
    cout << "Right foot position: " << endl << rightFootPosition << endl;

    cout << "Left hand orientation: " << endl << leftHandOrientation << endl;
    cout << "Right hand orientation: " << endl << rightHandOrientation << endl;
    cout << "Head orientation: " << endl << headOrientation << endl;
    cout << "Torso orientation: " << endl << torsoOrientation << endl;
    cout << "Left foot orientation: " << endl << leftFootOrientation << endl;
    cout << "Right foot orientation: " << endl << rightFootOrientation << endl;



    hr::core::KinematicTask rightFootTask(rightFootHandleId,rightFootBodyId,0,robot, (hr::core::kXY | hr::core::kWZ));
    rightFootTask.setGoal(rightFootGoal);
    rightFootTask.setGain(1.0);
    hr::core::KinematicTask leftFootTask(leftFootHandleId,leftFootBodyId,0,robot, (hr::core::kXY | hr::core::kWZ));
    leftFootTask.setGoal(leftFootGoal);
    leftFootTask.setGain(1.0);
    hr::core::KinematicTask rightHandTask(rightHandHandleId,rightHandBodyId,0,robot, hr::core::kPositionOrientation);
    rightHandTask.setGoal(rightHandGoal);
    rightHandTask.setGain(1.0);
    hr::core::KinematicTask leftHandTask(leftHandHandleId,leftHandBodyId,0,robot, hr::core::kPositionOrientation);
    leftHandTask.setGoal(leftHandGoal);
    leftHandTask.setGain(1.0);
    hr::core::KinematicTask torsoTask(torsoHandleId,torsoBodyId,0,robot, (  hr::core::kZ | hr::core::kWXYZ ));
    torsoTask.setGoal(torsoGoal);
    torsoTask.setGain(1.0);
    hr::core::KinematicTask headTask(headHandleId,headBodyId,0,robot,hr::core::kWXYZ);
    headTask.setGoal(headPose);
    headTask.setGain(1.0);
    hr::core::CoMTask comTask(0,robot, hr::core::kXY);
    comTask.setGoal(comGoal);
    comTask.setGain(1.0);

    //! Trajectories
    boost::shared_ptr<hr::core::PointTrajectory> leftFootTraj = boost::make_shared<hr::core::PointTrajectory>(0.0,2.0,leftFootPose,leftFootGoal,hr::core::kQuinticPolynomial);
    //boost::shared_ptr<hr::core::FootTrajectory> leftFootTraj = boost::make_shared<hr::core::FootTrajectory>(3.0,3.8,leftFootGoal,leftFootGoal,hr::core::kFootCycloidal);
    leftFootTask.setTrajectory(leftFootTraj);

    boost::shared_ptr<hr::core::PointTrajectory> rightFootTraj = boost::make_shared<hr::core::PointTrajectory>(0.0,2.0,rightFootPose,rightFootGoal,hr::core::kQuinticPolynomial);
    rightFootTask.setTrajectory(rightFootTraj);

    boost::shared_ptr<hr::core::PointTrajectory> torsoTraj = boost::make_shared<hr::core::PointTrajectory>(0.0,2.0,torsoPose,torsoGoal,hr::core::kQuinticPolynomial);
    torsoTask.setTrajectory(torsoTraj);

    boost::shared_ptr<hr::core::PointTrajectory> comTraj = boost::make_shared<hr::core::PointTrajectory>(0.0,3.0,comPose,comGoal,hr::core::kQuinticPolynomial);
    comTask.setTrajectory(comTraj);
    ///

    hr::real_t integrationStepSize = 0.01;

    hr::utils::SimpleHierarchicalSolver solver;


    hr::VectorXr lb_ = (robot->getJointLowLimits()-q)/integrationStepSize;
    hr::VectorXr ub_ = (robot->getJointUpLimits()-q)/integrationStepSize;

    hr::VectorXr lb(29);
    hr::VectorXr ub(29);

    lb.segment(0,24) = lb_.segment(0,24);
    lb.segment(24,5) = lb_.segment(25,5);

    ub.segment(0,24) = ub_.segment(0,24);
    ub.segment(24,5) = ub_.segment(25,5);

    hr::VectorXr ld(29);
    hr::VectorXr ud(29);
    hr::MatrixXr C;

    hr::VectorXr ud_ = robot->getJointUpSpeedLimits();
    hr::VectorXr ld_ = robot->getJointLowSpeedLimits();

    int n = 29;
    C = hr::MatrixXr::Identity(n,n);

    ld.segment(0,24) = ld_.segment(0,24);
    ld.segment(24,5) = ld_.segment(25,5);

    ud.segment(0,24) = ud_.segment(0,24);
    ud.segment(24,5) = ud_.segment(25,5);


    q = getNaoConfig(motionProxy);
    angleX = (float) memoryProxy.getData(keyAngleX);
    angleY = (float) memoryProxy.getData(keyAngleY);
    q(3) = (double) angleX;
    q(4) = (double) angleX;
    hr::VectorXr q_tmp = q;
    robot->setConfiguration(q);
    robot->computeForwardKinematics();

    hr::real_t t = 0.0;
    hr::real_t T = 5.0;
    int N = T/integrationStepSize;

    for(int i = 0; i != N; i++){

        q_tmp = getNaoConfig(motionProxy);
        q_tmp.segment(0,6) = q.segment(0,6);

        angleX = (float) memoryProxy.getData(keyAngleX);
        angleY = (float) memoryProxy.getData(keyAngleY);
        q_tmp(3) = (double) angleX;
        q_tmp(4) = (double) angleY;

        robot->setConfiguration(q_tmp);
        robot->computeForwardKinematics();

        //Calculate the new joint speed limit
        lb_ = (robot->getJointLowLimits()-q)/integrationStepSize;
        ub_ = (robot->getJointUpLimits()-q)/integrationStepSize;

        lb.segment(0,24) = lb_.segment(0,24);
        lb.segment(24,5) = lb_.segment(25,5);

        ub.segment(0,24) = ub_.segment(0,24);
        ub.segment(24,5) = ub_.segment(25,5);


        rightFootTask.update(t);
        leftFootTask.update(t);
        rightHandTask.update(t);
        leftHandTask.update(t);
        torsoTask.update(t);
        headTask.update(t);

        timer.start();
        comTask.update(t);
        timer.stop();
        cout << "CoMTask Computation time" << endl << timer.format() << endl;



        //! Log data ---------------------------------------------------------------
        torsoPosition = robot->getOperationalHandlePosition(torsoBodyId,torsoHandleId);
        std::string pName = "Body";
        int pSpace = 0; // FRAME_TORSO
        bool pUseSensors = true;
        std::vector<float> pos = motionProxy.getCOM(pName, pSpace, pUseSensors);
        Eigen::Map<Eigen::VectorXf> com(pos.data(),3);
        cout << "CoM" << endl << com.transpose() << endl;

        cout << "TorsoPosition " << torsoPosition.transpose() << endl;
        cout << "TorsoOrientation " << torsoOrientation.transpose() << endl;

        comPosition = robot->getCoM();
        cout << "robot_com " << comPosition.transpose() << endl;

        myfile << t << " " << torsoPosition(0)  << " " << torsoPosition(1) << " " << com(0)+torsoPosition(0)<< " " << com(1)+torsoPosition(1) << std::endl;
        //! end of Log data --------------------------------------------------------


        std::vector<hr::utils::TaskAbstract*> taskList;
        hr::utils::SimpleTask feetTask;
        hr::utils::SimpleTask handsTask;
        hr::VectorXr zeros(6);
        zeros.setZero();

        //Fix hands wrt to torso
        hr::MatrixXr righHandJacobian = rightHandTask.getMatrixA();
        hr::MatrixXr leftHandJacobian = leftHandTask.getMatrixA();
        righHandJacobian.leftCols(6).setZero();
        leftHandJacobian.leftCols(6).setZero();
        handsTask.addRows(righHandJacobian,zeros);
        handsTask.addRows(leftHandJacobian,zeros);
        //

        feetTask.addRows(leftFootTask.getMatrixA(),leftFootTask.getVectorb());
        feetTask.addRows(rightFootTask.getMatrixA(),rightFootTask.getVectorb());

        taskList.push_back(&torsoTask);
        taskList.push_back(&feetTask);

        //taskList.push_back(&headTask);
        //taskList.push_back(&handsTask);
        taskList.push_back(&comTask);

        hr::VectorXr dq = solver.Solve(taskList,C,ld,ud,lb,ub);
        //hr::VectorXr dq(n);
        //dq.setZero();
        std::cout << "t " << t << std::endl;

        t = t + integrationStepSize;


        // Integrate
        qtmp.segment(0,24) = q.segment(0,24);
        qtmp.segment(24,5) = q.segment(25,5);
        qtmp += dq*integrationStepSize;
        q.segment(0,24) = qtmp.segment(0,24);
        q(24) = qtmp(18);
        q.segment(25,5) = qtmp.segment(24,5);
        // End of integration

        angleX = (float) memoryProxy.getData(keyAngleX);
        angleY = (float) memoryProxy.getData(keyAngleY);

        cout << "Robot configuration" << endl << q.transpose() << endl;
        cout << "Robot angles (X,Y): " << angleX << ", " << angleY << endl;
        torsoPosition = robot->getOperationalHandlePosition(torsoBodyId,torsoHandleId);
        torsoOrientation = robot->getBodyOrientation(torsoBodyId).eulerAngles(0,1,2);
        cout << "Torso Position " << torsoPosition.transpose() << endl;
        cout << "Torso Orientation " << torsoOrientation.transpose() << endl;
        //Set the new configuration
        setNaoConfig(motionProxy,q, integrationStepSize);
    }

    myfile.close();
}


hr::VectorXr getNaoConfig(AL::ALMotionProxy & motionProxy){
    const int kNAO_DOF = 26;
    const bool useSensors = true;

    AL::ALValue jointNames;
    jointNames = AL::ALValue::array("Body");
    std::vector<float> sensorAngles = motionProxy.getAngles(jointNames, useSensors);
    Eigen::Map<Eigen::VectorXf> q_al(sensorAngles.data(),26);

    hr::VectorXr q_hr(30);
    alToHr(q_hr, q_al.cast<hr::real_t>());
    return q_hr;

}

bool setNaoConfig(AL::ALMotionProxy & motionProxy, const hr::VectorXr & q_hr, hr::real_t time ){
    const int kNAO_DOF = 26;
    const bool isAbsolute = true;

    AL::ALValue alAngles, timeLists;
    alAngles.clear();
    alAngles.arraySetSize(kNAO_DOF);

    timeLists.clear();
    timeLists.arraySetSize(kNAO_DOF);

    hr::VectorXr q_al(kNAO_DOF);

    hrToAl(q_hr,q_al);


    AL::ALValue jointNames;
    jointNames = AL::ALValue::array("Body");
    for (int i = 0; i!=kNAO_DOF; i++) {
        timeLists[i] = AL::ALValue::array((float) time);
        alAngles[i] = q_al(i);
    }
    motionProxy.setAngles(jointNames,alAngles,1.0);
    return true;

}





