//Example of the visual model predictive control

#include "openhrc/vision.h"
#include "qpOASES.hpp"
#include <boost/timer/timer.hpp>
#include <boost/shared_ptr.hpp>

using boost::timer::cpu_timer;
using std::cout;
using std::endl;

int main(){
    cpu_timer timer;
    /** VMPC parameters */
    int N_horizon = 3;
    int n_camera_dof = 6;
    int n_features = 8;
    hr::real_t Ts = 0.1;
    hr::VectorXr uk;      ///Control vector
    uk.resize(n_camera_dof*N_horizon);
    uk.setZero();

    /** These parameters describe the camera */
    hr::Matrix3r kMatrix;
    hr::MatrixX6r interactionMatrix;
    hr::VectorXr currentPixelFeatures(n_features);
    hr::VectorXr desiredPixelFeatures(n_features);
    hr::VectorXr currentFeatures(n_features);
    hr::VectorXr desiredFeatures(n_features);
    hr::VectorXr desiredHorizonFeatures(n_features*N_horizon);

    /** Initialization of the camera parameters */
    kMatrix << 531.712 , 0, 318.244, 0, 532.662, 250.698, 0,  0,  1;
    hr::vision::Camera camera(kMatrix);
    desiredPixelFeatures << 252 , 209, 353, 204, 359, 303, 255, 308;
    currentPixelFeatures << 417, 207, 522, 199, 529, 306, 419, 306;

    hr::VectorXr cameraPixelLimits(4);
    hr::VectorXr cameraPixelLimitsZone(4);

    hr::VectorXr cameraLimits(4);
    hr::VectorXr cameraLimitsZone(4);

    cameraPixelLimits << 640 , 480, 0, 0;
    cameraPixelLimitsZone << 600, 440, 40,40;
    camera.convertPoints(cameraPixelLimits,cameraLimits,hr::vision::kPixel2ImageCoordinates,true,hr::vision::k2DPoint);
    camera.convertPoints(cameraPixelLimitsZone,cameraLimitsZone,hr::vision::kPixel2ImageCoordinates,true,hr::vision::k2DPoint);

    std::cout << "Camera Limits" <<  cameraLimits.transpose() << std::endl;
    std::cout << "Camera Limits" <<  cameraLimitsZone.transpose() << std::endl;


    /** Initialization of the VMPC */
    hr::vision::VMPCData vmpcData = hr::vision::VMPCData(N_horizon,n_features);

    camera.convertPoints(currentPixelFeatures,currentFeatures,hr::vision::kPixel2ImageCoordinates,true,hr::vision::k2DPoint);
    camera.convertPoints(desiredPixelFeatures,desiredFeatures,hr::vision::kPixel2ImageCoordinates,true,hr::vision::k2DPoint);


    desiredHorizonFeatures << desiredFeatures, desiredFeatures , desiredFeatures;



    interactionMatrix = camera.getInteractionMatrix(desiredFeatures);
    cout << "init vmpcData" << endl;
    vmpcData.init(interactionMatrix);

    hr::VectorXr lowerBounds(n_camera_dof);
    hr::VectorXr upperBounds(n_camera_dof);
    lowerBounds << -0.5, 0, -0.5, 0, -0.5, 0;
    upperBounds <<  0.5, 0,  0.5, 0,  0.5, 0;

    hr::VectorXr lBounds(n_camera_dof*N_horizon);
    hr::VectorXr uBounds(n_camera_dof*N_horizon);
    lBounds << lowerBounds, lowerBounds, lowerBounds;
    uBounds << upperBounds, upperBounds, upperBounds;

    int nWSR = 1000;
    qpOASES::real_t cpuTime = 1.0;

    int nV = n_camera_dof*N_horizon;
    int nC = 0;
    boost::shared_ptr<qpOASES::SQProblem> quadprog;
    qpOASES::Options options;
    options.setToMPC();
    options.printLevel = qpOASES::PL_NONE;
    quadprog.reset(new qpOASES::SQProblem(nV,nC));
    quadprog->setOptions(options);
    cout << "Solve qpoases" << endl;

    timer.start();
    quadprog->init(vmpcData.getHessian().data(), vmpcData.getFvector(currentFeatures, desiredHorizonFeatures).data(),NULL,lBounds.data(),uBounds.data(),NULL,NULL,nWSR,&cpuTime);
    if(quadprog->getPrimalSolution(uk.data()) == qpOASES::RET_QP_NOT_SOLVED){
        return 0;
        cout << "failed to solve" << endl;
    }
    timer.stop();

    hr::VectorXr u(n_camera_dof);
    u = uk.head(n_camera_dof);
    hr::VectorXr nextFeatures(n_features);
    nextFeatures = currentFeatures+Ts*interactionMatrix*u;

    cout << "Desired features (pixels) " << desiredPixelFeatures.transpose() <<endl;
    cout << "Current features (pixels) " << currentPixelFeatures.transpose() <<endl;

    cout << "Desired features (m) " << desiredFeatures.transpose() <<endl;
    cout << "Current features (m) " << currentFeatures.transpose() <<endl;
    cout << "Control output" << endl << uk.transpose() << endl;
    cout << "Next features" << endl << nextFeatures.transpose() << endl;
    cout << "Squared norm error" << endl << (nextFeatures - desiredFeatures).norm() << endl;
    cout << "Execution time " << endl << timer.format()<<endl;



}
