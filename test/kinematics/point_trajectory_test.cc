#include "openhrc/kinematics/point_trajectory.h"
#include "gtest/gtest.h"
#include <iostream>

class PointTrajectoryTest : public ::testing::Test {
 protected:

  PointTrajectoryTest() {
  }

  virtual ~PointTrajectoryTest() {
  }

  virtual void SetUp() {

  }

  virtual void TearDown() {

  }
};

TEST_F(PointTrajectoryTest, TrajectoryParameters) {
    hr::real_t initialTime = 0.0;
    hr::real_t finalTime = 1.0;
    hr::SpatialVector initialPoint;
    initialPoint = hr::SpatialVector::Zero();
    hr::SpatialVector finalPoint;
    finalPoint = hr::SpatialVector::Zero();
    hr::core::PointTrajectory pointTrajectory = hr::core::PointTrajectory(0.0,1.0,initialPoint,finalPoint);
    EXPECT_EQ(pointTrajectory.getInitialTime(),initialTime);
    EXPECT_EQ(pointTrajectory.getFinalTime(),finalTime);

}

TEST_F(PointTrajectoryTest, InitialFinalPose) {

    hr::real_t initialTime = 0.0;
    hr::real_t finalTime = 1.0;
    hr::SpatialVector initialPoint;
    initialPoint << 0.5, 0.2, 0.3, 0.4, 0.6, 0.9;
    hr::SpatialVector finalPoint;
    finalPoint << 10.0, 5.0, -0.3, 4.5, 6.0, -2.0;
    hr::core::PointTrajectory pointTrajectory = hr::core::PointTrajectory(0.0,1.0,initialPoint,finalPoint);
    EXPECT_TRUE(pointTrajectory.getPose(initialTime) == initialPoint) << "Initial Pose:" << initialPoint.transpose() << " Vs: " << pointTrajectory.getPose(initialTime);
    EXPECT_TRUE(pointTrajectory.getVelocity(initialTime) == hr::SpatialVector::Zero());
    EXPECT_TRUE(pointTrajectory.getAcceleration(initialTime) == hr::SpatialVector::Zero());
    EXPECT_TRUE(pointTrajectory.getPose(finalTime) == finalPoint);
    EXPECT_TRUE(pointTrajectory.getVelocity(finalTime) == hr::SpatialVector::Zero());
    EXPECT_TRUE(pointTrajectory.getAcceleration(finalTime) == hr::SpatialVector::Zero());

}


