#include <iostream>
#include "openhrc/kinematics.h"

using std::cout;
using std::endl;

int main(){

    cout << "Loading the multibody" << endl;

    hr::core::World world = hr::core::World();
    std::string sFile = "../../../src/data/Nao_blue_noninertial.xbot";
    int robotId = world.getRobotsVector()->size();
    world.loadMultiBody(sFile,robotId, hr::core::kNAO);
    boost::shared_ptr<hr::core::MultiBody> robot = world.getRobot(0);
    hr::Vector3r rightFootHandle(0.0,0.01,-0.04);
    int rightFootBodyId = 30;
    int rightFootHandleId = robot->addOperationalHandle(rightFootBodyId,rightFootHandle);
    int taskId = 0;
    hr::SpatialVector goal;
    goal << 0.1,0.2,0.3,0.4,0.5,0.6;

    cout << "Setting the initial configuration" << endl;

    hr::VectorXr q(30);
    q.setZero();
    robot->setConfiguration(q);
    robot->computeForwardKinematics();

    cout << "Creating tasks" << endl;

    hr::core::KinematicTask task_01(rightFootHandleId,rightFootBodyId, taskId, robot);
    task_01.setGoal(goal);
    hr::core::KinematicTask task_02(rightFootHandleId,rightFootBodyId, taskId, robot, hr::core::kPosition);
    task_02.setGoal(goal);
    hr::core::KinematicTask task_03(rightFootHandleId,rightFootBodyId, taskId, robot, hr::core::kOrientation);
    task_03.setGoal(goal);
    hr::core::KinematicTask task_04(rightFootHandleId,rightFootBodyId, taskId, robot, (hr::core::kX | hr::core::kWXYZ));
    task_04.setGoal(goal);
    hr::core::KinematicTask task_05(rightFootHandleId,rightFootBodyId, taskId, robot, (hr::core::kYZ | hr::core::kWYZ));
    task_05.setGoal(goal);

    cout << "Task spaces test (XY) =" <<   (hr::core::kX | hr::core::kY) << endl;
    cout << "Task spaces test (WXY) =" <<   (hr::core::kWX | hr::core::kWY) << endl;
    cout << "Task spaces test (XWXYZ) =" <<   (hr::core::kX | hr::core::kWXYZ) << endl;

    cout << "Updating tasks" << endl;

    task_01.update();
    task_02.update();
    task_03.update();
    task_04.update();
    task_05.update();


    hr::MatrixXr jacobian_01;
    hr::MatrixXr jacobian_02;
    hr::MatrixXr jacobian_03;
    hr::MatrixXr jacobian_04;
    hr::MatrixXr jacobian_05;

    hr::VectorXr error_01;
    hr::VectorXr error_02;
    hr::VectorXr error_03;
    hr::VectorXr error_04;
    hr::VectorXr error_05;

    cout << "Calculate jacobians" << endl;


    jacobian_01 = task_01.getTaskJacobian();
    jacobian_02 = task_02.getTaskJacobian();
    jacobian_03 = task_03.getTaskJacobian();
    jacobian_04 = task_04.getTaskJacobian();
    jacobian_05 = task_05.getTaskJacobian();

    cout << "Calculate errors" << endl;
    error_01 = task_01.getTaskError();
    error_02 = task_02.getTaskError();
    error_03 = task_03.getTaskError();
    error_04 = task_04.getTaskError();
    error_05 = task_05.getTaskError();



    cout << "task error 01 " << endl << error_01 << endl;
    cout << "task error 02 " << endl << error_02 << endl;
    cout << "task error 03 " << endl << error_03 << endl;
    cout << "task error 04 " << endl << error_04 << endl;
    cout << "task error 05 " << endl << error_05 << endl;

}
