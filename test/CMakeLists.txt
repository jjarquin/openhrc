message("Build tests is active")


list(APPEND CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/cmake/Modules)
find_package(Gtest REQUIRED)
enable_testing()
add_subdirectory(kinematics)

